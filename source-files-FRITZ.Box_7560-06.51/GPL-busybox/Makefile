export CC			:= $(CROSS_COMPILE)gcc
export CXX			:= $(CROSS_COMPILE)g++
export AR			:= $(CROSS_COMPILE)ar
export RANLIB 		:= $(CROSS_COMPILE)ranlib
export STRIP		:= $(CROSS_COMPILE)strip
export LD			:= $(CROSS_COMPILE)ld
export OBJDUMP	 	:= $(CROSS_COMPILE)objdump
export OBJCOPY	 	:= $(CROSS_COMPILE)objcopy

TOPDIR				:= $(shell pwd)
FILESYSTEM	        := $(TOPDIR)/../filesystem_$(KERNEL_LAYOUT)

GU_MAKE_J_OPTION	?= 1

UDEV_VERSION	    := 175
USBUTILS_VERSION    := 005
PCIUTILS_VERSION    := 3.1.8
GPERF_VERSION	    := 3.0.3
UTIL_LINUX_VERSION	:= 2.17.2
PATCH					:=patches

TARGET				:= $(shell x=$(CROSS_COMPILE) ; xx=$${x\#\#*/} ; echo $${xx%-} )
# TARGET				:= $(shell x=$(CROSS_COMPILE) ; xx=${x} ; echo ${xx} )
# $(error TARGET=$(TARGET))

EXTRA_INCLUDE_FLAGS	        := -isystem $(FRITZ_BOX_BUILD_DIR)/archiv/tmp-0-gcc_x86_64/usr/include
export EXTRA_INCLUDE_FLAGS	+= $(shell for i in $(FRITZ_BOX_INCLUDE_PATH) ; do if [[ $$i = *release_kernel* ]]; then echo "" ; else echo "-I$$i" ; fi ; done)
EXTRA_LDFLAGS	            := $(shell for i in $(FRITZ_BOX_LIB_PATH) ; do echo " -Wl,-rpath-link -Wl,$$i" ; done)
EXTRA_LDFLAGS	            += $(shell for i in $(FRITZ_BOX_LIB_PATH) ; do echo " -L$$i" ; done)
export EXTRA_LDFLAGS	    += $(DEFAULT_LDFLAGS_LIB) # -g -ggdb # -nostdlibs # -Wl,--verbose 
export EXTRA_CPPFLAGS 		:= $(EXTRA_INCLUDE_FLAGS) 
export EXTRA_CFLAGS 		:= $(EXTRA_INCLUDE_FLAGS) $(DEFAULT_CFLAGS_LIB) # -g -ggdb
export EXTRA_CFLAGS 		+= -Du8=__u8 # für Buildroot Version ab 2010_05 und 32er Kernel




ifeq ($(PRODUKTE),)
CONFIG_FILE     := Config..$(KERNEL_LAYOUT)
TAR_FILE        := busybox-$(KERNEL_LAYOUT).tar.gz
else
CONFIG_FILE     := Config.$(PRODUKTE).$(KERNEL_LAYOUT)
TAR_FILE        := busybox-$(KERNEL_LAYOUT)-$(PRODUKTE).tar.gz
endif

##########################################################################################
#
##########################################################################################
all: udev-all util-linux-all
	
##########################################################################################
#
##########################################################################################
clean:  udev-clean util-linux-clean
	rm -rf filesystem
	
##########################################################################################
#
##########################################################################################
depend: 

##########################################################################################
#
##########################################################################################
install: udev-install util-linux-install
	( \
	for i in `find filesystem -type f` ; do \
		if file $$i | grep -q -i 'not stripped' ; then \
			strip_params="" ; strip_type="" ;\
			if file $$i | grep -q -i 'relocatable' ; then strip_type="relocatable" ; strip_params="$${strip_params} --strip-debug" ; fi ; \
			if file $$i | grep -q -i 'shared'      ; then strip_type="shared"      ; strip_params="$${strip_params} --discard-all --strip-debug --strip-unneeded " ; fi ; \
			if file $$i | grep -q -i 'executable'  ; then strip_type="executable"  ; strip_params="$${strip_params} -s" ; fi ; \
			before=`wc --bytes $$i | sed -e 's/\([0123456789]*\) .*/\1/g'` ; \
			$(STRIP) $${strip_params} $$i  || exit -1 ; \
			after=`wc --bytes $$i | sed -e 's/\([0123456789]*\) .*/\1/g'` ; \
			if [ ! "$$before" = "$$after" ] ; then \
				$(ECHO) "[install: strip] Datei $(ECHO_GRUEN)$$i ($${strip_type})$(ECHO_END) hat sich veraendert: zuvor $$before nachher $$after" ; \
			fi ; \
			continue ; \
		fi ; \
	done; \
		if [ -z "$(PRODUKTE)" ] ; then \
			produkt= ; \
		else \
			produkt=-$(PRODUKTE) ; \
		fi ; \
	  	cd filesystem ; \
		if [ ! -d etc/init.d ]; then mkdir -p etc/init.d ; fi ; \
		cp ../S21-udev etc/init.d -v ; \
		cp ../add-file-busybox . -v ; \
		test -f ../modprobe.conf.$(KERNEL_LAYOUT) && \
		  cp ../modprobe.conf.$(KERNEL_LAYOUT) etc/modprobe.conf -v ; \
		tar czf ../busybox-$(KERNEL_LAYOUT)$${produkt}.tar.gz * \
	)

##########################################################################################
#
##########################################################################################
#add-file:
#	( \
#	  	if [ -z "$(PRODUKTE)" ] ; then \
#			produkt= ; \
#		else \
#			produkt=\\/$(PRODUKTE) ; \
#		fi ; \
#		find filesystem -type d | sed -e "s/filesystem\(.*\)/BASIS    D    777    filesystem_busybox_$(KERNEL_LAYOUT)$${produkt}\/.\1    .\1/g" | grep -v ' .$$' ; \
#		find filesystem -type l | sed -e "s/filesystem\(.*\)/BASIS    L    777    filesystem_busybox_$(KERNEL_LAYOUT)$${produkt}\/.\1    \/bin\/busybox/g" ; \
#		find filesystem -type f | sed -e "s/filesystem\(.*\)/BASIS    F    777    filesystem_busybox_$(KERNEL_LAYOUT)$${produkt}\/.\1    .\1/g" ; \
#	) | grep -v 'add-file-busybox\|/man/\|\.h\|\.a' >add-file-busybox

	
##########################################################################################
#
##########################################################################################
install_tar:  install
	if [ ! "$(TAR_FILE)" = "$(INSTALL_TAR_FILE_NAME)" ]; then \
		cp -v $(TAR_FILE) $(INSTALL_TAR_FILE_NAME) || exit -1 ; \
	fi

##########################################################################################
#
##########################################################################################
udev-clean:
	echo "CROSS_COMPILE=$(CROSS_COMPILE)"
	rm -f udev-all
	rm -f udev-config
	if [ -f udev-$(UDEV_VERSION)/Makefile ]; then make -C udev-$(UDEV_VERSION) clean ; fi


udev-config:
	echo $(FRITZ_BOX_LIB_PATH)
	echo "$(EXTRA_LDFLAGS)"
#	( cd udev-$(UDEV_VERSION) ; autoconf -o configure configure.in )
	( cd udev-$(UDEV_VERSION) ; \
		export CFLAGS="$(EXTRA_CFLAGS) $(EXTRA_CPP_FLAGS)" ; \
		export LDFLAGS="$(EXTRA_LDFLAGS)" ; \
		export CPPFLAGS="$(EXTRA_CPP_FLAGS)" ; \
		./configure \
				--prefix=/ \
				--host=$(TARGET) \
				--disable-extras \
				--disable-gudev \
				--disable-logging \
				--disable-introspection \
				--with-usb-ids-path=/lib/usbutils-$(USBUTILS_VERSION)/usb.ids  \
				--with-pci-ids-path=/lib/pciutils-$(PCIUTILS_VERSION)/pci.ids  \
				LDFLAGS="$(EXTRA_LDFLAGS)" \
				CFLAGS="$(EXTRA_CFLAGS) $(EXTRA_CPPFLAGS)"  \
				CPPFLAGS="$(EXTRA_CPPFLAGS)" ; \
	)
	touch udev-config

# udev-all: # gperf-all gperf-install udev-config
udev-all: udev-config
	if [ ! -d udev-$(UDEV_VERSION) ] ; then \
		make udev-config || exit -1 ; \
	fi
	make -C udev-$(UDEV_VERSION) \
		LDFLAGS="$(EXTRA_LDFLAGS)" CPPFLAGS="$(EXTRA_CPPFLAGS)" CFLAGS="$(EXTRA_CFLAGS) $(EXTRA_CPPFLAGS)"  
	touch udev-all

udev-install:
	make -C udev-$(UDEV_VERSION) DESTDIR=$(TOPDIR)/filesystem install

##########################################################################################
#
##########################################################################################
gperf-clean:
	echo "CROSS_COMPILE=$(CROSS_COMPILE)"
	rm -rf gperf-$(GPERF_VERSION)
	rm -f gperf-all
	rm -f gperf-config


gperf-config:
	tar xzf gperf-$(GPERF_VERSION).tar.gz
	echo $(FRITZ_BOX_LIB_PATH)
	echo "$(EXTRA_LDFLAGS)"
#	( cd gperf-$(GPERF_VERSION) ; autoconf -o configure configure.in )
	( cd gperf-$(GPERF_VERSION) ; \
		export CFLAGS="$(EXTRA_C_FLAGS) $(EXTRA_CPP_FLAGS)" ; \
		export LDFLAGS="$(EXTRA_LDFLAGS)" ; \
		export CPPFLAGS="$(EXTRA_CPP_FLAGS)" ; \
		./configure --prefix=$(TOPDIR)/filesystem \
				--host=$(TARGET) \
				--include=$(TOPDIR)/filesystem \
				LDFLAGS="$(EXTRA_LDFLAGS)" \
				CPPFLAGS="$(EXTRA_CPPFLAGS)" ; \
	)
	touch gperf-config

gperf-all: gperf-config
	if [ ! -d gperf-$(GPERF_VERSION) ] ; then \
		make gperf-config || exit -1 ; \
	fi
	make -C gperf-$(GPERF_VERSION) 
	touch gperf-all

gperf-install:
	make -C gperf-$(GPERF_VERSION) install

##########################################################################################
#
##########################################################################################
util-linux-clean:
	echo "CROSS_COMPILE=$(CROSS_COMPILE)"
	if [ -f util-linux-ng-$(UTIL_LINUX_VERSION)/Makefile ] ; then \
		make util-linux-config || exit -1 ; \
		make -C util-linux-ng-$(UTIL_LINUX_VERSION) clean ;\
		rm util-linux-ng-$(UTIL_LINUX_VERSION)/Makefile ;\
	fi

util-linux-config:
	( cd util-linux-ng-$(UTIL_LINUX_VERSION) ; \
		export CFLAGS="$(EXTRA_CFLAGS)" ; \
		export LDFLAGS="$(EXTRA_LD_FLAGS)" ; \
		export CPPFLAGS="$(EXTRA_CPP_FLAGS)" ; \
	  	echo "here we go CFLAGS=$(CFLAGS)" > ~/mycflags ; \
		./configure \
				--host=mipsel-linux \
				--disable-tls --disable-largefile \
				--disable-mount --disable-fsck \
				--disable-nls --disable-rpath \
				--disable-agetty --disable-cramfs \
				--disable-switch_root --disable-pivot_root \
				--disable-fallocate --disable-unshare \
				--disable-schedutils --without-ncurses\
				;\
	)

util-linux-all:
	if [ ! -f util-linux-ng-$(UTIL_LINUX_VERSION)/Makefile ] ; then \
		make util-linux-config || exit -1 ; \
	fi
	( export CFLAGS="$(EXTRA_CFLAGS)" ; \
		export LDFLAGS="$(EXTRA_LD_FLAGS)" ; \
		export CPPFLAGS="$(EXTRA_CPP_FLAGS)" ; \
		make -C util-linux-ng-$(UTIL_LINUX_VERSION) ;\
	)

util-linux-install:
	mkdir -p $(TOPDIR)/filesystem/lib ; \
	mkdir -p $(TOPDIR)/filesystem/sbin ; \
	cp util-linux-ng-$(UTIL_LINUX_VERSION)/shlibs/uuid/src/.libs/libuuid.so.1.3.0 $(TOPDIR)/filesystem/lib ; \
	ln -s libuuid.so.1.3.0 $(TOPDIR)/filesystem/lib/libuuid.so.1 ; \
	ln -s libuuid.so.1.3.0 $(TOPDIR)/filesystem/lib/libuuid.so ; \
	cp util-linux-ng-$(UTIL_LINUX_VERSION)/shlibs/blkid/src/.libs/libblkid.so.1.1.0 $(TOPDIR)/filesystem/lib ; \
	ln -s libblkid.so.1.1.0  $(TOPDIR)/filesystem/lib/libblkid.so.1 ; \
	ln -s libblkid.so.1.1.0  $(TOPDIR)/filesystem/lib/libblkid.so ; \
	cp util-linux-ng-$(UTIL_LINUX_VERSION)/misc-utils/.libs/blkid $(TOPDIR)/filesystem/sbin ; \

.PHONY: install

