#
# $Id: ar7def.mk 154 2015-08-05 15:45:21Z sreschke $
# vim: fileencoding=utf8
##

# hostname with lower case letters
HOSTNAME                = $(shell hostname| tr '[:upper:]' '[:lower:]')

TARGETFS                =
KERNELBASEDIR           =

# Konfigs, die per default aktiviert sind
WITH_SLAB_WHERE_DEBUG   = yes
SLAB_INSTEAD_OF_MALLOC  = yes

# Konfigs, die separat ueber config.mk aktiviert werden muessen
WITH_DIAGNOSE           = no
WITH_IPV6               = no
WITH_UTF8               = no
WITH_LFS                = no

ifeq ($(wildcard $(SHAREDLIBS)/../config.mk),$(SHAREDLIBS)/../config.mk)
include $(SHAREDLIBS)/../config.mk
endif

ifeq ($(NO_PARALLEL_MAKE),y)
GU_MAKE_J_OPTION        = 1
MAKEFLAGS              += -j$(GU_MAKE_J_OPTION)
$(info : make: Using $(GU_MAKE_J_OPTION) Processors)
endif

ifeq ($(GU_MAKE_J_OPTION),)
GU_MAKE_J_OPTION        =1
endif
ifeq ($(MAKELEVEL),0)
MAKEFLAGS              += -j$(GU_MAKE_J_OPTION)
$(info : make: Using $(GU_MAKE_J_OPTION) Processors)
endif

TARGETFS                = $(SHAREDLIBS)/../filesystem
TARGETFS_TAR            = $(SHAREDLIBS)/../filesystem_tar

# ------------------------------------------------------------------

BUILDHOST               =$(shell /usr/bin/gcc -dumpmachine)

COMPILERPATH            =$(GU_COMPILER)
TARGETPATH              =$(GU_COMPILER)
CROSSTARGET             =$(GU_CROSSTARGET)
TARGET                  =$(GU_CROSSTARGET)

# ------------------------------------------------------------------
# PLATFORM:
#  local
#  GPL
#  "default"
#

ifeq ($(PLATFORM),local)
NO_CROSS                = yes
TARGETPATH              =
COMPILERPATH            =
TARGET                  = $(BUILDHOST)
CROSSTARGET             = $(BUILDHOST)
else ifeq ($(PLATFORM),GPL)

# empty

endif

ifneq ($(DEFAULT_CFLAGS),)
TARGETCFLAGS        =$(DEFAULT_CFLAGS)
else
TARGETCFLAGS        =$(DEFAULT_CFLAGS_SRC)
endif

KERNELBASEDIR           =$(GENENVFS)
KERNEL_MODULEDIR        =arch/kernel_$(KERNEL_LAYOUT)_modules
# --------------------------------------------

PROJECTDEFS             =
PROJECTCONFIGUREARGS    =

ifneq ($(PLATFORM),local)
PROJECTDEFS            += -DAR7
endif

ifeq ($(WITH_IPV6),yes)
PROJECTDEFS            += -DUSE_IPV6
PROJECTCONFIGUREARGS   += --enable-ipv6
endif

ifeq ($(WITH_UTF8),yes)
PROJECTDEFS            += -DFULL_UTF8 -DCONF_UTF8
PROJECTCONFIGUREARGS   += --enable-utf8
endif

ifeq ($(SLAB_INSTEAD_OF_MALLOC),yes)
PROJECTDEFS            += -DUSE_SLAB
#PROJECTDEFS            += -DREPLACE_MALLOC
ifeq ($(WITH_SLAB_WHERE_DEBUG),yes)
PROJECTDEFS            += -DUSE_SLAB_WHERE_DEBUG
endif
endif

ifeq ($(WITH_LFS),yes)
PROJECTDEFS            += -D_FILE_OFFSET_BITS=64
PROJECTCONFIGUREARGS   += --enable-lfs
endif

OPENSSL_INCLUDE_PATH    = $(filter %/archiv/tmp-$(ADD_FILE_PID)-openssl/sdk,$(GU_INCLUDE))
ZLIB_INCLUDE_PATH       = $(filter %/archiv/tmp-$(ADD_FILE_PID)-libz/include,$(GU_INCLUDE))
OPENSSL_LIB_PATH        = $(filter %/archiv/tmp-$(ADD_FILE_PID)-openssl/lib,$(GU_LIB))
ZLIB_LIB_PATH           = $(filter %/archiv/tmp-$(ADD_FILE_PID)-libz/lib,$(GU_LIB))

PROJECTCFLAGS           = -Os -fomit-frame-pointer
PROJECTCFLAGS          += -Wextra
PROJECTCFLAGS          += -W -Wall
PROJECTCFLAGS          += -Wshadow
PROJECTCFLAGS          += -Wmissing-include-dirs
PROJECTCFLAGS          += -Winit-self
PROJECTCFLAGS          += -Wmissing-prototypes
# problem with function sys_avm_fsusage used in smbd
#PROJECTCFLAGS += -Werror-implicit-function-declaration

PROJECTCFLAGS          += -fno-strict-aliasing \
                          -Wundef -Wstrict-prototypes -Wno-trigraphs \
                          -Wno-unused-parameter \
                          -Wformat \
                          -Wno-sign-compare

PROJECTCFLAGS          += -save-temps
PROJECTCFLAGS          += -Wno-write-strings
ifneq ($(NO_CROSS),yes)
PROJECTCFLAGS          += -Wno-sign-conversion
endif
PROJECTCFLAGS          += -Wno-pointer-sign
PROJECTCFLAGS          += -Wmissing-declarations
PROJECTCFLAGS          += -Wredundant-decls
PROJECTCFLAGS          += -Wformat-security

ifeq ($(PLATFORM),local)
PROJECTCFLAGS          += -ggdb
endif

ifneq ($(NO_CROSS),yes)
PROJECTCFLAGS          += $(TARGETCFLAGS)
endif

# exclude options that are not valid for C++
PROJECTCXXFLAGS         = $(filter-out -Wmissing-prototypes \
                          -Wold-style-definition \
                          -Werror-implicit-function-declaration \
                          -Wstrict-prototypes \
                          -Wdeclaration-after-statement \
                          -Wno-pointer-sign,$(PROJECTCFLAGS))
PROJECTCXXFLAGS        += \
                          -fno-rtti -fno-exceptions -fno-unwind-tables \
                          -fno-use-cxa-atexit

PROJECTLDFLAGS          = $(DEFAULT_LDFLAGS_APPL)
PROJECTLDFLAGS         += $(EXTRA_LD_FLAGS)

ifeq ($(WITH_DIAGNOSE),no)
PROJECTDEFS            += -DNO_DEBUGMSG -DNO_INFOMSG -DNO_ERRMSG -DNO_SYSERROR -DNO_BUGMSG
endif

ifeq ($(PLATFORM),arm1176)
PROJECTDEFS += -DPUMA -DNO_VINAX -DNO_TIATM
PROJECTDEFS += -DDATAPIPE_DHCPC_DEFAULT_BROADCASTFLAG=1
PROJECTDEFS += -DDATAPIPE_NO_PACKET_REUSE
PROJECTCONFIGUREARGS += --enable-puma
PROJECTDEFS += -DSPEEX_NO_OPTIMIZATIONS
USE_NEW_LED_INTERFACE=yes
endif

ifeq ($(PLATFORM),armPuma6)
PROJECTDEFS += -DPUMA6_ARM
PROJECTCONFIGUREARGS += --enable-armpuma6
endif

ifeq ($(HOSTNAME),embeddedvm)
PROJECTDEFS += -DCOMPILE_EMBEDDEDVM
PROJECTCONFIGUREARGS += --enable-compile-embeddedvm
endif

HOSTCC                  = gcc
# No libstdc++ on the boxes, therefore force compilation+link via gcc, which doesn't
# link libstdc++ by default
PROJECTCONFIGUREARGS   += CXX=$(notdir $(CROSS_COMPILE))gcc

ifneq (,$(findstring $(COMPILERPATH),$(PATH)))
# Found
else
# Not found
# only modify PATH without already configured COMPILERPATH
export PATH            := $(PATH):$(COMPILERPATH)
endif

export CCACHE_DISABLE

export SED              =/bin/sed
export EGREP            =/bin/egrep

export PROJECTCONFIGUREARGS
export PROJECTDEFS
export PROJECTCPPFLAGS
export PROJECTCFLAGS
export PROJECTCXXFLAGS
export PROJECTLDFLAGS
export OPENSSL_INCLUDE_PATH
export OPENSSL_LIB_PATH
export ZLIB_INCLUDE_PATH
export ZLIB_LIB_PATH
export TARGET
export TARGETFS

export EXTRA_LD_FLAGS
export EXTRA_INCLUDE_FLAGS
export GU_LIB
export GU_INCLUDE

export LC_ALL           =C
