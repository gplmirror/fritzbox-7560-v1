/*  webdav_shm.h: shared memory structure.
    Copyright (C) 2009 AVM 

    This file is part of davfs2.

    davfs2 is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    davfs2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with davfs2; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA. */

#ifndef DAV_WEBDAV_SHM_H
#define DAV_WEBDAV_SHM_H

//shared memory id
#define WEBDAV_SHM_ID 334455

#define  USB_CFG_STRLEN         2048

struct webdav_shm_usb_cfg_infos {
    char  username  [USB_CFG_STRLEN];
    char  password  [USB_CFG_STRLEN];
} webdav_shm_usb_cfg_infos_t;

#define STRLEN_FILE_NAME         2048

// longest line in the File
#define STRLEN_LINE_LEN          STRLEN_FILE_NAME + 40

//structure for shared infos
struct webdav_file_infos{
    unsigned int is_running;
    unsigned int connection_state;
    unsigned int cache_speed;
    __uint64_t cache_storage_avail;
    __uint64_t cache_storage_used;
    unsigned int dirty_files;
    unsigned int running_uploads;
    unsigned int running_downloads;
    __uint64_t finished_uploads;
    __uint64_t failed_uploads;
    __uint64_t sum_failed_uploads;
    __uint64_t finished_downloads;
    __uint64_t failed_downloads;
    __uint64_t storage_quota_avail;
    __uint64_t storage_quota_used;
    unsigned int storage_filecount;
    unsigned int storage_maxfilecount;
    unsigned int storage_maxfilesperfolder;
    unsigned int storage_maxfilenamelength;
    __uint64_t storage_maxfilesize;
    __uint64_t upload_quota_avail;
    __uint64_t upload_quota_used;
    __uint64_t download_quota_avail;
    __uint64_t download_quota_used;
    __uint64_t traffic_quota_avail;
    __uint64_t traffic_quota_used;
    __uint64_t avg_upload_speed;
    __uint64_t avg_download_speed;
    unsigned int low_upload_speed;
    __uint64_t size_downloads;
    __uint64_t size_uploads;
    char uploading_file1[STRLEN_FILE_NAME];
    char uploading_file2[STRLEN_FILE_NAME];
    char downloading_file1[STRLEN_FILE_NAME];
    char downloading_file2[STRLEN_FILE_NAME];
    __uint64_t place_holder_1;
    __uint64_t place_holder_2;
    __uint64_t place_holder_3;
    time_t alive_time;
};


// defines for the filenames and path
#define WEBDAVINFO_FILE                      ".webdavinfos"
#define WEBDAVINFO_FILE_CONNECTION_STATE     ".webdavinfos_connection_state"
#define WEBDAVINFO_FILE_CACHE_SPEED          ".webdavinfos_cache_speed"

#define WEBDAVINFO_FILETMP                   ".webdavinfos_tmp"
#define WEBDAVINFO_FILETMP_CONNECTION_STATE  ".webdavinfos_connection_state_tmp"
#define WEBDAVINFO_FILETMP_CACHE_SPEED       ".webdavinfos_cache_speed_tmp"

#ifdef PUMA6_ARM
	#define WEBDAVINFO_FILEPATH "/var/remote/var/tmp"
#else
	#define WEBDAVINFO_FILEPATH "/var/tmp"
#endif

// defines for the lines in the file
#define WEBDAVINFO_STR_IS_RUNNING                       "is_running"
#define WEBDAVINFO_STR_CONNECTION_STATE                 "connection_state"
#define WEBDAVINFO_STR_CACHE_SPEED                      "cache_speed"

#define WEBDAVINFO_STR_CACHE_STORAGE_AVAIL              "cache_storage_avail"
#define WEBDAVINFO_STR_CACHE_STORAGE_USED               "cache_storage_used"
#define WEBDAVINFO_STR_DIRTY_FILES                      "dirty_files"
#define WEBDAVINFO_STR_RUNNING_UPLOADS                  "running_uploads"
#define WEBDAVINFO_STR_RUNNING_DONWLOADS                "running_downloads"
#define WEBDAVINFO_STR_FINISHED_UPLOADS                 "finished_uploads"
#define WEBDAVINFO_STR_FAILED_UPLOADS                   "failed_uploads"

#define WEBDAVINFO_STR_SUM_FAILED_UPLOADS               "sum_failed_uploads"
#define WEBDAVINFO_STR_FINISHED_DOWNLOADS               "finished_downloads"
#define WEBDAVINFO_STR_FAILED_DOWNLOADS                 "failed_downloads"
#define WEBDAVINFO_STR_STORAGE_QUOTA_AVAIL              "storage_quota_avail"
#define WEBDAVINFO_STR_STORAGE_QUOTA_USED               "storage_quota_used"
#define WEBDAVINFO_STR_STORAGE_QUOTA_FILECOUNT          "storage_filecount"
#define WEBDAVINFO_STR_STORAGE_MAXFILECOUNT             "storage_maxfilecount"

#define WEBDAVINFO_STR_STORAGE_MAXFILESPERFOLDER        "storage_maxfilesperfolder"
#define WEBDAVINFO_STR_STORAGE_MAXFILENAMELENGTH        "storage_maxfilenamelength"
#define WEBDAVINFO_STR_STORAGE_MAXFILESIZE              "storage_maxfilesize"
#define WEBDAVINFO_STR_UPLOAD_QUOTA_AVAIL               "upload_quota_avail"
#define WEBDAVINFO_STR_UPLOAD_QUOTA_USED                "upload_quota_used"
#define WEBDAVINFO_STR_DOWNLOAD_QUOTA_AVAIL             "download_quota_avail"
#define WEBDAVINFO_STR_DOWNLOAD_QUOTA_USED              "download_quota_used"
#define WEBDAVINFO_STR_TRAFFIC_QUOTA_AVAIL              "traffic_quota_avail"
#define WEBDAVINFO_STR_TRAFFIC_QUOTA_USED               "traffic_quota_used"
#define WEBDAVINFO_STR_AVG_UPLOAD_SPEED                 "avg_upload_speed"
#define WEBDAVINFO_STR_AVM_DOWNSLOAD_SPEED              "avg_download_speed"
#define WEBDAVINFO_STR_LOW_UPLOAD_SPEED                 "low_upload_speed"
#define WEBDAVINFO_STR_SIZE_DOWNLOADS                   "size_downloads"
#define WEBDAVINFO_STR_SIZE_UPLOADS                     "size_uploads"
#define WEBDAVINFO_STR_UPLOADING_FILE1                  "uploading_file1"
#define WEBDAVINFO_STR_UPLOADING_FILE2                  "uploading_file2"
#define WEBDAVINFO_STR_DOWNLOADING_FILE1                "downloading_file1"
#define WEBDAVINFO_STR_DOWNLOADING_FILE2                "downloading_file2"
#define WEBDAVINFO_STR_ALIVE_TIME                       "alive_time"

#endif /* !DAV_WEBDAV_SHM_H */
