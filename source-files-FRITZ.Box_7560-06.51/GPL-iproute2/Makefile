ROOTDIR=$(DESTDIR)
PREFIX=/usr
LIBDIR=$(PREFIX)/lib
SBINDIR=/sbin
CONFDIR=/etc/iproute2
DATADIR=$(PREFIX)/share
DOCDIR=$(DATADIR)/doc/iproute2
MANDIR=$(DATADIR)/man
ARPDDIR=/var/lib/arpd

# Path to db_185.h include
DBM_INCLUDE:=$(ROOTDIR)/usr/include

SHARED_LIBS = y

DEFINES= -DRESOLVE_HOSTNAMES -DLIBDIR=\"$(LIBDIR)\"
ifneq ($(SHARED_LIBS),y)
DEFINES+= -DNO_SHARED_LIBS
endif

DEFINES+=-DCONFDIR=\"$(CONFDIR)\"

#options for decnet
ADDLIB+=dnet_ntop.o dnet_pton.o

#options for ipx
ADDLIB+=ipx_ntop.o ipx_pton.o

CC = $(CROSS_COMPILE)gcc
AR = $(CROSS_COMPILE)ar
HOSTCC = gcc
DEFINES += -D_GNU_SOURCE
CCOPTS = -O2 $(EXTRA_INCLUDE_FLAGS)
WFLAGS := -Wall -Wstrict-prototypes -Werror -Wmissing-prototypes
WFLAGS += -Wmissing-declarations -Wold-style-definition

CFLAGS = $(WFLAGS) $(CCOPTS) -I../include $(DEFINES)
YACCFLAGS = -d -t -v

SUBDIRS=lib ip tc bridge #misc netem genl man

LIBNETLINK=../lib/libnetlink.a ../lib/libutil.a
LDLIBS += $(LIBNETLINK)

INSTALL=install
INSTALL_STRIP=install -s --strip-program=$(CROSS_COMPILE)strip

all: Config
	@set -e; \
	for i in $(SUBDIRS); \
	do $(MAKE) $(MFLAGS) -C $$i; done

Config:
	sh configure $(KERNEL_INCLUDE)

install install-strip: all
	$(INSTALL) -m 0755 -d $(DESTDIR)$(SBINDIR)
	$(INSTALL) -m 0755 -d $(DESTDIR)$(CONFDIR)
	$(INSTALL) -m 0755 -d $(DESTDIR)$(ARPDDIR)
	$(INSTALL) -m 0755 -d $(DESTDIR)$(DOCDIR)/examples
	$(INSTALL) -m 0755 -d $(DESTDIR)$(DOCDIR)/examples/diffserv
	$(INSTALL) -m 0644 README.iproute2+tc $(shell find examples -maxdepth 1 -type f) \
		$(DESTDIR)$(DOCDIR)/examples
	$(INSTALL) -m 0644 $(shell find examples/diffserv -maxdepth 1 -type f) \
		$(DESTDIR)$(DOCDIR)/examples/diffserv
	@for i in $(SUBDIRS) doc; do $(MAKE) -C $$i $@; done
	$(INSTALL) -m 0644 $(shell find etc/iproute2 -maxdepth 1 -type f) $(DESTDIR)$(CONFDIR)

snapshot:
	echo "static const char SNAPSHOT[] = \""`date +%y%m%d`"\";" \
		> include/SNAPSHOT.h

clean:
	@for i in $(SUBDIRS) doc; \
	do $(MAKE) $(MFLAGS) -C $$i clean; done

clobber:
	touch Config
	$(MAKE) $(MFLAGS) clean
	rm -f Config cscope.*

distclean: clobber

cscope:
	cscope -b -q -R -Iinclude -sip -slib -smisc -snetem -stc

.EXPORT_ALL_VARIABLES:
