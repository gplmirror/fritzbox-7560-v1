/*
 * Copyright (c) 2009
 *  AVM GmbH, Berlin, Germany
 *
 * License: Free, use with no restriction.
 */

char *usermap_mapuser(char *name, char *map_buffer, size_t map_size);

int usermap_is_anonymous_allowed(void);


int usermap_is_compatibility_mode(void);
int usermap_is_skip_auth_from_homenetwork(char **pskip_username);

