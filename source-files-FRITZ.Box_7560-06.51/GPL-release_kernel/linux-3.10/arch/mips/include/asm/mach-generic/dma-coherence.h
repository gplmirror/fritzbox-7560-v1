/*
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * Copyright (C) 2006  Ralf Baechle <ralf@linux-mips.org>
 *
 */
#ifndef __ASM_MACH_GENERIC_DMA_COHERENCE_H
#define __ASM_MACH_GENERIC_DMA_COHERENCE_H

struct device;

static inline dma_addr_t plat_map_dma_mem(struct device *dev __maybe_unused, void *addr,
	size_t size __maybe_unused)
{
	return virt_to_phys(addr);
}

static inline dma_addr_t plat_map_dma_mem_page(struct device *dev __maybe_unused,
	struct page *page)
{
	return page_to_phys(page);
}

static inline unsigned long plat_dma_addr_to_phys(struct device *dev __maybe_unused,
	dma_addr_t dma_addr)
{
	return dma_addr;
}

static inline void plat_unmap_dma_mem(struct device *dev __maybe_unused, dma_addr_t dma_addr __maybe_unused,
	size_t size __maybe_unused, enum dma_data_direction direction __maybe_unused)
{
}

static inline int plat_dma_supported(struct device *dev __maybe_unused, u64 mask)
{
	/*
	 * we fall back to GFP_DMA when the mask isn't all 1s,
	 * so we can't guarantee allocations that must be
	 * within a tighter range than GFP_DMA..
	 */
	if (mask < DMA_BIT_MASK(24))
		return 0;

	return 1;
}

static inline int plat_device_is_coherent(struct device *dev __maybe_unused)
{
#ifdef CONFIG_DMA_COHERENT
        return 1;
#else
	return coherentio > 0;
#endif
}

#endif /* __ASM_MACH_GENERIC_DMA_COHERENCE_H */
