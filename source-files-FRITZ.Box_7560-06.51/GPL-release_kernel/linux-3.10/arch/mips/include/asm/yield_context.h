/*------------------------------------------------------------------------------------------*\
 *   Copyright (C) 2013 AVM GmbH <fritzbox_info@avm.de>
 *
 *   author: mbahr@avm.de
 *   description: yield-thread-interface mips34k
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation version 2 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
\*------------------------------------------------------------------------------------------*/

#ifndef __yield_context_h__
#define __yield_context_h__

#if defined(CONFIG_LANTIQ)
#include <lantiq_yield-avm.h>
#endif/*--- #if defined(CONFIG_LANTIQ) ---*/
#include <linux/sched.h>
#include <linux/module.h>
#include <linux/interrupt.h>

#include <asm/yield_context_os.h>

#if defined(CONFIG_DEBUG_LOCK_ALLOC)
#error Yield-context do not work with CONFIG_DEBUG_LOCK_ALLOC
#endif/*--- #if defined(CONFIG_DEBUG_LOCK_ALLOC) ---*/

#define YIELD_HANDLED           1
/*--------------------------------------------------------------------------------*\
 * start function in non-linux-yield-context
 * Attention ! Only kseg0/kseg1 segment allowed - also data access in yield_handler !
 * ret: >= 0 number of registered signal < 0: errno
 *
 * return of request_yield_handler() handled -> YIELD_HANDLED
\*--------------------------------------------------------------------------------*/
int request_yield_handler_on(int cpu, int tc, int signal, int (*yield_handler)(int signal, void *ref), void *ref);
/*--------------------------------------------------------------------------------*\
 * ret: == 0 ok
\*--------------------------------------------------------------------------------*/
int free_yield_handler_on(int cpu, int tc, int signal, void *ref);

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
void disable_yield_handler_on(int cpu, int tc, int signal);
void enable_yield_handler_on(int cpu, int tc, int signal);

/*--------------------------------------------------------------------------------*\
 * yield_tc:   tc to use for yield
 * yield_mask: wich signal(s) would be catched
 * core:       really core (non-vpe)
 *
 * actually YIELD_MAX_TC tc possible, no crossover of yield_mask allowed
 * only on yield per cpu possible
\*--------------------------------------------------------------------------------*/
int yield_context_init_on(int linux_cpu, unsigned int yield_tc, unsigned int yield_mask);

/*--------------------------------------------------------------------------------*\
 * same as cat /proc/yield/stat - but dump as printk
\*--------------------------------------------------------------------------------*/
void yield_context_dump(void);

#define yield_spin_lock_init(_lock) spin_lock_init(_lock)

/*--------------------------------------------------------------------------------*\
 * Speziallock: ist auch aus non-Linux-TC-Kontext aufrufbar 
\*--------------------------------------------------------------------------------*/
static inline void yield_spin_lock(spinlock_t *lock) {
    if(!yield_is_linux_context()) {
        __raw_spin_lock(&lock->rlock);
        return;
    }
    spin_lock(lock);
}
/*--------------------------------------------------------------------------------*\
 * Speziallock: ist auch aus non-Linux-TC-Kontext aufrufbar 
\*--------------------------------------------------------------------------------*/
static inline void yield_spin_unlock(spinlock_t *lock) {
    if(!yield_is_linux_context()) {
        __raw_spin_unlock(&lock->rlock);
        return;
    }
    spin_unlock(lock);
}
/*--------------------------------------------------------------------------------*\
 * Speziallock: ist auch aus non-Linux-TC-Kontext aufrufbar 
\*--------------------------------------------------------------------------------*/
static inline void yield_spin_lock_bh(spinlock_t *lock) {
    if(!yield_is_linux_context()) {
        __raw_spin_lock(&lock->rlock);
        return;
    }
    spin_lock_bh(lock);
}
/*--------------------------------------------------------------------------------*\
 * Speziallock: ist auch aus non-Linux-TC-Kontext aufrufbar 
\*--------------------------------------------------------------------------------*/
#define yield_spin_lock_irqsave(lock, flags) 				\
do {								\
    if(!yield_is_linux_context()) {    \
        flags = 0;              \
        __raw_spin_lock(&(lock)->rlock);  \
    } else {    \
        spin_lock_irqsave(lock, flags); \
    } \
} while (0)

/*--------------------------------------------------------------------------------*\
 * Speziallock: ist auch aus non-Linux-TC-Kontext aufrufbar 
\*--------------------------------------------------------------------------------*/
static inline void yield_spin_unlock_irqrestore(spinlock_t *lock, unsigned long flags) {
    if(!yield_is_linux_context()) {
        __raw_spin_unlock(&lock->rlock);
        return;
    }
    spin_unlock_irqrestore(lock, flags);
}
#if defined(CONFIG_AVM_IPI_YIELD)
/*--------------------------------------------------------------------------------*\
 * bestimmte Linuxfunktionalitaeten vom Yield aus triggern
\*--------------------------------------------------------------------------------*/
struct _yield_to_linux_ipi {
    enum  _yield_to_linux_ipi_func_type {
        wake_up_type = 0,
        schedule_work_type,         
        schedule_delayed_work_type,
        queue_work_on_type,
        tasklet_hi_schedule_type,
        try_module_get_type,
        module_put_type,
        panic_type,
        yieldexception_type,
        call_type,
    } ipi_func_type;
    union  _yield_to_linux_ipi_params {
        struct _yield_wake_up_param {
            wait_queue_head_t *q;
            unsigned int mode;
            int nr_exclusive;
            void *key;
        } wake_up_param;
        struct _yield_schedule_work_param {
            struct work_struct *work;
        } schedule_work_param;
        struct _yield_schedule_delayed_work_param {
            struct delayed_work *dwork;
            unsigned long delay;
        } schedule_delayed_work_param;
        struct _yield_queue_work_on_param {
            int cpu;
            struct workqueue_struct *wq;
            struct work_struct *work;
        } queue_work_on_param;
        struct _yield_tasklet_hi_schedule_param {
            struct tasklet_struct *t;
        } tasklet_hi_schedule_param;
        struct _yield_module_param {
            struct module *module;
        } module_param;
        struct _panic_param {
            const char *debugstr;
        } panic_param;
        struct _yieldexception_param {
            const void *handle;
        } yieldexception_param;
        struct _call_param {
            void (*func)(void *func_param);
            void *func_param;
        } call_param;
    } u;
    unsigned int cycle;
};

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
extern void yield_trigger_linux_ipi(int cpu, struct _yield_to_linux_ipi *obj);

/*--- #define TEST_YIELD_IPI ---*/
/*--------------------------------------------------------------------------------*\
 * aus beliebigen Kontext verwendbar
 * raw_smp_processor_id() funktioniert auch im YIELD-Thread (gp in thread_info mit initialisierter cpu)
\*--------------------------------------------------------------------------------*/
static inline void __yield_wake_up(wait_queue_head_t *q, unsigned int mode, int nr_exclusive, void *key){
    struct _yield_to_linux_ipi params;
#if !defined(TEST_YIELD_IPI)
    if(yield_is_linux_context()) {
        wake_up_interruptible(q);
        return;
    }
#else
    unsigned long flags;
    unsigned int is_yield = !yield_is_linux_context();
    if(is_yield == 0) {
        local_irq_save(flags);
    }
    printk(KERN_ERR"[%s](%p)\n", __func__, q);
#endif
    params.ipi_func_type                 = wake_up_type;
    params.u.wake_up_param.q             = q;
    params.u.wake_up_param.mode          = mode;
    params.u.wake_up_param.nr_exclusive  = nr_exclusive;
    params.u.wake_up_param.key           = key;
    yield_trigger_linux_ipi(raw_smp_processor_id(), &params);
#if defined(TEST_YIELD_IPI)
    if(is_yield == 0) {
        local_irq_restore(flags);
    }
#endif/*--- #if defined(TEST_YIELD_IP) ---*/
}

#define yield_wake_up(x)			__yield_wake_up(x, TASK_NORMAL, 1, NULL)
#define yield_wake_up_nr(x, nr)		__yield_wake_up(x, TASK_NORMAL, nr, NULL)
#define yield_wake_up_all(x)		__yield_wake_up(x, TASK_NORMAL, 0, NULL)

#define yield_wake_up_interruptible(x)        __yield_wake_up(x, TASK_INTERRUPTIBLE, 1, NULL)
#define yield_wake_up_interruptible_nr(x, nr) __yield_wake_up(x, TASK_INTERRUPTIBLE, nr, NULL)
#define yield_wake_up_interruptible_all(x)    __yield__ake_up(x, TASK_INTERRUPTIBLE, 0, NULL)

/*--------------------------------------------------------------------------------*\
 * aus beliebigen Kontext verwendbar
 * raw_smp_processor_id() funktioniert auch im YIELD-Thread (gp in thread_info mit initialisierter cpu)
\*--------------------------------------------------------------------------------*/
static inline bool yield_schedule_work(struct work_struct *work) {
    struct _yield_to_linux_ipi params;

    if(yield_is_linux_context()) {
        return schedule_work(work);
    }
#if 0
    if(WORK_STRUCT_PENDING & *work_data_bits(work)) {
        return false;
    }
#endif
    params.ipi_func_type               = schedule_work_type;
    params.u.schedule_work_param.work  = work;
    yield_trigger_linux_ipi(raw_smp_processor_id(), &params);
    return true;
}
/*--------------------------------------------------------------------------------*\
 * aus beliebigen Kontext verwendbar
\*--------------------------------------------------------------------------------*/
static inline bool yield_queue_work_on(int cpu, struct workqueue_struct *wq, struct work_struct *work){
    struct _yield_to_linux_ipi params;

    if(yield_is_linux_context()) {
        return queue_work_on(cpu, wq, work);
    }
#if 0
    if((WORK_STRUCT_PENDING & *work_data_bits(work))) {
        return false;
    }
#endif
    params.ipi_func_type               = queue_work_on_type;
    params.u.queue_work_on_param.cpu   = cpu;
    params.u.queue_work_on_param.wq    = wq;
    params.u.queue_work_on_param.work  = work;
    yield_trigger_linux_ipi(cpu, &params);
    return true;
}
/*--------------------------------------------------------------------------------*\
 * aus beliebigen Kontext verwendbar
 * raw_smp_processor_id() funktioniert auch im YIELD-Thread (gp in thread_info mit initialisierter cpu)
\*--------------------------------------------------------------------------------*/
static inline bool yield_schedule_delayed_work(struct delayed_work *dwork, unsigned long delay) {
    struct _yield_to_linux_ipi params;

    if(yield_is_linux_context()) {
        return schedule_delayed_work(dwork, delay);
    }
#if 0
    if((WORK_STRUCT_PENDING & *work_data_bits(work))) {
        return false;
    }
#endif
    params.ipi_func_type                       = schedule_delayed_work_type;
    params.u.schedule_delayed_work_param.dwork = dwork;
    params.u.schedule_delayed_work_param.delay = delay;
    yield_trigger_linux_ipi(raw_smp_processor_id(), &params);
    return true;
}
/*--------------------------------------------------------------------------------*\
 * aus beliebigen Kontext verwendbar
\*--------------------------------------------------------------------------------*/
static inline bool yield_try_module_get(struct module *module) {
    struct _yield_to_linux_ipi params;

    if(yield_is_linux_context()) {
        return try_module_get(module);
    }
    params.ipi_func_type         = try_module_get_type;
    params.u.module_param.module = module;
    /*--- gleiche CPU wie yield_module_put() um Nebenlaeufigkeiten zu verhindern! ---*/
    yield_trigger_linux_ipi(0, &params);
    return true;
}
/*--------------------------------------------------------------------------------*\
 * aus beliebigen Kontext verwendbar
\*--------------------------------------------------------------------------------*/
static inline void yield_module_put(struct module *module) {
    struct _yield_to_linux_ipi params;

    if(yield_is_linux_context()) {
        module_put(module);
        return;
    }
    params.ipi_func_type         = module_put_type;
    params.u.module_param.module = module;
    /*--- gleiche CPU wie yield_module_put() um Nebenlaeufigkeiten zu verhindern! ---*/
    yield_trigger_linux_ipi(0, &params);
}
/*--------------------------------------------------------------------------------*\
 * aus beliebigen Kontext verwendbar
 * raw_smp_processor_id() funktioniert auch im YIELD-Thread (gp in thread_info mit initialisierter cpu)
\*--------------------------------------------------------------------------------*/
static inline void yield_tasklet_hi_schedule(struct tasklet_struct *t) {
    struct _yield_to_linux_ipi params;

    if(yield_is_linux_context()) {
        tasklet_hi_schedule(t);
    }
    if ((t->state & (1 << TASKLET_STATE_SCHED)) == 0) {
        params.ipi_func_type                 = tasklet_hi_schedule_type;
        params.u.tasklet_hi_schedule_param.t = t;
        yield_trigger_linux_ipi(raw_smp_processor_id(), &params);
    }
}
/*--------------------------------------------------------------------------------*\
 * aus beliebigen Kontext verwendbar
 * raw_smp_processor_id() funktioniert auch im YIELD-Thread (gp in thread_info mit initialisierter cpu)
\*--------------------------------------------------------------------------------*/
static inline void yield_panic(const char *debugstr) {
    struct _yield_to_linux_ipi params;

    if(yield_is_linux_context()) {
        panic("%s\n", debugstr);
    }
    params.ipi_func_type          = panic_type;
    params.u.panic_param.debugstr = debugstr;
    yield_trigger_linux_ipi(raw_smp_processor_id(), &params);
}
/*--------------------------------------------------------------------------------*\
 * nur aus Yield-Kontext verwenden
 * raw_smp_processor_id() funktioniert auch im YIELD-Thread (gp in thread_info mit initialisierter cpu)
\*--------------------------------------------------------------------------------*/
static inline void yield_exception(const void *handle) {
    struct _yield_to_linux_ipi params;

    if(yield_is_linux_context()) {
        /*--- do nothing if called from Linux-OS ---*/
        return;
    }
    params.ipi_func_type        = yieldexception_type;
    params.u.yieldexception_param.handle = handle;
    yield_trigger_linux_ipi(raw_smp_processor_id(), &params);
}
/*--------------------------------------------------------------------------------*\
 * auch aus Linux-Kontext verwendbar
\*--------------------------------------------------------------------------------*/
static inline void yield_call(int cpu, void (*call_func)(void *func_param), void *func_param) {
    unsigned long flags, is_linux_context;
    struct _yield_to_linux_ipi params;

    params.ipi_func_type           = call_type;
    params.u.call_param.func       = call_func;
    params.u.call_param.func_param = func_param;
    is_linux_context = yield_is_linux_context();
    if(is_linux_context) local_irq_save(flags);
    yield_trigger_linux_ipi(cpu, &params);
    if(is_linux_context)local_irq_restore(flags);
}
#endif/*--- #if defined(CONFIG_AVM_IPI_YIELD) ---*/
#endif/*--- #ifndef __yield_context_h__ ---*/
