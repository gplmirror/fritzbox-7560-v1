
/*
 *
 * Gary Jennejohn <gj@denx.de>
 * Copyright (C) 2003 Gary Jennejohn
 *
 * ########################################################################
 *
 *  This program is free software; you can distribute it and/or modify it
 *  under the terms of the GNU General Public License (Version 2) as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  59 Temple Place - Suite 330, Boston MA 02111-1307, USA.
 *
 * ########################################################################
 *
 * Reset the VR9 reference board.
 *
 */



#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/pm.h>
#include <asm/reboot.h>
#include <linux/avm_hw_config.h>
#include <linux/avm_kernel_config.h>
#include <asm/mach_avm.h>


static void set_reboot_status(char *text, int force);

#define UPDATE_REBOOT_STATUS_TEXT            "(c) AVM 2015, Reboot Status is: Firmware-Update" \
                                             "(c) AVM 2015, Reboot Status is: Firmware-Update" \
                                             "(c) AVM 2015, Reboot Status is: Firmware-Update"
#define SOFT_REBOOT_STATUS_TEXT              "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot"
#define NMI_REBOOT_STATUS_TEXT               "(c) AVM 2015, Reboot Status is: NMI-Watchdog" \
                                             "(c) AVM 2015, Reboot Status is: NMI-Watchdog" \
                                             "(c) AVM 2015, Reboot Status is: NMI-Watchdog"
#define SOFTWATCHDOG_REBOOT_STATUS_TEXT      "(c) AVM 2015, Reboot Status is: Software-Watchdog" \
                                             "(c) AVM 2015, Reboot Status is: Software-Watchdog" \
                                             "(c) AVM 2015, Reboot Status is: Software-Watchdog"
#define POWERON_REBOOT_STATUS_TEXT           "(c) AVM 2015, Reboot Status is: Power-On-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Power-On-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Power-On-Reboot"
#define TEMP_REBOOT_STATUS_TEXT              "(c) AVM 2015, Reboot Status is: Temperature-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Temperature-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Temperature-Reboot"

static enum _avm_reset_status avm_reboot_status;

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
enum _avm_reset_status avm_reset_status(void){
    return avm_reboot_status;
}
EXPORT_SYMBOL(avm_reset_status);


#if defined(CONFIG_SOC_GRX500)

#if defined(CONFIG_OF_AVM_DT)
#include <linux/of_fdt.h>
#endif /* defined(CONFIG_OF_AVM_DT) */

unsigned char * get_mailbox(void){
    static unsigned char *mailbox = 0;
    int k;
    if(mailbox)
        return mailbox;

#if defined(CONFIG_OF_AVM_DT)
    //try to find the reserved entry for 'avm_reboot_string'
    for(k = 0; k < reserved_resources_used; k++){
        if(!strcmp(reserved_resources[k].name, "avm_reboot_string")){
            mailbox = (unsigned char *) KSEG1ADDR(reserved_resources[k].start);        
            pr_info("Found avm_reboot_string, setting mailbox to 0x%08x \n",(uint32_t) mailbox);
            return mailbox;
        }
    }
#endif /* defined(CONFIG_OF_AVM_DT) */
    mailbox = (unsigned char *)  AVM_REBOOT_STRING_LOCATION;
    pr_err("Didn't found avm_reboot_string, setting mailbox to fallback 0x%08x \n",(uint32_t)  mailbox);
    return mailbox;
}
#endif /* #if defined(CONFIG_SOC_GRX500) */

struct _reboot_info {
    enum _avm_reset_status status;
    char *matchtext;
    char *printouttext;
};

static const struct _reboot_info reboot_info[] = {
    { status: RS_SOFTWATCHDOG,      matchtext: SOFTWATCHDOG_REBOOT_STATUS_TEXT, printouttext: "Softwatchdog-Reboot"     },
    { status: RS_NMIWATCHDOG,       matchtext: NMI_REBOOT_STATUS_TEXT,          printouttext: "NMI-Watchdog-Reset"      },
    { status: RS_REBOOT,            matchtext: SOFT_REBOOT_STATUS_TEXT,         printouttext: "Soft-Reboot"             },
    { status: RS_FIRMWAREUPDATE,    matchtext: UPDATE_REBOOT_STATUS_TEXT,       printouttext: "Fw-Update"               },
    { status: RS_SHORTREBOOT,       matchtext: POWERON_REBOOT_STATUS_TEXT,      printouttext: "Short-PowerOff-Reboot"   },
    { status: RS_TEMP_REBOOT,       matchtext: TEMP_REBOOT_STATUS_TEXT,         printouttext: "Temperature-Reboot"      },
    /*--- definition: have to be last entry: ---*/
    { status: RS_POWERON,           matchtext: NULL,                            printouttext: "Power-On"   },
};

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int get_reboot_status(void) {
    char Buffer[512];
#if defined(CONFIG_SOC_GRX500)
    volatile unsigned char *mailbox = (volatile unsigned char *) get_mailbox();
#else
    volatile unsigned char *mailbox = (volatile unsigned char *)(0xA1000000 - 512);
#endif
    unsigned int i;
    memcpy(Buffer, (void *)mailbox, 512);
    Buffer[511] = '\0';
    /*--- printk("Reboot Status: '%s'\n", filter_buf(Buffer)); ---*/
    for(i = 0; i < ARRAY_SIZE(reboot_info); i++) {
        if(reboot_info[i].matchtext == NULL) {
            break;
        }
        if(strcmp(Buffer, reboot_info[i].matchtext) == 0) {
            break;
        }
    }
    avm_reboot_status = reboot_info[i].status;
    printk(KERN_ERR"Reboot Status is: %s\n", reboot_info[i].printouttext);

    set_reboot_status(POWERON_REBOOT_STATUS_TEXT, 1);
    return 0;
}
/*--------------------------------------------------------------------------------*\
 * zweites Setzen des Status wird ignoriert!
 * Spezialfall: status = RS_POWERON - lasse es wie Kaltstart aussehen
 * (PCMCR-Workarround fuer GRX)
\*--------------------------------------------------------------------------------*/
void avm_set_reset_status(enum _avm_reset_status status){
    unsigned int i;

    if(status == RS_POWERON) {
        set_reboot_status("-------", 1);
        return;
    }
    for(i = 0; i < ARRAY_SIZE(reboot_info); i++) {
        if((reboot_info[i].status == status) && reboot_info[i].matchtext) {
            set_reboot_status(reboot_info[i].matchtext, 0);
            return;
        }
    }
}
EXPORT_SYMBOL(avm_set_reset_status);

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void set_reboot_status(char *text, int force) {
    static char *reboot_cause_written = NULL;
#if defined(CONFIG_SOC_GRX500)
    volatile unsigned char *mailbox = (volatile unsigned char *)get_mailbox();
#else
    volatile unsigned char *mailbox = (volatile unsigned char *)(0xA1000000 - 512);
#endif
    int len;
    /*--- only if not valid set before: ---*/
    if(reboot_cause_written != NULL) {
        return;
    }
    if(force == 0){
        reboot_cause_written = text;
    }
    len = strlen(text);
    memcpy((char *)mailbox, text, len);
    mailbox += len;
    *mailbox = '\0';
}

arch_initcall(get_reboot_status);
