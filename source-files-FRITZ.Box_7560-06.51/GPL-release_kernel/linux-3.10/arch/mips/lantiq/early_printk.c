/*
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 2 as published
 *  by the Free Software Foundation.
 *
 *  Copyright (C) 2010 John Crispin <blogic@openwrt.org>
 */

#include <linux/cpu.h>
#include <lantiq_soc.h>

#define ASC_BUF		1024
#define LTQ_ASC_FSTAT	((u32 *)(LTQ_EARLY_ASC + 0x0048))
#ifdef __BIG_ENDIAN
#define LTQ_ASC_TBUF	((u32 *)(LTQ_EARLY_ASC + 0x0020 + 3))
#else
#define LTQ_ASC_TBUF	((u32 *)(LTQ_EARLY_ASC + 0x0020))
#endif
#define TXMASK		0x3F00
#define TXOFFSET	8

void prom_putchar(char c)
{
	unsigned long flags;

	local_irq_save(flags);
	do { } while ((ltq_r32(LTQ_ASC_FSTAT) & TXMASK) >> TXOFFSET);
	if (c == '\n')
		ltq_w8('\r', LTQ_ASC_TBUF);
	ltq_w8(c, LTQ_ASC_TBUF);
	local_irq_restore(flags);
}
#define PROM_PRINTF_BUF_SIZE 1024
void prom_printf(const char * fmt, ...)
{
    static char buf[PROM_PRINTF_BUF_SIZE];

    va_list args;
    int l;
    char *p, *buf_end;

    /*--- Low level, brute force, not SMP safe... ---*/
    va_start(args, fmt);
    l = vsnprintf(buf, PROM_PRINTF_BUF_SIZE, fmt, args); /* hopefully i < sizeof(buf) */
    va_end(args);
    buf_end = buf + l;

    for ( p = buf; p < buf_end; p++ ) {
        prom_putchar(*p);
    }
}
EXPORT_SYMBOL(prom_printf);
