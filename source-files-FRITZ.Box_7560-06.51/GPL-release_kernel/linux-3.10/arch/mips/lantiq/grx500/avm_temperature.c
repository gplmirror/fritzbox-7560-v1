#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <lantiq.h>
#include <asm/addrspace.h>



struct temp_mapping{
    uint32_t raw_value;
    int32_t temperature;
};

struct temp_mapping mapping_table[]={
    {3421 ,125 },
    {3437 ,120 },
    {3452 ,115 },
    {3467 ,110 },
    {3482 ,105 },
    {3496 ,100 },
    {3510 ,95 },
    {3524 ,90 },
    {3537 ,85 },
    {3550 ,80 },
    {3563 ,75 },
    {3575 ,70 },
    {3588 ,65 },
    {3600 ,60 },
    {3611 ,55 },
    {3623 ,50 },
    {3634 ,45 },
    {3645 ,40 },
    {3656 ,35 },
    {3667 ,30 },
    {3678 ,25 },
    {3688 ,20 },
    {3698 ,15 },
    {3708 ,10 },
    {3718 ,5 },
    {3728 ,0 },
    {3737 ,-5 },
    {3747 ,-10 },
    {3756 ,-15 },
    {3765 ,-20 },
    {3774 ,-25 },
    {3783 ,-30 },
    {3792 ,-35 },
    {3800 ,-40 }
};
//returns the temperature as fixed point with 2 bits after .
uint32_t convert_raw_to_temperature(struct seq_file *m, uint32_t raw_value){
    int i;
    if(raw_value < 3421){
        if(m){
            seq_printf(m, "[%s] Raw Value=%u smaller than 3421, cannot convert temperature, temperature is above 125°C\n", __FUNCTION__, raw_value); 
        }
        else{
            pr_err("[%s] Raw Value=%u smaller than 3421, cannot convert temperature, temperature is above 125°C\n", __FUNCTION__, raw_value); 
        }
        return 125 << 2;
    }
    if(raw_value > 3800){
        if(m){
            seq_printf(m, "[%s] Raw Value=%u higher than 3800, cannot convert temperature, temperature is below -40°C\n", __FUNCTION__, raw_value); 
        }
        else{
            pr_err("[%s] Raw Value=%u higher than 3800, cannot convert temperature, temperature is below -40°C\n", __FUNCTION__, raw_value); 
        }
        return -40 << 2;
    }
    for(i = 0; i < sizeof(mapping_table)/sizeof(struct temp_mapping); i++){
        //exact match
        if(raw_value == mapping_table[i].raw_value){
            return mapping_table[i].temperature << 2;
        }
        else if(raw_value < mapping_table[i].raw_value){
            //we break here, but need to check afterwards if the next entry might be a potiential exact match
            break;
        }
    }

    if(raw_value == mapping_table[i - 1].raw_value){
        return mapping_table[i - 1].temperature;
    }
    else{
        int32_t diff;
        int32_t diff2;
        uint32_t factor;
        int32_t ret;
        int32_t add;
        //we didn't have an exact match-> interpolate linearly
        diff = mapping_table[i].raw_value - mapping_table[i - 1].raw_value;
        /*--- seq_printf(m, "diff is %d\n", diff); ---*/
        diff2 = mapping_table[i].raw_value - raw_value;
        /*--- seq_printf(m, "diff2 is %d\n", diff2); ---*/
        diff2 = diff2 << 16;
        /*--- seq_printf(m, "diff2 is %d\n", diff2); ---*/
        factor = diff2  / diff;
        /*--- seq_printf(m, "factor is %d\n", factor); ---*/
        add = factor * (mapping_table[i - 1].temperature - mapping_table[i].temperature);
        /*--- seq_printf(m, "add is %d\n", add); ---*/
        add = add >> 14;
        /*--- seq_printf(m, "add is %d\n", add); ---*/
        ret = (mapping_table[i].temperature << 2) + add;
        /*--- seq_printf(m, "ret is %d\n", ret); ---*/
        return ret; 
    }

}

#define LTQ_TEMP_SENSOR ((u32 *)(KSEG1 + 0x16080100))
#define TEMP_SENSOR_SOC_SHIFT 0
#define TEMP_SENSOR_AVG_SEL_SHIFT 1
#define TEMP_SENSOR_AVG_SEL_MASK 0x3
#define TEMP_SENSOR_TSOVH_INT_EN_SHIFT 3
#define TEMP_SENSOR_CH_SEL_SHIFT 8
#define TEMP_SENSOR_CH_SEL_MASK 0x7
#define TEMP_SENSOR_TS_EN_SHIFT 11
#define TEMP_SENSOR_INT_LVL_SHIFT 16
#define TEMP_SENSOR_INT_LVL_MASK 0xFFFF

#define LTQ_TEMP_SENSOR_DATA ((u32 *)(KSEG1 + 0x16080104))
#define TEMP_SENSOR_DATA_TS_DV_SHIFT 31
#define TEMP_SENSOR_DATA_TS_CODE_SHIFT 0
#define TEMP_SENSOR_DATA_TS_CODE_MASK 0xFFF

#define LTQ_TEMP_SENSOR_TBIS ((u32 *)(KSEG1 + 0x16080108))

int grx_get_chip_temperature_raw(int channel) {
    uint32_t sensor_config, timeout;  
    int32_t raw_temperature;
    if(channel >= 8){
        //return maximum temperature on wrong channel
        pr_err("[%s] Error: Channel Number must not be greater than 7, returning 125 °C\n", __FUNCTION__);
        return 3421;
    }
    sensor_config = ltq_r32(LTQ_TEMP_SENSOR);
    //reset channel, reset AVG and enable sensor
    sensor_config &= ~((TEMP_SENSOR_AVG_SEL_MASK << TEMP_SENSOR_AVG_SEL_SHIFT) | (1 << TEMP_SENSOR_TS_EN_SHIFT) | (TEMP_SENSOR_CH_SEL_MASK << TEMP_SENSOR_CH_SEL_SHIFT));
    //set channel and start of conversion and AVG16
    sensor_config |= (channel << TEMP_SENSOR_CH_SEL_SHIFT) | (1 << TEMP_SENSOR_SOC_SHIFT) | ( 3 << TEMP_SENSOR_AVG_SEL_SHIFT);
    ltq_w32(sensor_config, LTQ_TEMP_SENSOR);
    //wait for busy bit to be reset (with timeout)
    timeout = 0;
    while((timeout < 100000) && ((ltq_r32(LTQ_TEMP_SENSOR_DATA) & (1 << TEMP_SENSOR_DATA_TS_DV_SHIFT)))){
        timeout ++;
    }
    //wait until conversion is finished
    while(! (ltq_r32(LTQ_TEMP_SENSOR_DATA) & (1 << TEMP_SENSOR_DATA_TS_DV_SHIFT)));
    raw_temperature = (ltq_r32(LTQ_TEMP_SENSOR_DATA) >> TEMP_SENSOR_DATA_TS_CODE_SHIFT) & (TEMP_SENSOR_DATA_TS_CODE_MASK);
    sensor_config = ltq_r32(LTQ_TEMP_SENSOR);
    sensor_config &= ~(1 << TEMP_SENSOR_SOC_SHIFT);
    sensor_config |= (1 << TEMP_SENSOR_TS_EN_SHIFT);
    ltq_w32(sensor_config, LTQ_TEMP_SENSOR);
    return raw_temperature;
}

//returns the temperature in fixed point float with two bits behind the comma
int grx_get_chip_temperature(int channel) {
    int32_t raw_temperature, temp;
    if(channel >= 8){
        //return maximum temperature on wrong channel
        pr_err("[%s] Error: Channel Number must not be greater than 7, returning 125 °C\n", __FUNCTION__);
        return 125 << 2;
    }
    raw_temperature = grx_get_chip_temperature_raw(channel);
    temp=convert_raw_to_temperature(NULL, raw_temperature);
    return temp;
}
#ifdef CONFIG_PROC_FS
static int temperature_proc_show(struct seq_file *m, void *v) {
    uint32_t channel;
    int32_t raw_temperature, temp, temp_points;
    seq_printf(m, "Reading Temperatures:\n");
    //enable sensor
    //sensor_config = ltq_r32(LTQ_TEMP_SENSOR);
    for(channel = 0; channel < 2; channel++){
        raw_temperature = grx_get_chip_temperature_raw(channel);
        temp=convert_raw_to_temperature(NULL, raw_temperature);
        //split the fixed point for printout
        temp_points = (temp & 0x3) * 25;
        temp       /= 4;
        seq_printf(m, "Channel %d: %d.%02d °C (Raw Value: %d)\n", channel, temp, temp_points, raw_temperature);

#if 0
        //reset channel, reset AVG and enable sensor
        sensor_config &= ~((TEMP_SENSOR_AVG_SEL_MASK << TEMP_SENSOR_AVG_SEL_SHIFT) | (1 << TEMP_SENSOR_TS_EN_SHIFT) | (TEMP_SENSOR_CH_SEL_MASK << TEMP_SENSOR_CH_SEL_SHIFT));
        //set channel and start of conversion and AVG16
        sensor_config |= (channel << TEMP_SENSOR_CH_SEL_SHIFT) | (1 << TEMP_SENSOR_SOC_SHIFT) | ( 3 << TEMP_SENSOR_AVG_SEL_SHIFT);
        ltq_w32(sensor_config, LTQ_TEMP_SENSOR);
        //wait for busy bit to be reset (with timeout)
        timeout = 0;
        while((timeout < 100000) && ((ltq_r32(LTQ_TEMP_SENSOR_DATA) & (1 << TEMP_SENSOR_DATA_TS_DV_SHIFT)))){
            timeout ++;
        }
        //wait until conversion is finished
        while(! (ltq_r32(LTQ_TEMP_SENSOR_DATA) & (1 << TEMP_SENSOR_DATA_TS_DV_SHIFT)));
        raw_temperature = (ltq_r32(LTQ_TEMP_SENSOR_DATA) >> TEMP_SENSOR_DATA_TS_CODE_SHIFT) & (TEMP_SENSOR_DATA_TS_CODE_MASK);
        temp=convert_raw_to_temperature(m, raw_temperature);
        //split the fixed point for printout
        temp_points = (temp & 0x3) * 25;
        if(temp & 0x80000000){
            temp = temp >> 2 | 0xC0000000;
        }
        else{
            temp = temp >> 2;
        }
        seq_printf(m, "Channel %d: %d.%02d °C (Raw Value: %d)\n", channel, temp, temp_points, raw_temperature);
        sensor_config = ltq_r32(LTQ_TEMP_SENSOR);
        sensor_config &= ~(1 << TEMP_SENSOR_SOC_SHIFT);
        sensor_config |= (1 << TEMP_SENSOR_TS_EN_SHIFT);
        ltq_w32(sensor_config, LTQ_TEMP_SENSOR);
#endif
    } 
    return 0;
}

static int temperature_proc_open(struct inode *inode, struct  file *file) {
  return single_open(file, temperature_proc_show, NULL);
}

static const struct file_operations temperature_proc_fops = {
  .owner = THIS_MODULE,
  .open = temperature_proc_open,
  .read = seq_read,
  .llseek = seq_lseek,
  .release = seq_release_private,
};

static int __init temperature_proc_init(void) {
  proc_create("chip_temperature", 0, NULL, &temperature_proc_fops);
  return 0;
}
late_initcall(temperature_proc_init);

static void __exit temperature_proc_exit(void) {
  remove_proc_entry("chip_temperature", NULL);
}
#endif
