/*
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 *
 * Copyright (C) 2015 Zhu YiXin<yixin.zhu@lantiq.com>
 * UMT Driver for GRX350 A21
 */
#define DEBUG
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/io.h>
#include <linux/module.h>
#include <linux/clk.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/proc_fs.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/errno.h>
#include <linux/dma-mapping.h>
#include <linux/sched.h>
#include <linux/wait.h>
#include <linux/of.h>
#include <linux/of_irq.h>
#include <linux/seq_file.h>
#include <lantiq_dmax.h>
		  
#include <lantiq.h>
#include <lantiq_soc.h>
#include <lantiq_irq.h>
		  
#include <net/datapath_proc_api.h>
#include "ltq_hwmcpy_addr.h"
#include <linux/ltq_hwmcpy.h>
#include "ltq_hwmcpy.h"

static u32 g_dma_ctrl = DMA1TX;

static inline void umt_set_mode(u32 umt_id, enum umt_mode umt_mode)
{
	u32 val, off;
	
	if (!umt_id)
		ltq_mcpy_w32_mask(0x2, ((u32)umt_mode) << 1, MCPY_GCTRL);
	else {
		off = 16 + (umt_id - 1) * 3;
		val = ltq_mcpy_r32(MCPY_GCTRL) & ~(BIT(off));
		ltq_mcpy_w32(val | (((u32)umt_mode) << off), MCPY_GCTRL);
	}
}

static inline void umt_set_msgmode(u32 umt_id, enum umt_msg_mode msg_mode)
{
	if (!umt_id)
		ltq_mcpy_w32((u32)msg_mode, MCPY_UMT_SW_MODE);
	else
		ltq_mcpy_w32((u32)msg_mode,
			MCPY_UMT_X_ADDR(umt_id, MCPY_UMT_XSW_MODE));
}

/* input in term of microseconds */
static inline u32 umt_us_to_cnt(int usec)
{
	struct clk *ngi_clk = clk_get_xbar();

	return (usec * (clk_get_rate(ngi_clk) / 1000000));
}

static inline void umt_set_period(u32 umt_id, u32 umt_period)
{
	umt_period = umt_us_to_cnt(umt_period);

	if (!umt_id)
		ltq_mcpy_w32(umt_period, MCPY_UMT_PERD);
	else
		ltq_mcpy_w32(umt_period,
			MCPY_UMT_X_ADDR(umt_id, MCPY_UMT_XPERIOD));
}

static inline void umt_set_dst(u32 umt_id, u32 umt_dst)
{
	if (!umt_id)
		ltq_mcpy_w32(umt_dst, MCPY_UMT_DEST);
	else
		ltq_mcpy_w32(umt_dst,
			MCPY_UMT_X_ADDR(umt_id, MCPY_UMT_XDEST));
}

static inline void umt_set_mux(u32 umt_id, u32 cbm_pid, u32 dma_cid)
{
	u32 mux_sel;

	cbm_pid = cbm_pid & 0xF;
	dma_cid = dma_cid & 0xF;
	mux_sel = ltq_mcpy_r32(MCPY_UMT_TRG_MUX) & ~((0xF000F) <<(umt_id * 4));
	mux_sel |= (dma_cid << (umt_id * 4)) | (cbm_pid << (16 + (umt_id * 4)));
	ltq_mcpy_w32(mux_sel, MCPY_UMT_TRG_MUX);
}

static inline void umt_set_endian(int dw_swp, int byte_swp)
{
	u32 val;

	val = ltq_mcpy_r32(MCPY_GCTRL);
	if (byte_swp)
		val |= BIT(28);
	else
		val &= ~(BIT(28));

	if (dw_swp)
		val |= BIT(29);
	else
		val &= ~(BIT(29));

	ltq_mcpy_w32(val, MCPY_GCTRL);
}

static inline void umt_en_expand_mode(void)
{
	u32 val;

	val = ltq_mcpy_r32(MCPY_GCTRL) | BIT(31);
	ltq_mcpy_w32(val, MCPY_GCTRL);

	if (IS_ENABLED(CONFIG_CPU_BIG_ENDIAN))
		umt_set_endian(1, 0);
	else
		umt_set_endian(1, 1);
}

static inline void umt_enable(u32 umt_id, enum umt_status status)
{
	u32 val, off;

	if (!umt_id)
		ltq_mcpy_w32_mask(0x4, ((u32)status) << 2, MCPY_GCTRL);
	else {
		off = 17 + (umt_id - 1) * 3;
		val = (ltq_mcpy_r32(MCPY_GCTRL) & ~BIT(off))
				| (((u32)status) << off);
		ltq_mcpy_w32(val, MCPY_GCTRL);
	}
}

static inline void umt_suspend(u32 umt_id, enum umt_status status)
{
	u32 val;

	if (status)
		val = ltq_mcpy_r32(MCPY_UMT_CNT_CTRL) | BIT(umt_id);
	else
		val = ltq_mcpy_r32(MCPY_UMT_CNT_CTRL) & (~(BIT(umt_id)));

	ltq_mcpy_w32(val, MCPY_UMT_CNT_CTRL); 
}

/*This function will disable umt */
static inline void umt_reset_umt(u32 umt_id)
{
	u32 mode;
	umt_enable(umt_id, UMT_DISABLE);

	mode = ltq_mcpy_r32(MCPY_UMT_X_ADDR(umt_id, MCPY_UMT_XSW_MODE));

	if (mode == UMT_SELFCNT_MODE) {
		umt_set_mode(umt_id, UMT_USER_MODE);
		umt_set_mode(umt_id, UMT_SELFCNT_MODE);
	} else {
		umt_set_mode(umt_id, UMT_SELFCNT_MODE);
		umt_set_mode(umt_id, UMT_USER_MODE);
	}

	return;
}

/**
 * intput:
 * @umt_id: UMT port id, (0 - 3)
 * @ep_id:  Aligned with datapath lib ep_id
 * @period: measured in microseconds.
 * ret:  Fail < 0 / Success: 0
 */
int ltq_umt_set_period(u32 umt_id, u32 ep_id, u32 period)
{
	struct mcpy_umt *pumt = mcpy_get_umt();
	struct umt_port *port;

	if (period < MIN_UMT_PRD || umt_id >= UMT_PORTS_NUM)
		goto period_err;

	if (pumt->status != UMT_ENABLE) {
		mcpy_dbg(MCPY_ERR, "UMT is not initialized!\n");
		return -ENODEV;
	}

	port = &pumt->ports[umt_id];

	spin_lock_bh(&port->umt_port_lock);
	if (port->ep_id != ep_id) {
		spin_unlock_bh(&port->umt_port_lock);
		goto period_err;
	}

	if (port->umt_period != period) {
		port->umt_period = period;
		umt_set_period(umt_id, port->umt_period);
	}
	spin_unlock_bh(&port->umt_port_lock);

	return 0;

period_err:
	mcpy_dbg(MCPY_ERR, "umt_id: %d, ep_id: %d, period: %d\n",
		umt_id, ep_id, period);

	return -EINVAL;
}
EXPORT_SYMBOL(ltq_umt_set_period);

/**
 * API to configure the UMT port.
 * input:
 * @umt_id: (0 - 3)
 * @ep_id: aligned with datapath lib EP
 * @umt_mode:  0-self-counting mode, 1-user mode.
 * @msg_mode:  0-No MSG, 1-MSG0 Only, 2-MSG1 Only, 3-MSG0 & MSG1.
 * @dst:  Destination PHY address.
 * @period(ms): only applicable when set to self-counting mode. 
 *              self-counting interval time. if 0, use the original setting.
 * @enable: 1-Enable/0-Disable
 * @ret:  Fail < 0 , SUCCESS:0
 */
int ltq_umt_set_mode(u32 umt_id, u32 ep_id, u32 umt_mode, u32 msg_mode,
			u32 phy_dst, u32 period, u32 enable)
{
	struct mcpy_umt *pumt = mcpy_get_umt();
	struct umt_port *port;

	if (pumt->status != UMT_ENABLE) {
		mcpy_dbg(MCPY_ERR, "UMT is not initialized!!\n");
		return -ENODEV;
	}
	if ((umt_mode >= (u32)UMT_MODE_MAX)
			|| (msg_mode >= (u32)UMT_MSG_MAX)
			|| (enable >= (u32)UMT_STATUS_MAX)
			|| (phy_dst == 0)
			|| (period == 0)
			|| (umt_id >= UMT_PORTS_NUM)) {
		mcpy_dbg(MCPY_ERR, "umt_id: %d, umt_mode: %d, msg_mode: %d, enable: %d, phy_dst: %d\n",
			umt_id, umt_mode, msg_mode, enable, phy_dst);
		return -EINVAL;
	}

	port = &pumt->ports[umt_id];

	spin_lock_bh(&port->umt_port_lock);
	if (port->ep_id != ep_id) {
		mcpy_dbg(MCPY_ERR, "input ep_id: %d, port ep_id: %d\n",
			ep_id, port->ep_id);
		spin_unlock_bh(&port->umt_port_lock);
		return -EINVAL;
	}

	umt_reset_umt(umt_id);

	port->umt_mode = (enum umt_mode)umt_mode;
	port->msg_mode = (enum umt_msg_mode)msg_mode;
	port->umt_dst	= phy_dst;
	port->umt_period = period;
	port->status = (enum umt_status)enable;

	umt_set_mode(umt_id, port->umt_mode);
	umt_set_msgmode(umt_id, port->msg_mode);
	umt_set_dst(umt_id, port->umt_dst);
	umt_set_period(umt_id, port->umt_period);
	umt_enable(umt_id, port->status);
	/* setup the CBM/DMA mapping */
	spin_unlock_bh(&port->umt_port_lock);

	return 0;
}
EXPORT_SYMBOL(ltq_umt_set_mode);

/**
 * API to enable/disable umt port
 * input:
 * @umt_id (0 - 3)
 * @ep_id: aligned with datapath lib EP
 * @enable: Enable: 1 / Disable: 0
 * ret:  Fail < 0, Success: 0
 */
int ltq_umt_enable(u32 umt_id, u32 ep_id, u32 enable)
{
	struct mcpy_umt *pumt = mcpy_get_umt();
	struct umt_port *port;

	if (umt_id >= UMT_PORTS_NUM)
		return -EINVAL;
	if (enable >= (u32)UMT_STATUS_MAX
			|| pumt->status != UMT_ENABLE)
		return -ENODEV;

	port = &pumt->ports[umt_id];

	spin_lock_bh(&port->umt_port_lock);
	if (port->ep_id != ep_id || port->umt_dst == 0 || port->ep_id == 0) {
		mcpy_dbg(MCPY_ERR, "input ep_id: %d, umt port ep_id: %d, umt_dst: 0x%x\n",
			ep_id, port->ep_id, port->umt_dst);
		goto en_err;
	}

	if (port->status != enable) {
		port->status = (enum umt_status)enable;
		umt_enable(umt_id, port->status);
	}
	spin_unlock_bh(&port->umt_port_lock);

	return 0;

en_err:
	spin_unlock_bh(&port->umt_port_lock);
	return -EINVAL;
}
EXPORT_SYMBOL(ltq_umt_enable);

/**
 * API to suspend/resume umt US/DS counter
 * input:
 * @umt_id (0 - 3)
 * @ep_id: aligned with datapath lib EP
 * @enable: suspend: 1 / resume: 0
 * ret:  Fail < 0, Success: 0
 */
int ltq_umt_suspend(u32 umt_id, u32 ep_id, u32 enable)
{
	struct mcpy_umt *pumt = mcpy_get_umt();
	struct umt_port *port;

	if (umt_id >= UMT_PORTS_NUM)
		return -EINVAL;
	if (enable >= (u32)UMT_STATUS_MAX
			|| pumt->status != UMT_ENABLE)
		return -ENODEV;

	port = &pumt->ports[umt_id];

	spin_lock_bh(&port->umt_port_lock);
	if (port->ep_id != ep_id || port->umt_dst == 0 || port->ep_id == 0) {
		mcpy_dbg(MCPY_ERR, "input ep_id: %d, umt port ep_id: %d, umt_dst: 0x%x\n",
			ep_id, port->ep_id, port->umt_dst);
		goto en_err;
	}

	if (port->suspend != enable) {
		port->suspend = (enum umt_status)enable;
		umt_enable(umt_id, port->status);
		umt_suspend(umt_id, port->suspend);
	}
	spin_unlock_bh(&port->umt_port_lock);

	return 0;

en_err:
	spin_unlock_bh(&port->umt_port_lock);
	return -EINVAL;
}
EXPORT_SYMBOL(ltq_umt_suspend);

/**
 * API to request and allocate UMT port
 * input:
 * @ep_id: aligned with datapath lib EP.
 * @cbm_pid: CBM Port ID(0-3), 0 - CBM port 4, 1 - CBM port 24,
 * 2 - CBM port 25, 3 - CBM port 26
 * output:
 * @dma_ctrlid: DMA controller ID. aligned with DMA driver DMA controller ID
 * @dma_cid: DMA channel ID. 
 * @umt_id: (0 - 3)
 * ret: Fail: < 0,  Success: 0
 */
int ltq_umt_request(u32 ep_id, u32 cbm_pid,
		u32 *dma_ctrlid, u32 *dma_cid, u32 *umt_id)
{
	int i, pid;
	struct mcpy_umt *pumt = mcpy_get_umt();
	struct umt_port *port;

	if (!dma_ctrlid || !dma_cid || !umt_id) {
		mcpy_dbg(MCPY_ERR, "Output pointer is NULL!\n");
		goto param_err;
	}

	if (pumt->status != UMT_ENABLE) {
		mcpy_dbg(MCPY_ERR, "UMT not initialized!\n");
		goto param_err;
	}
	if (!ep_id) {
		mcpy_dbg(MCPY_ERR, "%s: ep_id cannot be zero!\n", __func__);
		goto param_err;
	}

	if (cbm_pid >= UMT_PORTS_NUM) {
		mcpy_dbg(MCPY_ERR, "%s: cbm pid must be in ranage(0 - %d)\n",
			__func__, UMT_PORTS_NUM);
		goto param_err;
	}

	pid = -1;
	spin_lock_bh(&pumt->umt_lock);
	for (i = 0; i < UMT_PORTS_NUM; i++) {
		port = &pumt->ports[i];
		spin_lock_bh(&port->umt_port_lock);
		if (port->ep_id == ep_id) {
			pid = i;
			spin_unlock_bh(&port->umt_port_lock);
			break;
		} else if (port->ep_id == 0 && pid == -1)
			pid = i;
		spin_unlock_bh(&port->umt_port_lock);
	}
	spin_unlock_bh(&pumt->umt_lock);

	if (pid < 0) {
		mcpy_dbg(MCPY_ERR, "No free UMT port!\n");
		return -ENODEV;
	}

	port = &pumt->ports[pid];
	spin_lock_bh(&port->umt_port_lock);
	port->ep_id = ep_id;
	port->cbm_pid = cbm_pid;
	umt_set_mux(port->umt_pid, port->cbm_pid, port->dma_cid);
	*dma_ctrlid = pumt->dma_ctrlid;
	*dma_cid = port->dma_cid;
	*umt_id = port->umt_pid;
	spin_unlock_bh(&port->umt_port_lock);

	return 0;

param_err:
	return -EINVAL;
}
EXPORT_SYMBOL(ltq_umt_request);

/**
 * API to release umt port
 * input:
 * @umt_id (0 - 3)
 * @ep_id: aligned with datapath lib EP
 * 
 * ret:  Fail < 0, Success: 0
 */
int ltq_umt_release(u32 umt_id, u32 ep_id)
{
	struct mcpy_umt *pumt;
	struct umt_port *port;
	
	if (umt_id >= UMT_PORTS_NUM)
		return -ENODEV;

	pumt = mcpy_get_umt();
	if (pumt->status != UMT_ENABLE) {
		mcpy_dbg(MCPY_ERR, "UMT is not initialized!\n");
		return -ENODEV;
	}

	port = &pumt->ports[umt_id];

	spin_lock_bh(&port->umt_port_lock);
	if (port->ep_id != ep_id) {
		mcpy_dbg(MCPY_ERR, "input ep_id: %d, UMT port ep_id: %d\n",
			ep_id, port->ep_id);
		spin_unlock_bh(&port->umt_port_lock);

		return -ENODEV;
	} else {
		port->ep_id = 0;
		port->cbm_pid = 0;
		port->umt_dst = 0;
		port->umt_period = 0;
		port->status = UMT_DISABLE;

		umt_enable(port->umt_pid, UMT_DISABLE);
	}
	spin_unlock_bh(&port->umt_port_lock);

	return 0;
}
EXPORT_SYMBOL(ltq_umt_release);



static void umt_port_init(struct mcpy_umt *pumt,
		struct device_node *node, int pid)
{
	char res_cid[32];
	int cid;
	struct umt_port *port;

	port = &pumt->ports[pid];
	sprintf(res_cid, "lantiq,umt%d-dmacid", pid);
	if (of_property_read_u32(node, res_cid, &cid) < 0) {
		cid = UMT_DEF_DMACID + pid;
	}
	port->pctrl = pumt;
	port->umt_pid = pid;
	port->dma_cid = cid;
	port->ep_id = 0;
	port->status = UMT_DISABLE;
	spin_lock_init(&port->umt_port_lock);
}

static void *umt_port_seq_start(struct seq_file *s, loff_t *pos)
{
	struct mcpy_umt *pumt = s->private;
	struct umt_port *port;

	if (*pos >= UMT_PORTS_NUM)
		return NULL;

	port = &pumt->ports[*pos];

	return port;
}

static void *umt_port_seq_next(struct seq_file *s, void *v, loff_t *pos)
{
	struct mcpy_umt *pumt = s->private;
	struct umt_port *port;

	if (++*pos >= UMT_PORTS_NUM)
		return NULL;
	port = &pumt->ports[*pos];
	return port;
}

static void umt_port_seq_stop(struct seq_file *s, void *v)
{

}

static int umt_port_seq_show(struct seq_file *s, void *v)
{
	struct umt_port *port = v;
	int pid = port->umt_pid;
	u32 val;

	seq_printf(s, "\nUMT port %d configuration\n", pid);
	seq_puts(s, "-----------------------------------------\n");
	seq_printf(s, "UMT port ep_id: %d\n", port->ep_id);
	seq_printf(s, "UMT Mode: \t%s\n",
		port->umt_mode == UMT_SELFCNT_MODE ?
		"UMT SelfCounting Mode" : "UMT User Mode");
	switch (port->msg_mode) {
	case UMT_NO_MSG:
		seq_puts(s, "UMT MSG Mode: \tUMT NO MSG\n");
		break;
	case UMT_MSG0_ONLY:
		seq_puts(s, "UMT MSG Mode: \tUMT MSG0 Only\n");
		break;
	case UMT_MSG1_ONLY:
		seq_puts(s, "UMT MSG Mode: \tUMT MSG1 Only\n");
		break;
	case UMT_MSG0_MSG1:
		seq_puts(s, "UMT MSG Mode: \tUMT_MSG0_And_MSG1\n");
		break;
	default:
		seq_printf(s, "UMT MSG Mode Error! Msg_mode: %d\n",
			port->msg_mode);
	}
	seq_printf(s, "UMT DST: \t0x%x\n", port->umt_dst);
	if (port->umt_mode == UMT_SELFCNT_MODE)
		seq_printf(s, "UMT Period: \t%d(us)\n", port->umt_period);
	seq_printf(s, "UMT Status: \t%s\n",
			port->status == UMT_ENABLE ? "Enable" :
			port->status == UMT_DISABLE ? "Disable" : "Init Fail");
	seq_printf(s, "UMT DMA CID: \t%d\n", port->dma_cid);
	seq_printf(s, "UMT CBM PID: \t%d\n", port->cbm_pid);

	seq_printf(s, "++++Register dump of umt port: %d++++\n", pid);
	if (pid == 0) {
		seq_printf(s, "UMT Status: \t%s\n", 
			(ltq_mcpy_r32(MCPY_GCTRL) & BIT(2)) != 0 ?
			"Enable" : "Disable");
		seq_printf(s, "UMT Mode: \t%s\n",
			(ltq_mcpy_r32(MCPY_GCTRL) & BIT(1)) != 0 ?
			"UMT User MSG mode" : "UMT SelfCounting mode");
		seq_printf(s, "UMT MSG Mode: \t%d\n",
			ltq_mcpy_r32(MCPY_UMT_SW_MODE));
		seq_printf(s, "UMT Dst: \t0x%x\n",
			ltq_mcpy_r32(MCPY_UMT_DEST));
		seq_printf(s, "UMT Period: \t0x%x\n",
			ltq_mcpy_r32(MCPY_UMT_PERD));
		seq_printf(s, "UMT MSG0: \t0x%x\n",
			ltq_mcpy_r32(MCPY_UMT_MSG(0)));
		seq_printf(s, "UMT MSG1: \t0x%x\n",
			ltq_mcpy_r32(MCPY_UMT_MSG(1)));
		
	} else {
		seq_printf(s, "UMT Status: \t%s\n", 
			(ltq_mcpy_r32(MCPY_GCTRL) & BIT(17 + 3 * (pid - 1))) != 0 ?
			"Enable" : "Disable");
		seq_printf(s, "UMT Mode: \t%s\n",
			(ltq_mcpy_r32(MCPY_GCTRL) & BIT(16 + 3 * (pid - 1))) != 0 ?
			"UMT User MSG mode" : "UMT SelfCounting mode");
		seq_printf(s, "UMT MSG Mode: \t%d\n",
			ltq_mcpy_r32(MCPY_UMT_X_ADDR(pid, MCPY_UMT_XSW_MODE)));
		seq_printf(s, "UMT Dst: \t0x%x\n",
			ltq_mcpy_r32(MCPY_UMT_X_ADDR(pid, MCPY_UMT_XDEST)));
		seq_printf(s, "UMT Period: \t0x%x\n",
			ltq_mcpy_r32(MCPY_UMT_X_ADDR(pid, MCPY_UMT_XPERIOD)));
		seq_printf(s, "UMT MSG0: \t0x%x\n",
			ltq_mcpy_r32(MCPY_UMT_X_ADDR(pid, MCPY_UMT_XMSG(0))));
		seq_printf(s, "UMT MSG1: \t0x%x\n",
			ltq_mcpy_r32(MCPY_UMT_X_ADDR(pid, MCPY_UMT_XMSG(1))));
	}

	val = ltq_mcpy_r32(MCPY_UMT_TRG_MUX);
	seq_printf(s, "DMA CID: \t%d\n",
		(val & ((0xF) << (pid * 4))) >> (pid * 4));
	seq_printf(s, "CBM PID: \t%d\n",
		(val & ((0xF) << (16 + pid * 4))) >> (16 + pid * 4));

	return 0;
}


static const struct seq_operations umt_port_seq_ops = {
	.start = umt_port_seq_start,
	.next = umt_port_seq_next,
	.stop = umt_port_seq_stop,
	.show = umt_port_seq_show,
};

static int umt_cfg_read_proc_open(struct inode *inode, struct file *file)
{
	int ret = seq_open(file, &umt_port_seq_ops);

	if (ret == 0) {
		struct seq_file *m = file->private_data;
		m->private = PDE_DATA(inode);
	}
	return ret;
}

static const struct file_operations mcpy_umt_proc_fops = {
	.open           = umt_cfg_read_proc_open,
	.read           = seq_read,
	.llseek         = seq_lseek,
	.release        = seq_release,
};

static int umt_proc_init(struct mcpy_umt *pumt)
{
	struct proc_dir_entry *entry;

	pumt->proc = proc_mkdir("umt", pumt->ctrl->proc);
	if (!pumt->proc)
		return -ENOMEM;

	entry = proc_create_data("umt_info", 0, pumt->proc,
			&mcpy_umt_proc_fops, pumt);
	if (!entry)
		goto err1;

	return 0;

err1:
	mcpy_dbg(MCPY_ERR, "UMT proc create fail!\n");
	remove_proc_entry("umt", pumt->ctrl->proc);
	return -1;
}

/* TODO: Register UMT error interrupt Handler */
void umt_init(struct mcpy_ctrl *pctrl)
{
	struct device_node *node = pctrl->dev->of_node;
	struct mcpy_umt *pumt;
	int i;

	pumt = &pctrl->umt;
	pumt->ctrl = pctrl;
	pumt->dma_ctrlid = g_dma_ctrl;
	spin_lock_init(&pumt->umt_lock);
	umt_en_expand_mode();

	for (i = 0; i < UMT_PORTS_NUM; i++)
		umt_port_init(pumt, node, i);

	umt_proc_init(pumt);
	pumt->status = UMT_ENABLE;

	mcpy_dbg(MCPY_INFO, "UMT initialize success!\n");

	return;
}



