/******************************************************************************

				 Copyright (c) 2012, 2014, 2015
					Lantiq Deutschland GmbH

For licensing information, see the file 'LICENSE' in the root folder of
this software module.

******************************************************************************/


/*####################################
 *				Head File
 * ####################################
 */

/*Common Head File
 */
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/version.h>
#include <linux/types.h>
#include <linux/ctype.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/miscdevice.h>
#include <linux/etherdevice.h>
#include <linux/ethtool.h>
#include <linux/if_ether.h>
#include <linux/if_arp.h>
#include <asm/uaccess.h>
#include <asm/unistd.h>
#include <asm/irq.h>
#include <asm/delay.h>
#include <asm/addrspace.h>
#include <asm/io.h>
#include <linux/netdevice.h>
#include <net/ppa_ppe_hal.h>
#include <linux/list.h>
#include <linux/delay.h>


#include "./include/11ac_acc_data_structure_tx_be.h"
#include "./include/dlrx_drv.h"
#include <net/ppa_stack_al.h>
#include "../../../lantiq_ppa/platform/xrx500/directlink/dlrx_fw_data_structure_be.h"
#include "./include/dltx_fw_data_structure_be.h"

#include "../../../lantiq_ppa/platform/xrx500/directlink/dlrx_fw_def.h"
#include "./include/Dltx_fw_def.h"
#include "./include/dlrx_wlan_api.h"
#include "../../../lantiq_ppa/platform/xrx500/directlink/dlrx_dre_api.h"
#include "./include/dlrx_memory_lyt.h"

#include "../../../lantiq_ppa/platform/xrx500/directlink/dlrx_fw_version.h"
#include "./include/directlink_tx_cfg.h"
#include "./include/ltqmips_hal.h"
#include <net/ppa_api_directpath.h>
#include "./include/dltx_fw_comm_buf_data_structure_be.h"
#include <net/ppa_api.h>
#include <net/datapath_api.h>
#include <net/ltq_mpe_api.h>
#include <linux/dma-mapping.h>

#ifdef SUPPORT_MULTICAST_TO_UNICAST
#if defined(CONFIG_LANTIQ_MCAST_HELPER_MODULE) || defined(CONFIG_LANTIQ_MCAST_HELPER)
#include "../../../../net/lantiq_ppa/ppa_api/ppa_api_core.h"
#endif
#endif
#include <xway/switch-api/lantiq_gsw_api.h>

#include <linux/pci.h>
/*#define OLD_MPE_FAST_HOOK 1*/
#ifndef OLD_MPE_FAST_HOOK
#include <net/ltq_mpe_hal.h>
#endif
#define	MAX_DIRECTPATH_NUM	5
#define MAX_DIRECTLINK_NUM 16
struct ppe_directpath_data g_ppe_directpath_data[MAX_DIRECTPATH_NUM];
struct dl_drv_address_map g_dl_drv_address;
struct dl_buf_info g_dl_buf_info;
#define RX_NOT_SKB 1
#define MAX_TX_DESCRIPTOR 1420
/* #define DLTX_UNCACHED 1 */
/*****************************************************
 *	 Macro definition
 *****************************************************/
#define MOD_AUTHOR			"Zhu YiXin, Ho Nghia Duc"
#define MOD_DESCRIPTION		"Accelerate QCA 11AC TX RX Traffic"
#define MOD_LICENCE			"GPL"
#define DLRX_DRV_MAJOR		0
#define DLRX_DRV_MID		2
#define DLRX_DRV_MINOR		9	
#define DLRX_DRV_VERSION	((DLRX_DRV_MAJOR << 24) | (DLRX_DRV_MID << 16) | DLRX_DRV_MINOR)

#define INLINE				inline
#define DIRECTLINK_PROC_DIR	"dl"


#define NUM_ENTITY(x)		(sizeof(x) / sizeof(*(x)))

#define NEW_CHANGE 1

int dl_param_tx_descriptor = MAX_TX_DESCRIPTOR;

module_param(dl_param_tx_descriptor, int, S_IWUSR | S_IWGRP);
MODULE_PARM_DESC(dl_param_tx_descriptor, "Establish number of TX DESCRIPTOR");

/*****************************************************
 *	External functions
 *****************************************************/
#ifdef OLD_MPE_FAST_HOOK
enum MPE_Feature_Type {
	DL_TX_1 = 0,
	DL_TX_2
};
#define F_FEATURE_START     (1 << 25)
#define F_FEATURE_STOP      (1 << 26)

#ifdef NEW_CHANGE
extern int mpe_hal_dl_alloc_resource(uint32_t memSize,
	uint32_t *memAddr,
	uint32_t flags);
#else
extern int mpe_hal_dl_alloc_resouce(uint32_t memSize,
	uint32_t *memAddr,
	uint32_t flags);

#endif
extern int mpe_hal_feature_start(
	enum MPE_Feature_Type mpeFeature,
	uint32_t port_id,
	uint32_t *featureCfgBase,
	uint32_t flags
	);

#endif


/*****************************************************
 *	Global Parameter
 *****************************************************/
uint32_t g_ce5_desc_ring_num = CE5_DEST_DESC_RING_NUM;
uint32_t g_ce5_buf_ptr_ring_num = RX_PKT_BUF_PTR_RING_NUM;
/* uint32_t g_dtlk_memsize = DTLK_TX_RSV_MEM_SIZE + DTLK_RX_RSV_MEM_SIZE; */
u32 g11ACWirelessCardID = PEREGRINE_BOARD;
u32 g11ACWirelessCardID_SUBTYPE = SUBTYPE_NONE_BOARD;
#if 1
struct device *g_mpe_dev;
#endif
uint32_t g_mpe_dltx_tc;
/**************************************/

/* !!! These three base address must be
* initialized before access any FW structure !!! */
unsigned int *ddr_base;
unsigned int *ddr_mpe_base;
unsigned int *ddr_mpe_comp_base;
unsigned int *cfg_ctxt_base;
unsigned int *pcie_base;

extern dre_regfn_set_t g_dre_fnset;
extern uint32_t g_dlrx_max_inv_header_len;
extern uint32_t g_dlrx_cfg_offset_atten;

#ifdef SUPPORT_11AC_MULTICARD
#define PCI_VENDOR_ATHEROS 0x168C
#define PCI_ATH_DEV_PEREGRINE 0x003C
#define PCI_ATH_DEV_BEELINER 0x0040
#define PCI_ATH_DEV_BEELINER_CASCADE 0x0046
#endif

#define MAX_DTLK_NUM	16
#define MAX_RADIO_NUM	1
/* can extend to 8 */
#define MAX_TX_COMP_MAX 4
static DEFINE_SPINLOCK(g_ppe_dtlk_spinlock);
static struct ppe_radio_map	g_ppe_radio[MAX_RADIO_NUM];
#define DTLK_RX_ADDR(offset) (ddr_base + offset)
#define DTLK_RX_CTXT_ADDR(offset) (cfg_ctxt_base + offset)
#define MPE_TX_ADDR(offset) (ddr_mpe_base + offset)
#define MPE_TX_COML_ADDR(offset) (ddr_mpe_comp_base + offset)


void (*set_vap_itf_tbl_fn)(uint32_t, uint32_t) = NULL;
EXPORT_SYMBOL(set_vap_itf_tbl_fn);


/*****************************************************
 *	Internal Variables
 *****************************************************/
static struct proc_dir_entry *g_dtlk_proc_dir;

struct dtlk_dgb_info {
	char *cmd;
	char *description;
	uint32_t flag;
};

PPA_SUBIF gQuickRefSubIfFromVAPID[MAX_DTLK_NUM];


static struct dtlk_dgb_info dtlk_dbg_enable_mask_str[] = {
	{"err",		"error print",		DBG_ERR},
	{"init",	"init print",		DBG_INIT},
	{"rx",		"rx path print",	DBG_RX},
	{"tx",		"tx path print",	DBG_TX},
	{"cpu",		"cpu path print",	DBG_CPU},
	{"proc",	"proc print",		DBG_PROC},
	{"print",	"message print",	DBG_PRINT},
	/* the last one */
	{"all",		"enable all debug",	-1}
};
uint32_t g_dtlk_dbg_enable = DTLK_DBG_ENA;


/*****************************************************
 *	Internal functions
 *****************************************************/
static GSW_API_HANDLE gswr;
static GSW_register_t old_pce_ctrl;
static GSW_register_t old_pce_IGPTRM;

static int gsw_enable_ingress_port_remove(int portnum)
{
	GSW_register_t regr;
	int result = 0;
	gswr = gsw_api_kopen("/dev/switch_api/1");
	if (gswr == 0) {
		dtlk_debug(DBG_ERR, "%s: Open SWAPI device FAILED !!\n", __func__);
		return -EIO;
	}
	dtlk_debug(DBG_INIT, "%s: enable ingress at port [%d]\n", __func__, portnum);
	/*PCE Control register*/
	old_pce_ctrl.nRegAddr = (0x483 + (0xA * portnum));
	if (gsw_api_kioctl(gswr, GSW_REGISTER_GET, (unsigned int)&old_pce_ctrl) < 0) {
		dtlk_debug(DBG_ERR, "ERROR");
		result = -EIO;
		goto error;
	}
	dtlk_debug(DBG_INIT, "regr.nRegAddr:0x%08x, regr.nData=0x%08x\n", old_pce_ctrl.nRegAddr, old_pce_ctrl.nData);
	regr.nData = old_pce_ctrl.nData;
	regr.nRegAddr = (0x483 + (0xA * portnum));
	regr.nData |= (0x4000);
	if (gsw_api_kioctl(gswr, GSW_REGISTER_SET, (unsigned int)&regr) < 0) {
		dtlk_debug(DBG_ERR, "ERROR");
		result = -EIO;
		goto error;
	}
	/*IGPTRM register*/
	regr.nRegAddr = (0x544 + (0x10 * portnum));
	if (gsw_api_kioctl(gswr, GSW_REGISTER_GET, (unsigned int)&old_pce_IGPTRM) < 0) {
		dtlk_debug(DBG_ERR, "ERROR");
		result = -EIO;
		goto error;
	}
	dtlk_debug(DBG_INIT, "regr.nRegAddr:0x%08x, regr.nData=0x%08x\n", old_pce_IGPTRM.nRegAddr, old_pce_IGPTRM.nData);
	regr.nRegAddr = (0x544 + (0x10 * portnum));
	regr.nData = old_pce_IGPTRM.nData;
	regr.nData |= (0xffff);
	if (gsw_api_kioctl(gswr, GSW_REGISTER_SET, (unsigned int)&regr) < 0) {
		dtlk_debug(DBG_ERR, "ERROR");
		result = -EIO;
		goto error;
	}
error:
	gsw_api_kclose(gswr);
	return result;
}
#if 0
static int gsw_restore_ingress_port_remove(int portnum)
{
	GSW_register_t regr;
	int result = 0;
	gswr = gsw_api_kopen("/dev/switch_api/1");
	if (gswr == 0) {
		dtlk_debug(DBG_ERR, "%s: Open SWAPI device FAILED !!\n", __func__);
		return -EIO;
	}
	/*PCE Control register*/
	regr.nRegAddr = (0x483 + (0xA * portnum));
	if (gsw_api_kioctl(gswr, GSW_REGISTER_GET, (unsigned int)&regr) < 0) {
		dtlk_debug(DBG_ERR, "ERROR");
		result = -EIO;
		goto error;
	}
	dtlk_debug(DBG_INIT, "regr.nRegAddr:0x%08x, regr.nData=0x%08x\n", regr.nRegAddr, regr.nData);
	regr.nRegAddr = (0x483 + (0xA * portnum));
	regr.nData = old_pce_ctrl.nData;
	if (gsw_api_kioctl(gswr, GSW_REGISTER_SET, (unsigned int)&regr) < 0) {
		dtlk_debug(DBG_ERR, "ERROR");
		result = -EIO;
		goto error;
	}
	/*IGPTRM register*/
	regr.nRegAddr = (0x544 + (0x10 * portnum));
	if (gsw_api_kioctl(gswr, GSW_REGISTER_GET, (unsigned int)&regr) < 0) {
		dtlk_debug(DBG_ERR, "ERROR");
		result = -EIO;
		goto error;
	}
	dtlk_debug(DBG_INIT, "regr.nRegAddr:0x%08x, regr.nData=0x%08x\n", regr.nRegAddr, regr.nData);
	regr.nRegAddr = (0x544 + (0x10 * portnum));
	regr.nData = old_pce_IGPTRM.nData;
	if (gsw_api_kioctl(gswr, GSW_REGISTER_SET, (unsigned int)&regr) < 0) {
		dtlk_debug(DBG_INIT, "ERROR");
		result = -EIO;
		goto error;
	}

error:
	gsw_api_kclose(gswr);
	return result;
}
#endif

/* Function: dlrx_init_buf_size
* Purpose: Initilize DLRX FW Buffer
* Argument:
*		dlrx_bufnum : dlrx_bufsize_t
* Return: none
*/
static void dlrx_init_buf_size(
	dlrx_bufsize_t *dlrx_bufnum
	)
{
	/* Initializing the Length of data structure */
	dlrx_bufnum->wlan_desc_num				= WLAN_DESC_NUM;
	dlrx_bufnum->proto_desc_num				= PROTO_DESC_NUM;
	dlrx_bufnum->cpu_ce5_desc_ring_num		= CPU_CE5_DESC_RING_NUM;
	dlrx_bufnum->rx_pkt_buf_rel_msg_num		= RX_PKT_BUF_REL_MSG_NUM;
	dlrx_bufnum->ce5_dest_desc_ring_num		= g_ce5_desc_ring_num;
	dlrx_bufnum->ce5_dest_msg_buf_num		= g_ce5_desc_ring_num;
	dlrx_bufnum->rx_pkt_buf_ptr_ring_num	= g_ce5_buf_ptr_ring_num;
	dlrx_bufnum->rx_reorder_main_num		= RX_REORDER_MAIN_NUM;
	dlrx_bufnum->rx_reorder_desc_link_num	= RX_REORDER_DESC_LINK_NUM;

	return;
}

/** Function: ppa_dl_detect_11ac_card
* Purpose: detect wireless card. Peregrine or Beeliner.
* Argument: none
* Return: card type.
*/
u32 ppa_dl_detect_11ac_card(void)
{
	dtlk_debug(DBG_INIT, "%s:g11ACWirelessCardID[%d] subtype[%d]\n",
		__func__,
		g11ACWirelessCardID,
		g11ACWirelessCardID_SUBTYPE);
	return g11ACWirelessCardID;
}

/** Function: dtlk_dev_from_vapid
* Purpose: Get net device of given VAP.
* Argument:
*/
struct net_device *dtlk_dev_from_vapid(
	uint32_t vap_id
	)
{
	struct net_device *dev = NULL;
	int i;

	if (vap_id >= MAX_DTLK_NUM) {
		dtlk_debug(DBG_ERR, "VAP id [%d] is larger than the MAX DTLK NUM [%d]\n",
				vap_id,
				MAX_DTLK_NUM
				);
	}

	spin_lock_bh(&g_ppe_dtlk_spinlock);
	for (i = 0; i < MAX_DTLK_NUM; i++) {
		if (g_ppe_radio[0].g_ppe_dtlk_data[i].flags & PPE_DTLK_VALID) {
			/* == AVM/CMH 20160427 LTQ/QCA STA multicast fix == */
#if 0
			if (g_ppe_radio[0].g_ppe_dtlk_data[i].vap_id == vap_id)
				dev = (struct net_device *)g_ppe_radio[0].g_ppe_dtlk_data[vap_id].dev;
#else
			if (g_ppe_radio[0].g_ppe_dtlk_data[i].vap_id == vap_id) {
				dev = (struct net_device *)g_ppe_radio[0].g_ppe_dtlk_data[i].dev;
				break;
			}
#endif
	   }
	}
	spin_unlock_bh(&g_ppe_dtlk_spinlock);

	return dev;
}
EXPORT_SYMBOL(dtlk_dev_from_vapid);

/** Function: wlan_detect_ath_card
* Purpose: detect 11 QCA wireless card.
* Argument: None
* Return : None. The type of card will be saved to global variable named
* g11ACWirelessCardID.
*/
static void wlan_detect_ath_card(void)
{
	int index = 0;
	int ath_dev_id = PCI_ATH_DEV_PEREGRINE;
	int found = 0;
	struct pci_dev *dev_tel = NULL;

	for (index = 0; index < 3; index++) {
		dev_tel = pci_get_subsys(PCI_VENDOR_ATHEROS,
				ath_dev_id,
				PCI_ANY_ID,
				PCI_ANY_ID,
				NULL);
		if (dev_tel)
			found = 1;
		if (found)
			break;
		else {
			switch (ath_dev_id) {
			case PCI_ATH_DEV_PEREGRINE:
				ath_dev_id = PCI_ATH_DEV_BEELINER;
			break;
			case PCI_ATH_DEV_BEELINER:
				ath_dev_id = PCI_ATH_DEV_BEELINER_CASCADE;
				break;
			default:
				break;
			}
		}
	}
	if (!found)	{
		dtlk_debug(DBG_ERR, "Can not found any Atheros card\n");
		return ;
	}
	g11ACWirelessCardID_SUBTYPE = SUBTYPE_NONE_BOARD;
	if (ath_dev_id == PCI_ATH_DEV_PEREGRINE) {
		dtlk_debug(DBG_INIT, "Found PEREGRINE_BOARD card\n");
		g11ACWirelessCardID = PEREGRINE_BOARD;
	} else if (ath_dev_id == PCI_ATH_DEV_BEELINER) {
		dtlk_debug(DBG_INIT, "Found BEELINER_BOARD card\n");
		g11ACWirelessCardID = BEELINER_BOARD;
		/* changed in firmware, using 4 instead of 1 */
	} else if (ath_dev_id == PCI_ATH_DEV_BEELINER_CASCADE) {
		dtlk_debug(DBG_INIT, "Found CASCADE_BOARD card\n");
		g11ACWirelessCardID = BEELINER_BOARD;
		g11ACWirelessCardID_SUBTYPE = SUBTYPE_BEELINER_CASCADE_BOARD;
		/* changed in firmware, using 4 instead of 1 */
	}
}


extern unsigned int skb_list_get_skb(
	unsigned int rxpb_ptr
	);

/** Function: dlrx_data_structure_free
* Purpose: Free all resources which allocated by DTLK
* Argument: None
* Return: None
*/
void dlrx_data_structure_free(void)
{
	dlrx_rxpb_ptr_ring_t *dlrx_rxpb_ring_ptr, *dlrx_rxpb_ring_ptr_org;
	unsigned int index, j;
	unsigned int currentV;
	uint32_t numRingBuf;
	dlrx_bufsize_t dlrx_bufnum;

	dlrx_rxpb_ring_ptr =
		(dlrx_rxpb_ptr_ring_t *)DLRX_DDR_RX_PKT_BUF_RING_BASE;
	dlrx_rxpb_ring_ptr_org =
		(dlrx_rxpb_ptr_ring_t *)DLRX_DDR_RX_PKT_BUF_RING_BASE;
	/* Clear DLRX FW Buffer */
	dlrx_init_buf_size(&dlrx_bufnum);

	/* looking for duplicate
	* initial 1024 -1
	*/
	numRingBuf = dlrx_bufnum.rx_pkt_buf_ptr_ring_num;
	for (index = 0; index < (numRingBuf - 1); index++) {
		currentV = (dlrx_rxpb_ring_ptr_org + index)->rxpb_ptr;
		j = index + 1;
		for (; j < (dlrx_bufnum.rx_pkt_buf_ptr_ring_num - 1); j++) {
			if (currentV != 0 &&
				currentV == (dlrx_rxpb_ring_ptr_org+j)->rxpb_ptr) {
				/* dtlk_debug(DBG_RX, "Duplicate: %x index [%d]\n", currentV, j); */
				(dlrx_rxpb_ring_ptr_org+j)->rxpb_ptr = 0;
			}
		}
	}
}


/* * Function: dtlk_mem_base_get
 * Purpose: Return the address of DTLK TX and DTLK RX.
 *	  These value should get from MPE HAL
 * Argument:
 *	  dltx_base : uint32_t, contains DTLK TX base.
 *	  dlrx_base : uint32_t, contains DTLK RX base.
 * Return : None
 */
void dtlk_mem_base_get(
	uint32_t *dltx_base,
	uint32_t *dlrx_base
	)
{
	if (dltx_base) {
		/* *dltx_base = NULL; */
		*dltx_base = g_dl_buf_info.tx_cfg_ctxt_buf_base;
		dtlk_debug(DBG_INIT, "DTLK TX base :0x%x\n", *dltx_base);
	}
	if (dlrx_base) {
		/* *dlrx_base = NULL; */
		*dlrx_base = g_dl_buf_info.rx_msgbuf_base;
		dtlk_debug(DBG_INIT, "DTLK RX base :0x%x\n", *dlrx_base);
	}
	return ;
}
EXPORT_SYMBOL(dtlk_mem_base_get);

void dtlk_mem_comm_base_get(
	uint32_t *dltx_comm_base
	)
{
	if (dltx_comm_base)
			*dltx_comm_base = g_dl_buf_info.comm_buf_base;

	return;
}
EXPORT_SYMBOL(dtlk_mem_comm_base_get);
void dtlk_get_bank_base(
	uint32_t *dltx_bank_base
	)
{
	if (dltx_bank_base) {
		dma_addr_t phy_addr;
		uint32_t fragment = (uint32_t)MPE_TX_ADDR(DLTX_FRAG_DATA_OFFSET);
		phy_addr = dma_map_single(
						g_mpe_dev,
						(void *)fragment,
						4096 * 16 * 4,
						DMA_FROM_DEVICE
						);
		if (unlikely(dma_mapping_error(g_mpe_dev, phy_addr))) {
			dtlk_debug(DBG_ERR, "DMA error");
		}
		/*
					dma_unmap_single(
						g_mpe_dev,
						phy_addr,
						4,
						DMA_FROM_DEVICE
						);
						*/
		dtlk_debug(DBG_INIT, "%s: vir[0x%x] return[0x%x]\n", __func__, fragment, phy_addr);
		*dltx_bank_base = phy_addr;
	}
	return;
}
EXPORT_SYMBOL(dtlk_get_bank_base);

/* * Function: dtlk_mem_base_get
 * Purpose: Return the address of DTLK TX and DTLK RX.
 *	  These value should get from MPE HAL
 * Argument:
 *	  dtlk_ctxt : uint32_t, contains DTLK FW
 * Return : None
 */
void dtlk_mem_sram_base_get(
	uint32_t *dtlk_sram
	)
{
	/* *dtlk_sram = NULL; */
	*dtlk_sram = g_dl_buf_info.rx_cfg_ctxt_buf_base;
}

/* BEELINERTODO */
#define  SIZE_BEELINER_MSDU_ID		1424
#define  SIZE_BEELINER_FRAG_DESC	64
static inline uint32_t __bswap32(
	u_int32_t _x
	)
{
	return (u_int32_t)(
		  (((const u_int8_t *)(&_x))[0]) |
		  (((const u_int8_t *)(&_x))[1] << 8) |
		  (((const u_int8_t *)(&_x))[2] << 16) |
		  (((const u_int8_t *)(&_x))[3] << 24))	;
}

/** Function: ppa_dl_qca_h2t_ring_init
* Purpose: This function is called QCA driver to initialize H2T Ring buffer
* Argument:
* Return:
*/
#define __CE4SRC_BEELINER_HI			0x4
#define __CE4SRC_MISC_IE_BEELINER_LOW	0xb034

#define __CE4SRC_TARGET_WR_PTR_BEELINER_LOW		0xb03c
#define __CE4SRC_TARGET_RD_PTR_BEELINER_LOW		0xb044


uint32_t ppa_dl_qca_h2t_ring_init(
	uint32_t h2tRingSize,
	uint32_t entrySize,
	uint32_t src_ring_base,
	uint32_t pcie_base,
	uint32_t flags)
{
	int i;
	char *bufptr;



	uint32_t BADR_TXPB;
	uint32_t BADR_CE4DES;
	uint16_t NUM_CE4DES;
	uint32_t BADR_PCIEMEM;
	uint32_t BADR_HTT_TXDES;
	dltx_cfg_ctxt_ce4buf_t *dltx_cfg_txt;
	dltx_cfg_ctxt_circular_list_t *dltx_cfg_ctxt_circular;
	dltx_circular_list_t *dltx_circular;
	dltx_cfg_ctxt_qca_vap_id_map_t *dltx_cfg_ctxt_qca_vap_id_map;
	dltx_cfg_ctxt_buffer_pointer_table_t *dltx_cfg_ctxt_buffer_pointer_table;
	dltx_cfg_ctxt_ce4_write_index_track_t *dltx_cfg_ctxt_ce4_write_index_track;
	dltx_cfg_ctxt_cpu_ce4des_t *dltx_cfg_ctxt_cpu_ce4des;
	dltx_cfg_ctxt_comm_buff_t *dltx_cfg_ctxt_comm_buff;
	dltx_cfg_ctxt_frag_data_t *dltx_cfg_ctxt_frag_data;
	dltx_cfg_ctxt_tx_msg_t *dltx_cfg_ctxt_tx_msg;
	unsigned char offload_tx_desc_t[] = {
	0X00, 0X42, 0X00, 0X01, /* DW0 */
	0X00, 0X00, 0X00, 0X00, /* DW1 */
	0X07, 0XC0, 0X40, 0X01, /* DW2 */
	0X00, 0X01, 0X05, 0XEA, /* DW3 */
	0X00, 0X00, 0X00, 0X00, /* DW4 byte swap */
	0XFF, 0XFF, 0X00, 0X00 /* DW5 byte swap */
	};

	unsigned char offload_tx_src_ring_desc_t[] = {
	0X00, 0X00, 0X00, 0X00, /* DW0 */
	0X00, 0X04, 0X04, 0X04, /* DW1 */
	/* metadata 14b =1, ByteSwapEN 1b=0, Gather 1b=0,
	SourceBufferLen 16b=68 */
	};

	if ((h2tRingSize == 0x0) ||
		(entrySize == 0x0) ||
		(src_ring_base == 0x0) ||
		(pcie_base == 0x0))
		return PPA_FAILURE;

	BADR_CE4DES = src_ring_base;

	/* NUM_CE4DES = src_ring_size; */
	NUM_CE4DES = h2tRingSize;

	dtlk_mem_base_get(&BADR_TXPB, NULL);
	BADR_PCIEMEM = 0xf0000000 | pcie_base;
	/* TODO: GRX500 get TX from HAL layer, need to align with  MPE FW TX */
	/* initialize CE4 List handling */
	dltx_cfg_txt = (dltx_cfg_ctxt_ce4buf_t *)MPE_TX_ADDR(DLTX_CFG_CTXT_CE4BUF_OFFSET);
	dltx_cfg_txt->cfg_badr_ce4buf = BADR_CE4DES;
	dltx_cfg_txt->cfg_ce4des_low = LOW_MARK;
	dltx_cfg_txt->cfg_ce4_des_full = HIGH_MARK;
	dltx_cfg_txt->cfg_ce4_read_index_addr = BADR_PCIEMEM + (__CE4SRC_BEELINER_HI << 16) + __CE4SRC_TARGET_RD_PTR_BEELINER_LOW;
	dltx_cfg_txt->cfg_ce4_write_index_addr = BADR_PCIEMEM + (__CE4SRC_BEELINER_HI << 16) + __CE4SRC_TARGET_WR_PTR_BEELINER_LOW;
	dltx_cfg_txt->cfg_num_ce4buf = NUM_CE4DES;
	dltx_cfg_txt->load_ce4_read_index_req = 0x0;
	dltx_cfg_txt->local_ce4_read_index = 0x0;
	dltx_cfg_txt->local_ce4_write_index = 0x0;
	dltx_cfg_txt->_dw_res0[0] = BADR_PCIEMEM;

	/* initialize circular list */
	dltx_cfg_ctxt_circular =
	#ifdef DLTX_UNCACHED
		(dltx_cfg_ctxt_circular_list_t *)KSEG1ADDR(MPE_TX_ADDR(DLTX_CFG_CTXT_CIRCULAR_LIST_OFFSET));
		#else
		(dltx_cfg_ctxt_circular_list_t *)MPE_TX_ADDR(DLTX_CFG_CTXT_CIRCULAR_LIST_OFFSET);
		#endif
	dtlk_debug(DBG_INIT, "%s: dltx_cfg_ctxt_circular[%p]\n", __func__,
		(void *)dltx_cfg_ctxt_circular);
	#ifdef DLTX_UNCACHED
	dltx_cfg_ctxt_circular->circular_list_badr = (unsigned int)KSEG1ADDR(MPE_TX_ADDR(DLTX_CIRCULAR_LIST_OFFSET));
	#else
	dltx_cfg_ctxt_circular->circular_list_badr = (unsigned int)(MPE_TX_ADDR(DLTX_CIRCULAR_LIST_OFFSET));
	#endif
	dtlk_debug(DBG_INIT, "%s: dltx_cfg_ctxt_circular->circular_list_badr[0x%x]\n",
		__func__, dltx_cfg_ctxt_circular->circular_list_badr);
	#if 0
	dltx_cfg_ctxt_circular->circular_list_num = NUM_CE4DES;
	#else
	/*12-JUN-2015*/
	/*dltx_cfg_ctxt_circular->circular_list_num = 0x400;*/
	dltx_cfg_ctxt_circular->circular_list_num = dl_param_tx_descriptor;
	#endif
	dltx_cfg_ctxt_circular->circular_list_read_index = 0;
	dltx_cfg_ctxt_circular->circular_list_write_index = 0;
	dltx_cfg_ctxt_circular->consumed_pkt_ids = 0;
	dltx_circular =
			(dltx_circular_list_t *)dltx_cfg_ctxt_circular->circular_list_badr;
	dtlk_debug(DBG_INIT, "%s: dltx_circular[%p] %d\n", __func__,
		(void *)dltx_circular, dltx_cfg_ctxt_circular->circular_list_num);
	for (i = 0; i < dltx_cfg_ctxt_circular->circular_list_num; i++) {
		dltx_circular->packet_id = i + 1;
		dltx_circular++;
	}

	/* Configure context QCA VAP ID */
	dltx_cfg_ctxt_qca_vap_id_map =
		(dltx_cfg_ctxt_qca_vap_id_map_t *)MPE_TX_ADDR(DLTX_CFG_CTXT_QCA_VAP_ID_MAP_OFFSET);
	dltx_cfg_ctxt_qca_vap_id_map->qca_vap_id_map_badr =
		(unsigned int)MPE_TX_ADDR(DLTX_QCA_VAP_ID_MAP_OFFSET);
	dltx_cfg_ctxt_qca_vap_id_map->qca_vap_id_map_num = MAX_DTLK_NUM;

	/* Buffer pointer */
	dltx_cfg_ctxt_buffer_pointer_table =
		(dltx_cfg_ctxt_buffer_pointer_table_t *)MPE_TX_ADDR(DLTX_CFG_CTXT_BUFFER_POINTER_TABLE_OFFSET);
	dltx_cfg_ctxt_buffer_pointer_table->buffer_pointer_badr =
		(unsigned int)MPE_TX_ADDR(DLTX_BUFFER_POINTER_TABLE_OFFSET);
	dltx_cfg_ctxt_buffer_pointer_table->buffer_pointer_num = NUM_CE4DES;

	/* Write CE4 Write index track */
	dltx_cfg_ctxt_ce4_write_index_track =
		(dltx_cfg_ctxt_ce4_write_index_track_t *)MPE_TX_ADDR(DLTX_CFG_CTXT_CE4_WRITE_INDEX_TRACK_OFFSET);
	dltx_cfg_ctxt_ce4_write_index_track->write_index_track_badr =
		(unsigned int)MPE_TX_ADDR(DLTX_CE4_WRITE_INDEX_TRACK_OFFSET);
	dltx_cfg_ctxt_ce4_write_index_track->write_index_track_num = NUM_CE4DES;

	/* Comm CPU CE4 */
	dltx_cfg_ctxt_cpu_ce4des =
		(dltx_cfg_ctxt_cpu_ce4des_t *)MPE_TX_COML_ADDR(DLTX_CFG_CTXT_CPU_CE4DES_OFFSET);
	dltx_cfg_ctxt_cpu_ce4des->cfg_badr_cpu_ce4 =
		(unsigned int)MPE_TX_COML_ADDR(DLTX_CPU_CE4DES_FORMAT_OFFSET);
	dltx_cfg_ctxt_cpu_ce4des->cfg_num_cpu_ce4 = 64;
	dltx_cfg_ctxt_cpu_ce4des->cpu_ce4_read_index = 0;
	dltx_cfg_ctxt_cpu_ce4des->cpu_ce4_write_index = 0;
	dltx_cfg_ctxt_cpu_ce4des->cpu_ce4_ppe_read_index = 0;
	dltx_cfg_ctxt_cpu_ce4des->cpu_ce4_msg_done = 0;

	/* Comm TX Completion */
	dltx_cfg_ctxt_comm_buff =
		(dltx_cfg_ctxt_comm_buff_t *)MPE_TX_COML_ADDR(DLTX_CFG_CTXT_COMM_BUFF_OFFSET);
	dltx_cfg_ctxt_comm_buff->cfg_badr_tx_cmpl_flag =
		(unsigned int)MPE_TX_COML_ADDR(DLTX_TX_CMPL_FLAG_OFFSET);
	dltx_cfg_ctxt_comm_buff->cfg_badr_tx_cmpl_buf =
		(unsigned int)MPE_TX_COML_ADDR(DLTX_TX_CMPL_MSG_OFFSET);
	dltx_cfg_ctxt_comm_buff->cfg_num_tx_cmpl_buf = MAX_TX_COMP_MAX;

	/* Reset statictisc MIB */
	memset((void *)MPE_TX_ADDR(DLTX_DATA_MIB_OFFSET),
		0x0,
		sizeof(dltx_data_mib_t));
	/* Reset VAP MIB */
	memset((void *)MPE_TX_ADDR(DLTX_VAP_DATA_MIB_OFFSET(0)),
		0x0,
		sizeof(dltx_vap_data_mib_t) * MAX_DTLK_NUM);

	dltx_cfg_ctxt_frag_data =
		(dltx_cfg_ctxt_frag_data_t *)MPE_TX_ADDR(DLTX_CFG_CTXT_FRAG_DATA_OFFSET);
	dltx_cfg_ctxt_frag_data->frag_data_badr =
		(unsigned int)MPE_TX_ADDR(DLTX_FRAG_DATA_OFFSET);
	dltx_cfg_ctxt_frag_data->frag_data_num = 4096;

	dltx_cfg_ctxt_tx_msg =
		(dltx_cfg_ctxt_tx_msg_t *)MPE_TX_ADDR(DLTX_CFG_CTXT_TX_MSG_OFFSET);
	dltx_cfg_ctxt_tx_msg->cfg_size_tx_header = SIZE_TXHEADER;

	/*GRX350: the header for transmit should be intialized by MPE FW TX itself
	* becase packet will be get from CBM, driver does not pre-allocate buffer
	*/

	/* HTT_TXDES defined in PPE spec
	 * 5 HTT_TXDES template. each for one VAP (equal vdev_id ??)
	 * HTT_TXDES has 8 DWORD. DWORD0 is 0x0, DWORD1 DWORD2 DWORD3 DWORD4
	 * SIZE_HTT_TXDES info in DWORD1 DWORD2 DWORD3 DWORD4
	 */
	BADR_HTT_TXDES = (uint32_t)MPE_TX_ADDR(HTT_TX_DES_OFFSET(0));

	for (i = 0; i < NUM_HTT_TXDES; i++) {
		/* 32 or 16 <by default> */
		bufptr = (char *)(BADR_HTT_TXDES + (i * LEN_HTT_TXDES));
		memset(bufptr, 0x0, 4);
		memcpy(bufptr + 4, offload_tx_desc_t, SIZE_HTT_TXDES);
	}
	/* 512 or 4096 */
	for (i = 0; i < NUM_CE4DES; i++) {
		bufptr = (char *)(BADR_CE4DES + (i * SIZE_CE4DES));
		memcpy(bufptr, offload_tx_src_ring_desc_t, SIZE_CE4DES);
	}

	return (uint32_t)BADR_CE4DES;
}
EXPORT_SYMBOL(ppa_dl_qca_h2t_ring_init);


uint32_t ppa_dl_qca_cpu_h2t_ring_init(uint32_t h2tCpuMsgRingSize,
	uint32_t entrySize,
	uint32_t flags
	)
{
	dma_addr_t phy_addr;
	dltx_cpu_ce4des_format_t *dltx_cpu_ce4des_format;
	dltx_cfg_ctxt_cpu_ce4des_t *dltx_cfg_ctxt_cpu_ce4des;

	dtlk_debug(DBG_INIT, "%s: ringsize[%d] entrysize[%d]\n", __func__, h2tCpuMsgRingSize, entrySize);
	if ((h2tCpuMsgRingSize == 0x0) || (entrySize == 0x0))
		return PPA_FAILURE;
	if (h2tCpuMsgRingSize > 128)
		return PPA_FAILURE;
	dltx_cpu_ce4des_format =
		(dltx_cpu_ce4des_format_t *)MPE_TX_COML_ADDR(DLTX_CPU_CE4DES_FORMAT_OFFSET);

	dltx_cfg_ctxt_cpu_ce4des =
		(dltx_cfg_ctxt_cpu_ce4des_t *)MPE_TX_COML_ADDR(DLTX_CFG_CTXT_CPU_CE4DES_OFFSET);
	dltx_cfg_ctxt_cpu_ce4des->cfg_num_cpu_ce4 = h2tCpuMsgRingSize;
	phy_addr = dma_map_single(
				g_mpe_dev,
				(void *)dltx_cpu_ce4des_format,
				128 * 2 * 4,
				DMA_FROM_DEVICE
				);
	if (unlikely(dma_mapping_error(g_mpe_dev, phy_addr))) {
		dtlk_debug(DBG_ERR, "%s: DMA error", __func__);
	}
	/*dma_unmap_single(
				g_mpe_dev,
				phy_addr,
				4,
				DMA_FROM_DEVICE
				);*/
	dtlk_debug(DBG_INIT, "%s: return 0x%x %p h2tCpuMsgRingSize[%d]n",
		__func__,
		phy_addr,
		dltx_cpu_ce4des_format,
		h2tCpuMsgRingSize);
	return phy_addr;
}
EXPORT_SYMBOL(ppa_dl_qca_cpu_h2t_ring_init);

/** Function: ppa_dl_qca_cpu_h2t_ring_get_write_idx
* Purpose: Get write index of MPE FW TX
* Argument:
*	 flags - unit32_t, not use
* Return: None
*/
int32_t ppa_dl_qca_cpu_h2t_ring_get_write_idx(
	uint32_t flags
	)
{
	dltx_cfg_ctxt_cpu_ce4des_t *dltx_cfg_ctxt_cpu_ce4des
			= (dltx_cfg_ctxt_cpu_ce4des_t *)MPE_TX_COML_ADDR(DLTX_CFG_CTXT_CPU_CE4DES_OFFSET);
	return dltx_cfg_ctxt_cpu_ce4des->cpu_ce4_write_index;
}
EXPORT_SYMBOL(ppa_dl_qca_cpu_h2t_ring_get_write_idx);
typedef dma_addr_t CE_addr_t;

struct CE_src_desc {
	CE_addr_t src_ptr;
	u_int32_t	meta_data:12,
				target_int_disable:1,
				host_int_disable:1,
				byte_swap:1,
				gather:1,
				nbytes:16;
};

/** Function: ppa_dl_qca_cpu_h2t_ring_write_msg
* Purpose: Write message from CPU to MPE FW TX buffer
* Argument:
* Return: Current MPE FW TX write index.
*/
int32_t ppa_dl_qca_cpu_h2t_ring_write_msg(
	uint32_t writeIndex,
	uint32_t *msgPtr,
	uint32_t msgLen,
	uint32_t next_writeIndex,
	uint32_t flags)
{
	dltx_cpu_ce4des_format_t *bufptr;
	struct CE_src_desc *my_test;
	dltx_cpu_ce4des_format_t *dltx_cpu_ce4des_format;
	dltx_cfg_ctxt_cpu_ce4des_t *dltx_cfg_ctxt_cpu_ce4des =
		(dltx_cfg_ctxt_cpu_ce4des_t *)MPE_TX_COML_ADDR(DLTX_CFG_CTXT_CPU_CE4DES_OFFSET);
	dltx_cpu_ce4des_format =
		(dltx_cpu_ce4des_format_t *)MPE_TX_COML_ADDR(DLTX_CPU_CE4DES_FORMAT_OFFSET);
	if ((msgPtr == NULL) || (msgLen == 0x0))
		return PPA_FAILURE;
	my_test = (struct CE_src_desc *)msgPtr;
	bufptr = dltx_cpu_ce4des_format + writeIndex;
	dtlk_debug(DBG_CPU, "writeIndex[%d] bufprt[0x%p] msgLen[%d]\n", writeIndex, bufptr, msgLen);
	/* descriptor is 2 DWs */
	memcpy((char *)bufptr, (char *)msgPtr, msgLen);
	dltx_cfg_ctxt_cpu_ce4des->cpu_ce4_write_index = next_writeIndex;
	ppa_dl_dre_dma_writeback((unsigned int)bufptr, msgLen);
	/*dtlk_debug(DBG_INIT, "           src_ptr[0x%x] nbytes[0x%x]\n",
		my_test->src_ptr,
		my_test->nbytes);*/
	ppa_dl_dre_dma_writeback(KSEG0 | (0x0fffffff & my_test->src_ptr), my_test->nbytes);
	return writeIndex;
}
EXPORT_SYMBOL(ppa_dl_qca_cpu_h2t_ring_write_msg);

#define DRIVER_FREE_TX_COMPLETION 1

struct qca_offload_tx_release_t {
	uint8_t reserved1;
	uint8_t num_msdus;
	uint16_t reserved0;
	uint16_t msdu_ids[129*2];
};
#ifdef DRIVER_FREE_TX_COMPLETION

extern int cbm_buffer_free(uint32_t pid, uint32_t v_buf, uint32_t flag);
/*extern int cbm_buffer_free_hw(uint32_t pid, uint32_t v_buf, uint32_t flag);*/

void ppa_dl_dre_dma_invalidate1(
	unsigned int startAddr,
	unsigned int size
	);

#endif

#define DL_FW_COMP_TRIES 10
#define DL_FW_COMP_SLEEP 10000000
#define DL_DBG_INVALIDATE_PACKETID 1
void dl_sleep(unsigned int sleepCount)
{
	unsigned int i = 0;
	for (i = 0; i < sleepCount; i++)
		;
}

#if DTLK_DEBUG_TX_COMPLETION
static int dtlk_dbg_tx_completion(int packetid, unsigned int pointer)
{
	dltx_packet_id_trace_circular_list_t *dltx_packet_id_trace_circular_list =
		(dltx_packet_id_trace_circular_list_t *)MPE_TX_ADDR(DLTX_PACKET_ID_TRACE_CIRCULAR_LIST_OFFSET);
	dltx_packet_id_trace_circular_list += (packetid - 1);
	dltx_packet_id_trace_circular_list->tx_completion_count++;
	dltx_packet_id_trace_circular_list->tx_completion_ptr[dltx_packet_id_trace_circular_list->tx_completion_count_idx] = pointer;
	/* pr_err("%s: trace %d %p %p\n", __func__, packetid, pointer, dltx_packet_id_trace_circular_list);
	*/
	if (dltx_packet_id_trace_circular_list->tx_completion_count != dltx_packet_id_trace_circular_list->tx_count
		/* || (dltx_packet_id_trace_circular_list->tx_completion_ptr )!= dltx_packet_id_trace_circular_list->tx_ptr
		*/
		) {
		dtlk_debug(DBG_ERR, "====================ERROR:id[%d][%p]===============\n", packetid, (void *)pointer);
		dtlk_debug(DBG_ERR, "\t\t\t:debug pointer[%p][[%d][%d][%p]]===============\n",
			dltx_packet_id_trace_circular_list,
			dltx_packet_id_trace_circular_list->tx_count,
			dltx_packet_id_trace_circular_list->tx_completion_count,
			(void *)dltx_packet_id_trace_circular_list->tx_ptr);
		/* *(MPE_TX_ADDR(DLTX_PACKET_ID_TRACE_CIRCULAR_LIST_OFFSET) - 1) = 0xdeafbeef; stop TX */
		return 1;/* print, do not exit */
	} else {
		dltx_packet_id_trace_circular_list->tx_completion_count_idx++;
		if (dltx_packet_id_trace_circular_list->tx_completion_count_idx == DTLK_DEBUG_TX_COMPLETION_NUM - 4)
			dltx_packet_id_trace_circular_list->tx_completion_count_idx = 0;
	}
	return 0;
}
#endif

/** Function: ppa_dl_dre_txpkt_buf_release
* Purpose: DLRX FW release buffer to PPE FW after receive
*	 the TX complete message.
* Not use anymore, TX will do it.
*/
int32_t ppa_dl_dre_txpkt_buf_release (
	uint32_t num_msdus,
	uint32_t *msg,
	uint32_t flags
	)
{
	int i;
	struct qca_offload_tx_release_t *offload_tx;
	dltx_tx_cmpl_msg_t *dltx_tx_cmpl_msg;
	dltx_tx_cmpl_flag_t *dltx_tx_cmpl_flag;
#ifndef DRIVER_FREE_TX_COMPLETION
	int j;
	int tries = DL_FW_COMP_TRIES;
#endif
	/* sanity check */
	if (msg == NULL || !num_msdus) {
		dtlk_debug(DBG_ERR, "------Invalid argument for releasing [0x%p] [%d]------\n", msg, num_msdus);
		return PPA_FAILURE;
	}
	if (num_msdus > 64) {
		dtlk_debug(DBG_ERR, "------Number of msdus is too big [0x%p] msdus[%d]------\n", msg, num_msdus);
		num_msdus = 64;
	}
	/* find free slot */
	offload_tx = (struct qca_offload_tx_release_t *)msg;
	/* sanity check */
	if (offload_tx == NULL)
		return PPA_FAILURE;
#ifndef DRIVER_FREE_TX_COMPLETION
	/*rmb();*/
	while (tries--) {
		dltx_tx_cmpl_flag = (dltx_tx_cmpl_flag_t *)(MPE_TX_COML_ADDR(DLTX_TX_CMPL_FLAG_OFFSET));
		dltx_tx_cmpl_msg = (dltx_tx_cmpl_msg_t *)(MPE_TX_COML_ADDR(DLTX_TX_CMPL_MSG_OFFSET));
		for (i = 0; i < MAX_TX_COMP_MAX; i++) {
			if (!dltx_tx_cmpl_flag->cmpl_status) {
				/* copy the contain to the buffer */
				dltx_tx_cmpl_msg = dltx_tx_cmpl_msg + i;
				dltx_tx_cmpl_msg->num_pb_ptr_to_release = num_msdus;
				memcpy(dltx_tx_cmpl_msg->free_txpb_ptr,
					offload_tx->msdu_ids,
					512 /*128*4*/
					);
				/*
				if (num_msdus > 1){
					dtlk_debug(DBG_TX, "%s: free at %d with num_msdus[%d]\n", __func__, i, num_msdus);
					for(j = 0; j < (num_msdus + 1) / 2; j++){
						dtlk_debug(DBG_TX, "%s: offload_tx[%d]",
						__func__,
						(offload_tx->msdu_ids[j] << 16) + offload_tx->msdu_ids[j+1]);
						dtlk_debug(DBG_TX, "    packet_id[%08x]",
						dltx_tx_cmpl_msg->free_txpb_ptr[j]);
					}
				}
				*/
				/* occupy this */
				dltx_tx_cmpl_flag->cmpl_status = 1;
				wmb();
				break;
			}
			dltx_tx_cmpl_flag++;
		}
		ppa_dl_qca_ipi_interrupt();
		if (i == MAX_TX_COMP_MAX) {
			dtlk_debug(DBG_ERR, "------Too busy: cannot release------\n");
			dl_sleep(DL_FW_COMP_SLEEP);
			continue;
		} else {
			break;
		}
	}
	if (!tries) {
		dtlk_debug(DBG_ERR, "**************Error, cannot free***************\n");
	}
#else
	dltx_tx_cmpl_flag = (dltx_tx_cmpl_flag_t *)(MPE_TX_COML_ADDR(DLTX_TX_CMPL_FLAG_OFFSET));
	dltx_tx_cmpl_msg = (dltx_tx_cmpl_msg_t *)(MPE_TX_COML_ADDR(DLTX_TX_CMPL_MSG_OFFSET));
	if (num_msdus) {
		int current_write_index;
		#ifdef DL_DBG_INVALIDATE_PACKETID
		int old_write_index;
		#endif
		int numberOfFree = 0;
		int previous_release = 0;
		dltx_cfg_ctxt_circular_list_t *dltx_cfg_ctxt_circular_list
		#ifdef DTLK_FIX_CACHE_COHENRENT
			= (dltx_cfg_ctxt_circular_list_t *)MPE_TX_ADDR(DLTX_CFG_CTXT_CIRCULAR_LIST_LINUX_OFFSET);
		#else /* DTLK_FIX_CACHE_COHENRENT */
			= (dltx_cfg_ctxt_circular_list_t *)MPE_TX_ADDR(DLTX_CFG_CTXT_CIRCULAR_LIST_OFFSET);
		#endif /* DTLK_FIX_CACHE_COHENRENT */
		dltx_circular_list_t *dltx_circular_list
			= (dltx_circular_list_t *)MPE_TX_ADDR(DLTX_CIRCULAR_LIST_OFFSET);
		struct qca_offload_tx_release_t *qca_tx_completion;
		dltx_buffer_pointer_table_t *tx_pointer_table =
			(dltx_buffer_pointer_table_t *)MPE_TX_ADDR(DLTX_BUFFER_POINTER_TABLE_OFFSET);
		#ifdef DLTX_UNCACHED
		dltx_cfg_ctxt_circular_list =
			(dltx_cfg_ctxt_circular_list_t *)KSEG1ADDR(dltx_cfg_ctxt_circular_list);
		#endif/* DLTX_UNCACHED	*/
		current_write_index = dltx_cfg_ctxt_circular_list->circular_list_write_index;
		#ifdef DL_DBG_INVALIDATE_PACKETID
		old_write_index = current_write_index;
		#endif
		previous_release = dltx_cfg_ctxt_circular_list->_dw_res0[0];
		qca_tx_completion = (struct qca_offload_tx_release_t *)msg;
		/*
		if (num_msdus > 1) {
			dtlk_debug(DBG_TX, "Number of msdus: %d\n", num_msdus);
		}
		*/
		for (i = 0; i < num_msdus; i++) {
			dltx_buffer_pointer_table_t *tx_pointer_table_temp = NULL;
			dltx_circular_list_t *dltx_circular_list_temp = NULL;
			int tx_id = 0;
			if ((i % 2) == 0)
				tx_id = qca_tx_completion->msdu_ids[i+1];
			else
				tx_id = qca_tx_completion->msdu_ids[i-1];
			/*get buffer pointer*/
			tx_pointer_table_temp = tx_pointer_table;
			tx_pointer_table_temp += (tx_id - 1);
			tx_pointer_table_temp =
	#ifdef DLTX_UNCACHED
				(dltx_buffer_pointer_table_t *)KSEG1ADDR(tx_pointer_table_temp);
				#else
				(dltx_buffer_pointer_table_t *)(tx_pointer_table_temp);
				#endif
			/*
			if (num_msdus > 1) {
				dtlk_debug(DBG_TX, "    F: packet_id[%d] ptr[0x%08x] value[0x%08x]: write_index[%d]\n", tx_id, tx_pointer_table_temp, tx_pointer_table_temp->pointer_address, current_write_index);
			}
			*/
			/*dltx_cfg_ctxt_circular_list = KSEG1ADDR(dltx_cfg_ctxt_circular_list);*/
			/*dtlk_debug(DBG_TX, "%s: free tx id[%d] at 0x%08x pointer[0x%08x] read[%d]\n",
			__func__,
			tx_id,
			tx_pointer_table,
			tx_pointer_table->pointer_address,
			dltx_cfg_ctxt_circular_list->circular_list_read_index);*/
			/*ppa_dl_dre_dma_invalidate1(tx_pointer_table,32);*/
#if DTLK_DEBUG_TX_COMPLETION
			if (dtlk_dbg_tx_completion(tx_id, tx_pointer_table_temp->pointer_address)) {
				/* break; */
				dtlk_debug(DBG_ERR, "\tdon't break\n");
			}
#endif
			if (tx_pointer_table_temp->pointer_address) {
				/*free buffer first*/
				#if 1
				if (cbm_buffer_free(smp_processor_id(), tx_pointer_table_temp->pointer_address, 0) == -1) {
					dtlk_debug(DBG_ERR, "CMB Pointer: [0x%08x] at id[%d] failed!Num[%d]\n", tx_pointer_table_temp->pointer_address, tx_id, num_msdus);
				}
				#else
				if (cbm_buffer_free_hw(smp_processor_id(), tx_pointer_table_temp->pointer_address, 0) == -1) {
					dtlk_debug(DBG_ERR, "CMB Pointer: [0x%08x] at id[%d] failed!Num[%d]\n", tx_pointer_table_temp->pointer_address, tx_id, num_msdus);
				}
				#endif
				/*update free packet index to circular list*/
				dltx_circular_list_temp = dltx_circular_list;
				dltx_circular_list_temp += current_write_index;
				dltx_circular_list_temp =
				#ifdef DLTX_UNCACHED
					(dltx_circular_list_t *)KSEG1ADDR(dltx_circular_list_temp);
					#else
					(dltx_circular_list_t *)(dltx_circular_list_temp);
					#endif
				dltx_circular_list_temp->packet_id = tx_id;
				#ifdef DL_DBG_INVALIDATE_PACKETID
				dltx_circular_list_temp =
					(dltx_circular_list_t *)CACHE_ADDR(dltx_circular_list_temp);
				dltx_circular_list_temp->packet_id = tx_id;
				#endif
				/*update write index*/
				current_write_index++;
				if (current_write_index >= dl_param_tx_descriptor) /* dltx_cfg_ctxt_circular_list->circular_list_num) */
					current_write_index = 0;
				/*update consume index*/
				#if 0
				dltx_cfg_ctxt_circular_list = KSEG1ADDR(dltx_cfg_ctxt_circular_list);
				if (dltx_cfg_ctxt_circular_list->consumed_pkt_ids)
					dltx_cfg_ctxt_circular_list->consumed_pkt_ids--;
				else
					dtlk_debug(DBG_ERR, "%s: problem!why free when consume is 0 num_msdus[%d]\n", __func__, num_msdus);
				#else
				numberOfFree++;
				#endif


			} else {
				dtlk_debug(DBG_ERR, "%s: problem! NULL at [%d] num_msdus[%d]\n", __func__, tx_id, num_msdus);
			}
		}
		if (dltx_cfg_ctxt_circular_list->_dw_res0[1] > previous_release)
			dtlk_debug(DBG_ERR, "%s: %x %x %x ====DUC==== MEMORY CORRUPTION\n", __func__,
				previous_release,
				dltx_cfg_ctxt_circular_list->_dw_res0[1],
				numberOfFree);
		dltx_cfg_ctxt_circular_list->_dw_res0[1] = previous_release;
		dltx_cfg_ctxt_circular_list->_dw_res0[2] = numberOfFree;
		dltx_cfg_ctxt_circular_list->_dw_res0[0] = previous_release + numberOfFree;
		dltx_cfg_ctxt_circular_list->circular_list_write_index = current_write_index;
		/*ppa_dl_dre_dma_writeback(MPE_TX_ADDR(DLTX_CFG_CTXT_CIRCULAR_LIST_OFFSET),
			sizeof(dltx_cfg_ctxt_circular_list_t));*/
		/*wmb();*/
	}
#endif
	/*dtlk_debug(DBG_TX, "%s: trigger IPI\n", __func__);*/
	/* trigger IPI interrupt */
	/*ppa_dl_qca_ipi_interrupt();*/
	return PPA_SUCCESS;
}
EXPORT_SYMBOL(ppa_dl_dre_txpkt_buf_release);


/** Function: ppa_dl_qca_get_vap_stats
* Purpose: Get the VAP status
* Argument:
* Return: PPA_FAILURE or PPA_SUCCESS
*/
uint32_t ppa_dl_qca_get_vap_stats(
	uint32_t vapId,
	PPA_WLAN_VAP_Stats_t *vapStats,
	uint32_t flags
	)
{
	/* TODO : should get from DRLX buffer */
	dtlk_debug(DBG_CPU, "%s: vapId[%d] [%p]\n", __func__, vapId, ddr_base);
	if ((vapStats == NULL)
		|| (vapId > MAX_VAPID)
		) {
		dtlk_debug(DBG_ERR, "Invalid vapID[%d]\n", vapId);
		return PPA_FAILURE;
	} else {
		dltx_vap_data_mib_t *vap_mib_tx =
				(dltx_vap_data_mib_t *)MPE_TX_ADDR(DLTX_VAP_DATA_MIB_OFFSET(vapId));

		volatile vap_data_mib_t *vap_mib_rx =
				(vap_data_mib_t *)DLRX_VAP_MIB_BASE(vapId);
		uint64_t rxpdu = (uint64_t)vap_mib_rx->rx_rcv_pdu_low +
			(((uint64_t)vap_mib_rx->rx_rcv_pdu_high) << 32);
		uint64_t rxbytes = (uint64_t)vap_mib_rx->rx_rcv_bytes_low +
			(((uint64_t)vap_mib_rx->rx_rcv_bytes_high) << 32);

		uint64_t txpdu = (uint64_t)vap_mib_tx->txpdu_low +
			(((uint64_t)vap_mib_tx->txpdu_high) << 32);
		uint64_t txbytes = (uint64_t)vap_mib_tx->txbytes_low +
			(((uint64_t)vap_mib_tx->txbytes_high) << 32);
		vapStats->txdrop = vap_mib_tx->txdrop_low;
		dtlk_debug(DBG_CPU, "%s: RX [%llu]\n", __func__, rxbytes);
		/* get tx vap mib */
		vapStats->txpdu = txpdu;
		vapStats->txbytes = txbytes;
		/* get rx vap mib */
		vapStats->rx_rcv_pdu = rxpdu;
		vapStats->rx_rcv_bytes = rxbytes;
		vapStats->rx_discard_pdu = vap_mib_rx->rx_discard_pdu_low;
		vapStats->rx_discard_bytes = vap_mib_rx->rx_discard_bytes_low;
		vapStats->rx_fwd_pdu = vap_mib_rx->rx_fwd_pdu_low;
		vapStats->rx_fwd_bytes = vap_mib_rx->rx_fwd_bytes_low;
		vapStats->rx_inspect_pdu = vap_mib_rx->rx_inspect_pdu_low;
		vapStats->rx_inspect_bytes = vap_mib_rx->rx_inspect_bytes_low;
		vapStats->rx_pn_pdu = vap_mib_rx->rx_pn_pdu_low;
		vapStats->rx_pn_bytes = vap_mib_rx->rx_pn_bytes_low;
		vapStats->rx_drop_pdu = vap_mib_rx->rx_drop_pdu_low;
		vapStats->rx_drop_bytes = vap_mib_rx->rx_drop_bytes_low;

		dtlk_debug(DBG_CPU, "%s: vapId[%d] data returned:\n",
			__func__,
			vapId);
		dtlk_debug(DBG_CPU, "RX[%llu] TX[%llu]\n",
			vapStats->rx_rcv_bytes,
			vapStats->txbytes);


		return PPA_SUCCESS;
	}
	return PPA_FAILURE;
}
EXPORT_SYMBOL(ppa_dl_qca_get_vap_stats);
/** Funtion: dtlk_id_from_subif
* Purpose: Get
*/
int dtlk_get_radio_id_from_subif(
	PPA_SUBIF *subIf
	)
{
	int i;
	/* sanity check */
	if (!subIf)
		return -1;
	for (i = 0; i < MAX_RADIO_NUM; i++) {
		if ((g_ppe_radio[i].flags & PPE_DTLK_VALID)
				&& (g_ppe_radio[i].dl_sub_if.port_id ==
					subIf->port_id))
				return i;
	}
	return -1;
}

unsigned int ppa_dl_dre_get_sram_addr(void)
{
	return (unsigned int)cfg_ctxt_base;
}
EXPORT_SYMBOL(ppa_dl_dre_get_sram_addr);

unsigned int ppa_dl_dre_get_kseg0(void)
{
	return KSEG0;
}
EXPORT_SYMBOL(ppa_dl_dre_get_kseg0);

unsigned int ppa_dl_dre_get_kseg1(void)
{
	return KSEG1;
}
EXPORT_SYMBOL(ppa_dl_dre_get_kseg1);


void ppa_dl_dre_dma_invalidate(
	unsigned int startAddr,
	unsigned int size
	)
{
	#if 1
	dma_addr_t phy_addr;
	/*if (startAddr < KSEG0){
		startAddr = 	KSEG1ADDR(startAddr);
	} */
	/*dtlk_debug(DBG_RX, "%s: invalidate 0x%x size[%d]\n",
		__func__,
		startAddr,
		size
		); */
	if (!startAddr) {
		dtlk_debug(DBG_ERR, "%s: rxpb_ptr is NULL\n", __func__);
		return ;
	}
	if (g_mpe_dev) {
		phy_addr = dma_map_single(g_mpe_dev,
			(void *)startAddr,
			size,
			DMA_FROM_DEVICE
			);
		/*
		dtlk_debug(DBG_RX, "%s:phy 0x%x\n", __func__, phy_addr); */
		if (unlikely(dma_mapping_error(g_mpe_dev, phy_addr))) {
			dtlk_debug(DBG_ERR, "DMA address mapping error: buf: 0x%x, size: %d, dir: %d\n",
			(u32)startAddr, size, DMA_FROM_DEVICE);
			BUG();
		}
		/*
		dma_unmap_single(g_mpe_dev,
			phy_addr,
			size,
			DMA_FROM_DEVICE
			); */
	}
	#else
	dma_cache_inv(startAddr, size);
	#endif
}
EXPORT_SYMBOL(ppa_dl_dre_dma_invalidate);
void ppa_dl_dre_dma_invalidate1(
	unsigned int startAddr,
	unsigned int size
	)
{
	#if 1
	dma_addr_t phy_addr;
	/*
	if (startAddr < KSEG0){
		startAddr = 	KSEG1ADDR(startAddr);
	} */
	/*dtlk_debug(DBG_RX, "%s: invalidate 0x%x size[%d]\n",
		__func__,
		startAddr,
		size
		); */
	if (!startAddr) {
		dtlk_debug(DBG_ERR, "%s: rxpb_ptr is NULL\n", __func__);
		return ;
	}
	if (g_mpe_dev) {
		phy_addr = dma_map_single(g_mpe_dev,
			(void *)startAddr,
			size,
			DMA_FROM_DEVICE
			);
		/*
		dtlk_debug(DBG_RX, "%s:phy 0x%x\n", __func__, phy_addr); */
		if (unlikely(dma_mapping_error(g_mpe_dev, phy_addr))) {
			dtlk_debug(DBG_ERR, "DMA address mapping error: buf: 0x%x, size: %d, dir: %d\n",
			(u32)startAddr, size, DMA_FROM_DEVICE);
			BUG();
		}
		dma_unmap_single(g_mpe_dev,
			phy_addr,
			size,
			DMA_FROM_DEVICE
			);
	}
	#else
	dma_cache_inv(startAddr, size);
	#endif
}

inline void ppa_dl_dre_dma_writeback(
	unsigned int startAddr,
	unsigned int size
	)
{
	#if 1
	dma_addr_t phy_addr;
	if (startAddr < KSEG0) {
		startAddr = KSEG1ADDR(startAddr);
	}
	/*
	dtlk_debug(DBG_RX, "%s: writeback 0x%x size[%d]\n",
		__func__,
		startAddr,
		size
		); */
	if (!startAddr) {
		dtlk_debug(DBG_ERR, "%s: rxpb_ptr is NULL\n", __func__);
		return ;
	}
	if (g_mpe_dev) {
		phy_addr = dma_map_single(g_mpe_dev,
			(void *)startAddr,
			size,
			DMA_TO_DEVICE
			);
		/* dtlk_debug(DBG_RX, "%s:phy 0x%x\n", __func__, phy_addr); */
		if (unlikely(dma_mapping_error(g_mpe_dev, phy_addr))) {
			dtlk_debug(DBG_ERR, "%s: DMA address mapping error: buf: 0x%x, size: %d, dir: %d\n",
			__func__, (u32)startAddr, size, DMA_FROM_DEVICE);
			BUG();
		}
		/*
		dma_unmap_single(g_mpe_dev,
			phy_addr,
			size,
			DMA_TO_DEVICE
			); */
	}
	#else
	dma_cache_wback((u32)startAddr, size);
	#endif
}
EXPORT_SYMBOL(ppa_dl_dre_dma_writeback);


inline void ppa_dl_dre_dma_wback_inv(
	unsigned int startAddr,
	unsigned int size
	)
{
	#if 1
	dma_addr_t phy_addr;
	if (startAddr < KSEG0) {
		startAddr = KSEG1ADDR(startAddr);
	}
	/*
	dtlk_debug(DBG_RX, "%s: writeback 0x%x size[%d]\n",
		__func__,
		startAddr,
		size
		); */
	if (!startAddr) {
		dtlk_debug(DBG_ERR, "%s: rxpb_ptr is NULL\n", __func__);
		return ;
	}
	if (g_mpe_dev) {
		phy_addr = dma_map_single(g_mpe_dev,
			(void *)startAddr,
			size,
			DMA_TO_DEVICE
			);
		/* dtlk_debug(DBG_RX, "%s:phy 0x%x\n", __func__, phy_addr); */
		if (unlikely(dma_mapping_error(g_mpe_dev, phy_addr))) {
			dtlk_debug(DBG_ERR, "%s: DMA address mapping error: buf: 0x%x, size: %d, dir: %d\n",
			__func__, (u32)startAddr, size, DMA_FROM_DEVICE);
			BUG();
		}
		/*
		dma_unmap_single(g_mpe_dev,
			phy_addr,
			size,
			DMA_BIDIRECTIONAL
			); */
	}
	#else
	dma_cache_wback((u32)startAddr, size);
	#endif
}
EXPORT_SYMBOL(ppa_dl_dre_dma_wback_inv);


/** Funtion: dtlk_id_from_subif
* Purpose: Get
*/
int dtlk_get_id_from_subif(
	PPA_SUBIF *subIf
	)
{
	int i;
	int radio;
	/* sanity check */
	if (!subIf)
		return -1;
	radio = dtlk_get_radio_id_from_subif(subIf);
	if (radio < 0 || radio >= MAX_RADIO_NUM) {
		dtlk_debug(DBG_ERR, "%s:invalid id! if_id: %d",
			__func__,
			radio);
		return -1;
	}
	for (i = 0; i < MAX_DTLK_NUM; i++) {
		if ((g_ppe_radio[radio].g_ppe_dtlk_data[i].flags & PPE_DTLK_VALID)
				&& (g_ppe_radio[radio].g_ppe_dtlk_data[i].dl_sub_if.port_id ==
					subIf->port_id)
				&& (g_ppe_radio[radio].g_ppe_dtlk_data[i].dl_sub_if.subif ==
					subIf->subif)) {
				return i;
			}
	}
	return -1;
}

/* == AVM/CMH 20160506 make dtlk_get_subif_from_vap_id visible to dtlk_api.c == */
#if 0
inline int dtlk_get_subif_from_vap_id(
#else
int dtlk_get_subif_from_vap_id(
#endif
	PPA_SUBIF *subIf, int vap_id
	)
{
	int radio;
	/* sanity check */
	if (!subIf)
		return -1;
	radio = dtlk_get_radio_id_from_subif(subIf);
	if (radio < 0 || radio >= MAX_RADIO_NUM) {
		dtlk_debug(DBG_ERR, "invalid radio: %d", radio);
		return -1;
	}
	if (vap_id < 0 || vap_id >= MAX_DTLK_NUM) {
		dtlk_debug(DBG_ERR, "Invalid VAP ID %d\n", vap_id);
		return -1;
	}
	/*
	dtlk_debug(DBG_INIT, "%s: vap_id[%d] port[%d] subif[%d]\n",
		__func__,
		vap_id,
		gQuickRefSubIfFromVAPID[vap_id].port_id,
		gQuickRefSubIfFromVAPID[vap_id].subif
		); */
	subIf->port_id = gQuickRefSubIfFromVAPID[vap_id].port_id;
	subIf->subif = gQuickRefSubIfFromVAPID[vap_id].subif;
	return 0;
}

/** Function: dltk_remove_quickrefsub
* Purpose: remove sub interface entry in quick reference sub interface table
* Argument: sub interface
* Return: none. The entry in
*/
void dltk_remove_quickrefsub(PPA_SUBIF *subIf)
{
	int i = 0;
	for (i = 0; i < MAX_DTLK_NUM; i++) {
		if (gQuickRefSubIfFromVAPID[i].port_id == subIf->port_id &&
			gQuickRefSubIfFromVAPID[i].subif == subIf->subif) {
			gQuickRefSubIfFromVAPID[i].port_id = -1;
			gQuickRefSubIfFromVAPID[i].subif = -1;
		}
	}
}

/** Function: dltk_update_radio
* Purpose: update radio to datapath port(aka EP, aka port_id, aka pmac)
*
*/
void dltk_update_radio_datapath_port(
	unsigned radio_id,
	unsigned vap_id,
	PPA_SUBIF *subif,
	unsigned int flag
	)
{
	uint32_t *dltx_ep_radio_id
		= (uint32_t *)g_dl_drv_address.dl_ep_2radio_addr;
	dltx_qca_vap_id_map_t *dltx_qca_vap_id_map
		= (dltx_qca_vap_id_map_t *)MPE_TX_ADDR(DLTX_QCA_VAP_ID_MAP_OFFSET);
	dltx_ep_radio_id = dltx_ep_radio_id + subif->port_id;

	dtlk_debug(DBG_INIT, "%s: radio_id[%d] vap_id[%d] subif->port_id[%d] subif->subif[%d]\n",
		__func__, radio_id, vap_id, subif->port_id, subif->subif);
	/* update subif to vap_id mapping */
	dltx_qca_vap_id_map = dltx_qca_vap_id_map +
		/*MAX_DTLK_NUM * subif->port_id + subif->subif;*/
		subif->subif;
	/* update port id */
	/*if (flag & PPE_F_DTLK_REGISTER) {*/
	if (flag & PPE_F_DTLK_DP_REGISTER) {
		dtlk_debug(DBG_INIT, "%s: register\n", __func__);
		*(dltx_ep_radio_id + radio_id) = radio_id;
		dltx_qca_vap_id_map->qca_vap_id = vap_id;
	} else {
		dtlk_debug(DBG_INIT, "%s: deregister\n", __func__);
		*(dltx_ep_radio_id + radio_id) = 0;
		dltx_qca_vap_id_map->qca_vap_id = 0;
	}
}

#ifdef SUPPORT_MULTICAST_TO_UNICAST

#if defined(CONFIG_LANTIQ_MCAST_HELPER_MODULE) || defined(CONFIG_LANTIQ_MCAST_HELPER)
typedef void (*Mcast_module_callback_t)(unsigned int grpidx,
	struct net_device *netdev,
	void *mc_stream,
	unsigned int flag
	);

#endif


int dl_m2c_add_peer(
	uint32_t *mac_addr,
	uint16_t peer_id,
	uint16_t vap_id
	)
{
	char *temp, *tempDL;
	struct _dl_peer_mac_mapping_table *dl_m2c_table =
		(struct _dl_peer_mac_mapping_table *)MPE_TX_ADDR(DLTX_CFG_CTXT_M2C_PEER_TABLE);
	/* == AVM/CMH 20160506 fix MAC addr print == */
#if 0
	dtlk_debug(DBG_INIT, "======ADD: %d %d [%02x:%02x:%02x:%02x:%02x:%02x]\n",
		peer_id, vap_id,
		*(mac_addr),
		*(mac_addr + 1),
		*(mac_addr + 2),
		*(mac_addr + 3),
		*(mac_addr + 4),
		*(mac_addr + 5)
		);
#else
	dtlk_debug(DBG_INIT, "======ADD: %d %d [%pM]\n", peer_id, vap_id, mac_addr);
#endif
	if (peer_id >= MAX_PEER_NUM) {
		dtlk_debug(DBG_ERR, "peer id is out of range %d\n", peer_id);
		return DTLK_FAILURE;
	}
	if (mac_addr) {
		dl_m2c_table = (dl_m2c_table + peer_id);
		temp = (char *)(mac_addr);
		tempDL = (char *)dl_m2c_table + 2;
		memcpy((char *)tempDL, temp, 6);
		dl_m2c_table->valid = 1;
		dl_m2c_table->vap_id = vap_id;
		return DTLK_SUCCESS;
	}
	dtlk_debug(DBG_ERR, "Invalid data %d\n", peer_id);
	return DTLK_FAILURE;
}
int dl_m2c_find_peer(
	unsigned char *peer_mac
	);

int dl_m2c_remove_peer(
	uint32_t *mac_addr,
	uint16_t peer_id,
	uint16_t vap_id
	)
{
	struct _dl_peer_mac_mapping_table *dl_m2c_table =
		(struct _dl_peer_mac_mapping_table *)MPE_TX_ADDR(DLTX_CFG_CTXT_M2C_PEER_TABLE);
	int peer_idx = 0;
	/* == AVM/CMH 20160506 fix MAC addr print == */
#if 0
	dtlk_debug(DBG_INIT, "======REMOVE: %d %d [%02x:%02x:%02x:%02x:%02x:%02x]\n",
		peer_id, vap_id,
		*(mac_addr),
		*(mac_addr + 1),
		*(mac_addr + 2),
		*(mac_addr + 3),
		*(mac_addr + 4),
		*(mac_addr + 5)
		);
#else
	dtlk_debug(DBG_INIT, "======REMOVE: %d %d [%pM]\n", peer_id, vap_id, mac_addr);
#endif
#if 0
	if (peer_id >= MAX_PEER_NUM) {
		dtlk_debug(DBG_ERR, "peer id is out of range %d\n", peer_id);
		return DTLK_FAILURE;
	}
	dl_m2c_table = (dl_m2c_table + peer_id);
#else
	peer_idx = dl_m2c_find_peer((unsigned char *)mac_addr);
	if (peer_idx > -1) {
		dl_m2c_table = (dl_m2c_table + peer_idx);
		if (peer_idx != peer_id)
			dtlk_debug(DBG_INIT, "REMOVE peer index[%d]#id[%d]\n",
				peer_idx,
				peer_id);
	} else {
		dtlk_debug(DBG_ERR, "Can not find peer id\n");
		return DTLK_FAILURE;
	}

#endif
	if (dl_m2c_table->valid) {
		dl_m2c_table->valid = 0;
		return DTLK_SUCCESS;
	} else
		dtlk_debug(DBG_ERR, "Peer is not valid %d\n", peer_id);

	return DTLK_FAILURE;
}

int dl_m2c_find_peer(
	unsigned char *peer_mac
	)
{
	/* == AVM/CMH 20160506 fix MAC addr print == */
#if 0
	dtlk_debug(DBG_INIT, "%s %x:%x:%x:%x:%x:%x\n", __func__,
		*peer_mac,
		*(peer_mac + 1),
		*(peer_mac + 2),
		*(peer_mac + 3),
		*(peer_mac + 4),
		*(peer_mac + 5)
		);
#else
	dtlk_debug(DBG_INIT, "%s %pM\n",__func__, peer_mac);
#endif

	if (peer_mac) {
		struct _dl_peer_mac_mapping_table *dl_m2c_table =
		(struct _dl_peer_mac_mapping_table *)MPE_TX_ADDR(DLTX_CFG_CTXT_M2C_PEER_TABLE);
		int i = 0;
		char *temp = NULL;
		for (i = 0; i < MAX_PEER_NUM; i++) {
			if (dl_m2c_table->valid) {
				temp = (char *)dl_m2c_table + 2;
				if (!memcmp(peer_mac, temp, 6))
					return i;
			}
			dl_m2c_table++;
		}
	}
	/* == AVM/CMH 20160506 fix MAC addr print == */
#if 0
	dtlk_debug(DBG_ERR, "Can not find peer for %x:%x:%x:%x:%x:%x\n",
		*peer_mac,
		*(peer_mac + 1),
		*(peer_mac + 2),
		*(peer_mac + 3),
		*(peer_mac + 4),
		*(peer_mac + 5)
		);
#else
	dtlk_debug(DBG_ERR, "Can not find peer for %pM\n", peer_mac);
#endif
	return -1;
}
void dl_dp_mcast_group_modify_bitmap(
	unsigned int grpidx,
	unsigned int peer_id,
	unsigned int bSet
	)
{
	dtlk_debug(DBG_INIT, "%s: Start 1 grpidx[%d] peer_id[%d] bSet[%d]\n", __func__, grpidx, peer_id, bSet);
	if (grpidx < MAX_MCAST_GROUP &&
		peer_id < MAX_PEER_NUM) {
		int dwordOffset = peer_id / 32;
		int bitmapOffset = peer_id % 32;
		struct _dl_mcast_group_table *dl_mcast_group_table =
			(struct _dl_mcast_group_table *)MPE_TX_ADDR(DLTX_CFG_CTXT_M2C_GROUP_TABLE);
		dl_mcast_group_table += grpidx;
		if (bSet) {
			if ((dl_mcast_group_table->bitmap[dwordOffset]
				& (1 << bitmapOffset)) == 0) {
				dl_mcast_group_table->bitmap[dwordOffset] =
					dl_mcast_group_table->bitmap[dwordOffset] | (1 << bitmapOffset);
				dl_mcast_group_table->grpIdx++;
				dl_mcast_group_table->valid = 1;
			} else
				dtlk_debug(DBG_INIT, "%s:peer id[%d] is already in multicast group[%d]\n", __func__, peer_id, grpidx);
		} else {
			dl_mcast_group_table->bitmap[dwordOffset] =
				dl_mcast_group_table->bitmap[dwordOffset] & (~(1 << bitmapOffset));
			if (dl_mcast_group_table->grpIdx > 0)
				dl_mcast_group_table->grpIdx--;
			if (dl_mcast_group_table->grpIdx == 0)
				dl_mcast_group_table->valid = 0;
		}
	} else
		dtlk_debug(DBG_ERR, "Invalid group index %d peer [%d]\n", grpidx, peer_id);
	return;
}
int dl_dp_mcast_join(
	unsigned int grpidx,
	unsigned int peer_id,
	unsigned int bJoin
	)
{
	dtlk_debug(DBG_INIT, "%s: Start grpidx[%d] peer_id[%d] bJoin[%d]\n", __func__, grpidx, peer_id, bJoin);
	if (grpidx < MAX_MCAST_GROUP) {
		struct _dl_mcast_group_table *dl_mcast_group_table =
			(struct _dl_mcast_group_table *)MPE_TX_ADDR(DLTX_CFG_CTXT_M2C_GROUP_TABLE);
		dl_mcast_group_table += grpidx;
		if (bJoin) {
			dl_mcast_group_table->valid = 1;
			dl_dp_mcast_group_modify_bitmap(grpidx, peer_id, 1);
		} else
			dl_dp_mcast_group_modify_bitmap(grpidx, peer_id, 0);
	} else
		dtlk_debug(DBG_ERR, "Invalid group index %d\n", grpidx);
	return -1;
}
void dl_dp_mcast_cb(unsigned int grpidx,
	struct net_device *netdev,
	void *mc,
	unsigned int flag
	)
{
	int peer_id;
	dtlk_debug(DBG_ERR, "%s: Start grpidx[%d] netdev[%s] flag[%d]\n", __func__, grpidx, netdev->name, flag);
	if (mc) {
		mcast_stream_t *mc_stream = (mcast_stream_t *)mc;
		if (mc_stream->rx_dev && mc_stream->rx_dev->name)
			dtlk_debug(DBG_INIT, "\tdev[%s]\n", mc_stream->rx_dev->name);
		dtlk_debug(DBG_INIT, "\tdev[%x][%x][%x][%x][%x][%x]\n", mc_stream->macaddr[0],
			mc_stream->macaddr[1],
			mc_stream->macaddr[2],
			mc_stream->macaddr[3],
			mc_stream->macaddr[4],
			mc_stream->macaddr[5]
			);
		dtlk_debug(DBG_INIT, "\tdev[%x][%x][%x][%x][%x][%x]\n", mc_stream->src_mac[0],
			mc_stream->src_mac[1],
			mc_stream->src_mac[2],
			mc_stream->src_mac[3],
			mc_stream->src_mac[4],
			mc_stream->src_mac[5]
			);
		peer_id = dl_m2c_find_peer(mc_stream->macaddr);
		switch (flag) {
		case 1:
			if (peer_id != -1) {
				dl_dp_mcast_join(grpidx, peer_id, 1);
			}
			break;
		case 2:
			if (peer_id != -1) {
				dl_dp_mcast_join(grpidx, peer_id, 0);
			}
			break;
		default:
			break;
		}
	}
}


int32_t dl_dp_register_mcast_module(
	struct net_device *dev,
	struct module *owner,
	Mcast_module_callback_t cb,
	unsigned int flags
	)
{
    pr_info("[%s:%d] Entry [%s] [%d].\n", __func__, __LINE__, dev->name, flags);
#if defined(CONFIG_LANTIQ_MCAST_HELPER_MODULE) || defined(CONFIG_LANTIQ_MCAST_HELPER)
	mcast_helper_register_module(dev, owner, "DirectLink", (int32_t *)cb, flags);
    pr_info("[%s:%d] Exit, returned 0.\n", __func__, __LINE__);
	return DP_SUCCESS;
#else /* #if defined(CONFIG_LANTIQ_MCAST_HELPER_MODULE) || defined(CONFIG_LANTIQ_MCAST_HELPER) */
    pr_info("[%s:%d] Exit, returned -1.\n", __func__, __LINE__);
	return DP_FAILURE;
#endif /* #else */
}

#endif /* SUPPORT_MULTICAST_TO_UNICAST */

int datapath_dtlk_register(
	PPA_SUBIF *subIf,
	PPA_DTLK_T *dtlk)
{
	int i;
	int j;
	int radio = -1;
	struct ppe_radio_map *ppe_radio;

	/* sanity check */
	if (subIf == NULL || dtlk == NULL) {
		dtlk_debug(DBG_ERR, "%s: invalid subif\n", __func__);
		return PPA_EINVAL;
	}
	if (subIf->subif == -1) {
		dtlk_debug(DBG_ERR, "%s: invalid subif value\n", __func__);
		return PPA_EINVAL;
	}
	dtlk_debug(DBG_INIT, "%s: subIf->port_id[%d] subIf->subif[%d]\n", __func__, subIf->port_id, subIf->subif);
	spin_lock_bh(&g_ppe_dtlk_spinlock);
	/*if (dtlk->flags & PPE_F_DTLK_REGISTER) {*/
	if (dtlk->flags & PPE_F_DTLK_DP_REGISTER) {
		dtlk_debug(DBG_INIT, "%s: Register\n", __func__);
		/* looking for free or right radio */
		for (i = 0; i < MAX_RADIO_NUM; i++) {
			if ((g_ppe_radio[i].flags & PPE_DTLK_VALID)
				&& (g_ppe_radio[i].dl_sub_if.port_id == subIf->port_id)) {
				ppe_radio = &g_ppe_radio[i];
				for (j = 0; j < MAX_DTLK_NUM; j++) {
					if (ppe_radio->g_ppe_dtlk_data[j].dl_sub_if.subif ==
						subIf->subif) {
						dtlk_debug(DBG_ERR, "Register duplicate wifi device: %s",
							dtlk->dev->name);
						goto err_exit;
					}
				}
				radio = i;
				dtlk_debug(DBG_INIT, "%s: Register: find valid radio[%d] with port[%d]\n", __func__, radio,
				g_ppe_radio[i].dl_sub_if.port_id);
			}
			if (radio == -1 &&
				((g_ppe_radio[0].flags &
					PPE_DTLK_VALID) == 0)) {
					dtlk_debug(DBG_INIT, "%s: Register: find free radio[%d]\n", __func__, i);
				radio = i;
			}
		}
		/* no free or don't match with existing datapath port */
		if (radio == -1) {
			dtlk_debug(DBG_ERR, "Register Fail, no more ports\n");
			goto err_exit;
		}
		g_ppe_radio[radio].flags |= PPE_DTLK_VALID;
		g_ppe_radio[radio].dl_sub_if.port_id = subIf->port_id;
		dtlk_debug(DBG_INIT, "%s: Register: use free radio[%d] with port[%d]\n", __func__, radio, g_ppe_radio[radio].dl_sub_if.port_id);
		ppe_radio = &(g_ppe_radio[radio]);
		/* looking for free sub interface */
		for (j = 0; j < MAX_DTLK_NUM; j++) {
			if (ppe_radio->g_ppe_dtlk_data[j].dl_sub_if.subif ==
				-1) {
				dtlk_debug(DBG_INIT, "%s: Register: use free radio[%d] with sub[%d]\n", __func__,
					radio,
					j);
				if (dtlk->dev)
					g_ppe_radio[radio].g_ppe_dtlk_data[j].dev = (struct PPA_NETIF *)dtlk->dev;
				else
					dtlk_debug(DBG_ERR, "%s: DEV is NULL\n", __func__);
				g_ppe_radio[radio].g_ppe_dtlk_data[j].vap_id = dtlk->vap_id;
				ppe_radio->g_ppe_dtlk_data[j].dl_sub_if.port_id = subIf->port_id;
				ppe_radio->g_ppe_dtlk_data[j].dl_sub_if.subif = subIf->subif;
				/* update quick reference table */
				gQuickRefSubIfFromVAPID[dtlk->vap_id].port_id = subIf->port_id;
				gQuickRefSubIfFromVAPID[dtlk->vap_id].subif = subIf->subif;
				g_ppe_radio[radio].g_ppe_dtlk_data[j].flags |= PPE_DTLK_VALID;
				break;
			}
		 }
		/*
		dltk_update_radio_datapath_port(radio,
			dtlk->vap_id,
			&(gQuickRefSubIfFromVAPID[dtlk->vap_id]),
			PPE_F_DTLK_REGISTER);
			*/
		dltk_update_radio_datapath_port(radio,
			dtlk->vap_id,
			&(gQuickRefSubIfFromVAPID[dtlk->vap_id]),
			PPE_F_DTLK_DP_REGISTER);
		if (dtlk->dev)
			dtlk_debug(DBG_INIT, "%s: dtlk->dev->name[%s]\n", __func__, dtlk->dev->name);
		else
			dtlk_debug(DBG_ERR, "%s: wrong dtlk->dev is NULL\n", __func__);
		if (set_vap_itf_tbl_fn)
			set_vap_itf_tbl_fn(dtlk->vap_id, subIf->subif);
		/* switch setting */
		gsw_enable_ingress_port_remove(subIf->port_id);
#if SUPPORT_MULTICAST_TO_UNICAST
		/* register to mcast helper */
		dl_dp_register_mcast_module(dtlk->dev,
			 THIS_MODULE,
			 dl_dp_mcast_cb,
			 MC_F_REGISTER);
#endif
		} else {
		/*looking for right radio with respective datapath port*/
		dtlk_debug(DBG_INIT, "%s: DeRegister\n", __func__);
		for (i = 0; i < MAX_RADIO_NUM; i++) {
			if ((g_ppe_radio[i].flags & PPE_DTLK_VALID)
				&& (g_ppe_radio[i].dl_sub_if.port_id == subIf->port_id)) {
				radio = i;
			}
		}

		if (radio == -1) {
			dtlk_debug(DBG_ERR, "No record, deregistration failed\n");
			goto err_exit;
		}
		ppe_radio = &g_ppe_radio[radio];
		/* deregister VAP device */
		for (j = 0; j < MAX_DTLK_NUM; j++) {
			if (ppe_radio->g_ppe_dtlk_data[j].dl_sub_if.subif ==
				subIf->subif) {
				ppe_radio->g_ppe_dtlk_data[j].dev = NULL;
				ppe_radio->g_ppe_dtlk_data[j].vap_id = -1;
				ppe_radio->g_ppe_dtlk_data[j].flags &= (~PPE_DTLK_VALID);
				ppe_radio->g_ppe_dtlk_data[j].dl_sub_if.port_id = -1;
				ppe_radio->g_ppe_dtlk_data[j].dl_sub_if.subif = -1;
				/* update quick reference table */
				dltk_remove_quickrefsub(subIf);
				break;
			}
		}
		dltk_update_radio_datapath_port(radio,
			dtlk->vap_id,
			&(gQuickRefSubIfFromVAPID[dtlk->vap_id]),
			PPE_F_DTLK_DEREGISTER);
		if (set_vap_itf_tbl_fn)
			set_vap_itf_tbl_fn(dtlk->vap_id, 0xF);
#if defined(CONFIG_LANTIQ_MCAST_HELPER_MODULE) || defined(CONFIG_LANTIQ_MCAST_HELPER)
		/* register to mcast helper */
		dl_dp_register_mcast_module(dtlk->dev,
			 THIS_MODULE,
			 dl_dp_mcast_cb,
			 MC_F_DEREGISTER);
#endif
	}
	spin_unlock_bh(&g_ppe_dtlk_spinlock);

	return PPA_SUCCESS;

err_exit:
	spin_unlock_bh(&g_ppe_dtlk_spinlock);

	return PPA_EINVAL;
}
EXPORT_SYMBOL(datapath_dtlk_register);

/** Function: dltx_data_structure_init
* Purpose: Init DLTX data structure
* Argument: None
* Return: None.
*	List of initialization: TX free buffer management.
*
*/
int dltx_data_structure_init(void)
{
	memset((void *)g_dl_buf_info.tx_cfg_ctxt_buf_base,
		0,
		g_dl_buf_info.tx_cfg_ctxt_buf_size
		);
	/* init comm buffer */
	memset((void *)g_dl_buf_info.comm_buf_base,
		0,
		g_dl_buf_info.comm_buf_size);
	return DTLK_SUCCESS;
}

int dlrx_data_structure_init(void)
{
	unsigned int index;
	struct sk_buff *new_skb;
	unsigned int seqid;
	unsigned int temp_ce5_buf_size;
	unsigned int shift_size = 0;
	dlrx_bufsize_t dlrx_bufnum;
	/**************************************/
	/* Pointer to various data structures with respect to config context
	dlrx_cfg_ctxt_dma_t *dlrx_cfg_ctxt_dma_des_wlan_ptr;*/
	/* dlrx_cfg_ctxt_dma_t *dlrx_cfg_ctxt_dma_des_gswip_ptr; */



	dlrx_cfg_ctxt_cpu_ce5des_t *dlrx_cfg_ctxt_cpu_ce5des_ptr;
	dlrx_ce5des_format_t *dlrx_ce5des_format_ptr;

	dlrx_cfg_ctxt_ce5des_t *dlrx_cfg_ctxt_ce5des_ptr;
	dlrx_cfg_ctxt_ce5buf_t *dlrx_cfg_ctxt_ce5buf_ptr;

	dlrx_cfg_ctxt_rxpb_ptr_ring_t *dlrx_cfg_ctxt_rxpb_ptr_ring_ptr;

	dlrx_cfg_ctxt_ro_mainlist_t *dlrx_cfg_ctxt_ro_mainlist_ptr;
	dlrx_cfg_ctxt_ro_linklist_t *dlrx_cfg_ctxt_ro_linklist_ptr;

	dlrx_cfg_ctxt_rxpb_ptr_rel_msgbuf_t *dlrx_cfg_ctxt_rxpb_ptr_rel_msgbuf_ptr;
	dlrx_cfg_global_t *dlrx_cfg_global_ptr;

	/** This is the reorder main list in DDR memory
	* The starting of this	base address is stored in
	* "dlrx_cfg_ctxt_ro_mainlist_t" structure. */
	dlrx_ro_mainlist_t *dlrx_ro_mainlist_ptr;
	dlrx_ro_linklist_t *dlrx_ro_linklist_ptr;

	dlrx_rxpb_ptr_ring_t *dlrx_rxpb_ring_ptr;
	dlrx_cfg_vap2int_map1_t *dlrx_cfg_vap2int_map1_ptr;
	dlrx_cfg_vap2int_map2_t *dlrx_cfg_vap2int_map2_ptr;
	dlrx_cfg_ctxt_rxpb_t *dlrx_cfg_ctxt_rxpb_ptr;

	uint32_t *msg_buf_base;

	/* The below base addresses will be used by FW */
	dtlk_mem_base_get(NULL, (uint32_t *)&ddr_base);
	if (!ddr_base) {
		dtlk_debug(DBG_ERR, "No memory for DLRX\n");
		return DTLK_FAILURE;
	}
	dtlk_mem_sram_base_get((uint32_t *)&cfg_ctxt_base);
	if (!cfg_ctxt_base) {
		ddr_base = 0;
		dtlk_debug(DBG_ERR, "No memory for DLRX Context base\n");
		return DTLK_FAILURE;
	}
	/* ddr_base = (unsigned int *)KSEG0ADDR(ddr_base); */

	/* Set the memory range to 0 */
	memset((void *)ddr_base, 0, g_dl_buf_info.rx_msgbuf_size);
	memset((void *)cfg_ctxt_base, 0, g_dl_buf_info.rx_cfg_ctxt_buf_size);

#ifdef SUPPORT_11AC_MULTICARD
	if (ppa_dl_detect_11ac_card() ==  PEREGRINE_BOARD) {
		g_dlrx_max_inv_header_len = MAX_INV_PEREGRINE_HEADER_LEN;
		g_dlrx_cfg_offset_atten = QCA_PEREGRINE_11AC_CFG_OFFSET_ATTEN;
	} else {
		if (g11ACWirelessCardID_SUBTYPE == SUBTYPE_NONE_BOARD) {
			g_dlrx_max_inv_header_len = MAX_INV_BEELINER_HEADER_LEN;
			g_dlrx_cfg_offset_atten = QCA_BEELINER_11AC_CFG_OFFSET_ATTEN;
		} else {
			g_dlrx_max_inv_header_len = MAX_INV_CASCADE_HEADER_LEN;
			g_dlrx_cfg_offset_atten = QCA_CASCADE_11AC_CFG_OFFSET_ATTEN;
		}
	}
#else
	g_dlrx_max_inv_header_len = MAX_INV_PEREGRINE_HEADER_LEN;
	g_dlrx_cfg_offset_atten = QCA_PEREGRINE_11AC_CFG_OFFSET_ATTEN;
#endif
	/* Initializing the Length of data structure */
	dlrx_init_buf_size(&dlrx_bufnum);
	dlrx_cfg_ctxt_cpu_ce5des_ptr =
		(dlrx_cfg_ctxt_cpu_ce5des_t *)DLRX_CFG_CTXT_CPU_CE5DES_BASE;
	/* Initializing CPU CE5 Destination Ring Pointers
	* ==> 2 * (1 * 8) DWORDS ( this CPU_CE5DES is in SB )
	*/
	dlrx_cfg_ctxt_cpu_ce5des_ptr->cfg_badr_cpu_ce5 =
		(unsigned int)DLRX_DDR_CPU_CE5_DESC_BASE;
	dlrx_cfg_ctxt_cpu_ce5des_ptr->cfg_num_cpu_ce5 =
		dlrx_bufnum.cpu_ce5_desc_ring_num;
	dlrx_cfg_ctxt_cpu_ce5des_ptr->cpu_ce5_read_index = 0;
	dlrx_cfg_ctxt_cpu_ce5des_ptr->cpu_ce5_write_index = 1;
	dlrx_cfg_ctxt_cpu_ce5des_ptr->cpu_ce5_msg_done = 1;

	dlrx_cfg_ctxt_rxpb_ptr_rel_msgbuf_ptr =
		(dlrx_cfg_ctxt_rxpb_ptr_rel_msgbuf_t *)DLRX_CFG_CTXT_RXPB_PTR_REL_MSGBUF_BASE;
	/* Initializing RX PB Release Message Buffer
	* ==> 2 * (2 * 128) DWORDS */
	dlrx_cfg_ctxt_rxpb_ptr_rel_msgbuf_ptr->cfg_badr_rel_msgbuf =
		(unsigned int)DLRX_DDR_RX_PKT_BUF_REL_MSG_BASE;
	dlrx_cfg_ctxt_rxpb_ptr_rel_msgbuf_ptr->cfg_num_rel_msgbuf =
		dlrx_bufnum.rx_pkt_buf_rel_msg_num;

	dlrx_cfg_ctxt_rxpb_ptr =
		(dlrx_cfg_ctxt_rxpb_t *)DLRX_CFG_CTXT_RXPB_BASE;
	dlrx_cfg_ctxt_rxpb_ptr->cfg_offset_atten = 4;
	dlrx_cfg_ctxt_rxpb_ptr->cfg_size_rxpktdes = g_dlrx_cfg_offset_atten;

	dlrx_cfg_ctxt_rxpb_ptr_ring_ptr =
		(dlrx_cfg_ctxt_rxpb_ptr_ring_t *)DLRX_CFG_CTXT_RXPB_PTR_RING_BASE;

	/* Initializing RX PB Ring Ptr ===> 256 * (1 * 8) DWORDS */
	dlrx_cfg_ctxt_rxpb_ptr_ring_ptr->cfg_badr_rxpb_ptr_ring =
		(unsigned int)DLRX_DDR_RX_PKT_BUF_RING_BASE;
	dlrx_cfg_ctxt_rxpb_ptr_ring_ptr->cfg_num_rxpb_ptr_ring =
		dlrx_bufnum.rx_pkt_buf_ptr_ring_num;
	dlrx_cfg_ctxt_rxpb_ptr_ring_ptr->rxpb_ptr_read_index = 0;
	dlrx_cfg_ctxt_rxpb_ptr_ring_ptr->rxpb_ptr_write_index =
		RX_PKT_BUF_PTR_RING_ALLOC_NUM - 1;
	dlrx_rxpb_ring_ptr =
		(dlrx_rxpb_ptr_ring_t *)DLRX_DDR_RX_PKT_BUF_RING_BASE;
	/* initial 1024 -1 */
	for (index = 0;
		index < RX_PKT_BUF_PTR_RING_ALLOC_NUM - 1;
		index++) {
		/* Initializing RX PB PTR ==> 4096 * (1 * 4) DWORDS */
		new_skb = alloc_skb_rx();
		if (new_skb == NULL)
			return DTLK_FAILURE;
		else {
			dma_addr_t phy_addr;
			#if 0
			/* list_add((struct list_head *)new_skb, &g_rxbp_skblist); */
			dma_cache_inv((u32)new_skb->data,
				new_skb->end - new_skb->data
				);
			dlrx_rxpb_ring_ptr->rxpb_ptr =
				CPHYSADDR((uint32_t)new_skb->data);
			#else
			phy_addr = dma_map_single(
				g_mpe_dev,
				#ifndef RX_NOT_SKB
				(void *)new_skb->data,
				new_skb->end - new_skb->data,
				#else
				(void *) ((unsigned char *)new_skb + 128),
				1920,
				#endif
				DMA_FROM_DEVICE
				);
			if (unlikely(dma_mapping_error(g_mpe_dev, phy_addr))) {
				dtlk_debug(DBG_ERR, "%s: DMA error\n", __func__);
			}
			/*dma_unmap_single(
				g_mpe_dev,
				phy_addr,
				new_skb->end - new_skb->data,
				DMA_FROM_DEVICE
				);
				*/
			#endif
			/*
			dma_cache_inv((u32)new_skb->data,
				new_skb->end - new_skb->data
				);*/
			dlrx_rxpb_ring_ptr->rxpb_ptr = phy_addr;
			/* dtlk_debug(DBG_INIT, "%s: %x\n",__func__,new_skb); */
			dlrx_rxpb_ring_ptr++;
		}
	}
	/* This is message ring initialization */
	dlrx_cfg_ctxt_ce5buf_ptr =
		(dlrx_cfg_ctxt_ce5buf_t *)DLRX_CFG_CTXT_CE5BUF_BASE;
	/* Initializing CE5 Buffers and Buffer format pointers
	* ====> 4096 * 512 Bytes */
	dlrx_cfg_ctxt_ce5buf_ptr->cfg_badr_ce5buf =
		(unsigned int)DLRX_DDR_CE5BUF_BASE;
	dlrx_cfg_ctxt_ce5buf_ptr->cfg_num_ce5buf =
		dlrx_bufnum.ce5_dest_msg_buf_num;
	dlrx_cfg_ctxt_ce5buf_ptr->cfg_size_ce5buf =
		DLRX_CE5_DEST_BUF_SIZE;
	temp_ce5_buf_size = DLRX_CE5_DEST_BUF_SIZE;

	while (temp_ce5_buf_size != 1) {
		shift_size++;
		temp_ce5_buf_size >>= 1;
	}
	dlrx_cfg_ctxt_ce5buf_ptr->cfg_size_shift_ce5buf = shift_size;

	/* initial msg buf type to 0xFF */
	msg_buf_base = (uint32_t *)DLRX_DDR_CE5BUF_BASE;
	for (index = 0;
		index < dlrx_bufnum.ce5_dest_msg_buf_num;
		index++) {
		*(msg_buf_base + index * 512 / 4 + 2) = 0xFF;
	}
	/* Initializing Actual CE5 Descriptors format ====>4096 * 2 DWORDS */
	/* This is ce5 descriptor initialization */
	dlrx_ce5des_format_ptr = (dlrx_ce5des_format_t *)DLRX_DDR_CE5DESC_BASE;
	for (index = 0; index < dlrx_bufnum.ce5_dest_msg_buf_num; index++) {
		dlrx_ce5des_format_ptr->dest_ptr =
			dma_map_single(
			g_mpe_dev,
			(void *)dlrx_cfg_ctxt_ce5buf_ptr->cfg_badr_ce5buf +
			 (index << dlrx_cfg_ctxt_ce5buf_ptr->cfg_size_shift_ce5buf),
			512,
			DMA_FROM_DEVICE
			);
		if (unlikely(dma_mapping_error(g_mpe_dev, dlrx_ce5des_format_ptr->dest_ptr))) {
			dtlk_debug(DBG_ERR, "%s: DMA error", __func__);
		}
		/*dma_unmap_single(
			g_mpe_dev,
			dlrx_ce5des_format_ptr->dest_ptr,
			4,
			DMA_FROM_DEVICE
			);*/
		dlrx_ce5des_format_ptr->meta_data = 0;
		dlrx_ce5des_format_ptr->nbytes	= 0;
		dlrx_ce5des_format_ptr++;
	}
	dlrx_cfg_ctxt_ce5des_ptr =
		(dlrx_cfg_ctxt_ce5des_t *)DLRX_CFG_CTXT_CE5DES_BASE;
	/* Initializing General Configuration of CE5
	* Destination Descriptors ===>  1 * 4 DWORDS
	* (only used by driver) */
	dlrx_cfg_ctxt_ce5des_ptr->cfg_badr_ce5des =
		(unsigned int)(DLRX_DDR_CE5DESC_BASE);
	dlrx_cfg_ctxt_ce5des_ptr->cfg_num_ce5des =
		dlrx_bufnum.ce5_dest_desc_ring_num;
	dtlk_debug(DBG_INIT, "%s: dlrx_cfg_ctxt_ce5des_ptr->cfg_num_ce5des[%d]\n",
		__func__,
		dlrx_cfg_ctxt_ce5des_ptr->cfg_num_ce5des
		);

	dlrx_cfg_ctxt_ro_mainlist_ptr =
		(dlrx_cfg_ctxt_ro_mainlist_t *)DLRX_CFG_CTXT_RO_MAINLIST_BASE;
	/* Initializing RX REORDER Mainlist Ptr
	* ===> 128 * 16 = 2048 domains
	==> Each domain == 64 + 4 DWORDS */
	dlrx_cfg_ctxt_ro_mainlist_ptr->cfg_badr_ro_mainlist =
		(unsigned int)DLRX_DDR_RO_MAINLIST_BASE;
	dlrx_cfg_ctxt_ro_mainlist_ptr->cfg_num_ro_mainlist =
		dlrx_bufnum.rx_reorder_main_num;
	dlrx_cfg_ctxt_ro_mainlist_ptr->ro_mainlist_ptr = NULL_PTR;

	dlrx_ro_mainlist_ptr = (dlrx_ro_mainlist_t *)DLRX_DDR_RO_MAINLIST_BASE;
	for (index = 0; index < dlrx_bufnum.rx_reorder_main_num; index++) {
		dlrx_ro_mainlist_ptr->first_ptr = NULL_PTR;
		for (seqid = 1; seqid < 64; seqid++)
			dlrx_ro_mainlist_ptr->_dw_res0[seqid-1] = NULL_PTR;
		dlrx_ro_mainlist_ptr++;
	}

	dlrx_cfg_ctxt_ro_linklist_ptr =
		(dlrx_cfg_ctxt_ro_linklist_t *) DLRX_CFG_CTXT_RO_LINKLIST_BASE;
	/* Initializing RX REORDER Linklist Ptr
	* ===> 4095 Desc ==> Each Desc == 6 DWORDS */
	dlrx_cfg_ctxt_ro_linklist_ptr->cfg_badr_ro_linklist =
		(unsigned int)DLRX_DDR_RO_LINKLIST_BASE;
	dlrx_cfg_ctxt_ro_linklist_ptr->cfg_num_ro_linklist =
		dlrx_bufnum.rx_reorder_desc_link_num;
	dlrx_cfg_ctxt_ro_linklist_ptr->free_num_ro_linklist =
		dlrx_cfg_ctxt_ro_linklist_ptr->cfg_num_ro_linklist - 1;
	dlrx_cfg_ctxt_ro_linklist_ptr->ro_des_free_head_index = 0;
	dlrx_cfg_ctxt_ro_linklist_ptr->ro_des_free_tail_index =
		dlrx_cfg_ctxt_ro_linklist_ptr->cfg_num_ro_linklist - 1;

	dlrx_ro_linklist_ptr = (dlrx_ro_linklist_t *)DLRX_DDR_RO_LINKLIST_BASE;
	for (index = 0; index < dlrx_bufnum.rx_reorder_desc_link_num;
		index++) {
		dlrx_ro_linklist_ptr->next_ptr = index + 1;
		dlrx_ro_linklist_ptr++;
	}
	dlrx_cfg_global_ptr = (dlrx_cfg_global_t *)DLRX_CFG_GLOBAL_BASE;
	/* Initializing GLOBAL CONFIGURATION STRUCTURE */
	dlrx_cfg_global_ptr->dltx_enable = 0;
	dlrx_cfg_global_ptr->dlrx_enable = 0;
	dlrx_cfg_global_ptr->dlrx_pcie_base = (unsigned int)pcie_base;
	dlrx_cfg_global_ptr->dlrx_ddr_base = (unsigned int)ddr_base;
	dlrx_cfg_global_ptr->dlrx_cfg_ctxt_base = (unsigned int)cfg_ctxt_base;
	dlrx_cfg_global_ptr->dlrx_cfg_ctxt_max_size = DLRX_CFG_CTXT_MAX_SIZE;
	dlrx_cfg_global_ptr->dlrx_cfg_unload = 0;
#ifdef SUPPORT_11AC_MULTICARD
	dlrx_cfg_global_ptr->dlrx_qca_hw = ppa_dl_detect_11ac_card();
	dlrx_cfg_global_ptr->dlrx_qca_hw_sub_type = g11ACWirelessCardID_SUBTYPE;
#else
	dlrx_cfg_global_ptr->dlrx_qca_hw = 0;
#endif
	dlrx_cfg_vap2int_map1_ptr =
		(dlrx_cfg_vap2int_map1_t *) DLRX_CFG_VAP2INT_MAP1_BASE;
	dlrx_cfg_vap2int_map2_ptr =
		(dlrx_cfg_vap2int_map2_t *) DLRX_CFG_VAP2INT_MAP2_BASE;
	dlrx_cfg_vap2int_map1_ptr->vap0 = 0xF;
	dlrx_cfg_vap2int_map1_ptr->vap1 = 0xF;
	dlrx_cfg_vap2int_map1_ptr->vap2 = 0xF;
	dlrx_cfg_vap2int_map1_ptr->vap3 = 0xF;
	dlrx_cfg_vap2int_map1_ptr->vap4 = 0xF;
	dlrx_cfg_vap2int_map1_ptr->vap5 = 0xF;
	dlrx_cfg_vap2int_map1_ptr->vap6 = 0xF;
	dlrx_cfg_vap2int_map1_ptr->vap7 = 0xF;
	dlrx_cfg_vap2int_map2_ptr->vap8 = 0xF;
	dlrx_cfg_vap2int_map2_ptr->vap9 = 0xF;
	dlrx_cfg_vap2int_map2_ptr->vap10 = 0xF;
	dlrx_cfg_vap2int_map2_ptr->vap11 = 0xF;
	dlrx_cfg_vap2int_map2_ptr->vap12 = 0xF;
	dlrx_cfg_vap2int_map2_ptr->vap13 = 0xF;
	dlrx_cfg_vap2int_map2_ptr->vap14 = 0xF;
	dlrx_cfg_vap2int_map2_ptr->vap15 = 0xF;

	return DTLK_SUCCESS;
}


/** Function: get_next_argument.
 * Description: Get the next valid argument from given string, ignore space
 * Argument: pSrc [IN]: source pointer
 * Return: pSrc[OUT]: new pointer points to the starting of valid argument
 *			  len[OUT]: the len of ignorance space.
*/
static char *get_next_argument(
	char *pSrc,
	int *len
	)
{
	char *pTemp = pSrc;
	if (pTemp == NULL) {
		*len = 0;
		return NULL;
	}

	while (pTemp != NULL && *pTemp == ' ') {
		pTemp++;
	}

	return pTemp;

}
/** Function: Compare strings follow by given number of length,
 * ignore case senstive.
 * Description: Compare two strings, ignore the case sensitive
 * Argument: p1 [IN]: source pointer 1
 *				   p2 [IN]: source pointer 2
 *				   n [IN]: length of string to compare.
 * Return:0: identical
 *			 other: not match
*/
static INLINE int strincmp(
	const char *p1,
	const char *p2, int n
	)
{
	int c1 = 0, c2;

	while (n && *p1 && *p2)	{
		c1 = *p1 >= 'A' && *p1 <= 'Z' ? *p1 + 'a' - 'A' : *p1;
		c2 = *p2 >= 'A' && *p2 <= 'Z' ? *p2 + 'a' - 'A' : *p2;
		c1 -= c2;
		if (c1)
			return c1;
		p1++;
		p2++;
		n--;
	}

	return n ? *p1 - *p2 : c1;
}

/**	Proc struct def
 */
/* Functions and structure support proc/dl/peer_to_vap */
static int proc_read_dtlk_peer_to_vap(
	struct seq_file *seq,
	void *v
	)
{
	int i = 0;
	seq_printf(seq, "\n");
	seq_printf(seq, "Peer to VAP and PN Configuration table: \n");
	seq_printf(seq, "PN type: 0-no PN check,1-48 bit PB,2-128 bit PB Even,3-128 bit PB Odd\n");
	seq_printf(seq, "\n");
	seq_printf(seq, "[ ID:Acc:PN:Vap] [ ID:Acc:PN:Vap]\n");
	for (i = 0; i < MAX_PEER_NUM; i += 2) {
		uint32_t peer0 = 0, peer1 = 0;
		peer0 = *(volatile uint32_t *)DLRX_CFG_PEER_TO_VAP_PN_BASE(i);
		peer1 = *(volatile uint32_t *)DLRX_CFG_PEER_TO_VAP_PN_BASE(i + 1);
		seq_printf(seq, "[%3d:%3d:%2d:%3d] [%3d:%3d:%2d:%3d]\n",
			i,
			(peer0 & 0x40),
			(peer0 & 0x30),
			(peer0 & 0xF),
			i + 1,
			(peer1 & 0x40),
			(peer1 & 0x30),
			(peer1 & 0xF)
			);
	}
	seq_printf(seq, "\n");
	return 0;
}
/* Functions and structure support proc/dl/peer_id */
static int proc_read_dtlk_peer_id(
	struct seq_file *seq,
	void *v
	)
{
	int i = 0;
	seq_printf(seq, "\n");
	seq_printf(seq, "Peer ID to peer table address: %p\n",
		DLRX_CFG_PEER_ID_TO_PEER_MAP_BASE(0));
	seq_printf(seq, "\n");
	seq_printf(seq, "Valid Peer ID table:\n");
	seq_printf(seq, "  [ ID:   Peer]\n");
	for (i = 0; i < MAX_PEERID_NUM/4; i++) {
		uint32_t regs = 0;
		uint8_t peerid0 = 0, peerid1 = 0, peerid2 = 0, peerid3 = 0;
		regs = *(uint32_t *)DLRX_CFG_PEER_ID_TO_PEER_MAP_BASE(i);
		peerid0 = ((regs) & 0xff);
		peerid1 = ((regs >>  8) & 0xff);
		peerid2 = ((regs >> 16) & 0xff);
		peerid3 = ((regs >> 24) & 0xff);
		if ((peerid0 >> 7) & 1)
			seq_printf(seq, "  [%3d:%7d]\n", i * 4, (peerid0 & 0x7f));

		if ((peerid1 >> 7) & 1)
			seq_printf(seq, "  [%3d:%7d]\n", i * 4 + 1, (peerid1 & 0x7f));

		if ((peerid2 >> 7) & 1)
			seq_printf(seq, "  [%3d:%7d]\n", i * 4 + 2, (peerid2 & 0x7f));

		if ((peerid3 >> 7) & 1)
			seq_printf(seq, "  [%3d:%7d]\n", i * 4 + 3, (peerid3 & 0x7f));

	}
	seq_printf(seq, "\n");

	return 0;
}
/* Functions and structure support proc/dl/wifi_port */
static int proc_read_dtlk_wifi_port(
	struct seq_file *seq,
	void *v
	)
{
	int i = 0;
	int radio = 0;
	if (g_ppe_radio[0].flags | PPE_DTLK_VALID) {
		seq_printf(seq, "Radio [%d]: data port [%d]\n",
			radio,
			g_ppe_radio[0].dl_sub_if.port_id);
	for (i = 0; i < MAX_VAP_NUM; i++) {
		struct net_device *vapDev = dtlk_dev_from_vapid(i);
		if (vapDev) {
			int directpathID = 0;
			uint32_t vap2int;
			if (i < MAX_VAP_NUM / 2)
				vap2int = *(uint32_t *)DLRX_CFG_VAP2INT_MAP1_BASE;
			else
				vap2int = *(uint32_t *)DLRX_CFG_VAP2INT_MAP2_BASE;

			directpathID = (vap2int >> i*4) & 0xf;
			/*
			if (directpathID == 0xf)
				directpathID = -1;
				*/
			seq_printf(seq,
				"  [%d]: Sub interface: %d, VAP id: %d, wifi dev: %s\n",
				i,
				directpathID,
				i,
				vapDev->name
				);
		} else
			seq_printf(seq, "  [%d]: Invalid\n", i);
	}
	} else {
		seq_printf(seq, "Radio [%d]: Invalid data port\n", radio);
	}


	return 0;
}

static int proc_read_dtlk_peer_to_vap_seq_open(
	struct inode *inode,
	struct file *file
	)
{
	return single_open(file, proc_read_dtlk_peer_to_vap, NULL);
}

static int proc_read_dtlk_peer_id_seq_open(
	struct inode *inode,
	struct file *file
	)
{
	return single_open(file, proc_read_dtlk_peer_id, NULL);
}

static int proc_read_dtlk_wifi_port_seq_open(
	struct inode *inode,
	struct file *file
	)
{
	return single_open(file, proc_read_dtlk_wifi_port, NULL);
}

static struct file_operations g_proc_file_peer_to_vap_seq_fops = {
	.owner		= THIS_MODULE,
	.open		= proc_read_dtlk_peer_to_vap_seq_open,
	.read		= seq_read,
	.llseek 	= seq_lseek,
	.release	= single_release
};

static struct file_operations g_proc_file_peer_id_seq_fops = {
	.owner		= THIS_MODULE,
	.open		= proc_read_dtlk_peer_id_seq_open,
	.read		= seq_read,
	.llseek 	= seq_lseek,
	.release	= single_release
};

static struct file_operations g_proc_file_wifi_port_seq_fops = {
	.owner		= THIS_MODULE,
	.open		= proc_read_dtlk_wifi_port_seq_open,
	.read		= seq_read,
	.llseek 	= seq_lseek,
	.release	= single_release
};

static int proc_read_mem(
	struct seq_file *seq,
	void *v
	)
{
	return 0;
}
static int proc_read_mem_seq_open(
	struct inode *inode,
	struct file *file
	)
{
	return single_open(file, proc_read_mem, NULL);
}
static INLINE int get_token(
	char **p1,
	char **p2,
	int *len,
	int *colon
	) {
	int tlen = 0;
	while (*len && !((**p1 >= 'A' && **p1 <= 'Z') ||
		(**p1 >= 'a' && **p1 <= 'z') ||
		(**p1 >= '0' && **p1 <= '9'))) {
		(*p1)++;
		(*len)--;
	}
	if (!*len)
		return 0;

	if  (*colon) {
		*colon = 0;
		*p2 = *p1;
		while (*len && **p2 > ' ' && **p2 != ',') {
			if (**p2 == ':') {
				*colon = 1;
				break;
			}
			(*p2)++;
			(*len)--;
			tlen++;
		}
		**p2 = 0;
	} else {
		*p2 = *p1;
		while (*len && **p2 > ' ' && **p2 != ',') {
			(*p2)++;
			(*len)--;
			tlen++;
		}
		**p2 = 0;
	}

	return tlen;
}

static INLINE void ignore_space(
	char **p,
	int *len
	)
{
	while (*len && (**p <= ' ' || **p == ':' ||
		**p == '.' || **p == ',')) {
		(*p)++;
		(*len)--;
	}
}
static INLINE int stricmp(
	const char *p1,
	const char *p2
	)
{
	int c1, c2;

	while (*p1 && *p2) {
		c1 = *p1 >= 'A' && *p1 <= 'Z' ? *p1 + 'a' - 'A' : *p1;
		c2 = *p2 >= 'A' && *p2 <= 'Z' ? *p2 + 'a' - 'A' : *p2;
		c1 -= c2;
		if (c1)
			return c1;
		p1++;
		p2++;
	}

	return *p1 - *p2;
}

static INLINE int get_number(
	char **p,
	int *len,
	int is_hex
	)
{
	int ret = 0;
	int n = 0;

	if ((*p)[0] == '0' && (*p)[1] == 'x') {
		is_hex = 1;
		(*p) += 2;
		(*len) -= 2;
	}

	if (is_hex) {
		while (*len && ((**p >= '0' && **p <= '9') ||
			(**p >= 'a' && **p <= 'f') ||
			(**p >= 'A' && **p <= 'F'))) {
			if (**p >= '0' && **p <= '9')
				n = **p - '0';
			else if (**p >= 'a' && **p <= 'f')
			   n = **p - 'a' + 10;
			else if (**p >= 'A' && **p <= 'F')
				n = **p - 'A' + 10;
			ret = (ret << 4) | n;
			(*p)++;
			(*len)--;
		}
	} else {
		while (*len && **p >= '0' && **p <= '9') {
			n = **p - '0';
			ret = ret * 10 + n;
			(*p)++;
			(*len)--;
		}
	}

	return ret;
}

static ssize_t proc_write_mem(
	struct file *file,
	const char __user *buf,
	size_t count,
	loff_t *data
	)
{
	char *p1, *p2;
	int len;
	int colon;
	unsigned long *p = NULL;
	unsigned long dword;
	char local_buf[900];
	int i, n, l;


	len = sizeof(local_buf) < count ? sizeof(local_buf) - 1 : count;
	len = len - copy_from_user(local_buf, buf, len);
	local_buf[len] = 0;

	p1 = local_buf;
	p2 = NULL;
	colon = 1;
	while (get_token(&p1, &p2, &len, &colon)) {
		if (stricmp(p1, "w") == 0 ||
			stricmp(p1, "write") == 0 ||
			stricmp(p1, "r") == 0 ||
			stricmp(p1, "read") == 0)
			break;

		p1 = p2;
		colon = 1;
	}

	if (*p1 == 'w') {
		ignore_space(&p2, &len);
		if (p2[0] == 's' && p2[1] == 'w' &&
			(p2[2] == ' ' || p2[2] == '\t')) {
			/*
			unsigned long temp;
			is_switch = 1;
			p2 += 3;
			len -= 3;
			ignore_space(&p2, &len);
			temp = get_number(&p2, &len, 1);
			p = (unsigned long *)AR10_SWIP_MACRO_REG(temp);
			*/
		} else {
			p = (unsigned long *)get_number(&p2, &len, 1);
			/* p = (unsigned long *)sb_addr_to_fpi_addr_convert( (unsigned long) p); */
		}

		if ((u32)p >= KSEG0)
			while (1) {
				ignore_space(&p2, &len);
				if (!len || !((*p2 >= '0' && *p2 <= '9') ||
					(*p2 >= 'a' && *p2 <= 'f') ||
					(*p2 >= 'A' && *p2 <= 'F')))
					break;

				*p++ = (u32)get_number(&p2, &len, 1);
			}
	} else if (*p1 == 'r')	{
		ignore_space(&p2, &len);
		if (p2[0] == 's' && p2[1] == 'w' &&
			(p2[2] == ' ' || p2[2] == '\t')) {
			/* do nothing */
		} else {
			p = (unsigned long *)get_number(&p2, &len, 1);
			/* p = (unsigned long *)sb_addr_to_fpi_addr_convert( (unsigned long) p); */
		}
		/* printk("%s: p[0x%x] KSEG0[0x%x]\n", __func__, (u32)p, KSEG0); */
		if ((u32)p >= KSEG0) {
			ignore_space(&p2, &len);
			n = (int)get_number(&p2, &len, 0);
			if (n) {
				char str[32] = {0};
				char *pch = str;
				int k;
				char c;

				n += (l = ((int)p >> 2) & 0x03);
				p = (unsigned long *)((u32)p & ~0x0F);
				for (i = 0; i < n; i++) {
					if ((i & 0x03) == 0) {
						printk("%08X:", (u32)p);
						pch = str;
					}
					if (i < l) {
						printk("		 ");
						sprintf(pch, "	  ");
					} else {
						dword = *p;
						printk(" %08X", (u32)dword);
						for (k = 0; k < 4; k++) {
							c = ((char *)&dword)[k];
							pch[k] = c < ' ' ? '.' : c;
						}
					}
					p++;
					pch += 4;
					if ((i & 0x03) == 0x03) {
						pch[0] = 0;
						printk(" ; %s\n", str);
					}
				}
				if ((n & 0x03) != 0x00) {
					for (k = 4 - (n & 0x03); k > 0; k--)
						printk("		 ");
					pch[0] = 0;
					printk(" ; %s\n", str);
				}
			}
		}
	}

	return count;
}

static struct file_operations g_proc_file_mem_seq_fops = {
	.owner	= THIS_MODULE,
	.open	= proc_read_mem_seq_open,
	.read	= seq_read,
	.write	= proc_write_mem,
	.llseek	= seq_lseek,
	.release	= single_release,
};
#define DTLK_SWITCH_DISABLE_PARSE 1
#if DTLK_SWITCH_DISABLE_PARSE
extern int (*datapath_dtlk_switch_parser)(void);
int dtlk_switch_parser(void)
{
	dltx_cfg_global_t *mpe_dl_tx_cfg_global =
					(dltx_cfg_global_t *)MPE_TX_ADDR(DLTX_CFG_GLOBAL_OFFSET);
	return mpe_dl_tx_cfg_global->switch_parser_flag;
}
#endif
static int proc_read_parser(
	struct seq_file *seq,
	void *v
	)
{
	dltx_cfg_global_t *mpe_dl_tx_cfg_global =
				(dltx_cfg_global_t *)MPE_TX_ADDR(DLTX_CFG_GLOBAL_OFFSET);
	printk("%s: switch flag[%d]\n", __func__, mpe_dl_tx_cfg_global->switch_parser_flag);
	seq_printf(seq, "Current switch parser: %s\n",
		mpe_dl_tx_cfg_global->switch_parser_flag == 1 ? "Enable" : "Disable");
	return 0;
}
static int proc_read_parser_seq_open(
	struct inode *inode,
	struct file *file
	)
{
	return single_open(file, proc_read_parser, NULL);
}

static ssize_t proc_write_parser(
	 struct file *file,
	 const char __user *buf,
	 size_t count,
	 loff_t *data
	 )
{
	char *p1, *p2;
	int len;
	int colon;


	char local_buf[900];


	dltx_cfg_global_t *mpe_dl_tx_cfg_global =
			(dltx_cfg_global_t *)MPE_TX_ADDR(DLTX_CFG_GLOBAL_OFFSET);

	len = sizeof(local_buf) < count ? sizeof(local_buf) - 1 : count;
	len = len - copy_from_user(local_buf, buf, len);
	local_buf[len] = 0;

	p1 = local_buf;
	p2 = NULL;
	colon = 1;
	while (get_token(&p1, &p2, &len, &colon)) {
		if (stricmp(p1, "disable") == 0 ||
			stricmp(p1, "enable") == 0)
			break;
		p1 = p2;
		colon = 1;
	}

	if (*p1 == 'd') {
		printk("%s: Disable switch parser\n", __func__);
		mpe_dl_tx_cfg_global->switch_parser_flag = 0;
	} else {
		printk("%s: Enable switch parser\n", __func__);
		mpe_dl_tx_cfg_global->switch_parser_flag = 1;
	}
	return count;
}

static struct file_operations g_proc_file_parser_seq_fops = {
	.owner  = THIS_MODULE,
	.open	 = proc_read_parser_seq_open,
	.read	 = seq_read,
	.write  = proc_write_parser,
	.llseek = seq_lseek,
	.release	 = single_release,
};




 /* Functions and structure support proc/dtlk/reg */
static int proc_read_dtlk_reg(
	struct seq_file *seq,
	void *v
	)
{
	volatile dlrx_cfg_vap2int_map1_t *vap2int_map1 =
		(dlrx_cfg_vap2int_map1_t *)DLRX_CFG_VAP2INT_MAP1_BASE;
	volatile dlrx_cfg_vap2int_map2_t *vap2int_map2 =
		(dlrx_cfg_vap2int_map2_t *)DLRX_CFG_VAP2INT_MAP2_BASE;
	seq_printf(seq, "Firmware registers information\n");
	seq_printf(seq, "			CFG_VAP2INT_MAP1: %p\n", vap2int_map1);
	seq_printf(seq, "	VAP0: %d\n", vap2int_map1->vap0);
	seq_printf(seq, "	VAP1: %d\n", vap2int_map1->vap1);
	seq_printf(seq, "	VAP2: %d\n", vap2int_map1->vap2);
	seq_printf(seq, "	VAP3: %d\n", vap2int_map1->vap3);
	seq_printf(seq, "	VAP4: %d\n", vap2int_map1->vap4);
	seq_printf(seq, "	VAP5: %d\n", vap2int_map1->vap5);
	seq_printf(seq, "	VAP6: %d\n", vap2int_map1->vap6);
	seq_printf(seq, "	VAP7: %d\n", vap2int_map1->vap7);
	seq_printf(seq, "			CFG_VAP2INT_MAP2: %p\n", vap2int_map2);
	seq_printf(seq, "	VAP8: %d\n", vap2int_map2->vap8);
	seq_printf(seq, "	VAP9: %d\n", vap2int_map2->vap9);
	seq_printf(seq, "	VAP10: %d\n", vap2int_map2->vap10);
	seq_printf(seq, "	VAP11: %d\n", vap2int_map2->vap11);
	seq_printf(seq, "	VAP12: %d\n", vap2int_map2->vap12);
	seq_printf(seq, "	VAP13: %d\n", vap2int_map2->vap13);
	seq_printf(seq, "	VAP14: %d\n", vap2int_map2->vap14);
	seq_printf(seq, "	VAP15: %d\n", vap2int_map2->vap15);

	return 0;
}

static int proc_read_dtlk_reg_seq_open(
	struct inode *inode,
	struct file *file
	)
{
	return single_open(file, proc_read_dtlk_reg, NULL);
}



static struct file_operations g_proc_file_reg_seq_fops = {
	.owner		= THIS_MODULE,
	.open		= proc_read_dtlk_reg_seq_open,
	.read		= seq_read,
	.llseek 	= seq_lseek,
	.release	= single_release
};

 /* Functions and structure support proc/dtlk/ver */
static int proc_read_dtlk_ver(
	struct seq_file *seq,
	void *v
	)
{
	 dltx_cfg_global_t *mpe_dl_tx_cfg_global =
				 (dltx_cfg_global_t *)MPE_TX_ADDR(DLTX_CFG_GLOBAL_OFFSET);

	seq_printf(seq, "DirectLink driver information: \n");
	seq_printf(seq, "	 Version: %d.%d.%d\n",
		DLRX_DRV_MAJOR,
		DLRX_DRV_MID,
		DLRX_DRV_MINOR
		);
	seq_printf(seq, "DLRX Firmware information: \n");
	seq_printf(seq, "    Version:%08x\n", DRE_FW_VERSION);
	seq_printf(seq, "    Feature:%08x\n", DRE_FW_FEATURE);
	seq_printf(seq, "DLTX Firmware information: \n");
	seq_printf(seq, "	 Version:%08x\n", mpe_dl_tx_cfg_global->fw_ver_id);
	seq_printf(seq, "	 Feature:%08x\n", mpe_dl_tx_cfg_global->fw_feature);

	return 0;
}

static int proc_read_dtlk_ver_seq_open(
	struct inode *inode,
	struct file *file
	)
{
	return single_open(file, proc_read_dtlk_ver, NULL);
}



static struct file_operations g_proc_file_ver_seq_fops = {
	.owner		= THIS_MODULE,
	.open		= proc_read_dtlk_ver_seq_open,
	.read		= seq_read,
	.llseek 	= seq_lseek,
	.release	= single_release
};
/* Functions and structure support proc/dtlk/mib */
static int proc_read_dtlk_mib(struct seq_file *seq, void *v)
{
	int i = 0;
	uint64_t generalRXDrops = 0;
	uint64_t generalRXErrors = 0;
	uint64_t generalTXDrops = 0;

	for (i = 0; i < MAX_DTLK_NUM; i++) {
		struct net_device *vapDev = dtlk_dev_from_vapid(i);
		if (vapDev) {
			/* TODO: implement function to get mib from vap */
			volatile vap_data_mib_t *vap_mib_rx =
				(vap_data_mib_t *)DLRX_VAP_MIB_BASE(i);
			dltx_vap_data_mib_t *vap_mib_tx =
				(dltx_vap_data_mib_t *)MPE_TX_ADDR(DLTX_VAP_DATA_MIB_OFFSET(i));
			uint64_t rxpdu =
				(uint64_t)vap_mib_rx->rx_rcv_pdu_low +
				(((uint64_t)vap_mib_rx->rx_rcv_pdu_high) << 32);
			uint64_t rxbytes =
				(uint64_t)vap_mib_rx->rx_rcv_bytes_low +
				(((uint64_t)vap_mib_rx->rx_rcv_bytes_high) << 32);
			uint64_t txdrops =
				(uint64_t)vap_mib_tx->txdrop_low +
				(((uint64_t)vap_mib_tx->txdrop_high) << 32);
			uint64_t rxerros =
				((uint64_t)vap_mib_rx->rx_pn_bytes_low +
				(((uint64_t)vap_mib_rx->rx_pn_bytes_high) << 32)) +
				((uint64_t)vap_mib_rx->rx_discard_pdu_low +
				(((uint64_t)vap_mib_rx->rx_discard_pdu_high) << 32));
			uint64_t rxdrops = (uint64_t)vap_mib_rx->rx_drop_pdu_low +
				(((uint64_t)vap_mib_rx->rx_drop_pdu_high) << 32);
			uint64_t txpdu = (uint64_t)vap_mib_tx->txpdu_low +
				(((uint64_t)vap_mib_tx->txpdu_high) << 32);
			uint64_t txbytes = (uint64_t)vap_mib_tx->txbytes_low +
				(((uint64_t)vap_mib_tx->txbytes_high) << 32);
			seq_printf(seq, "VAP-Id = %d\n", i);
			seq_printf(seq, "  VAP-Name = %s\n", vapDev->name);
			seq_printf(seq, "   tx_pkts    = %llu\n", txpdu);
			seq_printf(seq, "   tx_bytes   = %llu\n", txbytes);
			seq_printf(seq, "   tx_drops   = %llu\n", txdrops);
			seq_printf(seq, "   rx_pkts    = %llu\n", rxpdu);
			seq_printf(seq, "   rx_bytes   = %llu\n", rxbytes);
			seq_printf(seq, "   rx_error_pkts = %llu\n", rxerros);
			seq_printf(seq, "   rx_drops_pkts = %llu\n", rxdrops);
			seq_printf(seq, "\n");
			generalTXDrops += txdrops;
			generalRXDrops += rxdrops;
			generalRXErrors += rxerros;
		} else {
			seq_printf(seq, "VAP-Id = %d\n", i);
			seq_printf(seq, "  VAP-Name = Unassigned\n");
		}
	}
	seq_printf(seq, "\n");
	seq_printf(seq, "General RX Drops = %10llu General RX Errors = %10llu\n",
		generalRXDrops,
		generalRXErrors
		);
	seq_printf(seq, "General TX Drops = %10llu\n", generalTXDrops);
	return 0;
}

static int proc_read_dtlk_mib_seq_open(
	struct inode *inode,
	struct file *file
	)
{
	return single_open(file, proc_read_dtlk_mib, NULL);
}

static ssize_t proc_write_dtlk_mib_seq(
	struct file *file,
	const char __user *buf,
	size_t count,
	loff_t *data
	)
{
	int len;
	char str[64];
	char *p;

	int spaceLen = 0;
	int i;
	uint32_t vapMask = 0;
	memset(str, 0, 64);
	len = min(count, (size_t)((unsigned long)sizeof(str) - 1));
	len -= copy_from_user(str, buf, len);
	while (len && str[len - 1] <= ' ')
		len--;
	str[len] = 0;
	for (p = str; *p && *p <= ' '; p++, len--)
		;
	if (!*p)
		return count;
	/* first, looking for VAP from user input  */

	p = get_next_argument(p, &spaceLen);
	len -= spaceLen;
	if (p == NULL) {
		dtlk_debug(
		DBG_PROC,
		"%s: Invalid input\n",
		__func__
		);
		return count;
	}
	/* get vap id */
	if (strincmp(p, "reset", strlen("reset")) == 0) {
		p += strlen("reset");
		len -= strlen("reset");

		while (p && *p) {
			int vapID = 0;
			p = get_next_argument(p, &spaceLen);
			if (!p)
				break;

			len -= spaceLen;
			if (strincmp(p, "all", strlen("all")) == 0) {
				/* mask all */
				vapMask = 0xffffffff;
				break;
			} else {
				if (*p >= '0' && *p <= '9') {
					sscanf(p, "%d", &vapID);
					if (vapID >= MAX_VAP_NUM) {
						dtlk_debug(
							DBG_PROC,
							"%s: invalid VAP id [%d]\n",
							__func__,
							vapID);
						break;/* while */
					}
					vapMask |= 1 << vapID;
					if (vapID > 9) {
						len -= 2;
						p += 2;
					} else {
						len -= 1;
						p += 1;
					}
				} else
					break;
			}
			if (!p)
				break;
			p += 1;
			len -= 1;
		}
	} else if (strincmp(p, "help", 4) == 0 || *p == '?') {
		dtlk_debug(DBG_PROC, "echo reset <0 1 2 ...> > /proc/%s/wifi_mib\n",
			DIRECTLINK_PROC_DIR);
		dtlk_debug(DBG_PROC, "echo reset all > /proc/%s/wifi_mib\n",
			DIRECTLINK_PROC_DIR);
		return count;
	}

	dtlk_debug(
		DBG_PROC,
		"%s: Going to clear: 0x%08x\n",
		__func__,
		vapMask
		);
	/* secondly, clear vap mib counter */
	if (vapMask == 0xffffffff) {
		dre_dl_reset_fn_t dre_dl_reset_fn;
		uint32_t *ptrTxMib = (void *)DTLK_RX_ADDR(__D6_PER_VAP_MIB_BASE);
		/* clear rx mib counter
		* resetMib->allreq = 1;
		* clear tx mib counter
		*/
		memset(ptrTxMib, 0x0, sizeof(mib_table_t)*MAX_VAP_NUM);
		dre_dl_reset_fn = g_dre_fnset.dre_dl_reset_fn;
		if (likely(dre_dl_reset_fn))
			dre_dl_reset_fn(DRE_RESET_MIB, 0xff);
		else
			dtlk_debug(DBG_ERR, "%s: Function DRE_RESET_MIB is not registered!\n", __func__);
	} else {
		dre_dl_reset_fn_t dre_dl_reset_fn;
		dre_dl_reset_fn = g_dre_fnset.dre_dl_reset_fn;
		for (i = 0; i < MAX_VAP_NUM; i++) {
			if ((1 << i) & vapMask) {
				uint32_t *ptrTxMib =
					(void *)DTLK_RX_ADDR(__D6_PER_VAP_MIB_BASE) +
					i*sizeof(mib_table_t);
				memset(ptrTxMib, 0x0, sizeof(mib_table_t));
				if (likely(dre_dl_reset_fn))
					dre_dl_reset_fn(DRE_RESET_MIB, i);
				else
					dtlk_debug(DBG_ERR, "%s:DRE_RESET_MIB is not registered!\n",
						__func__
						);
			}
		}
	}
	return count;

}


static struct file_operations g_proc_file_mib_seq_fops = {
	.owner		= THIS_MODULE,
	.open		= proc_read_dtlk_mib_seq_open,
	.read		= seq_read,
	.write		= proc_write_dtlk_mib_seq,
	.llseek 	= seq_lseek,
	.release	= single_release
};
/** ####################################
 *			  Local Function
 * ####################################
 */

/* Functions and structure support proc/dtlk/dbg*/
static int proc_read_dtlk_dbg(
	struct seq_file *seq,
	void *v
	)
{
	int i;
	/* skip -1 */
	for (i = 0; i < NUM_ENTITY(dtlk_dbg_enable_mask_str) - 1; i++)
		seq_printf(seq, "%-10s(%-40s):		  %-5s\n",
			dtlk_dbg_enable_mask_str[i].cmd,
			dtlk_dbg_enable_mask_str[i].description,
			(g_dtlk_dbg_enable & dtlk_dbg_enable_mask_str[i].flag) ? "enabled" : "disabled"
			);

	return 0;

}

static int proc_read_dtlk_dbg_seq_open(
	struct inode *inode,
	struct file *file
	)
{
	return single_open(file, proc_read_dtlk_dbg, NULL);
}

static ssize_t proc_write_dtlk_dbg_seq(
	struct file *file,
	const char __user *buf,
	size_t count, loff_t *data
	)
{
	int len;
	char str[64];
	char *p;
	int f_enable = 0;
	int f_dtlk = 0;
	int i;
	int spaceLen = 0;
	dltx_cfg_global_t *dltx_cfg_global =
			(dltx_cfg_global_t *)MPE_TX_ADDR(DLTX_CFG_GLOBAL_OFFSET);
	memset(str, 0, 64);
	len = min(count, (size_t)((unsigned long)sizeof(str) - 1));
	len -= copy_from_user(str, buf, len);
	while (len && str[len - 1] <= ' ')
		len--;
	str[len] = 0;
	for (p = str; *p && *p <= ' '; p++, len--)
		;
	if (!*p)
		return count;

	if (strincmp(p, "dltx", 4) == 0) {
		p += 4;
		len -= 4;
		f_dtlk = 1;
	} else if (strincmp(p, "enable", 6) == 0) {
		p += 6;
		len -= 6;
		f_enable = 1;
	} else if (strincmp(p, "disable", 7) == 0) {
		p += 7;
		len -= 7;
		f_enable = -1;
	} else if (strincmp(p, "help", 4) == 0 || *p == '?') {
		 dtlk_debug(DBG_PRINT, "echo <enable/disable/dltx> [");
		 for (i = 0; i < NUM_ENTITY(dtlk_dbg_enable_mask_str); i++)
			dtlk_debug(DBG_PRINT, "%s/", dtlk_dbg_enable_mask_str[i].cmd);
		 dtlk_debug(DBG_PRINT, "] > /proc/%s/dbg\n", DIRECTLINK_PROC_DIR);
		 return count;
	}

	p = get_next_argument(p, &spaceLen);
	if (p == NULL) {
		dtlk_debug(DBG_ERR, "NULL pointer\n");
		return count;
	}
	len -= spaceLen;

	if (f_enable) {
		if ((len <= 0) || (p[0] >= '0' && p[1] <= '9')) {
			if (f_enable > 0)
				g_dtlk_dbg_enable |= (~DTLK_DBG_ENA);
			else
				g_dtlk_dbg_enable &= DTLK_DBG_ENA;
		} else {
			do {
				for (i = 0; i < NUM_ENTITY(dtlk_dbg_enable_mask_str); i++) {
					if (dtlk_dbg_enable_mask_str[i].cmd == NULL)
						break;
					if (strlen(p) < strlen(dtlk_dbg_enable_mask_str[i].cmd))
						continue;
					if (strincmp(p,
						dtlk_dbg_enable_mask_str[i].cmd,
						strlen(dtlk_dbg_enable_mask_str[i].cmd)) == 0) {
						if (f_enable > 0)
							g_dtlk_dbg_enable |= dtlk_dbg_enable_mask_str[i].flag;
						else
							g_dtlk_dbg_enable &= ~dtlk_dbg_enable_mask_str[i].flag;
						/* skip one blank */
						p += strlen(dtlk_dbg_enable_mask_str[i].cmd);
						p = get_next_argument(p, &spaceLen);
						if (p == NULL) {
							break;
						}
						/*skip one blank. len maybe negative
						* now if there is no other parameters
						*/
						len -= strlen(dtlk_dbg_enable_mask_str[i].cmd) + spaceLen;
						break;
					}
				}
			} while (i < NUM_ENTITY(dtlk_dbg_enable_mask_str) && p);
		}
	}
	if (f_dtlk) {
		/* enable debug in DLTX */
		dtlk_debug(DBG_PROC, "DEBUG: enable DLTX debug\n");
		dltx_cfg_global->debug_print_enable = 1;
	} else {
		dtlk_debug(DBG_PROC, "DEBUG: disable DLTX debug\n");
		dltx_cfg_global->debug_print_enable = 0;
	}

	return count;

}


static struct file_operations g_proc_file_dbg_seq_fops = {
	.owner		= THIS_MODULE,
	.open		= proc_read_dtlk_dbg_seq_open,
	.read		= seq_read,
	.write		= proc_write_dtlk_dbg_seq,
	.llseek 	= seq_lseek,
	.release	= single_release
};

#ifdef SUPPORT_MULTICAST_TO_UNICAST

static int proc_read_m2c(
	struct seq_file *seq,
	void *v
	)
{
	struct _dl_mcast_group_table *dl_m2c_group_tbl =
				(struct _dl_mcast_group_table *)MPE_TX_ADDR(DLTX_CFG_CTXT_M2C_GROUP_TABLE);
	int i, j;
	dtlk_debug(DBG_PROC, "%s\n", __func__);
	for (i = 0; i < MAX_MCAST_GROUP; i++) {
		if (dl_m2c_group_tbl->valid) {
			seq_printf(seq, "Group: %d\n", i);
			for (j = 0; j < MAX_PEER_NUM; j++) {
				int byteOff = j / 32;
				int bitOff = j % 32;
				if (dl_m2c_group_tbl->bitmap[byteOff] & (1 << bitOff)) {
					struct _dl_peer_mac_mapping_table *dl_m2c_peer_tbl =
						(struct _dl_peer_mac_mapping_table *)MPE_TX_ADDR(DLTX_CFG_CTXT_M2C_PEER_TABLE);
					dl_m2c_peer_tbl += j;
					seq_printf(seq, "\tPeer: %d [%x:%x:%x:%x:%x:%x]\n",
						i,
						dl_m2c_peer_tbl->mac5,
						dl_m2c_peer_tbl->mac4,
						dl_m2c_peer_tbl->mac3,
						dl_m2c_peer_tbl->mac2,
						dl_m2c_peer_tbl->mac1,
						dl_m2c_peer_tbl->mac0
						);
				}
			}
		}
		dl_m2c_group_tbl++;
	}
	return 0;
}
static int proc_read_m2c_seq_open(
	struct inode *inode,
	struct file *file
	)
{
	return single_open(file, proc_read_m2c, NULL);
}

static ssize_t proc_write_m2c(
	 struct file *file,
	 const char __user *buf,
	 size_t count,
	 loff_t *data
	 )
{
	return count;
}

static struct file_operations g_proc_file_m2c_seq_fops = {
	.owner  = THIS_MODULE,
	.open	 = proc_read_m2c_seq_open,
	.read	 = seq_read,
	.write  = proc_write_m2c,
	.llseek = seq_lseek,
	.release	 = single_release,
};


#endif
/**
  * proc_file_create - Create proc files for driver
  *
  *
  * All proc files will be located in /proc/dtlk. Called by
  * dlrx_drv_init
  * Return: 0 : success. Others: fail
  */
static INLINE int proc_file_create(void)
{
	struct proc_dir_entry *res;
	int ret = 0;

	if (g_dtlk_proc_dir) {
		dtlk_debug(DBG_ERR, "More than one DirectLink device found!\n");
		return -EEXIST;
	}

	/* create parent proc directory */
	g_dtlk_proc_dir = proc_mkdir(DIRECTLINK_PROC_DIR, NULL);
	/* create mib */
	res  = proc_create("wifi_mib",
		S_IRUGO|S_IWUSR,
		g_dtlk_proc_dir,
		&g_proc_file_mib_seq_fops
		);

	if (!res) {
		dtlk_debug(DBG_ERR, "Failed to create mib\n");
		return -ENODEV;
	}

	/* create wifi_port */
	res = proc_create("wifi_port",
		S_IRUGO,
		g_dtlk_proc_dir,
		&g_proc_file_wifi_port_seq_fops
		);

	if (!res) {
		dtlk_debug(DBG_ERR, "Failed to create wifi_port\n");
		return -ENODEV;
	}

	/* create peer_id */
	res  = proc_create("peer_id",
		S_IRUGO,
		g_dtlk_proc_dir,
		&g_proc_file_peer_id_seq_fops
		);

	if (!res) {
		dtlk_debug(DBG_ERR, "Failed to create peer_id\n");
		return -ENODEV;
	}

	/* create peer_to_vap */
	res = proc_create("peer_to_vap",
		S_IRUGO,
		g_dtlk_proc_dir,
		&g_proc_file_peer_to_vap_seq_fops
		);

	if (!res) {
		dtlk_debug(DBG_ERR, "Failed to create peer_to_vap\n");
		return -ENODEV;
	}


	/* create dbg */
	res = proc_create("dbg",
		S_IRUGO|S_IWUSR,
		g_dtlk_proc_dir,
		&g_proc_file_dbg_seq_fops
		);

	if (!res) {
		dtlk_debug(DBG_ERR, "Failed to create dbg\n");
		ret = -ENODEV;
	}
	/* create mem */
	res = proc_create("mem",
		S_IRUGO|S_IWUSR,
		g_dtlk_proc_dir,
		&g_proc_file_mem_seq_fops
		);

	if (!res) {
		dtlk_debug(DBG_ERR, "Failed to create dbg\n");
		ret = -ENODEV;
	}

	/* create ver */
	res  = proc_create("ver",
		S_IRUGO,
		g_dtlk_proc_dir,
		&g_proc_file_ver_seq_fops
		);

	if (!res) {
		dtlk_debug(DBG_ERR, "Failed to create version\n");
		ret = -ENODEV;
	}

	/* create reg */
	res  = proc_create("reg",
		S_IRUGO,
		g_dtlk_proc_dir,
		&g_proc_file_reg_seq_fops
		);

	if (!res) {
		dtlk_debug(DBG_ERR, "Failed to create register\n");
		ret = -ENODEV;
	}

	/* create parser */
	res  = proc_create("parser",
		S_IRUGO,
		g_dtlk_proc_dir,
		&g_proc_file_parser_seq_fops
	);

	if (!res) {
		dtlk_debug(DBG_ERR, "Failed to create register\n");
		ret = -ENODEV;
	}
#ifdef SUPPORT_MULTICAST_TO_UNICAST
	/* create parser */
	res  = proc_create("m2c",
		S_IRUGO,
		g_dtlk_proc_dir,
		&g_proc_file_m2c_seq_fops
	);

	if (!res) {
		dtlk_debug(DBG_ERR, "Failed to create register\n");
		ret = -ENODEV;
	}
#endif
	return ret;
}

/**
  * proc_file_create - Delete all proc files created by this driver
  *
  *
  * Delete all proc files in /proc/dtlk.
  * Called by dlrx_drv_exit.
  */
static INLINE void proc_file_delete(void)
{
	/* delete reg  entry */
	remove_proc_entry("reg", g_dtlk_proc_dir);
	/* delete ver  entry */
	remove_proc_entry("ver", g_dtlk_proc_dir);
	/* delete mib entry */
	remove_proc_entry("wifi_mib", g_dtlk_proc_dir);
	/* delete dbg */
	remove_proc_entry("dbg", g_dtlk_proc_dir);
	#if 0
	/* delete test */
	remove_proc_entry("test", g_dtlk_proc_dir);
	#endif
	/* delete wifi_port */
	remove_proc_entry("wifi_port", g_dtlk_proc_dir);

	/* delete peer_id */
	remove_proc_entry("peer_id", g_dtlk_proc_dir);

	/* delete peer_to_vap */
	remove_proc_entry("peer_to_vap", g_dtlk_proc_dir);

	/* delete mem */
	remove_proc_entry("mem", g_dtlk_proc_dir);

	/* delete parser */
	remove_proc_entry("parser", g_dtlk_proc_dir);
	/* delete m2c */
	remove_proc_entry("m2c", g_dtlk_proc_dir);

	/* delete parent proc directory */
	remove_proc_entry(DIRECTLINK_PROC_DIR, NULL);

	g_dtlk_proc_dir = NULL;
}

void dtlk_variables_api_init(void){
	int i, j;
	for (i = 0; i < MAX_RADIO_NUM; i++) {
		struct ppe_radio_map *radio = &g_ppe_radio[i];
		radio->flags = 0;
		radio->dl_sub_if.port_id = -1;
		radio->dl_sub_if.subif = -1;
		for (j = 0; j < MAX_DTLK_NUM; j++) {
			struct ppe_dtlk_map *dtlk =
				&(radio->g_ppe_dtlk_data[j]);
			memset(&radio->g_ppe_dtlk_data[j],
				0,
				sizeof(struct ppe_dtlk_map));
			dtlk->dev = NULL;
			dtlk->dl_sub_if.port_id = -1;
			dtlk->dl_sub_if.subif = -1;
			dtlk->owner = NULL;
			dtlk->vap_id = -1;
		}
	}
}
#ifdef NEW_CHANGE

extern int (*datapath_dtlk_register_fn)(PPA_SUBIF *subIf, PPA_DTLK_T *dtlk);
#endif
static void dl_init_dltx_memory(struct dl_buf_info dl_mem)
{
	if (dl_mem.tx_cfg_ctxt_buf_size && dl_mem.tx_cfg_ctxt_buf_base) {
		memset((void *)(dl_mem.tx_cfg_ctxt_buf_base), 0, dl_mem.tx_cfg_ctxt_buf_size);
		ppa_dl_dre_dma_writeback(dl_mem.tx_cfg_ctxt_buf_base,
			dl_mem.tx_cfg_ctxt_buf_size);
	}
	if (dl_mem.rx_cfg_ctxt_buf_size && dl_mem.rx_cfg_ctxt_buf_base) {
		memset((void *)(dl_mem.rx_cfg_ctxt_buf_base), 0, dl_mem.rx_cfg_ctxt_buf_size);
		ppa_dl_dre_dma_writeback(dl_mem.rx_cfg_ctxt_buf_base,
			dl_mem.rx_cfg_ctxt_buf_size);
	}
	if (dl_mem.rx_msgbuf_size && dl_mem.rx_msgbuf_base) {
		memset((void *)(dl_mem.rx_msgbuf_base), 0, dl_mem.rx_msgbuf_size);
		ppa_dl_dre_dma_writeback(dl_mem.rx_msgbuf_base,
			dl_mem.rx_msgbuf_size);
	}
	if (dl_mem.uncached_addr_size && dl_mem.uncached_addr_base) {
		memset((void *)dl_mem.uncached_addr_base, 0, dl_mem.uncached_addr_size);
		ppa_dl_dre_dma_writeback(dl_mem.uncached_addr_base,
			dl_mem.uncached_addr_size);
	}
	if (dl_mem.comm_buf_size && dl_mem.comm_buf_base) {
		memset((void *)(dl_mem.comm_buf_base), 0, dl_mem.comm_buf_size);
		ppa_dl_dre_dma_writeback(dl_mem.comm_buf_base,
			dl_mem.comm_buf_size);
	}

}
int __init dlrx_drv_init(void)
{
	struct dl_drv_address_map dl_resource;
	if (g_ce5_desc_ring_num >= CE5_DEST_DESC_RING_NUM)
		g_ce5_desc_ring_num = CE5_DEST_DESC_RING_NUM;

	if (g_ce5_buf_ptr_ring_num < g_ce5_desc_ring_num)
	   g_ce5_buf_ptr_ring_num = g_ce5_desc_ring_num;
	dtlk_debug(DBG_INIT, "DirectLink's parameters\n");
	dtlk_debug(DBG_INIT, "\tNumber of TX DESCRIPTOR: %d\n", dl_param_tx_descriptor);
#ifdef PROFILING
	initProfiling();
#endif
	wlan_detect_ath_card();
	#ifdef NEW_CHANGE
	datapath_dtlk_register_fn = datapath_dtlk_register;
	#endif
	/* Get DirectLink resource from MPE HAL */
	#if 1
	g_mpe_dev = mpe_hal_dl_get_dev();
	#endif
	#ifdef NEW_CHANGE
	if (mpe_hal_dl_alloc_resource(0, (uint32_t *)&dl_resource, 0) < 0) {
		dtlk_debug(DBG_ERR, "Can not get DL BUF INFO resource\n");
		return -1;
	}
	#else
	if (mpe_hal_dl_alloc_resouce(0, (uint32_t *)&dl_resource, 0) < 0) {
		dtlk_debug(DBG_ERR, "Can not get DL BUF INFO resource\n");
		return -1;
	}
	#endif
#if DTLK_SWITCH_DISABLE_PARSE
	datapath_dtlk_switch_parser = dtlk_switch_parser;
#endif
	dtlk_debug(DBG_INIT, "%s: 0x%x 0x%x\n",
		__func__,
		dl_resource.dl_buf_info_addr,
		dl_resource.dl_ep_2radio_addr
		);
	memcpy(&g_dl_buf_info,
		(struct dl_buf_info *)dl_resource.dl_buf_info_addr,
		sizeof(g_dl_buf_info)
		);
	dl_init_dltx_memory(g_dl_buf_info);
	g_dl_drv_address.dl_buf_info_addr = dl_resource.dl_buf_info_addr;
	g_dl_drv_address.dl_ep_2radio_addr = dl_resource.dl_ep_2radio_addr;

	dtlk_debug(DBG_INIT, "DirectLink resource from MPE HAL\n");
	dtlk_debug(DBG_INIT, "    COMM base[%x]\n",
		g_dl_buf_info.comm_buf_base);
	dtlk_debug(DBG_INIT, "    COMM size[%d]\n",
		g_dl_buf_info.comm_buf_size);
	dtlk_debug(DBG_INIT, "    RX CFG context base[%x]\n",
		g_dl_buf_info.rx_cfg_ctxt_buf_base);
	dtlk_debug(DBG_INIT, "    RX CFG context size[%d]\n",
		g_dl_buf_info.rx_cfg_ctxt_buf_size);
	dtlk_debug(DBG_INIT, "    RX Message base[%x]\n",
		g_dl_buf_info.rx_msgbuf_base);
	dtlk_debug(DBG_INIT, "    RX Message size[%d]\n",
		g_dl_buf_info.rx_msgbuf_size);
	dtlk_debug(DBG_INIT, "    TX CFG context base[%x]\n",
		g_dl_buf_info.tx_cfg_ctxt_buf_base);
	dtlk_debug(DBG_INIT, "    TX CFG context size[%d]\n",
		g_dl_buf_info.tx_cfg_ctxt_buf_size);
	dtlk_debug(DBG_INIT, "    Uncached context base[%x]\n",
		g_dl_buf_info.uncached_addr_base);
	dtlk_debug(DBG_INIT, "    Uncached context size[%d]\n",
		g_dl_buf_info.uncached_addr_size);
	if (g_mpe_dev) {
	#if 0
		dma_addr_t phy_addr;
		phy_addr = dma_map_single(
						g_mpe_dev,
						(void *)g_dl_buf_info.tx_cfg_ctxt_buf_base,
						g_dl_buf_info.tx_cfg_ctxt_buf_size,
						DMA_FROM_DEVICE
						);
		if (unlikely(dma_mapping_error(g_mpe_dev, phy_addr))) {
			dtlk_debug(DBG_ERR, "DMA error");
		}
		phy_addr = dma_map_single(
						g_mpe_dev,
						(void *)g_dl_buf_info.rx_cfg_ctxt_buf_base,
						g_dl_buf_info.rx_cfg_ctxt_buf_size,
						DMA_FROM_DEVICE
						);
		if (unlikely(dma_mapping_error(g_mpe_dev, phy_addr))) {
			dtlk_debug(DBG_ERR, "DMA error");
		}
		phy_addr = dma_map_single(
						g_mpe_dev,
						(void *)g_dl_buf_info.rx_msgbuf_base,
						g_dl_buf_info.rx_msgbuf_size,
						DMA_FROM_DEVICE
						);
		if (unlikely(dma_mapping_error(g_mpe_dev, phy_addr))) {
			dtlk_debug(DBG_ERR, "DMA error");
		}
		#endif
		/*
		phy = dma_map_single(
						g_mpe_dev,
						(void *)g_dl_buf_info.comm_buf_base,
						4,
						DMA_FROM_DEVICE
						);
					dma_unmap_single(
						g_mpe_dev,
						phy,
						4,
						DMA_FROM_DEVICE
						);
						*/
		dtlk_debug(DBG_INIT, "    COMM base[%x] KSEG0[0x%x] KSEG1[0x%x] [0x%x]\n",
		g_dl_buf_info.comm_buf_base,
		KSEG0,
		KSEG1,
		(unsigned int)CPHYSADDR(g_dl_buf_info.comm_buf_base));
		} else{
		dtlk_debug(DBG_ERR, "%s: device is null\n", __func__);
		}
	if (g_dl_buf_info.rx_cfg_ctxt_buf_base) {
		uint32_t test = g_dl_buf_info.rx_cfg_ctxt_buf_base;
		dtlk_debug(DBG_INIT, "Test tao lao: 0x%x\n", test);
	}
	/* Memory resources */
	ddr_mpe_base = (unsigned int *)(g_dl_buf_info.tx_cfg_ctxt_buf_base);
	ddr_mpe_comp_base = (unsigned int *)(g_dl_buf_info.comm_buf_base);
	/* Initialize data structure */
	if (dlrx_data_structure_init() == DTLK_SUCCESS)
		dtlk_rx_api_init();
	if (dltx_data_structure_init() != DTLK_SUCCESS) {
		dtlk_debug(DBG_ERR, "%s:cannot initialize DLTX\n", __func__);
	}
	dtlk_variables_api_init();
	set_vap_itf_tbl_fn = set_vap_itf_tbl;
	dtlk_debug(DBG_INIT, "%s: before create proc\n", __func__);
	proc_file_create();

	dtlk_debug(DBG_INIT, "DLRX driver init successfully!\n");
	dtlk_debug(DBG_INIT, "DLRX driver version: 0x%x\n",
		DLRX_DRV_VERSION);
	/*Configure MPE DL TX*/
	if (ddr_mpe_base) {
		dltx_cfg_global_t *mpe_dl_tx_cfg_global =
			(dltx_cfg_global_t *)MPE_TX_ADDR(DLTX_CFG_GLOBAL_OFFSET);
		mpe_dl_tx_cfg_global->dltx_enable = 1;
		mpe_dl_tx_cfg_global->dltx_base_address = (unsigned int)ddr_mpe_base;
		mpe_dl_tx_cfg_global->switch_parser_flag = 0;
	}
	/* start MPE DL TX,assume we are running one radio */
	mpe_hal_feature_start(DL_TX_1, 0, NULL, F_FEATURE_START);
	return 0;

}

#define DLTX_DRV_TO_EXIT 1
#define DLTX_DRV_TO_DEBUG 2

static void dltx_stop(void)
{
	dltx_drv_msg_t *dltx_cfg_ctxt_drv_msg;
	dltx_cfg_ctxt_drv_msg = (dltx_drv_msg_t *)MPE_TX_COML_ADDR(DLTX_DRV_MSG_OFFSET);
	dltx_cfg_ctxt_drv_msg->valid = 1;
	dltx_cfg_ctxt_drv_msg->action_type = DLTX_DRV_TO_EXIT;
	ppa_dl_qca_ipi_interrupt();
}
static void __exit dlrx_drv_exit(void)
{
	int retries = 3;
	dltx_cfg_global_t *mpe_dl_tx_cfg_global =
			(dltx_cfg_global_t *)MPE_TX_ADDR(DLTX_CFG_GLOBAL_OFFSET);

	/* firstly: check for all activities and stop them
	firstly: stop RX */
	#ifdef NEW_CHANGE
	datapath_dtlk_register_fn = NULL;
	#endif
#if DTLK_SWITCH_DISABLE_PARSE
	datapath_dtlk_switch_parser = NULL;
#endif
#ifdef SUPPORT_UNLOAD_DTLK
	/* ppa_dp_dlrx_exit(); */
	dtlk_debug(DBG_INIT,
		"%s:Not disable DMA 7\n",
		__func__
		);
#endif

	/* secondly: stop firmware */

	/* clean proc */
	proc_file_delete();
	dtlk_debug(DBG_INIT, "After proc delete\n");
	dtlk_debug(DBG_INIT,
		"%s: Try to free all buffer in replenish list\n",
		__func__
		);
	dtlk_rx_api_exit();
	/*Stop MPE DL TX*/
	/*Configure MPE DL TX*/
	dltx_stop();
	while (mpe_dl_tx_cfg_global->dltx_enable && retries) {
		msleep(1000);
		retries--;
		dtlk_debug(DBG_INIT, "Try again\n");
	}

	dtlk_debug(DBG_INIT, "After dtlk_rx_api_exit\n");
	mpe_hal_feature_start(DL_TX_1, 0, NULL, F_FEATURE_STOP);

	return;
}


/* Module Definitions */
module_init(dlrx_drv_init);
module_exit(dlrx_drv_exit);

MODULE_AUTHOR(MOD_AUTHOR);
MODULE_DESCRIPTION(MOD_DESCRIPTION);
MODULE_LICENSE(MOD_LICENCE);

