/******************************************************************************

				Copyright (c) 2012, 2014, 2015
				Lantiq Deutschland GmbH

  For licensing information, see the file 'LICENSE' in the root folder of
  this software module.

******************************************************************************/



/*************************************************
 *	This file provide all the APIs that need export to DLRX_FW or 11AC driver
 *************************************************/

/*************************************************
 *			Head File
 *************************************************/
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/version.h>
#include <linux/types.h>
#include <linux/ctype.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/miscdevice.h>
#include <linux/init.h>
#include <linux/etherdevice.h>	/*	eth_type_trans	*/
#include <linux/ethtool.h>		/*	ethtool_cmd 	*/
#include <linux/if_ether.h>
#include <linux/if_arp.h>
#include <asm/uaccess.h>
#include <asm/unistd.h>
#include <asm/irq.h>
#include <asm/delay.h>
#include <asm/io.h>
#include <asm/gic.h>
#include <linux/skbuff.h>
#include <linux/dma-mapping.h>
#include <linux/list.h>

#include <net/ppa_ppe_hal.h>
#include <net/ppa_stack_al.h>
#include <linux/dma-mapping.h>

/* FW header files */
#include "./include/11ac_acc_data_structure_tx_be.h"
#include "../../../lantiq_ppa/platform/xrx500/directlink/dlrx_fw_data_structure_be.h"
#include "../../../lantiq_ppa/platform/xrx500/directlink/dlrx_fw_def.h"
#include "../../../lantiq_ppa/platform/xrx500/directlink/dlrx_fw_version.h"
/*DLRX driver header files */
#include "./include/dlrx_drv.h"
#include "./include/dlrx_memory_lyt.h"
#include "../../../lantiq_ppa/platform/xrx500/directlink/dlrx_dre_api.h"
#include "./include/dlrx_wlan_api.h"

#include "./include/directlink_tx_cfg.h"
#include "./include/ltqmips_hal.h"
#include <net/lantiq_cbm_api.h>
#include <net/ltq_mpe_api.h>
#include <net/ppa_stack_al.h>
#include <net/ppa_api.h>
#include <net/ppa_api_directpath.h>
#include <net/ppa_hook.h>
#include <net/ppa_ppe_hal.h>

#include "../cbm/cbm.h"
#include <net/lantiq_cbm_api.h>
#ifdef SUPPORT_MULTICAST_TO_UNICAST
#include "./include/dltx_fw_data_structure_be.h"
#include "./include/Dltx_fw_def.h"

#endif

#define dl_kseg0 KSEG0
#define dl_kseg1 KSEG1

#define RX_NOT_WRITEBACK 1
#define RX_NOT_SKB 1

/*************************************************
 *			Definition
 *************************************************/

/*************************************************
 *			Global Variable
 *************************************************/
uint32_t g_dlrx_max_inv_header_len;
uint32_t g_dlrx_cfg_offset_atten;
void *g_dlrx_handle;
PPA_QCA_DL_RX_CB g_dlrx_qca_cb = {0};
dre_regfn_set_t g_dre_fnset    = {0};
spinlock_t g_vap2int_tbl_lock;
spinlock_t g_prid2pr_tbl_lock;
spinlock_t g_pr2vap_tbl_lock;
spinlock_t g_pr2handler_tbl_lock;

extern PPA_SUBIF gQuickRefSubIfFromVAPID[];
#if 1
extern struct device *g_mpe_dev;
#endif
extern struct dl_buf_info g_dl_buf_info;

LIST_HEAD(g_dlrx_skblist);

#ifdef PROFILING
extern void *CycleCounter_Create(char *);
extern void CycleCounter_Start(void *);
extern void CycleCounter_End(void *);
void *profiling_alloc;
void *profiling_enqueue;
void *profiling_writeback;
void *profiling_cmb_alloc;
void *profiling_cmb_enqueue;

void initProfiling()
{
	dtlk_debug(DBG_INIT, "%s: Init profiling\n", __func__);
	profiling_alloc = CycleCounter_Create("Allocation");
	profiling_enqueue = CycleCounter_Create("Enqueue");
	profiling_writeback = CycleCounter_Create("Writeback");
	profiling_cmb_alloc = CycleCounter_Create("CBM Allocation");
	profiling_cmb_enqueue = CycleCounter_Create("CBM Enqueue");
}
#endif


int set_peer_id_to_peer_table(
	uint32_t dlrx_peer_reg_handle,
	uint32_t peer_id,
	uint32_t *peer,
	unsigned int vap_id,
	unsigned int pn_chk_type
	);
int remove_peer_id_from_table(
	uint32_t peer,
	uint32_t peer_id
	);
int remove_peer_from_table(
	uint32_t peer,
	uint32_t peer_id
	);
int get_handler_index(
	uint32_t index
	);
int get_free_peer_number(
	unsigned int *peer
	);
#define DL_DIRECT_ENQ_CBM 1

/*************************************************
 *			Static functions
 *************************************************/
/*
* Function: alloc_skb_tx
* Purpose: alloc CBM skb buffer
*/
extern	void *cbm_buffer_alloc(uint32_t pid, uint32_t flag);
extern int cbm_buffer_free(uint32_t pid, uint32_t buf, uint32_t flag);

/* == AVM/CMH 20160418 Directlink rx packet trace == */
#if 1
#define MAX_DTLK_NUM 16
int avm_get_vap_by_subif(int subif) {
	for (int i = 0; i < MAX_DTLK_NUM; i++) {
		if (gQuickRefSubIfFromVAPID[i].port_id != 0 && gQuickRefSubIfFromVAPID[i].subif == subif) {
			return i;
		}
	}
	return -1;
}
#undef MAX_DTLK_NUM
#endif

#define RX_RESERVE_BYTES 128
#define RX_OPTIMIZE 1
#ifdef DL_DIRECT_ENQ_CBM
struct sk_buff *my_alloc_skb_head(gfp_t gfp_mask)
{
	struct sk_buff *skb;

	/* Get the HEAD */
	skb = kzalloc(sizeof (struct sk_buff), gfp_mask);
#ifndef RX_OPTIMIZE
	if (!skb)
		goto out;

	/*
	 * Only clear those fields we need to clear, not those that we will
	 * actually initialise below. Hence, don't put any more fields after
	 * the tail pointer in struct sk_buff!
	 */
	memset(skb, 0, offsetof(struct sk_buff, tail));
	skb->head = NULL;
	skb->truesize = sizeof(struct sk_buff);
	atomic_set(&skb->users, 1);

#ifdef NET_SKBUFF_DATA_USES_OFFSET
	skb->mac_header = ~0U;
#endif
out:
#endif

	return skb;
}

struct sk_buff *dl_cbm_build_skb(void *data, unsigned int frag_size, gfp_t priority)
{
	struct sk_buff *skb;
	unsigned int size;
	size = frag_size;
	/*dtlk_debug(DBG_TX, "%s data 0x%x size %d\r\n",__func__, data, frag_size);*/
	skb = my_alloc_skb_head(GFP_ATOMIC);
	if (!skb) {
		dtlk_debug(DBG_ERR, "%s: SKB head alloc failed\r\n", __func__);
		return NULL;
	}

	/* size -= SKB_DATA_ALIGN(sizeof(struct skb_shared_info)); */
#ifndef RX_OPTIMIZE
	memset(skb, 0, offsetof(struct sk_buff, tail));
	skb->truesize = SKB_TRUESIZE(size);
	skb->head_frag = 0;
	atomic_set(&skb->users, 1);
#endif
	skb->head = data;
	skb->data = data;
	skb_reset_tail_pointer(skb);
	skb->end = skb->tail + size;
#ifndef RX_OPTIMIZE
#ifdef NET_SKBUFF_DATA_USES_OFFSET
	skb->mac_header = ~0U;
	skb->transport_header = ~0U;
#endif
#endif
	return skb;
}
void dealloc_skb_tx(struct sk_buff *skb)
{
	#ifndef RX_NOT_SKB
	cbm_buffer_free(smp_processor_id(), (uint32_t)skb->head, 0);
	#else
	cbm_buffer_free(smp_processor_id(), (uint32_t)skb, 0);
	#endif
	#ifndef RX_NOT_WRITEBACK
	/* free skb */
	kfree(skb);
	#endif
}

static struct sk_buff *alloc_skb_tx(
	int len
	)
{
#ifndef RX_NOT_SKB
	struct sk_buff *skb = NULL;
#endif
	#ifndef RX_NOT_WRITEBACK
	dma_addr_t phy_addr;
	#endif
	void *buf = NULL;
#ifdef PROFILING
	CycleCounter_Start(profiling_cmb_alloc);
#endif
	buf = cbm_buffer_alloc(smp_processor_id(), CBM_PORT_F_STANDARD_BUF);
#ifdef PROFILING
	CycleCounter_End(profiling_cmb_alloc);
#endif

#ifndef RX_NOT_SKB
	if (buf) {
		skb = dl_cbm_build_skb((void *)buf,
			len, GFP_ATOMIC);
	}
	if (skb) {
		skb_reserve(skb, RX_RESERVE_BYTES);
		*((u32 *)skb->data - 1) = (u32)skb;
		/* Debug Clear Attention bit. */
		*((uint32_t *)skb->data + 1) = 0;
		/* write back to real memory */
		#ifndef RX_NOT_WRITEBACK
		phy_addr = dma_map_single(
			g_mpe_dev,
			(u32)skb->data - sizeof(u32),
			sizeof(u32) + 3,
			DMA_TO_DEVICE
			);
		if (unlikely(dma_mapping_error(g_mpe_dev, phy_addr))) {
			dtlk_debug(DBG_ERR, "DMA error");
		}
		#endif
	} else {
		dtlk_debug(DBG_ERR, "%s: cannot create skb\n", __func__);
		/* cbm_buffer_free(smp_processor_id(),(uint32_t) buf, 0); */
		dealloc_skb_tx(skb);
	}
	return skb;
#else
	#if 0 /* 240Jun2015 no need now, for testing */
	#ifdef PROFILING
	CycleCounter_Start(profiling_writeback);
	#endif
	phy_addr = dma_map_single(
			g_mpe_dev,
			buf,
			2048,
			DMA_FROM_DEVICE
			);
	if (unlikely(dma_mapping_error(g_mpe_dev, phy_addr))) {
		dtlk_debug(DBG_ERR, "DMA error");
	}
	#ifdef PROFILING
		CycleCounter_End(profiling_writeback);
	#endif
	#endif
	return buf;

#endif
}
#else
static struct sk_buff *alloc_skb_tx(
	int len
	)
{
	struct sk_buff *skb = NULL;
	dma_addr_t phy_addr;
	void *buf = NULL;

	len = (len + DTLK_ALIGNMENT - 1) & ~(DTLK_ALIGNMENT - 1);
	len += RX_RESERVE_BYTES;

	skb = dev_alloc_skb(len);
	if (skb) {
		skb_reserve(skb, RX_RESERVE_BYTES);
		ASSERT(((u32)skb->data & (DTLK_ALIGNMENT - 1)) == 0, "skb->data (%#x) is not 8 DWORDS aligned", (u32)skb->data);

		*((u32 *)skb->data - 1) = (u32)skb;
		/* Debug Clear Attention bit. */
		*((uint32_t *)skb->data + 1) = 0;

		phy_addr = dma_map_single(
			g_mpe_dev,
			(u32)skb->data - sizeof(u32),
			/* sizeof(u32) + 3, */
			sizeof(u32) + (skb->end - skb->data),
			DMA_TO_DEVICE
			);
		if (unlikely(dma_mapping_error(g_mpe_dev, phy_addr))) {
			dtlk_debug(DBG_ERR, "DMA error");
		}
		/*
		dma_unmap_single(
			g_mpe_dev,
			phy_addr,
			sizeof(u32) + 3,
			DMA_TO_DEVICE
			);
			*/
	}

	if (skb->prev || skb->next) {
		dtlk_debug(DBG_ERR, "%s:==============SKB PREV[0x%x] or NEXT[0x%x]============\n", __func__, skb->prev, skb->next);
	}
	return skb;
}
#endif
static struct sk_buff *__get_skb_pointer(
	uint32_t dataptr,
	const char *func_name,
	unsigned int line_num
	)
{
	unsigned int skb_dataptr;
	struct sk_buff *skb;

	if (dataptr == 0) {
		dtlk_debug(DBG_ERR, "dataptr is 0, it's supposed to be invalid pointer");
		return NULL;
	}
	/* dtlk_debug(DBG_RX, "%s: skb[0x%x]\n", __func__, dataptr); */
	/* skb_dataptr = KSEG1ADDR(dataptr - 4); */
	#ifndef RX_NOT_WRITEBACK
	skb_dataptr = KSEG1 | (0x0fffffff & (dataptr - 4));
	#else
	skb_dataptr = KSEG0 | (0x0fffffff & (dataptr - 4));
	#endif
	/* skb_dataptr = dataptr - 4; */
	skb = *(struct sk_buff **)skb_dataptr;
	if ((uint32_t)skb < KSEG0 || (uint32_t)skb >= KSEG1)
		dtlk_debug(DBG_ERR, "%s:%d: invalid skb - skb = %#08x, dataptr = %#08x\n",
			func_name, line_num, (unsigned int)skb, dataptr
		);
	return skb;
}

static inline struct sk_buff *dlrx_get_skb_ptr(
	unsigned int dataptr
	)
{
	return __get_skb_pointer(dataptr, __FUNCTION__, __LINE__);
}

/*
*  Get the DRE FW API pointer
*/
static void *dlrx_get_dre_fn(
	unsigned int fntype
	)
{
	switch (fntype) {
	case DRE_MAIN_FN:
		return g_dre_fnset.dre_dl_main_fn;
	case DRE_GET_VERSION_FN:
		return g_dre_fnset.dre_dl_getver_fn;
	case DRE_RESET_FN:
		return g_dre_fnset.dre_dl_reset_fn;
	case DRE_GET_MIB_FN:
		return g_dre_fnset.dre_dl_getmib_fn;
	case DRE_GET_CURMSDU_FN:
		return g_dre_fnset.dre_dl_getmsdu_fn;
	case DRE_SET_MEMBASE_FN:
		return g_dre_fnset.dre_dl_set_membase_fn;
	case DRE_SET_RXPN_FN:
		return g_dre_fnset.dre_dl_set_rxpn_fn;
	case DRE_SET_DLRX_UNLOAD:
		return g_dre_fnset.dre_dl_set_dlrx_unload_t;
	default:
		return NULL;
	}

	return NULL;

}

/*
 *	Extract and Setup the skb structure
 */
static struct sk_buff *dlrx_skb_setup(
	unsigned int rxpb_ptr,
	unsigned int data_ptr,
	unsigned int data_len
	)
{
	struct sk_buff *skb;
#ifndef RX_NOT_SKB
	skb = dlrx_get_skb_ptr(rxpb_ptr);

	dtlk_debug(DBG_RX, "skb:0x%x, rxpb_ptr:0x%x, data_ptr:0x%x, data_len:0x%x\n",
		(uint32_t)skb, rxpb_ptr, data_ptr, data_len);

	/* adjust skb */
	skb->data = (unsigned char *)CACHE_ADDR(data_ptr);
	skb->len  = data_len;
	skb->tail = skb->data + data_len;
#else
	int header_len = data_ptr - rxpb_ptr;
	ppa_dl_dre_dma_invalidate(rxpb_ptr , header_len + data_len);
	skb = alloc_skb(header_len + data_len, GFP_KERNEL);
	if (skb == NULL) {
		dtlk_debug(DBG_ERR, "Can not allocate memory for DLRX\n");
		return NULL;
	}
	memcpy(skb_put(skb, header_len + data_len),
		(void *)rxpb_ptr, header_len + data_len);
	skb_pull(skb, header_len);
	/* free old cbm */
	cbm_buffer_free(smp_processor_id(), rxpb_ptr, 0);
#endif
	return skb;
}

static void dump_skb(
	struct sk_buff *skb,
	uint32_t len,
	char *title
	)
{
	int i;

	if (skb->len < len)
		len = skb->len;

	if (len > DTLK_PACKET_SIZE) {
		dtlk_debug(DBG_ERR, "Too big:skb=%08x,skb->data=%08x,skb->len=%d\n",
			(u32)skb, (u32)skb->data, skb->len);
		return;
	}

	dtlk_debug(DBG_RX, "%s\n", title);
	dtlk_debug(DBG_RX, "skb=0x%x,skb->data=%08X,skb->tail=%08X,skb->len=%d\n",
		(u32)skb, (u32)skb->data, (u32)skb->tail, (int)skb->len);
	for (i = 1; i <= len; i++) {
		if ((i % 16) == 1)
			dtlk_debug(DBG_RX, "  %4d:", i - 1);
		dtlk_debug(DBG_RX, " %02X", (int)(*((char *)skb->data + i - 1) & 0xFF));
		if ((i % 16) == 0)
			dtlk_debug(DBG_RX, "\n");
	}
	if (((i - 1) % 16) != 0)
		dtlk_debug(DBG_RX, "\n");
}

#define REP_SKB_NUM 256

void skb_list_replenish(
	struct list_head *head
	)
{
#ifndef RX_NOT_SKB
	int i;
	struct sk_buff *skb;
	dma_addr_t phy_addr;
	/* dtlk_debug(DBG_RX, "%s: ===========NEED MORE GAS===========\n", __func__); */
	for (i = 0; i < REP_SKB_NUM; i++) {
		/* create new CBM skb */
		skb = alloc_skb_rx();
		if (!skb) {
			dtlk_debug(DBG_ERR, "Alloc SKB fail!!!\n");
			return;
		}
		/* dma_cache_inv((u32)skb->data, skb->end - skb->data); */
		#if 0
		phy_addr = dma_map_single(g_mpe_dev,
					(void *)skb->data,
					skb->end - skb->data,
					DMA_FROM_DEVICE
					);
		dma_unmap_single(g_mpe_dev,
			phy_addr,
			skb->end - skb->data,
			DMA_FROM_DEVICE
			);
		#endif
		list_add((struct list_head *)skb, head);
	}
#endif
}

unsigned int skb_list_get_skb(
	unsigned int rxpb_ptr
	)
{
	struct list_head *pos, *q;
	list_for_each_safe(pos, q, &g_dlrx_skblist) {
		if (pos) {
			struct sk_buff *buff = (struct sk_buff *)pos;
			if (buff != NULL) {
				if ((unsigned int)(buff->data) == rxpb_ptr) {
					list_del(pos);
					return 1;
				}
			}
		}
	}
	return 0;
}
/*
* Free all skb buffers in the recycle list
*/
void skb_list_exhaust(
	struct list_head *head
	)
{
	struct list_head *pos, *q;
	int count = 0;
	list_for_each_safe(pos, q, head) {
		list_del(pos);
		if (pos) {
			struct sk_buff *buff = (struct sk_buff *)pos;
			if (buff != NULL) {
				dtlk_debug(DBG_ERR,
					"%s: %p shoudl be the same %x\n",
					__func__, buff, (unsigned int)(*((u32 *)buff->data - 1)));
				/* free the data */
				#if 0
				dev_kfree_skb_any((struct skb_buff *)pos);
				#else
				/* cbm_buffer_free(smp_processor_id(), buff->head, 0); */
				dealloc_skb_tx(buff);
				#endif
				count++;
			}
		}
	}
	dtlk_debug(DBG_RX,
		"%s: release %d in list\n", __func__, count);
}

void dlrx_skb_recycle(
	struct sk_buff *skb
	)
{
	if (unlikely(!skb))
		return;

	/* TODO: James test DMA */
	/* dma_cache_inv((u32)skb->data, g_dlrx_max_inv_header_len); */
	#if 0
	phy_addr = dma_map_single(g_mpe_dev,
					(void *)skb->data,
					g_dlrx_max_inv_header_len,
					DMA_FROM_DEVICE
					);
	dma_unmap_single(g_mpe_dev,
		phy_addr,
		g_dlrx_max_inv_header_len,
		DMA_FROM_DEVICE
		);
	#endif
	list_add((struct list_head *)skb, &g_dlrx_skblist);
}

/*************************************************
 *			Global Functions
 *************************************************/
struct sk_buff *alloc_skb_rx(void)
{
	struct sk_buff *result;
#ifdef PROFILING
	CycleCounter_Start(profiling_alloc);
#endif
	result = alloc_skb_tx(DTLK_PACKET_SIZE);
#ifdef PROFILING
	CycleCounter_End(profiling_alloc);
#endif
	return result;
}

/*
* This function is called when DTLK driver is unloaded.
*/
void dtlk_rx_api_exit(void)
{
	skb_list_exhaust(&g_dlrx_skblist);
	*DLRX_SKB_POOL_CTXT = 0;
}

void dtlk_rx_api_init(void)
{
	spin_lock_init(&g_vap2int_tbl_lock);
	spin_lock_init(&g_prid2pr_tbl_lock);
	spin_lock_init(&g_pr2vap_tbl_lock);
	spin_lock_init(&g_prid2pr_tbl_lock);
	spin_lock_init(&g_pr2handler_tbl_lock);

	skb_list_replenish(&g_dlrx_skblist);
	*DLRX_SKB_POOL_CTXT = (unsigned int)&g_dlrx_skblist;
	return;
}

/*************************************************
 *			APIs for DLRX FW
 *************************************************/
/*
 * Function: dlrx_dl_dre_rxpb_buf_alloc
 * Purpose: Allocate a new rxpb ring pkt buffer for DLRX FW
 * Return: return skb->data pointer. DLRX FW from this pointer
 *     can get original skb pointer.
 */
unsigned int dlrx_dl_dre_rxpb_buf_alloc(void)
{
#ifndef RX_NOT_SKB
	struct sk_buff *skb;

	if (unlikely(list_empty(&g_dlrx_skblist))) {
		skb_list_replenish(&g_dlrx_skblist);
		/* dtlk_debug(DBG_RX, "%s: R\n", __func__ ); */
	}


	skb = (struct sk_buff *)g_dlrx_skblist.next;
	if (!skb) {
		dtlk_debug(DBG_ERR, "No resource for RX");
		return 0;
	}
	list_del((struct list_head *)skb);
	/* dtlk_debug(DBG_RX, "%s: return 0x%x\n", __func__, skb->data); */
	if (skb->prev || skb->next) {
		/*
		dtlk_debug(DBG_RX, "%s: ========SKB[0x%x]==========NEXT[0x%x] PREV[0x%x]=========\n",
			__func__, skb, skb->next, skb->prev);
			*/
		skb->prev = NULL;
		skb->next = NULL;
	}
	return (unsigned int)skb->data;
#else
	struct sk_buff *skb;

	skb = alloc_skb_rx();
	if (!skb) {
		dtlk_debug(DBG_ERR, "%s:It's realy bad SKB fail!!!\n", __func__);
		return 0;
	}
	return (unsigned int)((unsigned char *)skb + RX_RESERVE_BYTES);
#endif

}
EXPORT_SYMBOL(dlrx_dl_dre_rxpb_buf_alloc);


/*
 * Free a  rxpb ring pkt buffer,called by DLRX firmware.
 */
int ppa_dl_dre_rxpb_buf_free(
	unsigned int rxpb_ptr
	)
{
	struct sk_buff *skb;

	/* sanity check */
	/*
	dtlk_debug(DBG_RX, "%s: going to free 0x%x\n",
		__func__,
		rxpb_ptr
		); */
	if (rxpb_ptr < KSEG0) {
		rxpb_ptr = 	CACHE_ADDR(rxpb_ptr);
	}
	if (!rxpb_ptr) {
		dtlk_debug(DBG_ERR, "%s: rxpb_ptr is NULL\n", __func__);
		return DTLK_FAILURE;
	}
	rxpb_ptr = 	CACHE_ADDR(rxpb_ptr);
	#ifndef RX_NOT_SKB
	skb = dlrx_get_skb_ptr(rxpb_ptr);
	if (!skb) {
		dtlk_debug(DBG_ERR, "%s: rxpb_ptr[%x] skb is NULL\n", __func__, rxpb_ptr);
		return DTLK_FAILURE;
	}
	#else
	skb = (struct sk_buff *)rxpb_ptr;
	#endif
	/*
	if (skb_list_get_skb(rxpb_ptr) == 1)
		dtlk_debug(DBG_ERR, "%s: Inside replenish \n", __func__);
		*/
	if (skb) {
		/* dtlk_debug(DBG_RX, "%s: rxpb[%x] skb[%x] cbm[%x]\n", __func__, rxpb_ptr, skb, skb->head); */
#ifdef DL_DIRECT_ENQ_CBM
		/* cbm_buffer_free(smp_processor_id(),skb->head, 0); */
		/* free the cbm */
		dealloc_skb_tx(skb);
#else
		dev_kfree_skb_any(skb);
#endif
	} else
		dtlk_debug(DBG_ERR, "%s: freed by replenish or invalid\n", __func__);

	return DTLK_SUCCESS;
}
EXPORT_SYMBOL(ppa_dl_dre_rxpb_buf_free);

extern int dtlk_get_subif_from_vap_id(
	PPA_SUBIF *subIf, int vap_id
	);
#ifndef RX_NOT_WRITEBACK
static uint32_t oldPointer;
#endif
#ifdef DL_DIRECT_ENQ_CBM
struct dma_rx_desc_1 dma_rx_desc_mask1;
struct dma_rx_desc_3 dma_rx_desc_mask3;
struct dma_rx_desc_0 dma_tx_desc_mask0;
struct dma_rx_desc_1 dma_tx_desc_mask1;
#define MY_SET_PMAC_SUBIF(pmac, subif) do {;\
	pmac->src_sub_inf_id2 = (subif) & 0xff; \
	pmac->src_sub_inf_id =  ((subif) >> 8) & 0x1f; } \
	while (0)
#define MY_SET_PMAC_PORTMAP(pmac, port_id) do { if (port_id <= 7)\
			pmac->port_map2 = 1 << (port_id); \
			else \
				pmac->port_map = (1 << (port_id-8)); } \
			while (0)

extern uint8_t get_lookup_qid_via_index(uint32_t index);
void my_dump_tx_dma_desc(struct dma_tx_desc_0 *desc_0,
					  struct dma_tx_desc_1 *desc_1,
					  struct dma_tx_desc_2 *desc_2,
					  struct dma_tx_desc_3 *desc_3)
{
	int lookup;

	if (!desc_0 || !desc_1 || !desc_2 || !desc_3) {
		dtlk_debug(DBG_ERR, "tx desc_0/1/2/3 NULL\n");
		return;
	}

	dtlk_debug(DBG_TX, " DMA Descripotr:D0=0x%08x D1=0x%08x D2=0x%08x D3=0x%08x\n",
			*(uint32_t *) desc_0, *(uint32_t *) desc_1,
			*(uint32_t *) desc_2, *(uint32_t *) desc_3);
	dtlk_debug(DBG_TX,
		"  DW0:resv0=%d tunnel_id=%02d flow_id=%d eth_type=%d dest_sub_if_id=0x%04x\n",
	 desc_0->field.resv0, desc_0->field.tunnel_id,
	 desc_0->field.flow_id, desc_0->field.eth_type,
	 desc_0->field.dest_sub_if_id);
	dtlk_debug(DBG_TX,
	"  DW1:session_id=0x%03x tcp_err=%d nat=%d dec=%d enc=%d mpe2=%d mpe1=%d \n",
	 desc_1->field.session_id, desc_1->field.tcp_err,
	 desc_1->field.nat, desc_1->field.dec, desc_1->field.enc,
	 desc_1->field.mpe2, desc_1->field.mpe1);
	dtlk_debug(DBG_TX, "  color=%02d ep=%02d resv1=%d classid=%02d\n",
			desc_1->field.color, desc_1->field.ep, desc_1->field.resv1,
			desc_1->field.classid);
	dtlk_debug(DBG_TX, "  DW2:data_ptr=0x%08x\n", desc_2->field.data_ptr);
	dtlk_debug(DBG_TX, "  DW3:own=%d c=%d sop=%d eop=%d dic=%d pdu_type=%d\n",
			desc_3->field.own, desc_3->field.c, desc_3->field.sop,
			desc_3->field.eop, desc_3->field.dic, desc_3->field.pdu_type);
	dtlk_debug(DBG_TX,
	"  byte_offset=%d atm_qid=%d mpoa_pt=%d mpoa_mode=%d data_len=% 4d\n",
	 desc_3->field.byte_offset, desc_3->field.qid,
	 desc_3->field.mpoa_pt, desc_3->field.mpoa_mode,
	 desc_3->field.data_len);
	lookup = ((desc_0->field.flow_id >> 6) << 12) |
			 ((desc_1->field.dec) << 11) |
			 ((desc_1->field.enc) << 10) |
			 ((desc_1->field.mpe2) << 9) |
			 ((desc_1->field.mpe1) << 8) |
			 ((desc_1->field.ep) << 4) |
			 ((desc_1->field.classid) << 0);
	dtlk_debug(DBG_TX, "  lookup index=0x%x qid=%d\n", lookup,
			get_lookup_qid_via_index(lookup));
}
void my_dump_tx_pmac(struct pmac_tx_hdr *pmac)
{
	int i;
	unsigned char *p = (char *)pmac;

	if (!pmac) {
		dtlk_debug(DBG_ERR, "dump_tx_pmac pmac NULL ??\n");
		return ;
	}

	dtlk_debug(DBG_TX, " PMAC at 0x%p:", p);

	for (i = 0; i < 8; i++)
		dtlk_debug(DBG_TX, "0x%02x ", p[i]);

	dtlk_debug(DBG_TX, "\n");
	/*byte 0 */
	dtlk_debug(DBG_TX, "  byte 0:tcp_chksum=%d res=%d ip_offset=%d\n",
			pmac->tcp_chksum, pmac->res1, pmac->ip_offset);
	/*byte 1 */
	dtlk_debug(DBG_TX, "  byte 1:tcp_h_offset=%d tcp_type=%d\n", pmac->tcp_h_offset,
			pmac->tcp_type);
	/*byte 2 */
	dtlk_debug(DBG_TX, "  byte 2:ppid=%d res=%d\n", pmac->sppid, pmac->res);
	/*byte 3 */
	dtlk_debug(DBG_TX,
	"  byte 3:port_map_en=%d res=%d time_dis=%d class_en=%d res=%d pkt_type=%d\n",
	 pmac->port_map_en, pmac->res2, pmac->time_dis, pmac->class_en,
	 pmac->res3, pmac->pkt_type);
	/*byte 4 */
	dtlk_debug(DBG_TX,
	"  byte 4:fcs_ins_dis=%d redirect=%d time_stmp=%d src_sub_inf_id=%d\n",
	 pmac->fcs_ins_dis, pmac->redirect, pmac->time_stmp,
	 pmac->src_sub_inf_id);
	/*byte 5 */
	dtlk_debug(DBG_TX, "  byte 5:src_sub_inf_id2=%d\n", pmac->src_sub_inf_id2);
	/*byte 6 */
	dtlk_debug(DBG_TX, "  byte 6:port_map=%d\n", pmac->port_map);
	/*byte 7 */
	dtlk_debug(DBG_TX, "  byte 7:port_map2=%d\n", pmac->port_map2);
}
extern void dp_dump_raw_data(char *buf, int len, char *prefix_str);

extern int cbm_setup_desc(struct cbm_desc *desc, uint32_t data_ptr, uint32_t data_len,
uint32_t DW0, uint32_t DW1);
extern int cbm_cpu_enqueue(uint32_t pid, struct cbm_desc *desc);
extern int cbm_cpu_enqueue_dl(uint32_t pid, struct cbm_desc *desc);
#ifndef RX_NOT_WRITEBACK
int32_t
dl_cbm_cpu_pkt_tx(
	struct sk_buff *skb,
	int ep,
	int subif,
	unsigned int data_ptr,
	unsigned int pmac_header_ptr,
	int flags
	)
{
	struct cbm_desc desc;
	uint32_t tmp_data_ptr;
	unsigned int txLen = skb->len + 8;
	unsigned int subIf = (subif << 8);
	struct pmac_tx_hdr *pmac = NULL;
	struct dma_tx_desc_0 *desc_0;
	struct dma_tx_desc_1 *desc_1;
	struct dma_tx_desc_2 *desc_2;
	struct dma_tx_desc_3 *desc_3;
	tmp_data_ptr = pmac_header_ptr;
	pmac = pmac_header_ptr;
	memset(pmac, 0, sizeof(struct pmac_tx_hdr));

	desc_0 = (struct dma_tx_desc_0 *)&skb->DW0;
	desc_1 = (struct dma_tx_desc_1 *)&skb->DW1;
	desc_2 = (struct dma_tx_desc_2 *)&skb->DW2;
	desc_3 = (struct dma_tx_desc_3 *)&skb->DW3;
	/* step1 : set pmac */
	if (flags == 0) {
		pmac->port_map_en = 0;
		pmac->port_map = 0xff;
		pmac->port_map2 = 0xff;
		pmac->sppid = ep;

		MY_SET_PMAC_SUBIF(pmac, subIf);
	} else {
		pmac->port_map_en = 1;
		MY_SET_PMAC_PORTMAP(pmac, (ep));
		pmac->sppid = PMAC_CPU_ID;
		MY_SET_PMAC_SUBIF(pmac, subIf);
	}
	/* step 2: set DMA */
	/*reset all descriptors as SWAS required since SWAS 3.7 */
	/*As new SWAS 3.7 required, MPE1/Color/FlowID is set by applications */
	desc_0->all &=  dma_tx_desc_mask0.all;
	desc_1->all &=  dma_tx_desc_mask1.all;
	desc_2->all = 0;
	desc_3->all = 0;

	desc_1->field.classid = 0;
	desc_2->field.data_ptr = tmp_data_ptr;
	if (flags == 0) {
		desc_1->field.enc = 1;
		desc_1->field.dec = 1;
		desc_1->field.mpe2 = 0;
	} else {
		desc_1->field.enc = 0;
		desc_1->field.dec = 0;
		desc_1->field.mpe2 = 0;
	}
	desc_1->field.ep = ep;

	desc_0->all = subIf;
	/* len */
	desc_3->field.data_len = txLen;
	ppa_dl_dre_dma_writeback(pmac, 8);
	#if 0
	if (flags) {
		dtlk_debug(DBG_RX, "==========================Forward==========================");
		}
	else
		dtlk_debug(DBG_RX, "==========================Not Forward==========================");
	my_dump_tx_dma_desc((struct dma_tx_desc_0 *)&skb->DW0,
							 (struct dma_tx_desc_1 *)&skb->DW1,
							 (struct dma_tx_desc_2 *)&skb->DW2,
							 (struct dma_tx_desc_3 *)&skb->DW3);
	my_dump_tx_pmac(pmac);

	dp_dump_raw_data(tmp_data_ptr,
							 (txLen > 256) ? txLen : 256,
							 "Tx Orig Data");

	#endif
	if (cbm_setup_desc ((struct cbm_desc *) &desc, tmp_data_ptr,
			(txLen < (ETH_ZLEN + 8)) ? (ETH_ZLEN + 8) : txLen,
			skb->DW1, skb->DW0)) {
		dtlk_debug(DBG_ERR, "cbm setup desc failed..\n");
		/* cbm_buffer_free(smp_processor_id(), skb->head, 0); */
		dealloc_skb_tx(skb);
		/*spin_unlock_irqrestore(&cbm_tx_lock, sys_flag);*/
		return -1;
	}
	if (cbm_cpu_enqueue_dl(smp_processor_id(), &desc) == CBM_FAILURE) {
		dtlk_debug(DBG_ERR, "cpu enqueue failed..\n");
		/* cbm_buffer_free(smp_processor_id(), skb->head, 0); */
		dealloc_skb_tx(skb);
		return -1;
	}
	/* free this skb. */
	kfree(skb);
	return 0;
}
#else

inline int32_t
dl_cbm_cpu_pkt_tx_no_skb(
	int data_len,
	int ep,
	int subif,
	unsigned int rxpb_ptr,
	unsigned int pmac_header_ptr,
	int flags
	)
{
	struct cbm_desc desc;
	uint32_t tmp_data_ptr;
	unsigned int txLen = data_len + 8;
	unsigned int subIf = (subif << 8);
	struct pmac_tx_hdr *pmac = NULL;
	struct dma_tx_desc_0 desc_0;
	struct dma_tx_desc_1 desc_1;
	/* struct dma_tx_desc_2 desc_2; */
	/* struct dma_tx_desc_3 desc_3; */
	/*
	dtlk_debug(DBG_TX, "<%s>: ep[%d] subif[%d] [%x] rxpb_ptr[0x%x] pmac_header_ptr[0x%x]\n",
	__func__, ep, subif, subIf, rxpb_ptr, pmac_header_ptr);
	*/
	/*pmac_header_ptr = (pmac_header_ptr & 0x0fffffff) | KSEG0;*/
	tmp_data_ptr = pmac_header_ptr;
	pmac = (struct pmac_tx_hdr *)pmac_header_ptr;
	memset(pmac, 0, sizeof(struct pmac_tx_hdr));
	/* step1 : set pmac */
	if (flags == 0) {
		/*pmac->port_map_en = 0;*/
		pmac->port_map = 0xff;
		pmac->port_map2 = 0xff;
		pmac->sppid = ep;

		MY_SET_PMAC_SUBIF(pmac, subIf);
	} else {
		pmac->port_map_en = 1;
		MY_SET_PMAC_PORTMAP(pmac, (ep));
		pmac->sppid = PMAC_CPU_ID;
		MY_SET_PMAC_SUBIF(pmac, subIf);
	}
	/* step 2: set DMA */
	/*reset all descriptors as SWAS required since SWAS 3.7 */
	/*As new SWAS 3.7 required, MPE1/Color/FlowID is set by applications */
	/*desc_0.all &=  dma_tx_desc_mask0.all;*/
	desc_1.all &=  dma_tx_desc_mask1.all;
	/* desc_2.all = 0; */
	/* desc_3.all = 0; */

	/*desc_1.field.classid = 0;*/
	/*desc_2.field.data_ptr = tmp_data_ptr;*/
	if (flags == 0) {
		desc_1.field.enc = 1;
		desc_1.field.dec = 1;
		/*desc_1.field.mpe2 = 0;*/
	} else {
		/*desc_1.field.enc = 0;*/
		/*desc_1.field.dec = 0;*/
		/*desc_1.field.mpe2 = 0;*/
	}
	desc_1.field.ep = ep;

	desc_0.all = subIf;
	/*len*/
	/*desc_3.field.data_len = txLen;*/
	/*ppa_dl_dre_dma_writeback(pmac,8);*/
	/*rxpb_ptr = (rxpb_ptr & 0x0fffffff) | KSEG0;*/
	/*dtlk_debug(DBG_RX, "%s: writeback rxpb_ptr[%x] pmac[%x] len[%d]\n",
		__func__,
		rxpb_ptr,
		pmac,
		(pmac_header_ptr - rxpb_ptr) + 8);*/
	ppa_dl_dre_dma_writeback(rxpb_ptr, (pmac_header_ptr - rxpb_ptr) + 8);

	if (cbm_setup_desc ((struct cbm_desc *) &desc, tmp_data_ptr,
			(txLen < (ETH_ZLEN + 8)) ? (ETH_ZLEN + 8) : txLen,
			desc_1.all, desc_0.all)) {
		dtlk_debug(DBG_ERR, "cbm setup desc failed..\n");
		/* cbm_buffer_free(smp_processor_id(), skb->head, 0);*/
		dealloc_skb_tx((struct sk_buff *)rxpb_ptr);
		/*spin_unlock_irqrestore(&cbm_tx_lock, sys_flag);*/
		return -1;
	}
	#if 0
	if (cbm_cpu_enqueue_dl(smp_processor_id(), &desc) == CBM_FAILURE) {
		dtlk_debug(DBG_ERR, "cpu enqueue failed..\n");
		/*cbm_buffer_free(smp_processor_id(), skb->head, 0);*/
		dealloc_skb_tx(data_ptr);
		return -1;
	}
	#else
	if (1) {
		uint32_t data_pointer, pointer_to_wb;
		pointer_to_wb = desc.desc2 & 0xfffff800;
		data_pointer = ((pointer_to_wb & 0x0fffffff) | 0x20000000);
		data_pointer += desc.desc2 - pointer_to_wb;
		#if 0
	if (flags) {
		dtlk_debug(DBG_TX, "==========================Forward==========================");
		}
	else
		dtlk_debug(DBG_TX, "==========================Not Forward==========================");

	my_dump_tx_dma_desc((struct dma_tx_desc_0 *)&(desc.desc0),
							 (struct dma_tx_desc_1 *)&(desc.desc1),
							 (struct dma_tx_desc_2 *)&(desc.desc2),
							 (struct dma_tx_desc_3 *)&(desc.desc3));
	my_dump_tx_pmac(pmac);
/*
	dp_dump_raw_data(tmp_data_ptr,
							 (txLen > 256) ? txLen : 256,
							 "Tx Orig Data");
	*/
	#endif
#ifdef PROFILING
		CycleCounter_Start(profiling_cmb_enqueue);
#endif

		/* == AVM/CMH 20160418 Directlink rx packet trace == */
#if 0
		if (g_dlrx_qca_cb.avm_directlink_trace_rx_fn) {
			uint8_t *data = desc.desc2;

			struct sk_buff *skb = dev_alloc_skb(2000);
			skb_put(skb, data_len);
			memcpy(skb->data, data + 14, data_len);

			uint32_t vap_id = avm_get_vap_by_subif(subif & 0xffff);
			struct net_device *dev = dtlk_dev_from_vapid(vap_id);

			if (dev) {
				skb->dev = dev;
				g_dlrx_qca_cb.avm_directlink_trace_rx_fn(dev, skb);
			}

			dev_kfree_skb_any(skb);
		}
#endif

		if (cbm_cpu_enqueue_hw(smp_processor_id(), (struct cbm_desc *)&desc, data_pointer, 0) == CBM_FAILURE) {
			dtlk_debug(DBG_ERR, "cpu enqueue failed..\n");
			/*cbm_buffer_free(smp_processor_id(), skb->head, 0);*/
			dealloc_skb_tx((struct sk_buff *)rxpb_ptr);
			return -1;
		}
#ifdef PROFILING
		CycleCounter_End(profiling_cmb_enqueue);
#endif
	}
	#endif
	/*free this skb.*/
	/*kfree(skb);*/
	return 0;
}

#endif

#endif

int ppa_dl_dre_gswip_dma_send(
	unsigned int vap_id,
	unsigned int rxpb_ptr,
	unsigned int data_ptr,
	unsigned int data_len,
	unsigned int release_flag,
	unsigned int pmac_hdr_ptr,
	unsigned int unmap_type
	)
{
#ifndef RX_NOT_WRITEBACK
	struct sk_buff *skb, *skb2;
#endif
	PPA_SUBIF dp_sub_if;
	struct net_device *dev;
	int32_t res = 0;
	uint32_t flags = 0;

	int header_len;

	/*dtlk_debug(DBG_RX, "%s: rxpb_ptr[%x] data_ptr[0x%x]\n",__func__, rxpb_ptr, data_ptr);*/
#ifdef PROFILING
	CycleCounter_Start(profiling_enqueue);
#endif
	/* get Datapath sub interface for this VAP ID */
	dp_sub_if = gQuickRefSubIfFromVAPID[vap_id & 0xffff];
	/* get original skb pointer */
	#ifndef RX_NOT_WRITEBACK
	skb = dlrx_get_skb_ptr(rxpb_ptr);
	/* Fix Klockwork check */
	if (skb == NULL)
		return DTLK_FAILURE;
	if ((uint32_t)skb < KSEG0 || (uint32_t)skb >= KSEG1) {
		dtlk_debug(DBG_ERR, "%s:invalid skb - skb = %#08x, rxpb_ptr = %#08x\n", __func__,
			(unsigned int)skb, rxpb_ptr
		);
		return DTLK_FAILURE;
	}
	#endif
	/*dtlk_debug(DBG_RX, "%s: release_flag[%d] data_ptr[0x%x]\n",__func__, release_flag, data_ptr);*/
	/*dev = dtlk_dev_from_vapid((uint32_t)vap_id);*/
	header_len = data_ptr - rxpb_ptr;
	/*dtlk_debug(DBG_RX, "%s: header[%d] rxpb_ptr[%x] data_ptr[%x]\n", __func__, header_len,rxpb_ptr , data_ptr);*/
	if (!release_flag) {
		#if 0
		/* do not release the packet
		*
		* */
		unsigned int real_data_ptr = rxpb_ptr + g_dlrx_cfg_offset_atten;
		unsigned padding = (vap_id >> 16);
		real_data_ptr = real_data_ptr + padding;
		ppa_dl_dre_dma_invalidate(rxpb_ptr, header_len + data_len);

		/* Get new CBM skb buffer */
		skb2 = alloc_skb_rx();
		if (!skb2) {
			dtlk_debug(DBG_ERR, "alloc_skb_rx failed");
			return DTLK_FAILURE;
		}
		new_buf = skb2;

		header_len = header_len - padding;
		/* Copy header and data, don't copy padding */
		memcpy(new_buf + RX_RESERVE_BYTES, rxpb_ptr, header_len);
		memcpy(new_buf + RX_RESERVE_BYTES + header_len, data_ptr, data_len);
		flags = DP_TX_TO_DL_MPEFW;
		new_data_buf = new_buf + RX_RESERVE_BYTES + header_len;
		ppa_dl_dre_dma_writeback(new_buf + RX_RESERVE_BYTES, header_len + data_len);
		dtlk_debug(DBG_RX, "    NOT REL: data 0x%x 0x%x %d %d padding[%d]\n", new_buf + RX_RESERVE_BYTES, rxpb_ptr, header_len, data_len, padding);
		#else

		#endif
	}
	#ifndef RX_NOT_WRITEBACK
	else {
		skb2 = skb;
		/* Fix Klockwork check */
		if (skb2 == NULL)
			return DTLK_FAILURE;
		if (oldPointer == skb2) {
			dtlk_debug(DBG_ERR, "Oh my, the same pointer with release 1,EXIT\n");
			return DTLK_FAILURE;
			}
		else
			oldPointer = skb2;
		if (data_len < 60) {
			/* dtlk_debug(DBG_ERR, "%s: small packet -> 60\n", __func__); */
			data_len = 60;
			}
		/*dtlk_debug(DBG_RX, "%s: 1 skb2->data[0x%x] data_len[%d]\n",
			__func__, skb2->data, data_len);*/
		/*skb2->data_len = data_len;*/
		#if 0
		flags = 0;
		skb2->ip_summed = CHECKSUM_UNNECESSARY;
		skb2->dev = dev;
		#endif
		skb2->data = data_ptr;
		skb2->tail = data_ptr;
		skb_put(skb2, data_len);
		/*16/6/2015: performance here*/
		/*ppa_dl_dre_dma_invalidate(data_ptr, data_len);*/
	}
	#endif
	vap_id = vap_id & 0xffff;
	if (!dtlk_get_subif_from_vap_id(&dp_sub_if, vap_id)) {
		/*dtlk_debug(DBG_RX, "%s: shift skb2->data[0x%x] vap_id[%d]  p[%d] if[%d]\n",
			__func__, skb2->data,
			vap_id, dp_sub_if.port_id,
			dp_sub_if.subif);*/
		/* Using datapath driver to send packet to switch */
		/* SET EP */
#if 0 /*16/6/2015: performance here*/
		skb2->DW1 = (skb->DW1 & (~0xF00)) |
			((dp_sub_if.port_id & 0xF) << 8);
		/* SET SUBIFID */
		skb2->DW0 = (skb->DW0 & ~0x7FFF) | (dp_sub_if.subif << 8);
#ifndef CONFIG_MIPS_UNCACHED
		ppa_dl_dre_dma_writeback((u32)skb2->data, skb2->len);
#endif
#endif
		/*+3 to invalidate attention config change as well.*/
		/*dma_cache_wback(skb2->data, skb2->len);*/
		if (release_flag) {
			/*
			if (skb2->prev || skb2->next){
				dtlk_debug(DBG_RX, "%s: skb[0x%x] has next[0x%x] prev[0x%x]\n",
				__func__, skb2,
				skb2->next, skb2->prev);
				skb2->next = 0;
				skb2->prev = 0;
				}
				*/
#ifndef DL_DIRECT_ENQ_CBM
			res = ppa_hook_directpath_ex_send_fn(
				&dp_sub_if,
				skb2,
				data_len,
				0);
#else
			/*direct enqueue to CBM buffer*/
			#ifndef RX_NOT_WRITEBACK
			if (dl_cbm_cpu_pkt_tx(skb, dp_sub_if.port_id,
				dp_sub_if.subif,
				data_ptr,
				data_ptr - 8,
				0) == -1) {
				dtlk_debug(DBG_ERR, "Cannot send to dl_cbm_cpu_pkt_tx");
				return DTLK_FAILURE;
			}
			#else

			/* == AVM/CMH 20160531 Intra-VAP unicast communication == */
#if 1
			/*
			 * release_flag == 1 && unmap_type == 1 indicates "to MPE-fw"
			 */
			if (unmap_type == 1) {
				flags = DP_TX_TO_DL_MPEFW;
			}
#endif

			if (dl_cbm_cpu_pkt_tx_no_skb(data_len,
				dp_sub_if.port_id,
				dp_sub_if.subif,
				rxpb_ptr,
				data_ptr - 8,
				flags) == -1) {
				dtlk_debug(DBG_ERR, "Cannot send to dl_cbm_cpu_pkt_tx");
				return DTLK_FAILURE;
			}
			#endif
#endif
		} else {
#if 1
			/* create new skb data */
			unsigned char *skb_data;
			struct sk_buff *skb2;
			dev = dtlk_dev_from_vapid(vap_id & 0xffff);
			if (!dev)
				dtlk_debug(DBG_ERR, "%s: Invalid device for vap id[0x%x]\n", __func__, vap_id);
			else {
				skb2 = alloc_skb(data_len, GFP_KERNEL);
				if (!skb2) {
					dtlk_debug(DBG_ERR, "alloc_skb_rx failed");
					return DTLK_FAILURE;
				}
				skb_data = skb_put(skb2, data_len);
				memset(skb_data, 0, data_len);
				memcpy(skb_data, (void *)data_ptr, data_len);
				skb2->dev = dev;
				res = ppa_hook_directpath_ex_send_fn(&dp_sub_if, skb2, data_len, 1);
			}
#else
			/*direct enqueue to CBM buffer*/
			#ifndef RX_NOT_WRITEBACK
			if (dl_cbm_cpu_pkt_tx(skb2, dp_sub_if.port_id, dp_sub_if.subif, skb2->data, skb2->data - 8, flags) == -1) {
				dtlk_debug(DBG_ERR, "Cannot send to dl_cbm_cpu_pkt_tx");
				return DTLK_FAILURE;
			}
			#else
			if (dl_cbm_cpu_pkt_tx_no_skb(data_len, dp_sub_if.port_id, dp_sub_if.subif, new_buf + RX_RESERVE_BYTES, new_data_buf - 8, flags) == -1) {
				dtlk_debug(DBG_ERR, "Cannot send to dl_cbm_cpu_pkt_tx");
				return DTLK_FAILURE;
			}
			#endif
#endif
		}

		if (res == DP_SUCCESS) {
		} else {
			dtlk_debug(DBG_ERR, "Cannot send to Datapath");
			return DTLK_FAILURE;
		}
	} else {
		dtlk_debug(DBG_ERR, "Cannot get sub interface of VAP[%d]\n", vap_id);
		return DTLK_FAILURE;
	}
#ifdef PROFILING
	CycleCounter_End(profiling_enqueue);
#endif
	return DTLK_SUCCESS;
}
EXPORT_SYMBOL(ppa_dl_dre_gswip_dma_send);

int ppa_dl_dre_ps_send(
	unsigned int rxpb_ptr,
	unsigned int data_ptr,
	unsigned int data_len,
	unsigned int vap_id
	)
{
	struct sk_buff *skb;
	struct net_device *dev;
	skb = dlrx_skb_setup(rxpb_ptr, data_ptr, data_len);

	if (!skb)
		return DTLK_FAILURE;

	dev = dtlk_dev_from_vapid((uint32_t)vap_id);
	/* sanity check */
	if (!dev) {
		dtlk_debug(DBG_ERR, "No valid device pointer!!!\n");
		dev_kfree_skb_any(skb);
		return DTLK_FAILURE;
	}
	skb->protocol = eth_type_trans(skb, dev);
	dtlk_debug(DBG_RX, "%s - skb: 0x%x, dev_name: %s, rxpb_ptr: 0x%x, data_ptr: 0x%x, data_len: 0x%x, vap_id: 0x%x, protocol: %d\n",
		__func__, (uint32_t)skb, dev->name, rxpb_ptr,
		(uint32_t)skb->data, skb->len, vap_id, skb->protocol);

	dump_skb(skb, skb->len, "Send To Protocol Stack");

	/* == AVM/CMH 20160418 Directlink slowpath rx packet trace == */
#if 1
	if (g_dlrx_qca_cb.avm_directlink_trace_rx_fn) {
		g_dlrx_qca_cb.avm_directlink_trace_rx_fn(dev, skb);
	}
#endif

	if (netif_rx(skb) == NET_RX_DROP) {
		dtlk_debug(DBG_ERR, "Cannot send to Protocol Stack\n");
		dev_kfree_skb_any(skb);
		return DTLK_FAILURE;
	}

	return DTLK_SUCCESS;
}
EXPORT_SYMBOL(ppa_dl_dre_ps_send);

/*
 *	DLRX FW send pkt to WLAN driver
 */
int ppa_dl_dre_wlan_pkt_send(
	unsigned int rxpb_ptr,
	unsigned int data_len,
	unsigned int pkt_status,
	unsigned int msg_ptr,
	unsigned int vap_id,
	unsigned int flags
	)
{
	struct sk_buff *skb;
	int ret = DTLK_FAILURE;
	unsigned padding = (flags >> 16);
	/* TODO: James add ( 23Jun) */
	skb = dlrx_skb_setup(rxpb_ptr,
			rxpb_ptr + padding,
			data_len + g_dlrx_cfg_offset_atten
			);
	/* skb = dlrx_skb_setup(rxpb_ptr, rxpb_ptr, data_len); */
	if (!skb)
		return DTLK_FAILURE;


#ifndef CONFIG_MIPS_UNCACHED
	/*dma_cache_inv((u32)skb->data, data_len);*/
	ppa_dl_dre_dma_invalidate((u32)skb->data, data_len);
#endif
	dtlk_debug(DBG_CPU, "%s, rxpb_ptr: 0x%x, data_len: 0x%x, pkt_status: 0x%x, msg_ptr: 0x%x, flags: 0x%x\n",
		__func__, rxpb_ptr, data_len, pkt_status, msg_ptr, flags);
	/* QCA will consume the CBM skb */
	if (likely(g_dlrx_qca_cb.rx_splpkt_fn))
		ret = g_dlrx_qca_cb.rx_splpkt_fn(g_dlrx_handle,
				pkt_status,
				data_len,
				skb,
				(uint32_t *)msg_ptr,
				flags
				);
	else {
		dtlk_debug(DBG_ERR, "No Special PKT fn handler!");
		dev_kfree_skb_any(skb);
	}
	/*
	   * The QCA does not pass the packet to protocol stack.
	   * DTLK will create new skb buffer for good packet and send it to protocol stack.
	   */
	if (flags) {
		/* Get the data pointer */
		unsigned int data_ptr = rxpb_ptr + g_dlrx_cfg_offset_atten;
		struct net_device *dev;
		struct sk_buff *skb3 = dev_alloc_skb(data_len + 32);

		data_ptr += padding;

		if (skb3 == NULL)
			dtlk_debug(DBG_ERR, "%s: Cannot alloc [%d]\n",
				__func__,
				data_len + 32);
		else {
			dev = dtlk_dev_from_vapid((uint32_t)vap_id);
			if (dev) {
				memcpy(skb_put(skb3, data_len), (void *)data_ptr, data_len);
				skb3->dev = dev;
				skb3->protocol = eth_type_trans(skb3, dev);
				skb3->ip_summed = CHECKSUM_UNNECESSARY;

				/* == AVM/CMH 20160418 Directlink slowpath rx packet trace == */
#if 1
				if (g_dlrx_qca_cb.avm_directlink_trace_rx_fn) {
					g_dlrx_qca_cb.avm_directlink_trace_rx_fn(dev, skb3);
				}
#endif

				if (netif_rx(skb3) != NET_RX_DROP) {
					dtlk_debug(DBG_CPU, "%s: Send PS successfully\n",
						__func__);
				} else {
					dev_kfree_skb_any(skb3);
					dtlk_debug(DBG_ERR, "%s: Cannot send to PS\n",
						__func__);
				}
			}
		}
	}
	return ret;
}
EXPORT_SYMBOL(ppa_dl_dre_wlan_pkt_send);

/*
 *	DLRX FW send msg to WLAN driver
 */
int ppa_dl_dre_wlan_msg_send(
	unsigned int msg_type,
	unsigned int msg_ptr,
	unsigned int msg_len,
	unsigned int flags
	)
{
	int ret = DTLK_FAILURE;
	if (likely(g_dlrx_qca_cb.rx_msg_fn)) {
		/*
		dtlk_debug(DBG_CPU, "%s, msg_type: %d, msg_len: %d, msg_ptr: 0x%x, flags: 0x%x\n",
		__func__, msg_type, msg_len, msg_ptr, flags);
		*/
		ret = g_dlrx_qca_cb.rx_msg_fn(g_dlrx_handle,
				msg_type,
				msg_len,
				(uint32_t *)msg_ptr,
				flags
				);
	} else
		dtlk_debug(DBG_ERR, "No Message fn handler!");

	return ret;
}
EXPORT_SYMBOL(ppa_dl_dre_wlan_msg_send);

void ppa_dl_dre_peer_act_fn(unsigned int peer_id)
{
	if (g_dlrx_qca_cb.peer_act_fn)
		g_dlrx_qca_cb.peer_act_fn(g_dlrx_handle, peer_id);
}
EXPORT_SYMBOL(ppa_dl_dre_peer_act_fn);

/*
*  DLRX register function
*/
int ppa_dl_dre_fn_register(unsigned int fntype, void *func)
{
	switch (fntype) {
	case DRE_MAIN_FN:
		g_dre_fnset.dre_dl_main_fn = func;
		break;

	case DRE_GET_VERSION_FN:
		g_dre_fnset.dre_dl_getver_fn = func;
		break;

	case DRE_RESET_FN:
		g_dre_fnset.dre_dl_reset_fn = func;
		break;

	case DRE_GET_MIB_FN:
		g_dre_fnset.dre_dl_getmib_fn = func;
		break;

	case DRE_GET_CURMSDU_FN:
		g_dre_fnset.dre_dl_getmsdu_fn = func;
		break;

	case DRE_SET_MEMBASE_FN:
		g_dre_fnset.dre_dl_set_membase_fn = func;
		break;

	case DRE_SET_RXPN_FN:
		g_dre_fnset.dre_dl_set_rxpn_fn = func;
		break;

	case DRE_SET_DLRX_UNLOAD:
		g_dre_fnset.dre_dl_set_dlrx_unload_t = func;
		break;
	case DRE_MAX_FN:
	default:
		dtlk_debug(DBG_ERR, "Register NO is Not valid:%d", fntype);
		return DTLK_FAILURE;
	}
	return DTLK_SUCCESS;
}
EXPORT_SYMBOL(ppa_dl_dre_fn_register);

/*
 * Get Peer value from PeerID
 * Return Peer Valid
 */
int ppa_dl_dre_peer_from_peerid(
	unsigned int peerid,
	unsigned int *peer
	)
{
	volatile unsigned int *pid2p_tbl = DLRX_CFG_PEER_ID_TO_PEER_MAP_BASE(0);
	unsigned int idx = peerid >> 2;
	unsigned int offset = peerid % 4;
	unsigned int peer_val;
	int result;

	if (peerid >= MAX_PEERID_NUM)
		return PEER_INVALID;

	spin_lock_bh(&g_prid2pr_tbl_lock);
	peer_val = *(pid2p_tbl + idx);
	spin_unlock_bh(&g_prid2pr_tbl_lock);

	peer_val = (peer_val >> (offset << 3));
	*peer = peer_val & 0x7F;
	result = ((peer_val >> 7) & 0x1);
	return result;
}
EXPORT_SYMBOL(ppa_dl_dre_peer_from_peerid);

extern void mpe_hal_dl_enable_gic(int irq_no);

void ppa_dl_qca_ipi_interrupt(void)
{
#if 0
	dtlk_debug(DBG_TX, "%s:ipi irq [%d]\n", __func__, g_dl_buf_info.DlCommmIpi);
	if (gTestIPI == 0)
		mpe_hal_dl_enable_gic(g_dl_buf_info.DlCommmIpi);
	else {
		dtlk_debug(DBG_TX, "%s:Test disable ipi irq [%d]\n", __func__, g_dl_buf_info.DlCommmIpi);
	}
#else
	/*dtlk_debug(DBG_TX, "%s:ipi irq [%d]\n", __func__, g_dl_buf_info.DlCommmIpi);*/
	mpe_hal_dl_enable_gic(g_dl_buf_info.DlCommmIpi);
#endif
}
EXPORT_SYMBOL(ppa_dl_qca_ipi_interrupt);

int32_t ppa_dl_qca_clear_stats(
	uint32_t vapId,
	uint32_t flags
	)
{
	dre_dl_reset_fn_t dre_dl_reset_fn;
	dre_dl_reset_fn = dlrx_get_dre_fn(DRE_RESET_FN);
	return DTLK_FAILURE;
}
EXPORT_SYMBOL(ppa_dl_qca_clear_stats);

/*
 * Set VAP info
 * Return -1 if peer or vapid out of range
 */
int ppa_dl_dre_vapinfo_set(
	unsigned int peer,
	unsigned int vapid,
	unsigned int sec_type,
	unsigned int acc_dis
	)
{
	volatile unsigned int *vapinfo_tbl = DLRX_CFG_PEER_TO_VAP_PN_BASE(0);
	unsigned int vapinfo;

	if (vapid >= MAX_VAP_NUM || peer >= MAX_PEER_NUM)
		return DTLK_FAILURE;

	vapinfo = ((acc_dis & 0x1) << 6) |
				((sec_type & 0x3) << 4) |
				(vapid & 0xF);
	spin_lock_bh(&g_pr2vap_tbl_lock);
	*(vapinfo_tbl + peer) = vapinfo;
	spin_unlock_bh(&g_pr2vap_tbl_lock);

	return DTLK_SUCCESS;
}
EXPORT_SYMBOL(ppa_dl_dre_vapinfo_set);


/*
 *	Get VAP info from Peer
 *	Return -1 if peer or vap id out of range
 */
int ppa_dl_dre_vapinfo_from_peer(
	unsigned int peer,
	unsigned int *vapid,
	unsigned int *sec_type,
	unsigned int *acc_dis
	)
{
	volatile unsigned int *vapinfo_tbl = DLRX_CFG_PEER_TO_VAP_PN_BASE(0);
	unsigned int vapinfo;

	if (peer >= MAX_PEER_NUM)
		return DTLK_FAILURE;

	spin_lock_bh(&g_pr2vap_tbl_lock);
	vapinfo = *(vapinfo_tbl + peer);
	spin_unlock_bh(&g_pr2vap_tbl_lock);

	*vapid	  = vapinfo & 0xf;
	*sec_type = (vapinfo >> 4) & 0x3;
	*acc_dis  = (vapinfo >> 6) & 0x1;

	return DTLK_SUCCESS;
}
EXPORT_SYMBOL(ppa_dl_dre_vapinfo_from_peer);

/*
 * Get interface id from VAP id
 */
unsigned int ppa_dl_dre_itf_from_vapid(
	unsigned int vap_id
	)
{
	volatile unsigned int *itf_tbl;
	volatile unsigned int itf_id;

	if (vap_id >= MAX_VAP_NUM)
		return DTLK_INVALID_ITFID;
	/* Range is defined in the spec */
	if (vap_id <= 7)
		itf_tbl = DLRX_CFG_VAP2INT_MAP1_BASE;
	else
		itf_tbl = DLRX_CFG_VAP2INT_MAP2_BASE;

	vap_id = vap_id % 8;
	spin_lock_bh(&g_vap2int_tbl_lock);
	itf_id = (*itf_tbl >> (vap_id<<2)) & 0xF;
	spin_unlock_bh(&g_vap2int_tbl_lock);

	return itf_id;

}
EXPORT_SYMBOL(ppa_dl_dre_itf_from_vapid);



/*************************************************
 *			APIs for 11AC Driver
 *************************************************/
void ppa_dl_qca_register(
	void *dl_rx_handle,
	PPA_QCA_DL_RX_CB *dl_qca_rxcb,
	uint32_t flags
	)
{
	/* == AVM/CMH 20160508 make PPA_F_DEREGISTER work == */
#if 0
	ASSERT((dl_rx_handle != NULL && dl_qca_rxcb != NULL),
		"dl_rx_handle or dl_qca_rxcb is NULL");
#else
	ASSERT((dl_rx_handle != NULL), "dl_rx_handle is NULL");
#endif
	ASSERT((flags == PPA_F_REGISTER) || (flags == PPA_F_DEREGISTER),
		"flag is not expected: %d\n",
		flags);
	dtlk_debug(DBG_INIT, "%s, dl_rx_handle: 0x%x, dl_qca_rxcb: 0x%x, flags: 0x%d\n",
		__func__, (uint32_t)dl_rx_handle, (uint32_t)dl_qca_rxcb, flags);
	g_dlrx_handle = dl_rx_handle;
	/* == AVM/CMH 20160508 make PPA_F_DEREGISTER work == */
#if 0
	/* Fix Klockwork's check */
	if (dl_qca_rxcb == NULL)
		return;
#endif
	switch (flags) {
	case PPA_F_REGISTER:
		{
		/* == AVM/CMH 20160508 make PPA_F_DEREGISTER work == */
#if 1
		if (dl_qca_rxcb == NULL) {
			panic("%s:%d dl_qca_rxcb == NULL on register!\n", __func__, __LINE__);
		}
#endif
		dre_dl_set_dlrx_unload_t dre_dl_set_dlrx_unload_fn;
		dre_dl_set_dlrx_unload_fn =
			g_dre_fnset.dre_dl_set_dlrx_unload_t;
		ASSERT((dl_rx_handle != NULL &&  dl_qca_rxcb != NULL),
			"dl_rx_handle or dl_qca_rxcb is NULL");
		g_dlrx_handle = dl_rx_handle;
		g_dlrx_qca_cb.rx_msg_fn	=
			dl_qca_rxcb->rx_msg_fn;
		g_dlrx_qca_cb.rx_splpkt_fn =
			dl_qca_rxcb->rx_splpkt_fn;
		g_dlrx_qca_cb.vap_stats_fn =
			dl_qca_rxcb->vap_stats_fn;
		g_dlrx_qca_cb.peer_act_fn =
			dl_qca_rxcb->peer_act_fn;
		/* == AVM/CMH 20160418 Directlink rx packet trace == */
#if 1
		g_dlrx_qca_cb.avm_directlink_trace_rx_fn =
			dl_qca_rxcb->avm_directlink_trace_rx_fn;
#endif
		}
		/* == AVM/CMH 20160508 make PPA_F_DEREGISTER work == */
#if 1
		dtlk_debug(DBG_INIT, "%s:%d PPA_F_REGISTER completed!\n", __func__, __LINE__);
#endif
		break;
	case PPA_F_DEREGISTER:
		{
			dre_dl_set_dlrx_unload_t dre_dl_set_dlrx_unload_fn;
			dtlk_debug(DBG_INIT, "%s: deregister from QCA\n",
				__func__);
			dre_dl_set_dlrx_unload_fn =
				g_dre_fnset.dre_dl_set_dlrx_unload_t;
			/* we stop dlrx firmware here */
			if (likely(dre_dl_set_dlrx_unload_fn)) {
				dtlk_debug(DBG_INIT, "%s: inform DRE to stop\n",
					__func__);
				dre_dl_set_dlrx_unload_fn();
			}
		}
		/* == AVM/CMH 20160508 make PPA_F_DEREGISTER work == */
#if 1
		dtlk_debug(DBG_INIT, "%s:%d PPA_F_DEREGISTER completed!\n", __func__, __LINE__);
#endif
		break;
	default:
		break;
	}
	return;
}
EXPORT_SYMBOL(ppa_dl_qca_register);

/*
* These functions need to merge with DL TX
* QCA Name: "QCA-11AC"
*/
void ppa_directlink_manage(
	char *name,
	uint32_t flags
	)
{
	/* Update the global structure cfg_global */
	dlrx_cfg_global_t *dlrx_global =
		(dlrx_cfg_global_t *)DLRX_CFG_GLOBAL_BASE;

	if (flags == PPA_F_INIT) {
		dlrx_global->dlrx_enable = TRUE;
		dlrx_global->dltx_enable = TRUE;
	} else if (flags == PPA_F_UNINIT) {
		dlrx_global->dlrx_enable = FALSE;
		dlrx_global->dltx_enable = FALSE;
	}

	/* ppa_directlink_enable(flags); */

	return;
}
EXPORT_SYMBOL(ppa_directlink_manage);
#ifdef SUPPORT_11AC_MULTICARD

/*API to get 11AC wireless card type */
extern int ppa_dl_detect_11ac_card(void);
#endif

/*
* This function initializes Target-to-Host (t2h) CE-5 Destination Ring
* in Direct Link between Target WLAN and PPE.
*/
void ppa_dl_qca_t2h_ring_init(
	uint32_t *t2h_ring_sz,
	uint32_t *dst_ring_base,
	uint32_t pcie_baddr,
	uint32_t flags
	)
{
	dre_dl_set_membase_fn_t  dre_dl_set_mb_fn;
	volatile dlrx_cfg_ctxt_ce5des_t *dlrx_cfg_ctxt_ce5des_ptr;
	volatile dlrx_cfg_ctxt_ce5buf_t *dlrx_cfg_ctxt_ce5buf_ptr;
	dlrx_cfg_global_t *dlrx_cfg_global_ptr;

	dtlk_debug(DBG_INIT, "%s:ddr base:0x%08x,cfg_ctxt_base:0x%x,pcie base:0x%08x\n",
		 __func__,
		 (uint32_t)ddr_base,
		 (uint32_t)cfg_ctxt_base,
		 (uint32_t)pcie_baddr
		 );

	/* pcie_base = (unsigned int *)KSEG1ADDR(pcie_baddr); */
	pcie_base = (unsigned int *)(0xF0000000 | pcie_baddr);
	dtlk_debug(DBG_INIT, "pcie base(virtual): 0x%x\n", (unsigned int)pcie_base);
	dtlk_debug(DBG_INIT, "pcie base(virtual): 0x%p\n", (unsigned int *)KSEG1ADDR(pcie_baddr));
	dlrx_cfg_global_ptr = (dlrx_cfg_global_t *)DLRX_CFG_GLOBAL_BASE;
	dlrx_cfg_global_ptr->dlrx_pcie_base = (unsigned int)pcie_base;

	dlrx_cfg_ctxt_ce5des_ptr =
		(dlrx_cfg_ctxt_ce5des_t *)DLRX_CFG_CTXT_CE5DES_BASE;
	#if 1
	*dst_ring_base = dma_map_single(
				g_mpe_dev,
				(void *)dlrx_cfg_ctxt_ce5des_ptr->cfg_badr_ce5des,
				512 * 2 * 4,
				DMA_FROM_DEVICE
				);
	if (unlikely(dma_mapping_error(g_mpe_dev, (u64)*dst_ring_base))) {
		dtlk_debug(DBG_ERR, "DMA error");
	}
	/*
			dma_unmap_single(
				g_mpe_dev,
				*dst_ring_base,
				4,
				DMA_FROM_DEVICE
				);
				*/
	#else
	*dst_ring_base = MY_CPHYSADDR(dlrx_cfg_ctxt_ce5des_ptr->cfg_badr_ce5des);
	#endif
	*t2h_ring_sz = dlrx_cfg_ctxt_ce5des_ptr->cfg_num_ce5des;
	dtlk_debug(DBG_INIT, "dst_ring_base: 0x%x\n", *dst_ring_base);
	dtlk_debug(DBG_INIT, "t2h_ring_sz: 0x%x\n", *t2h_ring_sz);

	/*
	QCA will update the HW register
	*DLRX_TARGET_CE5_READ_INDEX  = 0;
	*DLRX_TARGET_CE5_WRITE_INDEX = dlrx_cfg_ctxt_ce5buf_ptr->cfg_num_ce5buf - 1;
	*/
	dlrx_cfg_ctxt_ce5buf_ptr =
		(dlrx_cfg_ctxt_ce5buf_t *)DLRX_CFG_CTXT_CE5BUF_BASE;
#ifdef SUPPORT_11AC_MULTICARD
	if (ppa_dl_detect_11ac_card() == PEREGRINE_BOARD) {
		dtlk_debug(DBG_INIT, "%s: PEREGRINE_BOARD\n", __func__);
		dlrx_cfg_ctxt_ce5buf_ptr->cfg_badr_target_ce5_read_index =
			(unsigned int)DLRX_TARGET_CE5_READ_INDEX(DLRX_TARGET_CE5_PEREGRINE);
		dlrx_cfg_ctxt_ce5buf_ptr->cfg_badr_target_ce5_write_index =
			(unsigned int)DLRX_TARGET_CE5_WRITE_INDEX(DLRX_TARGET_CE5_PEREGRINE);
	} else {
		dtlk_debug(DBG_INIT, "%s: BEELINER_BOARD\n", __func__);
		dlrx_cfg_ctxt_ce5buf_ptr->cfg_badr_target_ce5_read_index =
			(unsigned int)DLRX_TARGET_CE5_READ_INDEX(DLRX_TARGET_CE5_BEELINER);
		dlrx_cfg_ctxt_ce5buf_ptr->cfg_badr_target_ce5_write_index =
			(unsigned int)DLRX_TARGET_CE5_WRITE_INDEX(DLRX_TARGET_CE5_BEELINER);
	}
	dtlk_debug(DBG_INIT, "%s: ce5 read 0x%x\n",
		__func__,
		dlrx_cfg_ctxt_ce5buf_ptr->cfg_badr_target_ce5_read_index);
	dtlk_debug(DBG_INIT, "%s: ce5 write 0x%x\n",
		__func__,
		dlrx_cfg_ctxt_ce5buf_ptr->cfg_badr_target_ce5_write_index);
#else
	dtlk_debug(DBG_ERR, "%s: why come here\n", __func__);
	dlrx_cfg_ctxt_ce5buf_ptr->cfg_badr_target_ce5_read_index =
		(unsigned int)DLRX_TARGET_CE5_READ_INDEX(DLRX_TARGET_CE5_PEREGRINE);
	dlrx_cfg_ctxt_ce5buf_ptr->cfg_badr_target_ce5_write_index =
		(unsigned int)DLRX_TARGET_CE5_WRITE_INDEX(DLRX_TARGET_CE5_PEREGRINE);
#endif

	dre_dl_set_mb_fn = dlrx_get_dre_fn(DRE_SET_MEMBASE_FN);
	if (dre_dl_set_mb_fn) {
		dre_dl_set_mb_fn((unsigned int)ddr_base,
			(unsigned int)cfg_ctxt_base,
			(unsigned int)pcie_base
			);
	} else
		dtlk_debug(DBG_ERR, "Not register function: set membase!!!");

	return;
}
EXPORT_SYMBOL(ppa_dl_qca_t2h_ring_init);


/*
* This function gets called from QCA driver
* to initialize or free the Rx packet buffers pool.
*/
void ppa_dl_qca_t2h_pktbuf_pool_manage(
	uint32_t *alloc_idx_ptr,
	uint32_t *t2h_rxpb_ring_sz,
	uint32_t *rxpb_ring_base,
	uint32_t flags
	)
{
	volatile dlrx_cfg_ctxt_rxpb_ptr_ring_t *dlrx_cfg_rxpb_ring;
	dlrx_cfg_rxpb_ring =
		(dlrx_cfg_ctxt_rxpb_ptr_ring_t *)DLRX_CFG_CTXT_RXPB_PTR_RING_BASE;

	if (flags == PPA_F_INIT) {
		/*dlrx_cfg_rxpb_ring->_dw_res0[1] = 255;*/
		/*RX_PKT_BUF_PTR_RING_ALLOC_NUM;, my test only*/
		/*dlrx_cfg_rxpb_ring->rxpb_ptr_write_index = 2;*/
		/*dlrx_cfg_rxpb_ring->rxpb_ptr_write_index =
			dlrx_cfg_rxpb_ring->rxpb_ptr_write_index - 1;*/
		#if 1
		/*my test only, system crash with + 1*/
		*alloc_idx_ptr =
			(uint32_t)((unsigned int)&dlrx_cfg_rxpb_ring->rxpb_ptr_write_index | 0x20000000);
			/* (uint32_t)(&dlrx_cfg_rxpb_ring->rxpb_ptr_write_index); */
			/*(uint32_t)(&dlrx_cfg_rxpb_ring->_dw_res0[1]);*/
		#else
		*(uint32_t *)g_dl_buf_info.uncached_addr_base =
			dlrx_cfg_rxpb_ring->rxpb_ptr_write_index + 1;
		/*my test only, system crash with + 1*/
		*alloc_idx_ptr =
			(uint32_t)(g_dl_buf_info.uncached_addr_base);
		#endif
		*t2h_rxpb_ring_sz =
			dlrx_cfg_rxpb_ring->cfg_num_rxpb_ptr_ring;
		#if 0
		*rxpb_ring_base =
			CPHYSADDR(dlrx_cfg_rxpb_ring->cfg_badr_rxpb_ptr_ring);
		#else
		#if 1
		*rxpb_ring_base = dma_map_single(
				g_mpe_dev,
				(void *)dlrx_cfg_rxpb_ring->cfg_badr_rxpb_ptr_ring,
				4096 * 4,
				DMA_FROM_DEVICE
				);
		if (unlikely(dma_mapping_error(g_mpe_dev, (u64)*rxpb_ring_base))) {
			dtlk_debug(DBG_ERR, "DMA error");
		}
		/*
			dma_unmap_single(
				g_mpe_dev,
				*rxpb_ring_base,
				4,
				DMA_FROM_DEVICE
				); */
		#else
		*rxpb_ring_base = CPHYSADDR(dlrx_cfg_rxpb_ring->cfg_badr_rxpb_ptr_ring);
		#endif
		#endif
		dtlk_debug(DBG_INIT, "%s: return to QCA:\n   alloc_idx_ptr[%x]\n   t2h_rxpb_ring_sz[%d]   rxpb_ring_base[0x%x]\n",
			__func__,
			*alloc_idx_ptr,
			*t2h_rxpb_ring_sz,
			*rxpb_ring_base);
	}

	return;
}
EXPORT_SYMBOL(ppa_dl_qca_t2h_pktbuf_pool_manage);

#ifdef SUPPORT_MULTICAST_TO_UNICAST

extern int dl_m2c_add_peer(
	uint32_t *dlrx_peer_reg_handle,
	uint16_t peer_id,
	uint16_t vap_id
	);
extern int dl_m2c_remove_peer(
	uint32_t *dlrx_peer_reg_handle,
	uint16_t peer_id,
	uint16_t vap_id
	);
#endif /* SUPPORT_MULTICAST_TO_UNICAST */


/*
* This hook function is invoked by QCA WLAN
* Driver for addition or deletion of peer.
*/
int32_t ppa_dl_qca_set_peer_cfg(
	uint32_t *dlrx_peer_reg_handle,
	uint16_t peer_id,
	uint16_t vap_id,
	PPA_WLAN_PN_CHECK_Type_t pn_chk_type,
	uint32_t *rxpn,
	uint32_t flags
	#ifdef SUPPORT_MULTICAST_TO_UNICAST
	, uint8_t *mac_addr
	#endif
	)
{
	/* Each Peer_id can have 8 peer values */
	unsigned int peer;
	unsigned int temp_acc_dis;
	unsigned int temp_vap_id;
	unsigned int temp_sec_type;
	dre_dl_set_rxpn_fn_t dre_dl_set_rxpn_fn;
	struct _dl_peer_mac_mapping_table dl_peer_mac_mapping;
	char *temp, *tempDL;

	dtlk_debug(DBG_RX, "peer handler: 0x%x, peer id: %d, flags: %d\n",
		(unsigned int)dlrx_peer_reg_handle,
		peer_id,
		flags
		);
	#ifdef SUPPORT_MULTICAST_TO_UNICAST
	temp = mac_addr;
	#else
	temp = (char *)(dlrx_peer_reg_handle + 6);
	#endif
	tempDL = (char *)&dl_peer_mac_mapping + 2;
	memcpy((char *)tempDL, temp, 6);
	dl_peer_mac_mapping.valid = 1;
	dl_peer_mac_mapping.vap_id = vap_id;
	printk("%s: %d %x:%x:%x:%x:%x:%x\n", __func__,
		dl_peer_mac_mapping.vap_id,
		dl_peer_mac_mapping.mac5,
		dl_peer_mac_mapping.mac4,
		dl_peer_mac_mapping.mac3,
		dl_peer_mac_mapping.mac2,
		dl_peer_mac_mapping.mac1,
		dl_peer_mac_mapping.mac0
		);
	switch (flags) {
	case PPA_WLAN_ADD_PEER_ID:
		/* Set peer ID to peer */
		if (vap_id >= MAX_VAP_NUM)
			return DTLK_FAILURE;

		if (set_peer_id_to_peer_table(
				(uint32_t)dlrx_peer_reg_handle,
				peer_id,
				&peer,
				(unsigned int)vap_id,
				(unsigned int)pn_chk_type
				) != DTLK_SUCCESS)
			return DTLK_FAILURE;

		dtlk_debug(DBG_ERR, "peer: %d, peer_id: %d, vap_id %d, pn_chk_type: %d\n",
			peer,
			peer_id, vap_id, pn_chk_type);
#ifdef SUPPORT_MULTICAST_TO_UNICAST
		dl_m2c_add_peer((unsigned int *)mac_addr, peer, vap_id);
#endif /* SUPPORT_MULTICAST_TO_UNICAST */

	break;

	case PPA_WLAN_REMOVE_PEER:/* FOR DELETE PEER */
		if (remove_peer_from_table(
				(uint32_t)dlrx_peer_reg_handle,
				(uint32_t)peer_id) != DTLK_SUCCESS)
			return DTLK_FAILURE;
#ifdef SUPPORT_MULTICAST_TO_UNICAST
		dtlk_debug(DBG_ERR, "remove peer: peer_id: %d, vap_id %d\n",
			peer_id, vap_id);
		dl_m2c_remove_peer(mac_addr, peer_id + 1, vap_id);
#endif /* SUPPORT_MULTICAST_TO_UNICAST */
	break;

	case PPA_WLAN_REMOVE_PEER_ID:/* For Delete PEER_ID */
		dtlk_debug(DBG_ERR, "remove peer_id: peer_id: %d, vap_id %d\n",
			peer_id, vap_id);
		if (remove_peer_id_from_table(
			(uint32_t)dlrx_peer_reg_handle,
			(uint32_t)peer_id) != DTLK_SUCCESS)
			return DTLK_FAILURE;
	break;

	case PPA_WLAN_SET_PN_CHECK:/* To Update PN SEC TYPE */
		peer = get_handler_index(
			(uint32_t)dlrx_peer_reg_handle
			);
		if (peer == HANDLER_NOT_FOUND)
			return DTLK_FAILURE;

		ppa_dl_dre_vapinfo_from_peer(
			peer,
			&temp_vap_id,
			&temp_sec_type,
			&temp_acc_dis
			);
		if (ppa_dl_dre_vapinfo_set(
				peer,
				temp_vap_id,
				(unsigned int)pn_chk_type,
				temp_acc_dis) == DTLK_SUCCESS) {
			/*
			* Peer may still store old information
			* from previous connection.
			* Reset Peer information as well as PNs value.
			*/
			dlrx_ro_mainlist_t *dlrx_ro_mainlist_ptr;
			int seqid = 0;
			int tid = 0;
			dlrx_ro_mainlist_ptr =
				(dlrx_ro_mainlist_t *)DLRX_DDR_RO_MAINLIST_BASE;
			dlrx_ro_mainlist_ptr += (peer*NUM_TID);
			for (tid = 0; tid < NUM_TID; tid++) {
				dlrx_ro_mainlist_ptr->last_pn_dw0 = 0;
				dlrx_ro_mainlist_ptr->last_pn_dw1 = 0;
				dlrx_ro_mainlist_ptr->last_pn_dw2 = 0;
				dlrx_ro_mainlist_ptr->last_pn_dw3 = 0;
				dlrx_ro_mainlist_ptr->first_ptr = NULL_PTR;
				for (seqid = 1; seqid < 64; seqid++)
					dlrx_ro_mainlist_ptr->_dw_res0[seqid-1] = NULL_PTR;
				dlrx_ro_mainlist_ptr++;
			}
		}
		dtlk_debug(DBG_RX, "peer:%d,peer_id:%d,vap_id:%d,sec_type:%d,acc_dis:%d\n",
			peer,
			peer_id,
			temp_vap_id,
			pn_chk_type,
			temp_acc_dis
			);
	break;

	case PPA_WLAN_SET_PN_CHECK_WITH_RXPN:
		peer = get_handler_index(
			(uint32_t)dlrx_peer_reg_handle
			);
		if (peer == HANDLER_NOT_FOUND) {
			dtlk_debug(DBG_ERR, "Invalid peer");
			return DTLK_FAILURE;
		}
		ppa_dl_dre_vapinfo_from_peer(
			peer,
			&temp_vap_id,
			&temp_sec_type,
			&temp_acc_dis
			);
		ppa_dl_dre_vapinfo_set(
			peer,
			temp_vap_id,
			(unsigned int)pn_chk_type,
			temp_acc_dis
			);
		dtlk_debug(DBG_RX, "SET PN:peer:%d,peer_id:%d,vap_id:%d,sec_type:%d,acc_dis:%d,rxpn:0x%x\n",
			peer,
			peer_id,
			temp_vap_id,
			pn_chk_type,
			temp_acc_dis,
			(uint32_t)rxpn
			);
		dre_dl_set_rxpn_fn = dlrx_get_dre_fn(DRE_SET_RXPN_FN);
		if (likely(dre_dl_set_rxpn_fn))
			dre_dl_set_rxpn_fn(peer, rxpn);
		else
			dtlk_debug(DBG_ERR, "Function set_rxpn is not registered!");

	break;
	}

	return DTLK_SUCCESS;
}
EXPORT_SYMBOL(ppa_dl_qca_set_peer_cfg);

int set_peer_id_to_peer_table(
	uint32_t dlrx_peer_reg_handle,
	uint32_t peer_id,
	uint32_t *peer,
	unsigned int vap_id,
	unsigned int pn_chk_type
	)
{
	unsigned int vld;
	unsigned int peer_val;
	unsigned int peer_index;
	unsigned int peer_offset;
	unsigned int mask_value;
	unsigned int peerinfo;
	unsigned int temp_peerinfo;
	int handler_index;
	volatile unsigned int *pid2p_tbl =
		DLRX_CFG_PEER_ID_TO_PEER_MAP_BASE(0);
	volatile dlrx_cfg_ctxt_peer_handler_t *peer_handler_tbl = NULL;

	handler_index = get_handler_index(dlrx_peer_reg_handle);
	dtlk_debug(DBG_RX,
		"%s: peer: %d, peer_id:%d\n",
		__func__,
		handler_index,
		peer_id
		);

	if (handler_index == HANDLER_NOT_FOUND) {
		if (get_free_peer_number(&peer_val) == DTLK_FAILURE)
			return DTLK_FAILURE;

		/* Get the handler index address by using the base address */
		peer_handler_tbl =
			(dlrx_cfg_ctxt_peer_handler_t *)DLRX_CFG_CTXT_PEER_HANDLER_BASE(peer_val);
		/* Send the handler table data */
		peer_handler_tbl->cfg_peer_count++;
		peer_handler_tbl->cfg_peer_handler = dlrx_peer_reg_handle;
	} else {
		/* Get the handler index address by using the base address */
		peer_handler_tbl =
			(dlrx_cfg_ctxt_peer_handler_t *)DLRX_CFG_CTXT_PEER_HANDLER_BASE(handler_index);
		peer_handler_tbl->cfg_peer_count++;
		peer_val = (unsigned int)handler_index;
	}

	ppa_dl_dre_vapinfo_set(peer_val, vap_id, pn_chk_type, 0);

	vld = VALID;
	peerinfo = ((vld << 7) | (peer_val & 0x7F));

	peer_index = peer_id >> 2;
	peer_offset = peer_id % 4;

	mask_value = ~(0xFF << (peer_offset * 8));

	spin_lock_bh(&g_prid2pr_tbl_lock);
	temp_peerinfo = *(pid2p_tbl + peer_index);
	temp_peerinfo &= mask_value;
	*(pid2p_tbl + peer_index) =
		(temp_peerinfo | (peerinfo << (peer_offset << 3)));
	spin_unlock_bh(&g_prid2pr_tbl_lock);

	*peer = peer_val;
	return DTLK_SUCCESS;
}

int remove_peer_from_table(
	uint32_t dlrx_peer_reg_handle,
	uint32_t peer_id
	)
{
	int handler_index;
	unsigned int peer;
	unsigned int loop_index_1;
	unsigned int loop_index_2;
	unsigned int peer_id_loop_num;
	unsigned int temp_peerinfo;
	unsigned int peerinfo;
	unsigned int temp_peer;
	unsigned int temp_peer_vld;
	unsigned int mask_value;
	volatile dlrx_cfg_ctxt_peer_handler_t *peer_handler_tbl = NULL;
	volatile unsigned int *pid2p_tbl = DLRX_CFG_PEER_ID_TO_PEER_MAP_BASE(0);
	dre_dl_reset_fn_t dre_dl_reset_fn;
	unsigned int *peer_bit_field;
	unsigned int peer_bit_field_offset;
	unsigned int peer_bit_field_index;

	peer_bit_field = (unsigned int *)DLRX_CFG_CTXT_PEER_BITMAP_BASE(0);
	/* have 4 element */

	handler_index = get_handler_index(dlrx_peer_reg_handle);

	if (handler_index == HANDLER_NOT_FOUND)
		return DTLK_FAILURE;
	else {
		peer = (unsigned int)handler_index;
		/* Get the handler index address by using the base address */
		peer_handler_tbl =
			(dlrx_cfg_ctxt_peer_handler_t *)DLRX_CFG_CTXT_PEER_HANDLER_BASE(peer);
		/* Send the handler table data */
		peer_handler_tbl->cfg_peer_handler = 0;

		peer_id_loop_num = MAX_PEERID_NUM >> 2;

		if (peer_handler_tbl->cfg_peer_count) {
			spin_lock_bh(&g_prid2pr_tbl_lock);
			for (loop_index_1 = 0;
					loop_index_1 < peer_id_loop_num;
					loop_index_1++) {
				temp_peerinfo = *(pid2p_tbl + loop_index_1);
				for (loop_index_2 = 0;
					loop_index_2 < 4;
					loop_index_2++) {
					mask_value =
						(0xFF << (loop_index_2 << 3));
					peerinfo =
						(temp_peerinfo & mask_value) >>
						(loop_index_2 << 3);
					temp_peer = peerinfo & 0x7F;
					temp_peer_vld = (peerinfo & 0x80) >> 7;
					if ((temp_peer_vld == 1) &&
						(temp_peer == peer)) {
						peerinfo = 0;
						mask_value = ~mask_value;
						temp_peerinfo &= mask_value;
						*(pid2p_tbl + loop_index_1) =
							temp_peerinfo;
					}
				}
			}
			spin_unlock_bh(&g_prid2pr_tbl_lock);
		}
		peer_handler_tbl->cfg_peer_count = 0;

		dtlk_debug(DBG_RX, "%s: reset_peer, peer: %d\n", __func__, peer);
		dre_dl_reset_fn = dlrx_get_dre_fn(DRE_RESET_FN);
		if (likely(dre_dl_reset_fn))
			dre_dl_reset_fn(DRE_RESET_PEER, peer);
		else
			dtlk_debug(DBG_ERR, "Function DRE_RESET_PEER is not registered!");

		ppa_dl_dre_vapinfo_set(peer, 0, 0, 0);

		/* Set the corresponding peer bit field value to 0 */
		peer_bit_field_offset = peer >> 5;/* Divide by 32 */
		peer_bit_field_index = peer % 32;
		/* Calculate the mask value
		* to set the corresponding peer bit to zero */
		mask_value = ~(1 << peer_bit_field_index);
		peer_bit_field[peer_bit_field_offset] &= mask_value;
	}
	return DTLK_SUCCESS;
}


/*
* Remove peer ID PEER table
*/
int remove_peer_id_from_table(
	uint32_t dlrx_peer_reg_handle,
	uint32_t peer_id
	)
{
	unsigned int handler_index;
	unsigned int peer_index;
	unsigned int peer_offset;
	unsigned int peer;
	unsigned int temp_peerinfo;
	unsigned int mask_value;
	volatile dlrx_cfg_ctxt_peer_handler_t *peer_handler_tbl = NULL;
	volatile unsigned int *pid2p_tbl = DLRX_CFG_PEER_ID_TO_PEER_MAP_BASE(0);

	handler_index = get_handler_index(dlrx_peer_reg_handle);

	dtlk_debug(DBG_RX, "%s: peer: %d\n", __func__, handler_index);
	if (handler_index == HANDLER_NOT_FOUND)
		return DTLK_FAILURE;
	else {
		peer_index = peer_id >> 2;
		peer_offset = peer_id % 4;
		peer = (unsigned int)handler_index;
		/* Get the handler index address by using the base address */
		peer_handler_tbl =
			(dlrx_cfg_ctxt_peer_handler_t *)DLRX_CFG_CTXT_PEER_HANDLER_BASE(peer);
		/* Send the handler table data */
		peer_handler_tbl->cfg_peer_count--;
		mask_value = ~(0xFF << (peer_offset * 8));
		spin_lock_bh(&g_prid2pr_tbl_lock);
		temp_peerinfo = *(pid2p_tbl + peer_index);
		temp_peerinfo &= mask_value;
		*(pid2p_tbl + peer_index) = temp_peerinfo;
		spin_unlock_bh(&g_prid2pr_tbl_lock);
	}

	return DTLK_SUCCESS;
}

int get_handler_index(
	uint32_t dlrx_peer_reg_handle
	)
{
	int index;
	int handler_index = HANDLER_NOT_FOUND;
	volatile dlrx_cfg_ctxt_peer_handler_t *peer_handler_tbl = NULL;

	for (index = 0; index < MAX_PEER_NUM; index++) {
		peer_handler_tbl =
			(dlrx_cfg_ctxt_peer_handler_t *)DLRX_CFG_CTXT_PEER_HANDLER_BASE(index);
		if (peer_handler_tbl->cfg_peer_handler ==
			dlrx_peer_reg_handle)
			return index;
	}
	return handler_index;
}

int get_free_peer_number(
	unsigned int *peer_val
	)
{
	unsigned int index;
	unsigned int temp_bit_field;
	unsigned int free_peer_num = 0;
	unsigned int *peer_bit_field;

	/* To store handler value and number
	* of peer count for each handler
	* NOTE : MAXIMUM SUPPORTED
	* HANDLER VALUE IS 128
	*/
	/* have 4 element */
	peer_bit_field = (unsigned int *)DLRX_CFG_CTXT_PEER_BITMAP_BASE(0);
	for (index = 0; index < 4; index++) {
		temp_bit_field = peer_bit_field[index];
		/* No free peer in this Dword */
		if (temp_bit_field == 0xFFFFFFFF) {
			free_peer_num += 32;
			continue;
		}
		while (temp_bit_field & 1) {
			temp_bit_field >>= 1;
			free_peer_num++;
		}
		break;
	}
	if (index >= 4)
		return DTLK_FAILURE;

	*peer_val = free_peer_num;
	peer_bit_field[index] =
		(peer_bit_field[index] | (1 << (*peer_val % 32)));

	return DTLK_SUCCESS;
}


/* This function transfers control for DirectLink in CE-5 Rx in system.
* The function gets called by QCA WLAN driver as part of handling
* legacy interrupt when CE-5 handling is made.
* Flags -- This is used for future use
*/
int32_t ppa_hook_dl_qca_rx_offload(
	uint32_t flags
	)
{
	int ret = DTLK_FAILURE;
	dre_dl_main_fn_t dre_dl_main_fn;
	/*dtlk_debug(DBG_RX, "%s\n", __func__);*/
	dre_dl_main_fn = dlrx_get_dre_fn(DRE_MAIN_FN);
	if (likely(dre_dl_main_fn))
		ret = dre_dl_main_fn();
	else
		dtlk_debug(DBG_ERR, "FW Callback FN: dre_main is not registered");

	return (int32_t)ret;
}
EXPORT_SYMBOL(ppa_hook_dl_qca_rx_offload);

/*
* This function picks a corresponding network packet buffer
* for previous handed over message in callback. The function
* gets called by QCA WLAN driver after its PPA_QCA_DL_RX_MSG_FN
* callback gets a message of type RX_IND or RX_FRAG_IND to it.
* This network buffer should be getting freed
* inside QCA driver or somewhere in the path of protocol stack.
*/
int32_t ppa_dl_qca_get_rx_net_buf(
	struct sk_buff **rx_skb,
	uint32_t flags
	)
{
	int ret = DTLK_SUCCESS;
	dre_dl_getmsdu_fn_t dre_dl_getmsdu_fn;
	unsigned int rx_pb, data_len;
	/*dtlk_debug(DBG_RX, "%s: - \n", __func__);*/
	*rx_skb = NULL;
	dre_dl_getmsdu_fn = dlrx_get_dre_fn(DRE_GET_CURMSDU_FN);
	if (likely(dre_dl_getmsdu_fn)) {
		ret = dre_dl_getmsdu_fn(&rx_pb, &data_len);
		if (ret != DTLK_SUCCESS)
			return (int32_t)ret;
	} else {
		dtlk_debug(DBG_ERR, "FW CallBack FN: get_curmsdu is not registered!");
		return (int32_t)ret;
	}
	/*dtlk_debug(DBG_RX, "     rx_pb[0x%x] len[%d]\n", __func__, rx_pb, data_len);*/
	*rx_skb =
		dlrx_skb_setup(
			rx_pb,
			rx_pb,
			data_len + g_dlrx_cfg_offset_atten
			);

	return DTLK_SUCCESS;
}
EXPORT_SYMBOL(ppa_dl_qca_get_rx_net_buf);

/* TODO:  Need discuss the mib functions */
int32_t ppa_dl_qca_get_msg_mdu_stats(
	PPA_DL_WLAN_MSG_STATS_t *msg_stats,
	PPA_DL_WLAN_RX_MPDU_MSDU_STATS_t *mdu_stats,
	uint32_t flags
	)
{
	volatile dlrx_data_mib_t *wlan_mdu_stats = (dlrx_data_mib_t *)DLRX_DATA_MIB_BASE;
	volatile dlrx_msg_mib_t *wlan_msg_stats = (dlrx_msg_mib_t *)DLRX_MSG_MIB_BASE;

	msg_stats->ce4_cpu_msgs = wlan_msg_stats->total_ce4_cpu_msg;
	msg_stats->ce5_cpu_msgs = wlan_msg_stats->total_ce5_cpu_msg;
	msg_stats->rx_ind_msgs = wlan_msg_stats->total_rx_ind_msg;
	msg_stats->rx_flush_msgs = wlan_msg_stats->total_rx_flush_msg;
	msg_stats->tx_comp_msgs = wlan_msg_stats->total_tx_cmp_msg;
	msg_stats->rx_ind_wl_msgs = wlan_msg_stats->total_rx_ind_wlan_msg;
	msg_stats->rx_flush_wl_msgs = wlan_msg_stats->total_rx_flush_wlan_msg;
	msg_stats->rx_frag_msgs = wlan_msg_stats->total_rx_frag_ind_msg;

	mdu_stats->rx_mpdu_ok = wlan_mdu_stats->rx_success_mpdu;
	mdu_stats->rx_msdu_ok = wlan_mdu_stats->rx_success_msdu;
	mdu_stats->rx_mpdu_err2 = wlan_mdu_stats->rx_error2_mpdu;
	mdu_stats->rx_msdu_err2 = wlan_mdu_stats->rx_error2_msdu;
	mdu_stats->rx_mpdu_err3 = wlan_mdu_stats->rx_error3_mpdu;
	mdu_stats->rx_msdu_err3 = wlan_mdu_stats->rx_error3_msdu;
	mdu_stats->rx_mpdu_err4 = wlan_mdu_stats->rx_error4_mpdu;
	mdu_stats->rx_msdu_err4 = wlan_mdu_stats->rx_error4_msdu;
	mdu_stats->rx_mpdu_err5 = wlan_mdu_stats->rx_error5_mpdu;
	mdu_stats->rx_msdu_err5 = wlan_mdu_stats->rx_error5_msdu;
	mdu_stats->rx_mpdu_err6 = wlan_mdu_stats->rx_error6_mpdu;
	mdu_stats->rx_msdu_err6 = wlan_mdu_stats->rx_error6_msdu;
	mdu_stats->rx_mpdu_err7 = wlan_mdu_stats->rx_error7_mpdu;
	mdu_stats->rx_msdu_err7 = wlan_mdu_stats->rx_error7_msdu;
	mdu_stats->rx_mpdu_err8 = wlan_mdu_stats->rx_error8_mpdu;
	mdu_stats->rx_msdu_err8 = wlan_mdu_stats->rx_error8_msdu;
	mdu_stats->rx_mpdu_err9 = wlan_mdu_stats->rx_error9_mpdu;
	mdu_stats->rx_msdu_err9 = wlan_mdu_stats->rx_error9_msdu;
	mdu_stats->rx_mpdu_errA = wlan_mdu_stats->rx_errora_mpdu;
	mdu_stats->rx_msdu_errA = wlan_mdu_stats->rx_errora_msdu;

	return DTLK_SUCCESS;
}
EXPORT_SYMBOL(ppa_dl_qca_get_msg_mdu_stats);

int32_t ppa_dl_qca_set_seq_mask(
	uint32_t *dlrx_peer_reg_handle,
	uint32_t ex_tid,
	uint32_t seq_mask,
	uint32_t flags
	)
{
	int peer;
	uint32_t *seq_mask_base = DLRX_DDR_SEQ_MASK_BASE;

	if (unlikely(!dlrx_peer_reg_handle))
		return DTLK_FAILURE;

	peer = get_handler_index((uint32_t)dlrx_peer_reg_handle);
	if (unlikely(peer == HANDLER_NOT_FOUND))
		return DTLK_FAILURE;


	seq_mask_base[(peer * 16) + ex_tid] = seq_mask;

	return DTLK_SUCCESS;
}
EXPORT_SYMBOL(ppa_dl_qca_set_seq_mask);


/*************************************************
 *		   Functions called by datapath driver
 *************************************************/
void set_vap_itf_tbl(
	uint32_t vap_id,
	uint32_t fid
	)
{
	volatile unsigned int *itf_tbl;
	volatile unsigned int itf_id;
	dtlk_debug(DBG_RX, "%s: vap_id[%d] fid[%d]\n", __func__, vap_id, fid);
	if (vap_id >= MAX_VAP_NUM)
		return;
	/* Range is defined in the spec */
	if (vap_id <= 7)
		itf_tbl = DLRX_CFG_VAP2INT_MAP1_BASE;
	else
		itf_tbl = DLRX_CFG_VAP2INT_MAP2_BASE;


	vap_id = vap_id % 8;
	spin_lock_bh(&g_vap2int_tbl_lock);
	itf_id = *(itf_tbl);
	dtlk_debug(DBG_RX, "%s: itf_tbl[0x%p] itf_id[%x]\n", __func__, itf_tbl, itf_id);
	*(itf_tbl) =
		(itf_id & ~(0xF << (vap_id * 4))) |
		((fid & 0xF) << (vap_id * 4));
	dtlk_debug(DBG_RX, "%s: itf_tbl[%p] value[%x]\n", __func__, itf_tbl, *itf_tbl);
	spin_unlock_bh(&g_vap2int_tbl_lock);

	return;
}


/*************************************************
 *			Export Functions
 *************************************************/

