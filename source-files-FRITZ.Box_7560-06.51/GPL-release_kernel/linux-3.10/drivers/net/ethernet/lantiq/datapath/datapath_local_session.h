#ifndef DP_LOCAL_SESSION_123
#define DP_LOCAL_SESSION_123

extern int tcp_v4_rcv(struct sk_buff *skb); /*defined in tcp_ipv4.c */
int dp_tcp_fast_ok(int ep, struct sk_buff *skb, struct pmac_rx_hdr *pmac);
int dp_local_session_fast_init(uint32_t flag);

#endif

