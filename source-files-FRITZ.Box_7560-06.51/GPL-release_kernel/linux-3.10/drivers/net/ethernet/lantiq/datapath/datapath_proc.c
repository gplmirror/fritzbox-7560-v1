#include <linux/module.h>
#include <lantiq.h>
#include <net/datapath_proc_api.h>	/*for proc api */
#include <net/datapath_api.h>
#include "datapath_pmac.h"
#include <net/drv_tmu_ll.h>
#include <net/lantiq_cbm.h>
#include <net/lantiq_cbm_api.h>
#include <asm/ltq_vmb.h>  /*vmb */
#include <asm/ltq_itc.h>  /*mips itc */

#include <xway/switch-api/lantiq_gsw_api.h>
/*#include <xway/switch-api/lantiq_gsw_flow.h>*/

#include "datapath.h"
#include "datapath_mib.h"

#define DP_PROC_NAME       "dp"
#define DP_PROC_BASE       "/proc/"DP_PROC_NAME"/"
#define DP_PROC_PARENT     ""

#define DP_PROC_FILE_DBG   "dbg"
#define DP_PROC_FILE_PORT   "port"
#define DP_PROC_FILE_PARSER "parser"
#define DP_PROC_FILE_RMON_PORTS  "rmon"
#define DP_PROC_FILE_MEM "mem"
#define DP_PROC_FILE_EP "ep"         /*EP/port ID info */
#define DP_PROC_FILE_DPORT "dport"  /*TMU dequeue port info*/
#define DP_PROC_PRINT_MODE "print_mode"
#define DP_PROC_FILE_CHECKSUM "checksum"
#define DP_PROC_FILE_MIB_TIMER "mib_timer"
#define DP_PROC_FILE_MIB_INSIDE "mib_inside"
#define DP_PROC_FILE_MIBPORT "mib_port"
#define DP_PROC_FILE_MIBVAP "mib_vap"
#define DP_PROC_FILE_COC "coc"
#define DP_PROC_FILE_COMMON_CMD "cmd"

static ssize_t proc_port_write(struct file *file, const char *buf, size_t count,
			       loff_t *ppos);
static void proc_dbg_read(struct seq_file *s);
static int proc_port_start(void);
static int proc_port_dump(struct seq_file *s, int pos);
static ssize_t proc_dbg_write(struct file *, const char *, size_t, loff_t *);
static void proc_parser_read(struct seq_file *s);
static ssize_t proc_parser_write(struct file *, const char *, size_t, loff_t *);
static int proc_gsw_rmon_port_start(void);
static int proc_gsw_port_rmon_dump(struct seq_file *s, int pos);
static ssize_t proc_gsw_rmon_write(struct file *file, const char *buf, size_t count,
				   loff_t *ppos);
static int proc_write_mem(struct file *, const char *, size_t, loff_t *);
static int proc_dport_dump(struct seq_file *s, int pos);
static int proc_ep_dump(struct seq_file *s, int pos);
static ssize_t ep_port_write(struct file *, const char *, size_t, loff_t *);
static void proc_checksum_read(struct seq_file *s);
static int proc_common_cmd_dump(struct seq_file *s, int pos);
static int proc_common_cmd_start(void);

static int rmon_display_tmu_mib = 1;
static int rmon_display_port_full = 0;

static struct dp_proc_entry dp_proc_entries[] = {
	/*name single_callback_t multi_callback_t/_start write_callback_t */
	{DP_PROC_FILE_DBG, proc_dbg_read, NULL, NULL, proc_dbg_write},
	{DP_PROC_FILE_PORT, NULL, proc_port_dump, proc_port_start, proc_port_write},
	{DP_PROC_FILE_PARSER, proc_parser_read, NULL, NULL, proc_parser_write},
	{DP_PROC_FILE_RMON_PORTS, NULL, proc_gsw_port_rmon_dump, proc_gsw_rmon_port_start, proc_gsw_rmon_write},
	{DP_PROC_FILE_MEM, NULL, NULL, NULL, proc_write_mem},
	{DP_PROC_FILE_EP, NULL, proc_ep_dump, NULL, ep_port_write},
	{DP_PROC_FILE_DPORT, NULL, proc_dport_dump, NULL, NULL},
	{DP_PROC_PRINT_MODE, proc_print_mode_read, NULL, NULL, proc_print_mode_write},
	{DP_PROC_FILE_CHECKSUM, proc_checksum_read, NULL, NULL, NULL},
#ifdef CONFIG_LTQ_DATAPATH_MIB
	{DP_PROC_FILE_MIB_TIMER, proc_mib_timer_read, NULL, NULL, proc_mib_timer_write},
	{DP_PROC_FILE_MIB_INSIDE, NULL, proc_mib_inside_dump, proc_mib_inside_start, proc_mib_inside_write},
	{DP_PROC_FILE_MIBPORT, NULL, proc_mib_port_dump, proc_mib_port_start, NULL},
	{DP_PROC_FILE_MIBVAP, NULL, proc_mib_vap_dump, proc_mib_vap_start, proc_mib_vap_write},
#endif

#ifdef CONFIG_LTQ_DATAPATH_CPUFREQ
	{DP_PROC_FILE_COC, proc_coc_read, NULL, NULL, proc_coc_write},
#endif
	{DP_PROC_FILE_COMMON_CMD, NULL, proc_common_cmd_dump, proc_common_cmd_start, NULL},

	/*the last place holder*/
	{NULL, NULL, NULL, NULL, NULL}
};
static struct proc_dir_entry *dp_proc_node;

struct proc_dir_entry *dp_proc_install(void)
{

	dp_proc_node = proc_mkdir(DP_PROC_PARENT DP_PROC_NAME, NULL);

	if (dp_proc_node != NULL) {
		int i;

		for (i = 0; i < ARRAY_SIZE(dp_proc_entries); i++)
			dp_proc_entry_create(dp_proc_node,
					     &dp_proc_entries[i]);
	} else {
		PRINTK("cannot create proc entry");
		return NULL;
	}

	return dp_proc_node;
}

int proc_port_dump(struct seq_file *s, int pos)
{
	int i;
	int ret;
	struct pmac_port_info *port = get_port_info(pos);

	if (!port) {
		PR_ERR("Why port is NULL\n");
		return -1;
	}

	if (port->status == PORT_FREE) {
		if (pos == 0) {
			seq_printf(s,
				   "Reserved Port: rx_err_drop=0x%08x  tx_err_drop=0x%08x\n",
				   port->rx_err_drop, port->tx_err_drop);

		} else
			seq_printf(s,
				   "%02d: rx_err_drop=0x%08x  tx_err_drop=0x%08x\n",
				   pos, port->rx_err_drop, port->tx_err_drop);

		goto EXIT;
	}

	seq_printf(s,
		   "%02d: module=0x0x%0x(name:%8s) dev_port=%02d dp_port=%02d\n",
		   pos, (u32)port->owner, port->owner->name,
		   port->dev_port, port->port_id);
	seq_printf(s,  "    status:            %s\n",
		   dp_port_status_str[port->status]);

	seq_printf(s, "    allocate_flags:    ");

	for (i = 0; i < get_dp_port_type_str_size(); i++) {
		if (port->alloc_flags & dp_port_flag[i])
			seq_printf(s,  "%s ", dp_port_type_str[i]);
	}

	seq_printf(s, "\n");

	seq_printf(s, "    cb->rx_fn:         0x%0x\n",
		   (u32) port->cb.rx_fn);
	seq_printf(s, "    cb->restart_fn:    0x%0x\n",
		   (u32) port->cb.restart_fn);
	seq_printf(s, "    cb->stop_fn:       0x%0x\n",
		   (u32) port->cb.stop_fn);
	seq_printf(s, "    cb->get_subifid_fn:0x%0x\n",
		   (u32) port->cb.get_subifid_fn);
	seq_printf(s, "    num_subif:         %02d\n",
		   port->num_subif);

	for (i = 0; i < MAX_SUBIF_PER_PORT; i++) {
		if (port->subif_info[i].flags) {
			seq_printf(s,
				   "      [%02d]: subif=0x%04x(vap=%d) netif=0x%0x(name=%s), device_name=%s\n",
				   i, port->subif_info[i].subif,
				   (port->
				    subif_info[i].subif >> VAP_OFFSET)
				   & 0xF, (u32)port->subif_info[i].netif,
				   port->subif_info[i].netif ? port->subif_info[i].netif->name : "NULL/DSL",
				   port->subif_info[i].device_name);
			seq_printf(s,
				   "          : rx_fn_rxif_pkt =0x%08x\n",
				   port->subif_info[i].
				   mib.rx_fn_rxif_pkt);
			seq_printf(s,
				   "          : rx_fn_txif_pkt =0x%08x\n",
				   port->subif_info[i].
				   mib.rx_fn_txif_pkt);
			seq_printf(s,
				   "          : rx_fn_dropped  =0x%08x\n",
				   port->subif_info[i].
				   mib.rx_fn_dropped);
			seq_printf(s,
				   "          : tx_cbm_pkt     =0x%08x\n",
				   port->subif_info[i].mib.tx_cbm_pkt);
			seq_printf(s,
				   "          : tx_tso_pkt     =0x%08x\n",
				   port->subif_info[i].mib.tx_tso_pkt);
			seq_printf(s,
				   "          : tx_pkt_dropped =0x%08x\n",
				   port->subif_info[i].
				   mib.tx_pkt_dropped);
			seq_printf(s,
				   "          : tx_clone_pkt   =0x%08x\n",
				   port->subif_info[i].
				   mib.tx_clone_pkt);
			seq_printf(s,
				   "          : tx_hdr_room_pkt=0x%08x\n",
				   port->subif_info[i].
				   mib.tx_hdr_room_pkt);
		}
	}

	ret = seq_printf(s,  "    rx_err_drop=0x%08x  tx_err_drop=0x%08x\n",
			 port->rx_err_drop, port->tx_err_drop);

	if (ret)
		return pos;

EXIT:
	pos++;

	if (pos >= PMAC_MAX_NUM)
		pos = -1;	/*end of the loop */

	return pos;
}

int proc_port_start(void)
{

	return 0;
}

int display_port_info(u8 pos, int start_vap, int end_vap, u32 flag)
{
	int i;
	int ret;
	struct pmac_port_info *port = get_port_info(pos);

	if (!port) {
		PR_ERR("Why port is NULL\n");
		return -1;
	}

	if (port->status == PORT_FREE) {
		if (pos == 0) {
			PRINTK("Reserved Port: rx_err_drop=0x%08x  tx_err_drop=0x%08x\n",
			       port->rx_err_drop, port->tx_err_drop);

		} else
			PRINTK("%02d: rx_err_drop=0x%08x  tx_err_drop=0x%08x\n",
			       pos, port->rx_err_drop, port->tx_err_drop);

		goto EXIT;
	}

	PRINTK("%02d: module=0x0x%0x(name:%8s) dev_port=%02d dp_port=%02d\n",
	       pos, (u32)port->owner, port->owner->name,
	       port->dev_port, port->port_id);
	PRINTK("    status:            %s\n",
	       dp_port_status_str[port->status]);

	PRINTK("    allocate_flags:    ");

	for (i = 0; i < get_dp_port_type_str_size(); i++) {
		if (port->alloc_flags & dp_port_flag[i])
			PRINTK("%s ", dp_port_type_str[i]);
	}

	PRINTK("\n");

	if (!flag) {
		PRINTK("    cb->rx_fn:         0x%0x\n",
		       (u32) port->cb.rx_fn);
		PRINTK("    cb->restart_fn:    0x%0x\n",
		       (u32) port->cb.restart_fn);
		PRINTK("    cb->stop_fn:       0x%0x\n",
		       (u32) port->cb.stop_fn);
		PRINTK("    cb->get_subifid_fn:0x%0x\n",
		       (u32) port->cb.get_subifid_fn);
		PRINTK("    num_subif:         %02d\n",
		       port->num_subif);
	}

	for (i = start_vap; i < end_vap; i++) {
		if (port->subif_info[i].flags) {
			PRINTK("      [%02d]: subif=0x%04x(vap=%d) netif=0x%0x(name=%s), device_name=%s\n",
			       i, port->subif_info[i].subif,
			       (port->
				subif_info[i].subif >> VAP_OFFSET)
			       & 0xF, (u32)port->subif_info[i].netif,
			       port->subif_info[i].netif ? port->subif_info[i].netif->name : "NULL/DSL",
			       port->subif_info[i].device_name);
			PRINTK("          : rx_fn_rxif_pkt =0x%08x\n",
			       port->subif_info[i].
			       mib.rx_fn_rxif_pkt);
			PRINTK("          : rx_fn_txif_pkt =0x%08x\n",
			       port->subif_info[i].
			       mib.rx_fn_txif_pkt);
			PRINTK("          : rx_fn_dropped  =0x%08x\n",
			       port->subif_info[i].
			       mib.rx_fn_dropped);
			PRINTK("          : tx_cbm_pkt     =0x%08x\n",
			       port->subif_info[i].mib.tx_cbm_pkt);
			PRINTK("          : tx_tso_pkt     =0x%08x\n",
			       port->subif_info[i].mib.tx_tso_pkt);
			PRINTK("          : tx_pkt_dropped =0x%08x\n",
			       port->subif_info[i].
			       mib.tx_pkt_dropped);
			PRINTK("          : tx_clone_pkt   =0x%08x\n",
			       port->subif_info[i].
			       mib.tx_clone_pkt);
			PRINTK("          : tx_hdr_room_pkt=0x%08x\n",
			       port->subif_info[i].
			       mib.tx_hdr_room_pkt);
		}
	}

	ret = PRINTK("    rx_err_drop=0x%08x  tx_err_drop=0x%08x\n",
		     port->rx_err_drop, port->tx_err_drop);
EXIT:
	return 0;
}

ssize_t proc_port_write(struct file *file, const char *buf, size_t count,
			loff_t *ppos)
{
	int len;
	char str[64];
	int num, i;
	u8 index_start = 0;
	u8 index_end = PMAC_MAX_NUM;
	u8 vap_start = 0;
	u8 vap_end = MAX_SUBIF_PER_PORT;
	char *param_list[10];

	len = (sizeof(str) > count) ? count : sizeof(str) - 1;
	len -= copy_from_user(str, buf, len);
	str[len] = 0;
	num = dp_split_buffer(str, param_list, ARRAY_SIZE(param_list));

	if (num < 1) goto help;

	if (param_list[1]) {
		index_start = dp_atoi(param_list[1]);
		index_end = index_start + 1;
	}

	if (param_list[2]) {
		vap_start = dp_atoi(param_list[2]);
		vap_end = vap_start + 1;
	}

	if (index_start >= PMAC_MAX_NUM) {
		PR_ERR("wrong index: 0 ~ 15\n");
		return count;
	}

	if (vap_start >= MAX_SUBIF_PER_PORT) {
		PR_ERR("wrong VAP: 0 ~ 15\n");
		return count;
	}

	if (dp_strcmpi(param_list[0], "mib") == 0) {
		for (i = index_start; i < index_end; i++)
			display_port_info(i, vap_start, vap_end, 1);

	} else if (dp_strcmpi(param_list[0], "port") == 0) {
		for (i = index_start; i < index_end; i++)
			display_port_info(i, vap_start, vap_end, 0);

	} else goto help;

	return count;
help:
	PRINTK("usage: \n");
	PRINTK("  echo mib  [ep][vap] > /prooc/dp/port\n");
	PRINTK("  echo port [ep][vap] > /prooc/dp/port\n");

	return count;
}

void proc_dbg_read(struct seq_file *s)
{
	int i;

	seq_printf(s, "dp_dbg_flag=0x%08x\n", dp_dbg_flag);
	seq_printf(s, "Supported Flags =%d\n",
		   get_dp_dbg_flag_str_size());
	seq_printf(s, "Enabled Flags(0x%0x):", dp_dbg_flag);

	for (i = 0; i < get_dp_dbg_flag_str_size(); i++)
		if ((dp_dbg_flag & dp_dbg_flag_list[i]) ==
		    dp_dbg_flag_list[i])
			seq_printf(s, "%s ", dp_dbg_flag_str[i]);

	seq_printf(s, "\n\n");

	seq_printf(s, "dp_drop_all_tcp_err=%d @ 0x%p\n",
		   dp_drop_all_tcp_err, &dp_drop_all_tcp_err);
	seq_printf(s, "dp_pkt_size_check  =%d @ 0x%p\n",
		   dp_pkt_size_check, &dp_pkt_size_check);

	seq_printf(s, "dp_rx_test_mode  =%d @ 0x%p\n",
		   dp_rx_test_mode, &dp_rx_test_mode);

	print_parser_status(s);
}

ssize_t proc_dbg_write(struct file *file, const char *buf, size_t count,
		       loff_t *ppos)
{
	int len, i, j;
	char str[64];
	int num;
	char *param_list[20];
	int f_enable;

	len = (sizeof(str) > count) ? count : sizeof(str) - 1;
	len -= copy_from_user(str, buf, len);
	str[len] = 0;
	num = dp_split_buffer(str, param_list, ARRAY_SIZE(param_list));

	if (dp_strcmpi(param_list[0], "enable") == 0)
		f_enable = 1;
	else if (dp_strcmpi(param_list[0], "disable") == 0)
		f_enable = -1;
	else
		goto help;

	if (!param_list[1]) {	/*no parameter after enable or disable */
		set_ltq_dbg_flag(dp_dbg_flag, f_enable, -1);
		goto EXIT;
	}

	for (i = 1; i < num; i++) {
		for (j = 0; j < get_dp_dbg_flag_str_size(); j++)
			if (dp_strcmpi(param_list[i],
				       dp_dbg_flag_str[j]) == 0) {
				set_ltq_dbg_flag(dp_dbg_flag,
						 f_enable, dp_dbg_flag_list[j]);
				break;
			}
	}

EXIT:
	return count;
help:
	PRINTK("echo <enable/disable> ");

	for (i = 0; i < get_dp_dbg_flag_str_size(); i++)
		PRINTK("%s ", dp_dbg_flag_str[i]);

	PRINTK(" > /proc/dp/dbg\n");
	PRINTK(" display command command: cat /proc/dp/cmd\n");
	
	return count;
}

static void proc_parser_read(struct seq_file *s)
{
	int8_t cpu, mpe1, mpe2, mpe3;

	dp_get_gsw_parser(&cpu, &mpe1, &mpe2, &mpe3);
	seq_printf(s, "cpu : %s with parser size =%d bytes\n",
		   parser_flag_str(cpu), parser_size_via_index(0));
	seq_printf(s, "mpe1: %s with parser size =%d bytes\n",
		   parser_flag_str(mpe1), parser_size_via_index(1));
	seq_printf(s, "mpe2: %s with parser size =%d bytes\n",
		   parser_flag_str(mpe2), parser_size_via_index(2));
	seq_printf(s, "mpe3: %s with parser size =%d bytes\n",
		   parser_flag_str(mpe3), parser_size_via_index(3));
}

ssize_t proc_parser_write(struct file *file, const char *buf, size_t count,
			  loff_t *ppos)
{
	int len;
	char str[64];
	int num, i;
	char *param_list[20];
	int8_t cpu = 0, mpe1 = 0, mpe2 = 0, mpe3 = 0, flag = 0;
	static int pce_rule_id = 0;
	union gsw_var rule;

	len = (sizeof(str) > count) ? count : sizeof(str) - 1;
	len -= copy_from_user(str, buf, len);
	str[len] = 0;
	num = dp_split_buffer(str, param_list, ARRAY_SIZE(param_list));

	if (dp_strcmpi(param_list[0], "enable") == 0) {
		for (i = 1; i < num; i++) {
			if (dp_strcmpi(param_list[i], "cpu") == 0) {
				flag |= 0x1;
				cpu = 2;
			}

			if (dp_strcmpi(param_list[i], "mpe1") == 0) {
				flag |= 0x2;
				mpe1 = 2;
			}

			if (dp_strcmpi(param_list[i], "mpe2") == 0) {
				flag |= 0x4;
				mpe2 = 2;
			}

			if (dp_strcmpi(param_list[i], "mpe3") == 0) {
				flag |= 0x8;
				mpe3 = 2;
			}
		}

		if (!flag) {
			flag = 0x1 | 0x2 | 0x4 | 0x8;
			cpu = mpe1 = mpe2 = mpe3 = 2;
		}

		DP_DEBUG(DP_DBG_FLAG_DBG, "flag=0x%x mpe3/2/1/cpu=%d/%d/%d/%d\n",
		       flag, mpe3, mpe2, mpe1, cpu);
		dp_set_gsw_parser(flag, cpu, mpe1, mpe2, mpe3);
	} else if (dp_strcmpi(param_list[0], "disable") == 0) {
		for (i = 1; i < num; i++) {
			if (dp_strcmpi(param_list[i], "cpu") == 0) {
				flag |= 0x1;
				cpu = 0;
			}

			if (dp_strcmpi(param_list[i], "mpe1") == 0) {
				flag |= 0x2;
				mpe1 = 0;
			}

			if (dp_strcmpi(param_list[i], "mpe2") == 0) {
				flag |= 0x4;
				mpe2 = 0;
			}

			if (dp_strcmpi(param_list[i], "mpe3") == 0) {
				flag |= 0x8;
				mpe3 = 0;
			}
		}

		if (!flag) {
			flag = 0x1 | 0x2 | 0x4 | 0x8;
			cpu = mpe1 = mpe2 = mpe3 = 0;
		}

		DP_DEBUG(DP_DBG_FLAG_DBG, "flag=0x%x mpe3/2/1/cpu=%d/%d/%d/%d\n",
		       flag, mpe3, mpe2, mpe1, cpu);
		dp_set_gsw_parser(flag, cpu, mpe1, mpe2, mpe3);
	} else if (dp_strcmpi(param_list[0], "refresh") == 0) {
		dp_get_gsw_parser(NULL, NULL, NULL, NULL);
		PR_INFO("value:cpu=%d mpe1=%d mpe2=%d mpe3=%d\n",
			pinfo[0].v, pinfo[1].v, pinfo[2].v, pinfo[3].v);
		PR_INFO("size :cpu=%d mpe1=%d mpe2=%d mpe3=%d\n",
			pinfo[0].size, pinfo[1].size,
			pinfo[2].size, pinfo[3].size);
		return count;
	} else if(dp_strcmpi(param_list[0], "mark") == 0) {
		int flag = dp_atoi(param_list[1]);
		if (flag <0 )
			flag = 0;
		else if (flag >3) flag = 3;
		PR_INFO("eProcessPath_Action set to %d\n", flag);
		
		/*: All packets set to same mpe flag as specified*/
	     	memset(&rule,0,sizeof(rule));
		rule.pce.pattern.nIndex=pce_rule_id;
		rule.pce.pattern.bEnable=1;
	        
		rule.pce.pattern.bParserFlagMSB_Enable = 1;
	        //rule.pce.pattern.nParserFlagMSB = 0x0021;
	        rule.pce.pattern.nParserFlagMSB_Mask = 0xffff;
	        rule.pce.pattern.bParserFlagLSB_Enable = 1;
	        //rule.pce.pattern.nParserFlagLSB = 0x0000;
	        rule.pce.pattern.nParserFlagLSB_Mask = 0xffff;
	        //rule.pce.pattern.eDstIP_Select = 2;
		//in6_pton("ff00:0:0:0:0:0:0:0",-1,(void*)&rule.pce.pattern.nDstIP.nIPv6,-1,&end);
		rule.pce.pattern.nDstIP_Mask= 0xffffffff;
	        rule.pce.pattern.bDstIP_Exclude= 0;
		
		rule.pce.action.bRtDstPortMaskCmp_Action = 1;
		rule.pce.action.bRtSrcPortMaskCmp_Action = 1;
		rule.pce.action.bRtDstIpMaskCmp_Action = 1;
		rule.pce.action.bRtSrcIpMaskCmp_Action = 1;

		rule.pce.action.bRoutExtId_Action=1;
		rule.pce.action.nRoutExtId = 0; /*RT_EXTID_UDP;*/
		rule.pce.action.bRtAccelEna_Action=1;
		rule.pce.action.bRtCtrlEna_Action = 1;
		rule.pce.action.eProcessPath_Action = flag;
		rule.pce.action.bRMON_Action = 1; 
		rule.pce.action.nRMON_Id = 0; /*RMON_UDP_CNTR;*/

		if(dp_gsw_kioctl(GSWIP_R_DEV_NAME, GSW_PCE_RULE_WRITE, (u32)&rule)) {
			PRINTK( "PCE rule add returned failure for GSW_PCE_RULE_WRITE \n");
		    	return count;
		}
		
	} else if(dp_strcmpi(param_list[0], "unmark") == 0) {
		/*: All packets set to same mpe flag as specified*/
		memset(&rule,0,sizeof(rule));
		rule.pce.pattern.nIndex=pce_rule_id;
		rule.pce.pattern.bEnable=0;
		if(dp_gsw_kioctl(GSWIP_R_DEV_NAME, GSW_PCE_RULE_WRITE, (u32)&rule)) {
			PRINTK( "PCE rule add returned failure for GSW_PCE_RULE_WRITE \n");
		    	return count;
		}
	} else {
		PRINTK("Usage: echo <enable/disable> [cpu] [mpe1] [mpe2] [mpe3] > parser\n");
		PRINTK("Usage: echo <refresh> parser\n");

		PRINTK("Usage: echo mark eProcessPath_Action_value(0~3) > parser\n");
		PRINTK("Usage: echo unmark > parser\n");
		return count;
	}

	return count;
}

#define MAX_GSW_L_PMAC_PORT  7
#define MAX_GSW_R_PMAC_PORT  16
static GSW_RMON_Port_cnt_t gsw_l_rmon_mib[MAX_GSW_L_PMAC_PORT];
static GSW_RMON_Port_cnt_t gsw_r_rmon_mib[MAX_GSW_R_PMAC_PORT];
static GSW_RMON_Redirect_cnt_t  gswr_rmon_redirect;
enum RMON_MIB_TYPE {
	RX_GOOD_PKTS = 0,
	RX_FILTER_PKTS,
	RX_DROP_PKTS,
	RX_OTHERS,

	TX_GOOD_PKTS,
	TX_ACM_PKTS,
	TX_DROP_PKTS,
	TX_OTHERS,

	REDIRECT_MIB,
	DP_DRV_MIB,

	/*last entry */
	RMON_MAX
};

static char f_q_mib_title_proc;

#define GSW_PORT_RMON_PRINT(res, title, var)  do { \
		res = seq_printf(s, \
				 "%-14s%10s %12u %12u %12u %12u %12u %12u %12u\n", \
				 title, "L(0-6)", \
				 gsw_l_rmon_mib[0].var, gsw_l_rmon_mib[1].var, \
				 gsw_l_rmon_mib[2].var, gsw_l_rmon_mib[3].var, \
				 gsw_l_rmon_mib[4].var, gsw_l_rmon_mib[5].var, \
				 gsw_l_rmon_mib[6].var); \
		res = seq_printf(s, \
				 "%-14s%10s %12u %12u %12u %12u %12u %12u %12u %12u\n", \
				 title, "R(0-6,15)", \
				 gsw_r_rmon_mib[0].var, gsw_r_rmon_mib[1].var, \
				 gsw_r_rmon_mib[2].var, gsw_r_rmon_mib[3].var, \
				 gsw_r_rmon_mib[4].var, gsw_r_rmon_mib[5].var, \
				 gsw_r_rmon_mib[6].var, gsw_r_rmon_mib[15].var); \
		res = seq_printf(s, \
				 "%-14s%10s %12u %12u %12u %12u %12u %12u %12u %12u\n", \
				 title, "R(7-14)", \
				 gsw_r_rmon_mib[7].var, gsw_r_rmon_mib[8].var, \
				 gsw_r_rmon_mib[9].var, gsw_r_rmon_mib[10].var, \
				 gsw_r_rmon_mib[11].var, gsw_r_rmon_mib[12].var, \
				 gsw_r_rmon_mib[13].var, gsw_r_rmon_mib[14].var); \
		res = seq_printf(s, "------------------------------------------------------------------------------------------------------------------------------------\n"); \
	} while (0)

int low_10dec(u64 x)
{ 
	char buf[26];
	char *p;
	int len;

	sprintf(buf, "%llu", x);
	len = strlen(buf);
	if(len >=10 ) {
		p = buf + len - 10;
	} else 
		p = buf;
	
	return dp_atoi(p);
}

int high_10dec(u64 x) 
{ 
	char buf[26];
	int len;

	sprintf(buf, "%llu", x);
	len = strlen(buf);
	if(len >=10 ) {
		buf[len - 10] = 0;
	} else 
		buf[0] = 0;
	
	return dp_atoi(buf);
}

#define GSW_PORT_RMON64_PRINT(res, title, var)  do { \
		res = seq_printf(s, \
				 "%-14s%10s %12u %12u %12u %12u %12u %12u %12u\n", \
				 title"(H)", "L(0-6)", \
				 high_10dec(gsw_l_rmon_mib[0].var), high_10dec(gsw_l_rmon_mib[1].var), \
				 high_10dec(gsw_l_rmon_mib[2].var), high_10dec(gsw_l_rmon_mib[3].var), \
				 high_10dec(gsw_l_rmon_mib[4].var), high_10dec(gsw_l_rmon_mib[5].var), \
				 high_10dec(gsw_l_rmon_mib[6].var)); \
		res = seq_printf(s, \
				 "%-14s%10s %12u %12u %12u %12u %12u %12u %12u\n", \
				 title"(L)", "L(0-6)", \
				 low_10dec(gsw_l_rmon_mib[0].var), low_10dec(gsw_l_rmon_mib[1].var), \
				 low_10dec(gsw_l_rmon_mib[2].var), low_10dec(gsw_l_rmon_mib[3].var), \
				 low_10dec(gsw_l_rmon_mib[4].var), low_10dec(gsw_l_rmon_mib[5].var), \
				 low_10dec(gsw_l_rmon_mib[6].var)); \
		res = seq_printf(s, \
				 "%-14s%10s %12u %12u %12u %12u %12u %12u %12u %12u\n", \
				 title"(H)", "R(0-6,15)", \
				 high_10dec(gsw_r_rmon_mib[0].var), high_10dec(gsw_r_rmon_mib[1].var), \
				 high_10dec(gsw_r_rmon_mib[2].var), high_10dec(gsw_r_rmon_mib[3].var), \
				 high_10dec(gsw_r_rmon_mib[4].var), high_10dec(gsw_r_rmon_mib[5].var), \
				 high_10dec(gsw_r_rmon_mib[6].var), high_10dec(gsw_r_rmon_mib[15].var)); \
		res = seq_printf(s, \
				 "%-14s%10s %12u %12u %12u %12u %12u %12u %12u %12u\n", \
				 title"(L)", "R(0-6,15)", \
				 low_10dec(gsw_r_rmon_mib[0].var), low_10dec(gsw_r_rmon_mib[1].var), \
				 low_10dec(gsw_r_rmon_mib[2].var), low_10dec(gsw_r_rmon_mib[3].var), \
				 low_10dec(gsw_r_rmon_mib[4].var), low_10dec(gsw_r_rmon_mib[5].var), \
				 low_10dec(gsw_r_rmon_mib[6].var), low_10dec(gsw_r_rmon_mib[15].var)); \
		res = seq_printf(s, \
				 "%-14s%10s %12u %12u %12u %12u %12u %12u %12u %12u\n", \
				 title"(H)", "R(7-14)", \
				 high_10dec(gsw_r_rmon_mib[7].var), high_10dec(gsw_r_rmon_mib[8].var), \
				 high_10dec(gsw_r_rmon_mib[9].var), high_10dec(gsw_r_rmon_mib[10].var), \
				 high_10dec(gsw_r_rmon_mib[11].var), high_10dec(gsw_r_rmon_mib[12].var), \
				 high_10dec(gsw_r_rmon_mib[13].var), high_10dec(gsw_r_rmon_mib[14].var)); \
		res = seq_printf(s, \
				 "%-14s%10s %12u %12u %12u %12u %12u %12u %12u %12u\n", \
				 title"(L)", "R(7-14)", \
				 low_10dec(gsw_r_rmon_mib[7].var), low_10dec(gsw_r_rmon_mib[8].var), \
				 low_10dec(gsw_r_rmon_mib[9].var), low_10dec(gsw_r_rmon_mib[10].var), \
				 low_10dec(gsw_r_rmon_mib[11].var), low_10dec(gsw_r_rmon_mib[12].var), \
				 low_10dec(gsw_r_rmon_mib[13].var), low_10dec(gsw_r_rmon_mib[14].var)); \
		res = seq_printf(s, "------------------------------------------------------------------------------------------------------------------------------------\n"); \
	} while (0)

int proc_gsw_port_rmon_dump(struct seq_file *s, int pos)
{
	int i;
	int ret = 0;
	GSW_API_HANDLE gsw_handle = 0;
	char flag_buf[20];

	if (pos == 0) {
		memset(gsw_r_rmon_mib, 0, sizeof(gsw_r_rmon_mib));
		memset(gsw_l_rmon_mib, 0, sizeof(gsw_l_rmon_mib));

		/*read gswip-r rmon counter*/
		gsw_handle = gsw_api_kopen((char *)GSWIP_R_DEV_NAME);

		if (gsw_handle == 0) {
			PR_ERR("Open GSWIP-R device FAILED !\n");
			return -1;
		}

		//seq_printf(s, "GSW-R gsw_handle=0x%x\n", gsw_handle);
		for (i = 0; i < ARRAY_SIZE(gsw_r_rmon_mib); i++) {
			gsw_r_rmon_mib[i].nPortId = i;
			ret = gsw_api_kioctl(gsw_handle, GSW_RMON_PORT_GET,
					     (u32)&gsw_r_rmon_mib[i]);

			if (ret != GSW_statusOk) {
				PR_ERR("RMON_PORT_GET fail for Port %d\n", i);
				return -1;
			}
		}

		/*read pmac rmon redirect mib */
		memset(&gswr_rmon_redirect, 0, sizeof(gswr_rmon_redirect));
		ret = gsw_api_kioctl(gsw_handle, GSW_RMON_REDIRECT_GET,
				     (u32)&gswr_rmon_redirect);

		if (ret != GSW_statusOk) {
			PR_ERR("GSW_RMON_REDIRECT_GET fail for Port %d\n", i);
			return -1;
		}

		gsw_api_kclose(gsw_handle);

		/*read gswip-l rmon counter*/
		gsw_handle = gsw_api_kopen((char *)GSWIP_L_DEV_NAME);

		if (gsw_handle == 0) {
			PR_ERR("Open GSWIP-L FAILED !!\n");
			return -1;
		}

		//seq_printf(s, "GSW-L gsw_handle=0x%x\n", gsw_handle);
		for (i = 0; i < ARRAY_SIZE(gsw_l_rmon_mib); i++) {
			gsw_l_rmon_mib[i].nPortId = i;
			ret = gsw_api_kioctl(gsw_handle, GSW_RMON_PORT_GET,
					     (u32)&gsw_l_rmon_mib[i]);

			if (ret != GSW_statusOk) {
				PR_ERR("RMON_PORT_GET fail for Port %d\n", i);
				return -1;
			}
		}

		gsw_api_kclose(gsw_handle);

		ret = seq_printf(s, "%-24s %12u %12u %12u %12u %12u %12u %12u\n",
				 "GSWIP-L", 0, 1, 2, 3, 4, 5, 6);
		ret = seq_printf(s, "%-24s %12u %12u %12u %12u %12u %12u %12u %12u\n",
				 "GSWIP-R(Fixed)", 0, 1, 2, 3, 4, 5, 6, 15);
		ret = seq_printf(s, "%-24s %12u %12u %12u %12u %12u %12u %12u %12u\n",
				 "GSWIP-R(Dynamic)", 7, 8, 9, 10, 11, 12, 13, 14);
		ret = seq_printf(s, "------------------------------------------------------------------------------------------------------------------------------------\n");
	}

	if (pos == RX_GOOD_PKTS)
		GSW_PORT_RMON_PRINT(ret, "RX_Good", nRxGoodPkts);
	else if (pos == RX_FILTER_PKTS)
		GSW_PORT_RMON_PRINT(ret, "RX_FILTER", nRxFilteredPkts);
	else if (pos == RX_DROP_PKTS)
		GSW_PORT_RMON_PRINT(ret, "RX_DROP", nRxDroppedPkts);
	else if (pos == RX_OTHERS) {
		if (!rmon_display_port_full)
			goto NEXT;

		GSW_PORT_RMON_PRINT(ret, "RX_UNICAST", nRxUnicastPkts);
		GSW_PORT_RMON_PRINT(ret, "RX_BROADCAST", nRxBroadcastPkts);
		GSW_PORT_RMON_PRINT(ret, "RX_MULTICAST", nRxMulticastPkts);
		GSW_PORT_RMON_PRINT(ret, "RX_FCS_ERR", nRxFCSErrorPkts);
		GSW_PORT_RMON_PRINT(ret, "RX_UNDER_GOOD", nRxUnderSizeGoodPkts);
		GSW_PORT_RMON_PRINT(ret, "RX_OVER_GOOD", nRxOversizeGoodPkts);
		GSW_PORT_RMON_PRINT(ret, "RX_UNDER_ERR", nRxUnderSizeErrorPkts);
		GSW_PORT_RMON_PRINT(ret, "RX_OVER_ERR", nRxOversizeErrorPkts);
		GSW_PORT_RMON_PRINT(ret, "RX_ALIGN_ERR", nRxAlignErrorPkts);
		GSW_PORT_RMON_PRINT(ret, "RX_64B", nRx64BytePkts);
		GSW_PORT_RMON_PRINT(ret, "RX_127B", nRx127BytePkts);
		GSW_PORT_RMON_PRINT(ret, "RX_255B", nRx255BytePkts);
		GSW_PORT_RMON_PRINT(ret, "RX_511B", nRx511BytePkts);
		GSW_PORT_RMON_PRINT(ret, "RX_1023B", nRx1023BytePkts);
		GSW_PORT_RMON_PRINT(ret, "RX_MAXB", nRxMaxBytePkts);
		GSW_PORT_RMON64_PRINT(ret, "RX_BAD_b", nRxBadBytes);
	} else if (pos == TX_GOOD_PKTS)
		GSW_PORT_RMON_PRINT(ret, "TX_Good", nTxGoodPkts);
	else if (pos == TX_ACM_PKTS)
		GSW_PORT_RMON_PRINT(ret, "TX_ACM_DROP", nTxAcmDroppedPkts);
	else if (pos == TX_DROP_PKTS)
		GSW_PORT_RMON_PRINT(ret, "TX_Drop", nTxDroppedPkts);
	else if (pos == TX_OTHERS) {
		if (!rmon_display_port_full)
			goto NEXT;

		GSW_PORT_RMON_PRINT(ret, "TX_UNICAST", nTxUnicastPkts);
		GSW_PORT_RMON_PRINT(ret, "TX_BROADAST", nTxBroadcastPkts);
		GSW_PORT_RMON_PRINT(ret, "TX_MULTICAST", nTxMulticastPkts);
		GSW_PORT_RMON_PRINT(ret, "TX_SINGLE_COLL", nTxSingleCollCount);
		GSW_PORT_RMON_PRINT(ret, "TX_MULT_COLL", nTxMultCollCount);
		GSW_PORT_RMON_PRINT(ret, "TX_LATE_COLL", nTxLateCollCount);
		GSW_PORT_RMON_PRINT(ret, "TX_EXCESS_COLL", nTxExcessCollCount);
		GSW_PORT_RMON_PRINT(ret, "TX_COLL", nTxCollCount);
		GSW_PORT_RMON_PRINT(ret, "TX_PAUSET", nTxPauseCount);
		GSW_PORT_RMON_PRINT(ret, "TX_64B", nTx64BytePkts);
		GSW_PORT_RMON_PRINT(ret, "TX_127B", nTx127BytePkts);
		GSW_PORT_RMON_PRINT(ret, "TX_255B", nTx255BytePkts);
		GSW_PORT_RMON_PRINT(ret, "TX_511B", nTx511BytePkts);
		GSW_PORT_RMON_PRINT(ret, "TX_1023B", nTx1023BytePkts);
		GSW_PORT_RMON_PRINT(ret, "TX_MAX_B", nTxMaxBytePkts);
		GSW_PORT_RMON_PRINT(ret, "TX_UNICAST", nTxUnicastPkts);
		GSW_PORT_RMON_PRINT(ret, "TX_UNICAST", nTxUnicastPkts);
		GSW_PORT_RMON_PRINT(ret, "TX_UNICAST", nTxUnicastPkts);
		GSW_PORT_RMON64_PRINT(ret, "TX_GOOD_b", nTxGoodBytes);

	} else if (pos == REDIRECT_MIB) {
		/*GSWIP-R PMAC Redirect conter */
		ret = seq_printf(s, "%-24s %12s %12s %12s %12s\n",
				 "GSW-R Redirect",
				 "Rx_Pkts", "Tx_Pkts", "Rx_DropsPkts", "Tx_DropsPkts");

		ret = seq_printf(s, "%-24s %12d %12d %12d %12d\n", "",
				 gswr_rmon_redirect.nRxPktsCount,
				 gswr_rmon_redirect.nTxPktsCount,
				 gswr_rmon_redirect.nRxDiscPktsCount,
				 gswr_rmon_redirect.nTxDiscPktsCount);
		ret = seq_printf(s, "------------------------------------------------------------------------------------------------------------------------------------\n");
	} else if (pos == DP_DRV_MIB) {
		u64 eth0_rx = 0, eth0_tx = 0;
		u64 eth1_rx = 0, eth1_tx = 0;
		u64 dsl_rx = 0, dsl_tx = 0;
		u64 other_rx = 0, other_tx = 0;
		int i , j;
		struct pmac_port_info *port;

		for (i = 1; i < PMAC_MAX_NUM; i++) {
			port = get_port_info(i);

			if (!port) continue;

			if (i < 6) {
				for (j = 0; j < 16; j++) {
					eth0_rx += port->subif_info[j].mib.rx_fn_rxif_pkt;
					eth0_rx += port->subif_info[j].mib.rx_fn_txif_pkt;
					eth0_tx += port->subif_info[j].mib.tx_cbm_pkt;
					eth0_tx += port->subif_info[j].mib.tx_tso_pkt;
				}
			} else if (i == 15) {
				for (j = 0; j < 16; j++) {
					eth1_rx += port->subif_info[j].mib.rx_fn_rxif_pkt;
					eth1_rx += port->subif_info[j].mib.rx_fn_txif_pkt;
					eth1_tx += port->subif_info[j].mib.tx_cbm_pkt;
					eth1_tx += port->subif_info[j].mib.tx_tso_pkt;
				}
			} else if (port->alloc_flags & DP_F_FAST_DSL) {
				for (j = 0; j < 16; j++) {
					dsl_rx += port->subif_info[j].mib.rx_fn_rxif_pkt;
					dsl_rx += port->subif_info[j].mib.rx_fn_txif_pkt;
					dsl_tx += port->subif_info[j].mib.tx_cbm_pkt;
					dsl_tx += port->subif_info[j].mib.tx_tso_pkt;
				}
			} else {
				for (j = 0; j < 16; j++) {
					other_rx += port->subif_info[j].mib.rx_fn_rxif_pkt;
					other_rx += port->subif_info[j].mib.rx_fn_txif_pkt;
					other_tx += port->subif_info[j].mib.tx_cbm_pkt;
					other_tx += port->subif_info[j].mib.tx_tso_pkt;
				}
			}
		}

		ret = seq_printf(s, "%-15s %22s %22s %22s %22s\n",
				 "DP Drv Mib",
				 "ETH_LAN", "ETH_WAN", "DSL", "Others");

		ret = seq_printf(s, "%15s %22llu %22llu %22llu %22llu\n",
				 "Rx_Pkts",
				 eth0_rx, eth1_rx, dsl_rx, other_rx);
		ret = seq_printf(s, "%15s %22llu %22llu %22llu %22llu\n",
				 "Tx_Pkts",
				 eth0_tx, eth1_tx, dsl_tx, other_tx);
		ret = seq_printf(s, "------------------------------------------------------------------------------------------------------------------------------------\n");
	} else if ((pos >= RMON_MAX) &&
		   (pos < (RMON_MAX + EGRESS_QUEUE_ID_MAX))) {
		uint32_t qdc[4], enq_c, deq_c, index;
		uint32_t wq, qrth, qocc, qavg;
		struct tmu_equeue_link equeue_link;
		char *flag_s;

		if (!rmon_display_tmu_mib)
			goto NEXT;

		index = pos - RMON_MAX;
		enq_c = get_enq_counter(index);
		deq_c = get_deq_counter(index);
		tmu_qdct_read(index, qdc);
		tmu_qoct_read(index, &wq, &qrth, &qocc, &qavg);
		tmu_equeue_link_get(index, &equeue_link);
		flag_s = get_dma_flags_str(equeue_link.epn,
					   flag_buf, sizeof(flag_buf));

		if ((enq_c || deq_c || (qdc[0] + qdc[1] + qdc[2] + qdc[3]))
		    || qocc || qavg) {
			if (!f_q_mib_title_proc) {
				ret = seq_printf(s, "%-15s %10s %10s %10s (%10s %10s %10s %10s) %10s %10s %10s\n",
						 "TMU MIB     QID", "enq", "deq",
						 "drop", "No-Color", "Green", "Yellow", "Red",
						 "qocc", "qavg", "  DMA  Flag");
				f_q_mib_title_proc = 1;
			}

			ret = seq_printf(s, "%15d %10u %10u %10u (%10u %10u %10u %10u) %10u %10u %10s\n",
					 index, enq_c, deq_c,
					 (qdc[0] + qdc[1] + qdc[2] + qdc[3]),
					 qdc[0], qdc[1], qdc[2], qdc[3], qocc,
					 (qavg >> 8), flag_s ? flag_s : "");

		} else
			goto NEXT;
	} else
		goto NEXT;

	if (ret) /*buffer over flow and don't increase pos */
		return pos;

NEXT:
	pos++;

	if (pos - RMON_MAX + 1 >= EGRESS_QUEUE_ID_MAX)
		return -1;

	return pos;
}

int proc_gsw_rmon_port_start(void)
{
	f_q_mib_title_proc = 0;
	return 0;
}

void dp_mib_reset_all(uint32_t flag)
{
#ifdef CONFIG_LTQ_DATAPATH_MIB
	dp_reset_mib(0);
#else
	gsw_mib_reset(0, 0);
	gsw_mib_reset(1, 0);
#endif
}

ssize_t proc_gsw_rmon_write(struct file *file, const char *buf, size_t count,
			    loff_t *ppos)
{
	int len;
	char str[64];
	int num;
	char *param_list[10];

	len = (sizeof(str) > count) ? count : sizeof(str) - 1;
	len -= copy_from_user(str, buf, len);
	str[len] = 0;
	num = dp_split_buffer(str, param_list, ARRAY_SIZE(param_list));

	if (num < 1) goto help;

	if (dp_strcmpi(param_list[0], "clear") == 0 ||
	    dp_strcmpi(param_list[0], "c") == 0 ||
	    dp_strcmpi(param_list[0], "rest") == 0 ||
	    dp_strcmpi(param_list[0], "r") == 0) {
		dp_mib_reset_all(0);
		goto EXIT_OK;
	}

	if (dp_strcmpi(param_list[0], "TMU") == 0) {
		if (dp_strcmpi(param_list[1], "on") == 0) {
			rmon_display_tmu_mib = 1;
			goto EXIT_OK;
		} else if (dp_strcmpi(param_list[1], "off") == 0) {
			rmon_display_tmu_mib = 0;
			goto EXIT_OK;
		}
	}

	if (dp_strcmpi(param_list[0], "RMON") == 0) {
		if (dp_strcmpi(param_list[1], "Full") == 0) {
			rmon_display_port_full = 1;
			goto EXIT_OK;
		} else if (dp_strcmpi(param_list[1], "Basic") == 0) {
			rmon_display_port_full = 0;
			goto EXIT_OK;
		}
	}

	/*unknow command */
	goto help;

EXIT_OK:
	return count;

help:
	PRINTK("usage: echo clear > /proc/dp/rmon\n");
	PRINTK("usage: echo TMU on > /proc/dp/rmon\n");
	PRINTK("usage: echo TMU off > /proc/dp/rmon\n");
	PRINTK("usage: echo RMON Full > /proc/dp/rmon\n");
	PRINTK("usage: echo RMON Basic > /proc/dp/rmon\n");
	return count;
}

/**
* \brief directly read memory address with 4 bytes alignment.
* \param  reg_addr memory address ( it must be 4 bytes alignment)
* \param  shift to the expected bits ( its value is from 0 ~ 31)
* \param  size the bits number ( its value is from 1 ~ 32).
*  Note: shift + size <= 32
* \param  buffer destionation
* \return on Success return 0
*/
int32_t dp_mem_read(uint32_t reg_addr, uint32_t shift, uint32_t size, uint32_t *buffer, uint32_t base)
{
	u32 v;
	uint32_t mask = 0;
	int i;

	/*generate mask */
	for (i = 0; i < size; i++)
		mask |= 1 << i;

	mask = mask << shift;

	/*read data from specified address*/
	if (base == 4)
		v = *(u32 *)reg_addr;
	else if (base == 2)
		v = *(u16 *)reg_addr;
	else
		v = *(u8 *)reg_addr;

	v = dp_get_val(v, mask, shift);

	*buffer = v;
	return 0;
}

/**
* \brief directly write memory address with
* \param  reg_addr memory address ( it must be 4 bytes alignment)
* \param  shift to the expected bits ( its value is from 0 ~ 31)
* \param  size the bits number ( its value is from 1 ~ 32)
* \param  value value writen to
* \return on Success return 0
*/
int32_t dp_mem_write(uint32_t reg_addr, uint32_t shift, uint32_t size, uint32_t value, uint32_t base)
{
	u32 tmp = 0;
	uint32_t mask = 0;
	int i;

	/*generate mask */
	for (i = 0; i < size; i++)
		mask |= 1 << i;

	mask = mask << shift;

	/*read data from specified address*/
	if (base == 4)
		tmp = *(u32 *)reg_addr;
	else if (base == 2)
		tmp = *(u16 *)reg_addr;
	else if (base == 1)
		tmp = *(u8 *)reg_addr;
	else {
		PR_ERR("wrong base in dp_mem_write\n");
		return 0;
	}

	dp_set_val(tmp, value, mask, shift);

	if (base == 4)
		*(u32 *)reg_addr = tmp;
	else if (base == 2)
		*(u16 *)reg_addr = tmp;
	else
		*(u8 *)reg_addr = tmp;

	return 0;
}

#define MODE_ACCESS_BYTE  1
#define MODE_ACCESS_SHORT 2
#define MODE_ACCESS_DWORD 4

#define ACT_READ   1
#define ACT_WRITE  2
static int proc_write_mem(struct file *file, const char *buf, size_t count,
			  loff_t *ppos)
{
	char str[100];
	int num;
	char *param_list[20] = {NULL};
	int i, k, len;
	u32 line_max_num = 32; /* per line number printed */
	u32 addr = 0;
	u32 v = 0;
	u32 act = ACT_READ;
	u32 bit_offset = 0;
	u32 bit_num = 32;
	u32 repeat = 1;
	u32 mode = MODE_ACCESS_DWORD;
	int v_flag = 0;

	len = sizeof(str) < count ? sizeof(str) - 1 : count;
	len = len - copy_from_user(str, buf, len);
	str[len] = 0;
	num = dp_split_buffer(str, param_list, ARRAY_SIZE(param_list));

	if (num < 2)
		goto proc_help;

	if (dp_strncmpi(param_list[0], "w", 1) == 0)
		act = ACT_WRITE;
	else if (dp_strncmpi(param_list[0], "r", 1) == 0)
		act = ACT_READ;
	else
		goto proc_help;

	if (num < 3)
		goto proc_help;

	k = 1;

	while (k < num) {
		if (dp_strcmpi(param_list[k], "-s") == 0) {
			addr = dp_atoi(param_list[k + 1]);
			k += 2;
		} else if (dp_strcmpi(param_list[k], "-o") == 0) {
			bit_offset = dp_atoi(param_list[k + 1]);
			k += 2;
		} else if (dp_strcmpi(param_list[k], "-n") == 0) {
			bit_num = dp_atoi(param_list[k + 1]);
			k += 2;
		} else if (dp_strcmpi(param_list[k], "-r") == 0) {
			repeat = dp_atoi(param_list[k + 1]);
			k += 2;
		} else if (dp_strcmpi(param_list[k], "-v") == 0) {
			v = dp_atoi(param_list[k + 1]);
			k += 2;
			v_flag = 1;
		} else if (dp_strcmpi(param_list[k], "-b") == 0) {
			mode = MODE_ACCESS_BYTE;
			k += 1;
		} else if (dp_strcmpi(param_list[k], "-w") == 0) {
			mode = MODE_ACCESS_SHORT;
			k += 1;
		} else if (dp_strcmpi(param_list[k], "-d") == 0) {
			mode = MODE_ACCESS_DWORD;
			k += 1;
		} else {
			PR_INFO("unknown command option: %s\n", param_list[k]);
			break;
		}
	}

	if (bit_num > mode * 8)
		bit_num = mode * 8;

	if (repeat == 0)
		repeat = 1;

	if (!addr) {
		PR_ERR("addr cannot be zero\n");
		goto EXIT;
	}

	if ((mode != MODE_ACCESS_DWORD) && (mode != MODE_ACCESS_SHORT) &&
	    (mode != MODE_ACCESS_BYTE)) {
		PR_ERR("wrong access mode: %d bytes\n", mode);
		goto EXIT;
	}

	if ((act == ACT_WRITE) && !v_flag) {
		PR_ERR("For write command it needs to provide -v\n");
		goto EXIT;
	}

	if (bit_offset > mode * 8 - 1) {
		PR_ERR("valid bit_offset range:0 ~ %d\n", mode * 8 - 1);
		goto EXIT;
	}

	if ((bit_num > mode * 8) || (bit_num < 1)) {
		PR_ERR("valid bit_num range:0 ~ %d. Current bit_num=%d\n",
		       mode * 8, bit_num);
		goto EXIT;
	}

	if ((bit_offset + bit_num) > mode * 8) {
		PR_ERR("valid bit_offset+bit_num range:0 ~ %d\n", mode * 8);
		goto EXIT;
	}

	if ((addr % mode) != 0) { /*access alignment */
		PR_ERR("Cannot access 0x%08x in %d bytes\n",
		       addr, mode);
		goto EXIT;
	}

	line_max_num /= mode;

	if (act == ACT_WRITE)
		PRINTK("act=%s addr=0x%08x mode=%s bit_offset=%d bit_num=%d v=0x%08x\n",
		       "write",
		       addr,
		       (mode == MODE_ACCESS_DWORD) ? "dword" :
		       ((mode == MODE_ACCESS_SHORT) ? "short" : "DWORD"),
		       bit_offset, bit_num, v);
	else if (act == ACT_READ)
		PRINTK("act=%s addr=0x%08x mode=%s bit_offset=%d bit_num=%d\n",
		       "Read",
		       addr,
		       (mode == MODE_ACCESS_DWORD) ? "dword" :
		       ((mode == MODE_ACCESS_SHORT) ? "short" : "DWORD"),
		       bit_offset, bit_num);

	if (act == ACT_WRITE)
		for (i = 0; i < repeat; i++)
			dp_mem_write(addr + mode * i, bit_offset,
				     bit_num, v, mode);
	else {
		for (i = 0; i < repeat; i++) {
			v = 0;
			dp_mem_read(addr + mode * i, bit_offset, bit_num,
				    &v, mode);

			/*print format control*/
			if ((i % line_max_num) == 0)
				PRINTK("0x%08x: ", addr + mode * i);

			if (mode == MODE_ACCESS_DWORD)
				PRINTK("0x%08x ", v);
			else if (mode == MODE_ACCESS_SHORT)
				PRINTK("0x%04x ", v);
			else
				PRINTK("0x%02x ", v);

			if ((i % line_max_num) == (line_max_num - 1))
				PRINTK("\n");
		}
	}

	PRINTK("\n");
EXIT:
	return count;
proc_help:
	PR_INFO("echo <write/w> [-d/w/b] -s <start_v_addr> [-r <repeat_times>] -v <value> [-o <bit_offset>] [-n <bit_num>]\n");
	PR_INFO("echo <read/r>  [-d/w/b] -s <start_v_addr> [-r <repeat_times>] [-o <bit_offset>] [-n <bit_num>]\n");
	PR_INFO("\t -d: default read/write in dwords, ie 4 bytes\n");
	PR_INFO("\t -w: read/write in short, ie 2 bytes\n");
	PR_INFO("\t -b: read/write in bytes, ie 1 bytes\n");

	return count;
}

int proc_ep_dump(struct seq_file *s, int pos)
{
#if defined(NEW_CBM_API) && NEW_CBM_API
	uint32_t num;
	cbm_tmu_res_t *res = NULL;
	uint32_t flag = 0;
	int i;
	struct pmac_port_info *port = get_port_info(pos);

	if (cbm_dp_port_resources_get(&pos, &num, &res,
				      port ? port->alloc_flags : flag) == 0) {
		for (i = 0; i < num; i++) {
			seq_printf(s, "ep=%d tmu_port=%d queue=%d sid=%d\n", pos,
				   res[i].tmu_port,
				   res[i].tmu_q,
				   res[i].tmu_sched);
		}

		kfree(res);
	}

#endif
	pos++;

	if (pos >= PMAC_MAX_NUM)
		pos = -1;	/*end of the loop */

	return pos;
}
typedef int (*ingress_pmac_set_callback_t)(dp_pmac_cfg_t *pmac_cfg, u32 value);
typedef int (*egress_pmac_set_callback_t)(dp_pmac_cfg_t *pmac_cfg, u32 value);
struct ingress_pmac_entry {
	char *name;
	ingress_pmac_set_callback_t ingress_callback;
};
struct egress_pmac_entry {
	char *name;
	egress_pmac_set_callback_t  egress_callback;
};
static int ingress_err_disc_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	pmac_cfg->ig_pmac.err_disc = value;
	pmac_cfg->ig_pmac_flags = IG_PMAC_F_ERR_DISC;
	return 0;
}
static int ingress_pmac_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	pmac_cfg->ig_pmac.pmac = value;
	pmac_cfg->ig_pmac_flags = IG_PMAC_F_PRESENT;
	return 0;
}
static int ingress_pmac_pmap_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	pmac_cfg->ig_pmac.def_pmac_pmap = value;
	pmac_cfg->ig_pmac_flags = IG_PMAC_F_PMAP;
	return 0;
}
static int ingress_pmac_en_pmap_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	pmac_cfg->ig_pmac.def_pmac_en_pmap = value;
	pmac_cfg->ig_pmac_flags = IG_PMAC_F_PMAPENA;
	return 0;
}
static int ingress_pmac_tc_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	pmac_cfg->ig_pmac.def_pmac_tc = value;
	pmac_cfg->ig_pmac_flags = IG_PMAC_F_CLASS;
	return 0;
}
static int ingress_pmac_en_tc_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	pmac_cfg->ig_pmac.def_pmac_en_tc = value;
	pmac_cfg->ig_pmac_flags = IG_PMAC_F_CLASSENA;
	return 0;
}
static int ingress_pmac_subifid_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	pmac_cfg->ig_pmac.def_pmac_subifid = value;
	pmac_cfg->ig_pmac_flags = IG_PMAC_F_SUBIF;
	return 0;
}
static int ingress_pmac_srcport_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	pmac_cfg->ig_pmac.def_pmac_src_port = value;
	pmac_cfg->ig_pmac_flags = IG_PMAC_F_SPID;
	return 0;
}
static int ingress_pmac_hdr1_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	uint8_t hdr;
	hdr = (uint8_t)value;
	pmac_cfg->ig_pmac.def_pmac_hdr[0] = hdr;
	pmac_cfg->ig_pmac_flags = IG_PMAC_F_PMACHDR1;
	return 0;
}
static int ingress_pmac_hdr2_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	uint8_t hdr;
	hdr = (uint8_t)value;
	pmac_cfg->ig_pmac.def_pmac_hdr[1] = hdr;
	pmac_cfg->ig_pmac_flags = IG_PMAC_F_PMACHDR2;
	return 0;
}
static int ingress_pmac_hdr3_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	uint8_t hdr;
	hdr = (uint8_t)value;
	pmac_cfg->ig_pmac.def_pmac_hdr[2] = hdr;
	pmac_cfg->ig_pmac_flags = IG_PMAC_F_PMACHDR3;
	return 0;
}
static int ingress_pmac_hdr4_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	uint8_t hdr;
	hdr = (uint8_t)value;
	pmac_cfg->ig_pmac.def_pmac_hdr[3] = hdr;
	pmac_cfg->ig_pmac_flags = IG_PMAC_F_PMACHDR4;
	return 0;
}
static int ingress_pmac_hdr5_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	uint8_t hdr;
	hdr = (uint8_t)value;
	pmac_cfg->ig_pmac.def_pmac_hdr[4] = hdr;
	pmac_cfg->ig_pmac_flags = IG_PMAC_F_PMACHDR5;
	return 0;
}
static int ingress_pmac_hdr6_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	uint8_t hdr;
	hdr = (uint8_t)value;
	pmac_cfg->ig_pmac.def_pmac_hdr[5] = hdr;
	pmac_cfg->ig_pmac_flags = IG_PMAC_F_PMACHDR6;
	return 0;
}
static int ingress_pmac_hdr7_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	uint8_t hdr;
	hdr = (uint8_t)value;
	pmac_cfg->ig_pmac.def_pmac_hdr[6] = hdr;
	pmac_cfg->ig_pmac_flags = IG_PMAC_F_PMACHDR7;
	return 0;
}
static int ingress_pmac_hdr8_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	uint8_t hdr;
	hdr = (uint8_t)value;
	pmac_cfg->ig_pmac.def_pmac_hdr[7] = hdr;
	pmac_cfg->ig_pmac_flags = IG_PMAC_F_PMACHDR8;
	return 0;
}
static int egress_fcs_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	pmac_cfg->eg_pmac.fcs = value;
	pmac_cfg->eg_pmac_flags = EG_PMAC_F_FCS;
	return 0;
}
static int egress_l2hdr_bytes_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	pmac_cfg->eg_pmac.num_l2hdr_bytes_rm = value;
	pmac_cfg->eg_pmac_flags = EG_PMAC_F_L2HDR_RM;
	return 0;
}
static int egress_rx_dma_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	pmac_cfg->eg_pmac.rx_dma_chan = value;
	pmac_cfg->eg_pmac_flags = EG_PMAC_F_RXID;
	return 0;
}
static int egress_pmac_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	pmac_cfg->eg_pmac.pmac = value;
	pmac_cfg->eg_pmac_flags = EG_PMAC_F_PMAC;
	return 0;
}
static int egress_res_dw_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	pmac_cfg->eg_pmac.res_dw1 = value;
	pmac_cfg->eg_pmac_flags = EG_PMAC_F_RESDW1;
	return 0;
}
static int egress_res1_dw_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	pmac_cfg->eg_pmac.res1_dw0 = value;
	pmac_cfg->eg_pmac_flags = EG_PMAC_F_RES1DW0;
	return 0;
}
static int egress_res2_dw_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	pmac_cfg->eg_pmac.res2_dw0 = value;
	pmac_cfg->eg_pmac_flags = EG_PMAC_F_RES2DW0;
	return 0;
}
static int egress_tc_ena_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	pmac_cfg->eg_pmac.tc_enable = value;
	pmac_cfg->eg_pmac_flags = EG_PMAC_F_TCENA;
	return 0;
}
static int egress_dec_flag_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	pmac_cfg->eg_pmac.dec_flag = value;
	pmac_cfg->eg_pmac_flags = EG_PMAC_F_DECFLG;
	return 0;
}
static int egress_enc_flag_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	pmac_cfg->eg_pmac.enc_flag = value;
	pmac_cfg->eg_pmac_flags = EG_PMAC_F_ENCFLG;
	return 0;
}
static int egress_mpe1_flag_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	pmac_cfg->eg_pmac.mpe1_flag = value;
	pmac_cfg->eg_pmac_flags = EG_PMAC_F_MPE1FLG;
	return 0;
}
static int egress_mpe2_flag_set(dp_pmac_cfg_t *pmac_cfg, u32 value)
{
	pmac_cfg->eg_pmac.mpe2_flag = value;
	pmac_cfg->eg_pmac_flags = EG_PMAC_F_MPE2FLG;
	return 0;
}

static struct ingress_pmac_entry ingress_entries[] = {
	{"errdisc", ingress_err_disc_set},
	{"pmac", ingress_pmac_set},
	{"pmac_pmap", ingress_pmac_pmap_set},
	{"pmac_en_pmap", ingress_pmac_en_pmap_set},
	{"pmac_tc", ingress_pmac_tc_set},
	{"pmac_en_tc", ingress_pmac_en_tc_set},
	{"pmac_subifid", ingress_pmac_subifid_set},
	{"pmac_srcport", ingress_pmac_srcport_set},
	{"pmac_hdr1", ingress_pmac_hdr1_set},
	{"pmac_hdr2", ingress_pmac_hdr2_set},
	{"pmac_hdr3", ingress_pmac_hdr3_set},
	{"pmac_hdr4", ingress_pmac_hdr4_set},
	{"pmac_hdr5", ingress_pmac_hdr5_set},
	{"pmac_hdr6", ingress_pmac_hdr6_set},
	{"pmac_hdr7", ingress_pmac_hdr7_set},
	{"pmac_hdr8", ingress_pmac_hdr8_set},
	{NULL, NULL}
};

static struct egress_pmac_entry egress_entries[] = {
	{"rx_dmachan", egress_rx_dma_set},
	{"rm_l2hdr", egress_l2hdr_bytes_set},
	{"fcs", egress_fcs_set},
	{"pmac", egress_pmac_set},
	{"res_dw1", egress_res_dw_set},
	{"res1_dw0", egress_res1_dw_set},
	{"res2_dw0", egress_res2_dw_set},
	{"tc_enable", egress_tc_ena_set},
	{"dec_flag", egress_dec_flag_set},
	{"enc_flag", egress_enc_flag_set},
	{"mpe1_flag", egress_mpe1_flag_set},
	{"mpe2_flag", egress_mpe2_flag_set},
	{NULL, NULL}
};

ssize_t ep_port_write(struct file *file, const char *buf, size_t count,
		      loff_t *ppos)
{
	int len;
	char str[64];
	int num, i, j, ret;
	u32 value;
	uint32_t port;
	char *param_list[10];
	dp_pmac_cfg_t pmac_cfg;
	memset(&pmac_cfg, 0, sizeof(dp_pmac_cfg_t));
	len = (sizeof(str) > count) ? count : sizeof(str) - 1;
	len -= copy_from_user(str, buf, len);
	str[len] = 0;
	num = dp_split_buffer(str, param_list, ARRAY_SIZE(param_list));

	if (dp_strcmpi(param_list[0], "ingress") == 0) {
		port = dp_atoi(param_list[1]);

		for (i = 2; i < num; i += 2) {
			for (j = 0; j < ARRAY_SIZE(ingress_entries); j++) {
				if (dp_strcmpi(param_list[i], ingress_entries[j].name) == 0) {
					value = dp_atoi(param_list[i + 1]);
					ingress_entries[j].ingress_callback(&pmac_cfg, value);
					PR_INFO("ingress pmac ep %s configured\n", ingress_entries[j].name);
					break;
				}
			}
		}

		ret = dp_pmac_set(port, &pmac_cfg);

		if (ret != 0) {
			PR_INFO("pmac set configuration failed\n");
			return -1;
		}
	} else if (dp_strcmpi(param_list[0], "egress") == 0) {
		port = dp_atoi(param_list[1]);

		for (i = 2; i < num; i += 2) {
			for (j = 0; j < ARRAY_SIZE(egress_entries); j++) {
				if (dp_strcmpi(param_list[i], egress_entries[j].name) == 0) {
					if (dp_strcmpi(egress_entries[j].name, "rm_l2hdr") == 0) {
						if (dp_atoi(param_list[i + 1]) > 0) {
							pmac_cfg.eg_pmac.rm_l2hdr = 1;
							value = dp_atoi(param_list[i + 1]);
							egress_entries[j].egress_callback(&pmac_cfg, value);
							PR_INFO("egress pmac ep %s configured successfully\n", egress_entries[j].name);
							break;
						}

						pmac_cfg.eg_pmac.rm_l2hdr = dp_atoi(param_list[i + 1]);
					} else {
						value = dp_atoi(param_list[i + 1]);
						egress_entries[j].egress_callback(&pmac_cfg, value);
						PR_INFO("egress pmac ep %s configured successfully\n", egress_entries[j].name);
						break;
					}
				}
			}
		}

		ret = dp_pmac_set(port, &pmac_cfg);

		if (ret != 0) {
			PR_INFO("pmac set configuration failed\n");
			return -1;
		}
	} else {
		PR_ERR("wrong command\n");
		goto help;
	}

	return count;
help:
	PR_INFO("echo ingress/egress [ep_port] ['ingress/egress fields'] [value] > /proc/dp/ep\n");
	PR_INFO("(eg) echo ingress 1 pmac 1 > /proc/dp/ep\n");
	PR_INFO("(eg) echo egress 1 rm_l2hdr 2 > /proc/dp/ep\n");
	PR_INFO("echo ingress [ep_port] ['errdisc/pmac/pmac_pmap/pmac_en_pmap/pmac_tc");
	PR_INFO("                         /pmac_en_tc/pmac_subifid/pmac_srcport'] [value] > /proc/dp/ep\n");
	PR_INFO("echo egress [ep_port] ['rx_dmachan/fcs/pmac/res_dw1/res1_dw0/res2_dw0] [value] > /proc/dp/ep\n");
	PR_INFO("echo egress [ep_port] ['rm_l2hdr'] [value] > /proc/dp/ep\n");
	return count;
}

void dp_send_packet(u8 *pdata, int len, char *devname, u32 flag)
{
	struct sk_buff *skb;
	dp_subif_t subif = {0};

	skb = cbm_alloc_skb(len + 8, GFP_ATOMIC);

	if (unlikely(!skb)) {
		PR_ERR("allocate cbm buffer fail\n");
		return;
	}

	skb->DW0 = 0;
	skb->DW1 = 0;
	skb->DW2 = 0;
	skb->DW3 = 0;
	memcpy(skb->data, pdata, len);
	skb->len = len;
	skb_put(skb, skb->len);
	skb->dev = dev_get_by_name(&init_net, devname);

	if (dp_get_netif_subifid(skb->dev, skb, NULL, skb->data, &subif, 0)) {
		PR_ERR("dp_get_netif_subifid failed for %s\n", skb->dev->name);
		dev_kfree_skb_any(skb);
		return;
	}

	((struct dma_tx_desc_1 *) & (skb->DW1))->field.ep = subif.port_id;
	((struct dma_tx_desc_0 *) & (skb->DW0))->field.dest_sub_if_id = subif.subif;
	dp_xmit(skb->dev, &subif, skb, skb->len, flag);
}

static u8 ipv4_plain_udp[] = {
	0x00, 0x00, 0x01, 0x00, 0x00, 0x01,  /*mac*/
	0x00, 0x10, 0x94, 0x00, 0x00, 0x02,
	0x08, 0x00, /*type*/
	0x45, 0x00, 0x00, 0x3E, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x11, /*ip header*/
	0x3A, 0x56, 0xC0, 0x55, 0x01, 0x02, 0xC0, 0x00, 0x00, 0x01,
	0x04, 0x00, 0x00, 0x00, 0x00, 0x2A, 0x7A, 0x41, 0x00, 0x00, /*udp header*/
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00
};
static u8 ipv4_plain_tcp[] = {
	0x00, 0x00, 0x01, 0x00, 0x00, 0x01, /*mac*/
	0x00, 0x10, 0x94, 0x00, 0x00, 0x02,
	0x08, 0x00,  /*type*/
	0x45, 0x00, 0x00, 0x3E, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x06, /*ip header*/
	0x3A, 0x61, 0xC0, 0x55, 0x01, 0x02, 0xC0, 0x00, 0x00, 0x01,
	0x04, 0x00, 0x04, 0x00, 0x00, 0x01, 0xE2, 0x40, 0x00, 0x03, /*tcp header*/
	0x94, 0x47, 0x50, 0x10, 0x10, 0x00, 0x9F, 0xD9, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00
};

static u8 ipv6_plain_udp[] = {
	0x00, 0x00, 0x01, 0x00, 0x00, 0x01,  /*mac*/
	0x00, 0x10, 0x94, 0x00, 0x00, 0x02,
	0x86, 0xDD, /*type*/
	0x60, 0x00, 0x00, 0x00, 0x00, 0x3E, 0x11, 0xFF, 0x20, 0x00, /*ip header*/
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x02, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
	0x04, 0x00, 0x00, 0x00, 0x00, 0x3E, 0xBB, 0x6F, 0x00, 0x00, /*udp header*/
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00
};
static u8 ipv6_plain_tcp[] = {
	0x00, 0x00, 0x01, 0x00, 0x00, 0x01, /*mac*/
	0x00, 0x10, 0x94, 0x00, 0x00, 0x02,
	0x86, 0xDD, /*type*/
	0x60, 0x00, 0x00, 0x00, 0x00, 0x46, 0x06, 0xFF, 0x20, 0x00, /*ip header*/
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x02, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
	0x04, 0x00, 0x04, 0x00, 0x00, 0x01, 0xE2, 0x40, 0x00, 0x03, /*tcp header*/
	0x94, 0x47, 0x50, 0x10, 0x10, 0x00, 0xE1, 0x13, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /*data*/
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

static u8 ipv6_extensions_udp[] = {
	0x00, 0x00, 0x01, 0x00, 0x00, 0x01,/*mac*/
	0x00, 0x10, 0x94, 0x00, 0x00, 0x02,
	0x86, 0xDD,/*type*/
	0x60, 0x00, 0x00, 0x00, 0x00, 0x8E, 0x00, 0xFF, 0x20, 0x00, /*ip header*/
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x02, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
	0x3C, 0x00, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00,/*next extension:hop*/
	0x2B, 0x00, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00,/*next extension:Destination*/
	0x11, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,/*next extension:Routing*/
	0x04, 0x00, 0x00, 0x00, 0x00, 0x76, 0xBA, 0xFF, 0x00, 0x00, /*udp header*/
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};
static u8 ipv6_extensions_tcp[] = {
	0x00, 0x00, 0x01, 0x00, 0x00, 0x01, /*mac*/
	0x00, 0x10, 0x94, 0x00, 0x00, 0x02,
	0x86, 0xDD, /*type*/
	0x60, 0x00, 0x00, 0x00, 0x00, 0x8E, 0x00, 0xFF, 0x20, 0x00, /*ip header*/
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x02, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
	0x3C, 0x00, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, /*next extension:hop*/
	0x2B, 0x00, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, /*next extension:Destination*/
	0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /*next extension:Routing*/
	0x04, 0x00, 0x04, 0x00, 0x00, 0x01, 0xE2, 0x40, 0x00, 0x03, /*tcp header*/
	0x94, 0x47, 0x50, 0x10, 0x10, 0x00, 0xE0, 0xE3, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

static u8 rd6_ip4_ip6_udp[] = {
	0x00, 0x00, 0x01, 0x00, 0x00, 0x01, /*mac*/
	0x00, 0x10, 0x94, 0x00, 0x00, 0x02,
	0x08, 0x00, /*type*/
	0x45, 0x00, 0x00, 0x6E, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x29, /*ip4 header*/
	0x3A, 0x0E, 0xC0, 0x55, 0x01, 0x02, 0xC0, 0x00, 0x00, 0x01,
	0x60, 0x00, 0x00, 0x00, 0x00, 0x32, 0x11, 0xFF, 0x20, 0x00, /*ip6 header*/
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x02, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
	0x04, 0x00, 0x00, 0x00, 0x00, 0x32, 0xBB, 0x87, 0x00, 0x00, /*udp header*/
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};
static u8 rd6_ip4_ip6_tcp[] = {
	0x00, 0x00, 0x01, 0x00, 0x00, 0x01,/*mac*/
	0x00, 0x10, 0x94, 0x00, 0x00, 0x02,
	0x08, 0x00, /*type*/
	0x45, 0x00, 0x00, 0x6E, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x29, /*ip4 header*/
	0x3A, 0x0E, 0xC0, 0x55, 0x01, 0x02, 0xC0, 0x00, 0x00, 0x01,
	0x60, 0x00, 0x00, 0x00, 0x00, 0x32, 0x06, 0xFF, 0x20, 0x00, /*ip6 header*/
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x02, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
	0x04, 0x00, 0x04, 0x00, 0x00, 0x01, 0xE2, 0x40, 0x00, 0x03, /*tcp header*/
	0x94, 0x47, 0x50, 0x10, 0x10, 0x00, 0xE1, 0x27, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};
static u8 dslite_ip6_ip4_udp[] = {
	0x00, 0x00, 0x01, 0x00, 0x00, 0x01, /*mac*/
	0x00, 0x10, 0x94, 0x00, 0x00, 0x02,
	0x86, 0xDD, /*type*/
	0x60, 0x00, 0x00, 0x00, 0x00, 0x46, 0x04, 0xFF, 0x20, 0x00, /*ip6 header*/
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x02, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
	0x45, 0x00, 0x00, 0x46, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x11, /*ip4 header*/
	0x3A, 0x4E, 0xC0, 0x55, 0x01, 0x02, 0xC0, 0x00, 0x00, 0x01,
	0x04, 0x00, 0x00, 0x00, 0x00, 0x32, 0x7A, 0x31, 0x00, 0x00, /*udp header*/
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

static u8 dslite_ip6_ip4_tcp[] = {
	0x00, 0x00, 0x01, 0x00, 0x00, 0x01,/*mac*/
	0x00, 0x10, 0x94, 0x00, 0x00, 0x02,
	0x86, 0xDD, /*type*/
	0x60, 0x00, 0x00, 0x00, 0x00, 0x46, 0x04, 0xFF, 0x20, 0x00,  /*ip6 header*/
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x02, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
	0x45, 0x00, 0x00, 0x46, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x06, /*ip4 header*/
	0x3A, 0x59, 0xC0, 0x55, 0x01, 0x02, 0xC0, 0x00, 0x00, 0x01,
	0x04, 0x00, 0x04, 0x00, 0x00, 0x01, 0xE2, 0x40, 0x00, 0x03, /*tcp header*/
	0x94, 0x47, 0x50, 0x10, 0x10, 0x00, 0x9F, 0xD1, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

void proc_checksum_read(struct seq_file *s)
{
	char *devname = "eth0_4";

	seq_printf(s, "\nsend pmac checksum ipv4_plain_udp new via %s\n", devname);
	dp_send_packet(ipv4_plain_udp, sizeof(ipv4_plain_udp),
		       devname, DP_TX_CAL_CHKSUM);

	seq_printf(s, "\nsend pmac checksum ipv4_plain_tcp new via %s\n", devname);
	dp_send_packet(ipv4_plain_tcp, sizeof(ipv4_plain_tcp),
		       devname, DP_TX_CAL_CHKSUM);

	seq_printf(s, "\nsend pmac checksum ipv6_plain_udp new via %s\n", devname);
	dp_send_packet(ipv6_plain_udp, sizeof(ipv6_plain_udp),
		       devname, DP_TX_CAL_CHKSUM);

	seq_printf(s, "\nsend pmac checksum ipv6_plain_tcp new via %s\n", devname);
	dp_send_packet(ipv6_plain_tcp, sizeof(ipv6_plain_tcp),
		       devname, DP_TX_CAL_CHKSUM);

	seq_printf(s, "\nsend pmac checksum ipv6_extensions_udp new via %s\n", devname);
	dp_send_packet(ipv6_extensions_udp, sizeof(ipv6_extensions_udp),
		       devname, DP_TX_CAL_CHKSUM);

	seq_printf(s, "\nsend pmac checksum ipv6_extensions_tcp via %s\n", devname);
	dp_send_packet(ipv6_extensions_tcp, sizeof(ipv6_extensions_tcp),
		       devname, DP_TX_CAL_CHKSUM);

	seq_printf(s, "\nsend pmac checksum rd6_ip4_ip6_udp via %s\n", devname);
	dp_send_packet(rd6_ip4_ip6_udp, sizeof(rd6_ip4_ip6_udp),
		       devname, DP_TX_CAL_CHKSUM);

	seq_printf(s, "\nsend pmac checksum rd6_ip4_ip6_tcp via %s\n", devname);
	dp_send_packet(rd6_ip4_ip6_tcp, sizeof(rd6_ip4_ip6_tcp),
		       devname, DP_TX_CAL_CHKSUM);

	seq_printf(s, "\nsend pmac checksum dslite_ip6_ip4_udp via %s\n", devname);
	dp_send_packet(dslite_ip6_ip4_udp, sizeof(dslite_ip6_ip4_udp),
		       devname, DP_TX_CAL_CHKSUM);

	seq_printf(s, "\nsend pmac checksum dslite_ip6_ip4_tcp via %s\n", devname);
	dp_send_packet(dslite_ip6_ip4_tcp, sizeof(dslite_ip6_ip4_tcp),
		       devname, DP_TX_CAL_CHKSUM);
}

int proc_dport_dump(struct seq_file *s, int pos)
{
	int i;
	cbm_dq_port_res_t res;
	uint32_t flag = 0;
	memset(&res, 0, sizeof(cbm_dq_port_res_t));

	if (cbm_dequeue_port_resources_get(pos, &res, flag) == 0) {
		seq_printf(s, "Dequeue port=%d free_base=0x%x\n", pos,
			   (u32)res.cbm_buf_free_base);

		for (i = 0; i < res.num_deq_ports; i++) {
			seq_printf(s, "%d:deq_port_base=0x%x num_desc=%d port = %d tx chan %d\n", i,
				   (u32)res.deq_info[i].cbm_dq_port_base,
				   res.deq_info[i].num_desc,
				   res.deq_info[i].port_no,
				   res.deq_info[i].dma_tx_chan);
		}

		if (res.deq_info)
			kfree(res.deq_info);
	}

	pos++;

	if (pos >= PMAC_MAX_NUM)
		pos = -1;	/*end of the loop */

	return pos;
}

int proc_common_cmd_start(void)
{
	return 0;
}


int proc_common_cmd_dump(struct seq_file *s, int pos)
{
	struct cmd_list {
		char *description;
		char *cmd;
	};
	int res = 0;
	static struct cmd_list cmd_list[]={
		{"Pecostat", "CPU utilization: pecostat -c pic0=0,pic1=1:EXL,K,S,U,IE 1"},
		{"Check gsw consumed buffer seg", "dev=1; switch_cli  dev=$dev GSW_REGISTER_GET nRegAddr=0x47"},
		{"Check gsw default Multicast map", "dev=1; switch_cli  dev=$dev GSW_REGISTER_GET nRegAddr=0x454"},
		{"Check gsw default unknown map", "dev=1; switch_cli  dev=$dev GSW_REGISTER_GET nRegAddr=0x455"},
		{"Check gsw backpressue", "\n     dev=0 \
			\n     tx_channel=0   #DMA TX Channel, Range: 0~15 \
			\n     switch_cli dev=$dev GSW_REGISTER_SET nRegAddr=0xD45 nData=$tx_channel \
			\n     switch_cli dev=$dev GSW_REGISTER_SET nRegAddr=0xD46 nData=0x8000 \
			\n     switch_cli dev=$dev GSW_REGISTER_GET nRegAddr=0xD42 #RX Port Congestion Mask (bit 15:0)\
			\n     switch_cli dev=$dev GSW_REGISTER_GET nRegAddr=0xD43 #TX Queue Congestion Mask (bit 15:0)\
			\n     switch_cli dev=$dev GSW_REGISTER_GET nRegAddr=0xD44 #TX Queue Congestion Mask (bit 31:16)"},
		{"Disable gsw backpressue", "\n     dev=0 \
			\n     tx_channel=0   #DMA TX Channel, Range: 0~15 \
			\n     switch_cli dev=$dev GSW_REGISTER_SET nRegAddr=0xD42 nData=0x0     \
			\n     switch_cli dev=$dev GSW_REGISTER_SET nRegAddr=0xD43 nData=0x0     \
			\n     switch_cli dev=$dev GSW_REGISTER_SET nRegAddr=0xD44 nData=0x0     \
			\n     switch_cli dev=$dev GSW_REGISTER_SET nRegAddr=0xD45 nData=$tx_channel \
			\n     switch_cli dev=$dev GSW_REGISTER_SET nRegAddr=0xD46 nData=0x8020"},
		{"Check egress pmac", "\n    dev=1 \
				       \n    nTrafficClass=1 #Range:0~15 \
				       \n    nFlowIDMsb=1 #Range:0~3 \
				       \n    nDestPortId=15 #Range:0~15 \
				       \n    switch_cli dev=$dev GSW_PMAC_EG_CFG_GET nTrafficClass=$nTrafficClass nFlowIDMsb=$nFlowIDMsb nDestPortId=$nDestPortId"},
		{"Check ingress pmac", "\n    dev=1; \
			                \n    nTxDmaChanId=1 #Range:0~15 \
			                \n    switch_cli dev=$dev GSW_PMAC_IG_CFG_GET nTxDmaChanId=$nTxDmaChanId"},
		{"Check gsw link status", "dev=0; nPortId=6; switch_cli dev=$dev GSW_PORT_LINK_CFG_GET nPortId=$nPortId"},
		{"Check gsw queue map", "dev=1; nPortId=6; switch_cli  dev=$dev GSW_QOS_QUEUE_PORT_GET nPortId=$nPortId"},
		{"Add static MAC addres", "\n    dev=1; nPortId=7; switch_cli dev=$dev GSW_MAC_TABLE_ENTRY_ADD nPortId=$nPortId nSubIfId=0x100 bStaticEntry=1 nMAC=00:10:94:00:00:01"},
		{"Disable PMAC Checksum", "dev=0; switch_cli  dev=$dev GSW_REGISTER_SET nRegAddr=0xD03 nData=0x0C0"},
		
		{"Debugger example", "\n    d.load.elf \"X:\\tmp2\\shaoguoh\\project\\ugw61_grx500\\openwrt\\core\\kernel_tree\\vmlinux\" /gnu /strippart \"/tmp2\" /path \"X:\\tmp2\" /nocode"},
		{"While example", "\n     i=0 \n     while [ $i -lt 16 ] \n     do \n       xxxx \n     i=$(($i+1)) \n     done"},
		{"For example", "\n     for i in 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 \n     do  \n       xxxxxx \n    done"},



		/*last place holder*/
		{NULL, NULL}
	};

	
	if (!cmd_list[pos].description) 
		return -1;

	if (!cmd_list[pos].cmd) {
		seq_printf(s, "key in wrongly cmd for pos=%d with command: %s\n",
			pos, cmd_list[pos].cmd);
	} else if (dp_strncmpi(cmd_list[pos].cmd, "\n    ", 5) == 0) {
		res=seq_printf(s,"---%s: ", cmd_list[pos].description);
		res=seq_printf(s,"%s\n\n", cmd_list[pos].cmd);
	} else 
		res=seq_printf(s,"---%s: %s\n\n", cmd_list[pos].description,
			cmd_list[pos].cmd);
	if (res)
		return pos; //repeat since proc buffer not enough

	pos++;

	if (pos >= ARRAY_SIZE(cmd_list))
		pos = -1;	/*end of the loop */

	return pos;

	
}
