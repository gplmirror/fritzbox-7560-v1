
#ifndef __DLRX_FW_DATA_STRUCTURE_H_
#define __DLRX_FW_DATA_STRUCTURE_H_

    #ifdef __LITTLE_ENDIAN
        #include "dlrx_fw_data_structure_le.h"
    #else
        #include "dlrx_fw_data_structure_be.h"
    #endif
#endif

