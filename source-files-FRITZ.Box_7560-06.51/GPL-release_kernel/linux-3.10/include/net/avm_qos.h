#pragma once
#include <linux/netdevice.h>

#ifdef CONFIG_AVM_QOS_GRX_TMU
#define AVM_QOS_PORT_NUM  16
#define AVM_QOS_TC_NUM 16
#else
#error "please define the appropriate HW resource limits"
#endif

int avm_qos_add_hw_queue(struct net_device *netdev,
                         uint32_t classid,
                         uint32_t priority,
                         uint32_t weight);

int avm_qos_remove_hw_queue(struct net_device *netdev,
                            uint32_t classid,
                            uint32_t priority,
                            uint32_t weight);

int avm_qos_flush_hw_queues(struct net_device *netdev);
int avm_qos_reset_prio_shaper(struct net_device *netdev, uint32_t classid);
int avm_qos_reset_port_shaper(struct net_device *netdev);
int avm_qos_set_port_shaper(struct net_device *netdev,
                            uint32_t pir,
                            uint32_t cir,
                            uint32_t pbs,
                            uint32_t cbs);
int avm_qos_set_prio_shaper(struct net_device *netdev,
                            uint32_t classid,
                            uint32_t pir,
                            uint32_t cir,
                            uint32_t pbs,
                            uint32_t cbs);
void avm_qos_set_default_queue(struct net_device *netdev, uint32_t classid);
