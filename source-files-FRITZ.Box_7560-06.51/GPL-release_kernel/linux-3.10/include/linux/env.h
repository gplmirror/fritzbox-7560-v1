/*------------------------------------------------------------------------------------------*\
 *
\*------------------------------------------------------------------------------------------*/
#ifndef _ENV_H_
#define _ENV_H_

/*------------------------------------------------------------------------------------------*\
 * [0] : 123
 * [1] : 1=paged flash, 0 restricted flash
 * [2] : 0=normal audio I2C adr, 1=altenative audio I2C adr
 * [3] : unused
 * [4] : unused
\*------------------------------------------------------------------------------------------*/
extern unsigned int davinci_revision[5];

enum _env_location {
    ENV_LOCATION_FLASH = 0,
    ENV_LOCATION_PHY_RAM = 1,
    ENV_LOCATION_VIRT_RAM = 2
};

#if IS_ENABLED(CONFIG_AVM_PROM_ENVIRONMENT)

extern void env_init(int *penv, enum _env_location );
extern char *prom_getenv(char *envname);
extern char *getcmdline(void);

#else
#warning AVM PROM environment support is NOT enabled!
#endif

/*------------------------------------------------------------------------------------------*\
 * Header WLAN - DECT - Config
\*------------------------------------------------------------------------------------------*/
#define AVM_MAX_CONFIG_ENTRIES  8

enum wlan_dect_type {
    WLAN,           /*--- 0 ---*/
    DECT,           /*--- 1 ---*/
    WLAN2,          /*--- 2 ---*/
    ZERTIFIKATE,    /*--- 3 ---*/
    DOCSIS,         /*--- 4 ---*/
    DSL,            /*--- 5 ---*/
    PROLIFIC,       /*--- 6 ---*/
    WLAN_ZIP,       /*--- 7 ---*/
    WLAN2_ZIP,      /*--- 8 ---*/
    MAX_TYPE        /*--- 9 ---*/
};

struct __attribute__ ((packed)) wlan_dect_config {
	unsigned char		Version;	/*--- z.Z. 1 ---*/
	enum  wlan_dect_type	Type :8;	/*--- 0 - WLAN; 1 - DECT ---*/
	unsigned short		Len;		/*--- 384 - WLAN, 128 - DECT ---*/
};

extern int set_wlan_dect_config_address(unsigned int *pConfig);
extern int get_wlan_dect_config(enum wlan_dect_type Type, unsigned char *buffer, unsigned int len);

/*--- extern int prom_wlan_get_base_memory(unsigned int *base, unsigned int *len); ---*/


#if IS_ENABLED(CONFIG_AVM_ENHANCED)
#if IS_ENABLED(CONFIG_MIPS)
#include <asm/prealloc_memory.h>
#endif
#endif

#endif

