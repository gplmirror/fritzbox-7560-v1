#include <linux/kmod.h>
#include <net/pkt_sched.h>
#include <net/avm_qos.h>

static struct Qdisc_class_ops hw_sched_tbf_class_ops;
static struct Qdisc_ops tbf_ops;
#define SCH_HW_MAX_CLASSES 32

struct hw_sched_priv {
	struct gnet_stats_basic_packed class_stats[SCH_HW_MAX_CLASSES];
	struct Qdisc *leaf;
	struct Qdisc_ops *orig_ops;
};

static struct hw_sched_priv *qdisc_priv_priv(const struct Qdisc *qdisc)
{
	uint32_t orig_priv_size =
	 qdisc->ops->priv_size - sizeof(struct hw_sched_priv);
	return (struct hw_sched_priv *)(qdisc_priv((struct Qdisc *)qdisc) +
	                                orig_priv_size);
}

static int hw_sched_graft(struct Qdisc *sch, unsigned long arg, struct Qdisc *new, struct Qdisc **old)
{
	struct hw_sched_priv *priv = qdisc_priv_priv(sch);
	int rv;

	rv = priv->orig_ops->cl_ops->graft(sch, arg, new, old);
	if(rv) return rv;

	priv->leaf = priv->orig_ops->cl_ops->leaf(sch, arg);

	return 0;
}

#if defined(CONFIG_NET_SCH_HW_BYPASS)
static struct sk_buff *hw_sched_dequeue(struct Qdisc *sch)
{
	struct sk_buff *skb;
	struct hw_sched_priv *priv;
	u32 classid;
	priv = qdisc_priv_priv(sch);

	if(likely(priv->leaf)) {
		skb = priv->leaf->dequeue(priv->leaf);
	} else {
		pr_debug("no leaf found, dequeue from self\n");
		skb = qdisc_dequeue_head(sch);
	}
	if(unlikely(!skb)) return NULL;

	sch->q.qlen--;
	qdisc_bstats_update(sch, skb);
	classid = TC_H_MIN(skb->priority);
	if(likely(classid && classid < SCH_HW_MAX_CLASSES)) {
		bstats_update(&priv->class_stats[TC_H_MIN(skb->priority)], skb);
	}

	return skb;
}

static int hw_sched_enqueue(struct sk_buff *skb, struct Qdisc *sch)
{
	int rv;
	struct hw_sched_priv *priv;
	priv = qdisc_priv_priv(sch);

	if(likely(priv->leaf)) {
		rv = qdisc_enqueue(skb, priv->leaf);
	} else {
		pr_debug("no leaf found, enqueue to self\n");
		rv = qdisc_enqueue_tail(skb, sch);
	}
	if (rv != NET_XMIT_SUCCESS) {
		if (net_xmit_drop_count(rv))
			sch->qstats.drops++;
		return rv;
	}

	sch->q.qlen++;
	return rv;
}
#endif


static void hw_sched_tbf_destroy(struct Qdisc *sch)
{
	if(sch->parent == TC_H_ROOT) {
		avm_qos_reset_port_shaper(sch->dev_queue->dev);
	} else {
		avm_qos_reset_prio_shaper(sch->dev_queue->dev, TC_H_MIN(sch->parent));
	}
	tbf_ops.destroy(sch);
}

static int hw_sched_tbf_set(struct Qdisc *sch, struct nlattr *opt)
{
	int err;
	struct nlattr *tb[TCA_TBF_PTAB + 1];
	struct tc_tbf_qopt *qopt;
	struct qdisc_rate_table *rtab = NULL;
	struct qdisc_rate_table *ptab = NULL;
	int size, peak_size, n;

	err = nla_parse_nested(tb, TCA_TBF_PTAB, opt, NULL);
	if(err < 0) return err;

	err = -EINVAL;
	if(tb[TCA_TBF_PARMS] == NULL) return err;

	qopt = nla_data(tb[TCA_TBF_PARMS]);
	rtab = qdisc_get_rtab(&qopt->rate, tb[TCA_TBF_RTAB]);
	if(rtab == NULL) goto done;

	if(qopt->peakrate.rate) {
		if(qopt->peakrate.rate > qopt->rate.rate)
			ptab = qdisc_get_rtab(&qopt->peakrate, tb[TCA_TBF_PTAB]);
		if(ptab == NULL) goto done;
	}

	for(n = 0; n < 256; n++)
		if(rtab->data[n] > qopt->buffer) break;
	size = (n << qopt->rate.cell_log) - 1;
	if(ptab) {
		for(n = 0; n < 256; n++)
			if(ptab->data[n] > qopt->mtu) break;
		peak_size = (n << qopt->peakrate.cell_log) - 1;
	}

	if(sch->parent == TC_H_ROOT) {
		err = avm_qos_set_port_shaper(sch->dev_queue->dev,
		                              ptab ? ptab->rate.rate : 0,
		                              rtab->rate.rate,
		                              ptab ? peak_size : 0,
		                              size);
	} else {
		err = avm_qos_set_prio_shaper(sch->dev_queue->dev,
		                              TC_H_MIN(sch->parent),
		                              ptab ? ptab->rate.rate : 0,
		                              rtab->rate.rate,
		                              ptab ? peak_size : 0,
		                              size);
	}

done:
	if(rtab) qdisc_put_rtab(rtab);
	if(ptab) qdisc_put_rtab(ptab);

	return err;
}

static int hw_sched_tbf_change(struct Qdisc *sch, struct nlattr *opt)
{
	int err;
	err = tbf_ops.change(sch, opt);
	if(err < 0) return err;

	pr_debug("changing tbf filter\n");
	if(sch->parent == TC_H_ROOT) {
		avm_qos_reset_port_shaper(sch->dev_queue->dev);
	} else {
		avm_qos_reset_prio_shaper(sch->dev_queue->dev, TC_H_MIN(sch->parent));
	}
	return hw_sched_tbf_set(sch, opt);
}

static int hw_sched_tbf_init(struct Qdisc *sch, struct nlattr *opt)
{
	int err;
	err = tbf_ops.init(sch, opt);
	if(err < 0) return err;

	memcpy(&hw_sched_tbf_class_ops,
	       tbf_ops.cl_ops,
	       sizeof(hw_sched_tbf_class_ops));

	if(IS_ENABLED(CONFIG_NET_SCH_HW_BYPASS)) {
		struct hw_sched_priv *priv;
		hw_sched_tbf_class_ops.graft = hw_sched_graft;
		priv = qdisc_priv_priv(sch);
		memset(priv, 0, sizeof(*priv));
		priv->orig_ops = &tbf_ops;
	}

	pr_debug("init tbf filter\n");

	return hw_sched_tbf_set(sch, opt);
}

static struct Qdisc_class_ops hw_sched_llq_class_ops;
static struct Qdisc_ops llq_ops;

static u32 _hw_sched_llq_get_classid(struct Qdisc *sch, unsigned long cl)
{
	static struct sk_buff skb_dummy = { 0 };
	struct tcmsg tcm;

	sch->ops->cl_ops->dump(sch, cl, &skb_dummy, &tcm);
	BUG_ON(TC_H_MAJ(sch->handle) != TC_H_MAJ(tcm.tcm_handle));

	return tcm.tcm_handle;
}

static int hw_sched_llq_dump_class_stats(struct Qdisc *sch,
                                         unsigned long cl,
                                         struct gnet_dump *d)
{
	u32 classid;
	struct hw_sched_priv *priv;
	classid = _hw_sched_llq_get_classid(sch, cl);
	BUG_ON(TC_H_MIN(classid) >= SCH_HW_MAX_CLASSES);
	priv = qdisc_priv_priv(sch);

	if(gnet_stats_copy_basic(d, &priv->class_stats[TC_H_MIN(classid)]) < 0) {
		return -1;
	} else {
		return 0;
	}
}

static int hw_sched_llq_change_class(struct Qdisc *sch,
                                     u32 classid,
                                     u32 parentid,
                                     struct nlattr **tca,
                                     unsigned long *arg)
{
	struct nlattr *opt = tca[TCA_OPTIONS];
	struct nlattr *tb[TCA_LLQ_MAX + 1];
	struct tc_llq_copt *params;
	int err = -EINVAL;

	BUG_ON(TC_H_MIN(classid) >= SCH_HW_MAX_CLASSES);

	/* call original SW-implementation*/
	err = llq_ops.cl_ops->change(sch, classid, parentid, tca, arg);
	if(err) return err;

	if(opt == NULL || nla_parse_nested(tb, TCA_LLQ_MAX, opt, NULL))
		return -EINVAL;

	if(!tb[TCA_LLQ_OPTIONS]) return -EINVAL;

	params = nla_data(tb[TCA_LLQ_OPTIONS]);

	/* TODO identify non-effective weights properly */
	avm_qos_add_hw_queue(sch->dev_queue->dev,
	                     classid,
	                     params->priority,
	                     params->weight != 1 ? params->weight : 0);


	return err;
}

static void hw_sched_llq_destroy(struct Qdisc *sch)
{
	struct net_device *netdev;

	netdev = sch->dev_queue->dev;
	avm_qos_flush_hw_queues(netdev);

	llq_ops.destroy(sch);
}

static int hw_sched_llq_init(struct Qdisc *sch, struct nlattr *opt)
{
	struct tc_llq_qopt *qopt = nla_data(opt);
	pr_debug("new qdisc maxq=%d minq=%d defaultclass=%u\n",
	         qopt->maxq,
	         qopt->minq,
	         qopt->defaultclass);

	avm_qos_set_default_queue(sch->dev_queue->dev, qopt->defaultclass);

	memcpy(&hw_sched_llq_class_ops,
	       llq_ops.cl_ops,
	       sizeof(hw_sched_llq_class_ops));
	hw_sched_llq_class_ops.change = hw_sched_llq_change_class;

	if(IS_ENABLED(CONFIG_NET_SCH_HW_BYPASS)) {
		struct hw_sched_priv *priv;
		/* fall back to generic stats */
		hw_sched_llq_class_ops.dump_stats = hw_sched_llq_dump_class_stats;
		hw_sched_llq_class_ops.graft = hw_sched_graft;
		priv = qdisc_priv_priv(sch);
		memset(priv, 0, sizeof(*priv));
		priv->orig_ops = &llq_ops;
	}

	BUG_ON(llq_ops.init == NULL);
	llq_ops.init(sch, opt);

	return 0;
}

static struct Qdisc_ops hw_sched_llq_ops = {
	.cl_ops = &hw_sched_llq_class_ops,
	.id = "llq", /* mimic llq in hardware */
	.init = hw_sched_llq_init,
	.destroy = hw_sched_llq_destroy,

#if defined(CONFIG_NET_SCH_HW_BYPASS)
	/* fold original discipline to save performance */
	.enqueue = hw_sched_enqueue,
	.dequeue = hw_sched_dequeue,
	.peek = qdisc_peek_head,
	.drop = qdisc_queue_drop,
	.reset = qdisc_reset_queue,
	.priv_size = sizeof(struct hw_sched_priv),
#endif
};

static struct Qdisc_ops hw_sched_tbf_ops = {
	.cl_ops = &hw_sched_tbf_class_ops,
	.id = "tbf", /* mimic tbf in hardware */
	.destroy = hw_sched_tbf_destroy,
	.change = hw_sched_tbf_change,
	.init = hw_sched_tbf_init,

#if defined(CONFIG_NET_SCH_HW_BYPASS)
	/* fold original discipline to save performance */
	.enqueue = hw_sched_enqueue,
	.dequeue = hw_sched_dequeue,
	.peek = qdisc_peek_head,
	.drop = qdisc_queue_drop,
	.reset = qdisc_reset_queue,
	.priv_size = sizeof(struct hw_sched_priv),
#endif
};

int hook_qdisc_ops(struct Qdisc_ops *sw_ops, const struct Qdisc_ops *hw_ops)
{
	struct nlattr *kind;
	struct Qdisc_ops *ops;

	pr_debug("hooking qdisc ops for sch_%s... ", hw_ops->id);
	kind = kmalloc(strlen(hw_ops->id) + 1 + NLA_HDRLEN, GFP_KERNEL);
	if(!kind) return -ENOMEM;
	kind->nla_len = strlen(hw_ops->id) + 1 + NLA_HDRLEN;
	kind->nla_type = NLA_NUL_STRING;
	strcpy(nla_data(kind), hw_ops->id);
	ops = qdisc_lookup_ops(kind);
#ifdef CONFIG_MODULES
	if(!ops) request_module("sch_%s", hw_ops->id);
	ops = qdisc_lookup_ops(kind);
#endif
	kfree(kind);
	if(!ops) {
		pr_debug("not found\n");
		return -ENOENT;
	}

	memcpy(sw_ops, ops, sizeof(*ops));

	if(IS_ENABLED(CONFIG_NET_SCH_HW_BYPASS)) {
		ops->priv_size += hw_ops->priv_size;
	}
#define QDISC_OP_CHECK_AND_REPLACE(name)           \
	do {                                           \
		if(hw_ops->name) ops->name = hw_ops->name; \
	} while(0)

	QDISC_OP_CHECK_AND_REPLACE(cl_ops);
	QDISC_OP_CHECK_AND_REPLACE(enqueue);
	QDISC_OP_CHECK_AND_REPLACE(dequeue);
	QDISC_OP_CHECK_AND_REPLACE(peek);
	QDISC_OP_CHECK_AND_REPLACE(drop);
	QDISC_OP_CHECK_AND_REPLACE(init);
	QDISC_OP_CHECK_AND_REPLACE(reset);
	QDISC_OP_CHECK_AND_REPLACE(destroy);
	QDISC_OP_CHECK_AND_REPLACE(change);
	QDISC_OP_CHECK_AND_REPLACE(attach);
	QDISC_OP_CHECK_AND_REPLACE(dump);
	QDISC_OP_CHECK_AND_REPLACE(dump_stats);

#undef QDISC_OP_CHECK_AND_REPLACE
	pr_debug("done\n");

	return 0;
}

static int __init hw_sched_module_init(void)
{
	int rv;
	rv = hook_qdisc_ops(&llq_ops, &hw_sched_llq_ops);
	rv |= hook_qdisc_ops(&tbf_ops, &hw_sched_tbf_ops);

	return rv;
}

static void __exit hw_sched_module_exit(void)
{
	/* TODO */
	BUG();
}

module_init(hw_sched_module_init)
module_exit(hw_sched_module_exit)
